#define BAUD_RATE 500000

#define LED_RW_IDLE_ON    true
#define RUMBLE_LEVEL      35
#define DRIVE_OFFSET_IF1  0
#define DRIVE_OFFSET_QL   0
#define HW_GROUP_START    0
#define HW_GROUP_END      0
#define HW_GROUP_LOCK     false
