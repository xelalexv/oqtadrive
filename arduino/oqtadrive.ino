/*
    OqtaDrive - Sinclair Microdrive emulator
    Copyright (c) 2024, Alexander Vollschwitz

    developed on: Arduino Nano

    This file is part of OqtaDrive.

    OqtaDrive is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OqtaDrive is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/


// ----------------------------------------------------------------- CONFIG ---
//  This section contains all configuration items. Do not change anything below
//  this section unless you know what you're doing!
//
//  If you need to maintain configs for various adapters, you can alternatively
//  place your settings in different header files in the `config` folder next
//  to this file, one per adapter, and `#include` the desired one before
//  uploading. Each header file only needs to contain the settings you want to
//  change. All other settings will remain at their defaults. Note that for
//  convenience, files in the `config` folder are git-ignored.
//
//  NOTE:
//
//  - After any firmware config changes, you need to build & flash the firmware
//    to the adapter.
//
//  - When flashing the firmware or performing an upgrade using the Makefile
//    or the web UI, this very file will be reset to the version being
//    flashed/upgraded! To make your config changes persist, do not make any
//    changes here and instead use a header config file in location
//    `{install folder}/oqtadrive/config.h`, e.g. `/home/pi/oqtadrive/config.h`.
//
//
//#include "config/dongle.h"
//#include "config/spectrum.h"
//#include "config/if1.h"
//#include "config/ql.h"
//#include "config/pi.h"
//#include "config/linino.h"

//  Set whether read & write LEDs should be on when idling.
#ifndef LED_RW_IDLE_ON
#define LED_RW_IDLE_ON true
#endif

// Set whether the read & write LEDs should indicate that the adapter is
// waiting to sync with the daemon (LEDs alternate)
#ifndef LED_SYNC_WAIT
#define LED_SYNC_WAIT true
#endif

// rumble strength; this is a PWM setting (0-255), set to 0 for off
#ifndef RUMBLE_LEVEL
#define RUMBLE_LEVEL 35
#endif

/*
    Automatic offset check only works for QL. If you're using OqtaDrive with an
    actual Microdrive between IF1 and the adapter, you can set a fixed offset
    here. Likewise for the QL, if the automatic check doesn't work reliably.
    The offset denotes how many actual Microdrives are present between the
    Microdrive interface and the adapter. So an offset of 0 means the adapter is
    directly connected to the IF1 or internal Microdrive interface on the QL,
    bypassing the two built-in drives. Max accepted value is 7. Keep at -1 to
    use automatic offset check.
 */
#ifndef DRIVE_OFFSET_IF1
#define DRIVE_OFFSET_IF1 0
#endif

#ifndef DRIVE_OFFSET_QL
#define DRIVE_OFFSET_QL -1
#endif

/*
    If you want to map hardware drives, i.e. move them to different slots within
    the daisy chain, you need to chain them behind the OqtaDrive adapter. This
    requires routing the COMMS_OUT signal from the adapter to the COMMS_IN of
    the first hardware drive. See the documentation for more details.

    Once you have set up OqtaDrive in this way, you can define here to which
    slots the drives are mapped after the adapter starts up. During operation
    you can then control the mapping via oqtactl. The hardware drives are always
    mapped as a group. Setting start and end to 0 will deactivate the hardware
    drives. Setting HW_GROUP_LOCK to true will lock the group settings so that
    they cannot be changed with oqtactl.

    Note: Set offsets above to 0, since hardware drive mapping requires the
          OqtaDrive adapter to be first in the chain.
 */
#ifndef HW_GROUP_START
#define HW_GROUP_START 0
#endif

#ifndef HW_GROUP_END
#define HW_GROUP_END 0
#endif

#ifndef HW_GROUP_LOCK
#define HW_GROUP_LOCK true
#endif

//  Use these settings to force either Interface 1 or QL, but not both! When
//  left at false, automatic detection is used.
#ifndef FORCE_IF1
#define FORCE_IF1 false
#endif

#ifndef FORCE_QL
#define FORCE_QL false
#endif

/*
    This is the baud rate of the serial link between adapter and daemon. It is
    highly recommended to use the value defined here, which is the minimum speed
    required for error free communication, and at the same time the maximum speed
    at which an Arduino Nano can reliably operate the serial port. On some boards
    used for running the daemon, such as the BananaPi M2 Zero, the 1 Mbps speed
    is not available due to the combination of frequency and divider used to
    clock its UART. For this platform, 500 kbps is currently being tested and
    may work.

 */
#ifndef BAUD_RATE
#define BAUD_RATE 1000000
#endif

/*
	If write protect is not working reliably with your Spectrum, the voltage
	asserted to the /WR.PR line may not be high enough. An original Microdrive
	outputs 9V to signal a writable cartridge, while OqtaDrive only outputs 5V.
	In most cases, this is enough, but there have been reports about problems
	with this. In this case, insert a transistor into the WR.PR output as
	described in the project README, and change this setting to true.
 */
#ifndef WR_PROTECT_BOOST
#define WR_PROTECT_BOOST false
#endif

// ----------------------------------- END OF CONFIG - START OF DANGER ZONE ---

#include <EEPROM.h>

#define NOP __asm__ __volatile__ ("nop\n\t"); // for fine-grained wait (62.5ns)

/*
	Implementation notes:
	- _delay_us only takes compile time constants as argument
 */

#define FIRMWARE_VERSION 30

// Change this to true for a calibration run. When not connecting the adapter to
// an Interface 1/QL during calibration, choose the desired interface via the
// force settings below.
#define CALIBRATION false

// --- port & pin assignments -------------------------------------------------
//
// Note: When adapting to new micro-controller, serial register, control port,
//       track port, net port, and LED port have to be figured out
//
#if defined(__AVR_ATmega328P__) // --------------------------------------------

#define SERIAL_PORT     Serial
#define SERIAL_REGISTER UDR0

// port containing COMMS_IN, COMMS_OUT, COMMS_CLK, READ_WRITE, ERASE
#define CONTROL_PORT_IN  PIND
#define CONTROL_PORT_OUT PORTD

// port containing the tracks
#define TRACK_PORT_IN    PINC
#define TRACK_PORT_OUT   PORTC
#define TRACK_PORT_DDR   DDRC

// port for network
#define NET_PORT_IN      PINC
#define NET_PORT_OUT     PORTC

// port for LEDs
#define LED_PORT         PORTB

const int PIN_COMMS_CLK  = 2; // HIGH idle on IF1, LOW on QL; interrupt
const int PIN_COMMS_IN   = 4;
const int PIN_COMMS_OUT  = 7;
const int PIN_ERASE      = 5; // LOW active
const int PIN_READ_WRITE = 3; // READ is HIGH; interrupt
const int PIN_WR_PROTECT = 6; // LOW active

const int PIN_RUMBLE     = 10;
const int PIN_LED_WRITE  = 11;
const int PIN_LED_READ   = 12;

const int PIN_TRACK_1 = A4;
const int PIN_TRACK_2 = A0;

const int PIN_NET_IN      = A1; // NOTE these two pins must not use bit 7
const int PIN_NET_IN_BIT  = 1;
const int PIN_NET_OUT     = A2;
const int PIN_NET_OUT_BIT = 2;

// --- pin masks --------------------------------------------------------------
const uint8_t MASK_COMMS_CLK  = B00000100;
const uint8_t MASK_COMMS_IN   = B00010000;
const uint8_t MASK_COMMS_OUT  = B10000000;
const uint8_t MASK_ERASE      = B00100000;
const uint8_t MASK_READ_WRITE = B00001000;
const uint8_t MASK_RECORDING  = MASK_ERASE | MASK_READ_WRITE;

const uint8_t MASK_LED_WRITE  = B00001000;
const uint8_t MASK_LED_READ   = B00010000;

const uint8_t MASK_NET_IN     = B00000010;
const uint8_t MASK_NET_OUT    = B00000100;

#elif defined(__AVR_ATmega32U4__) // ------------------------------------------

#define SERIAL_PORT     Serial1
#define SERIAL_REGISTER UDR1

// port containing COMMS_IN, COMMS_OUT, COMMS_CLK, READ_WRITE, ERASE
#define CONTROL_PORT_IN  PIND
#define CONTROL_PORT_OUT PORTD

// port containing the tracks
#define TRACK_PORT_IN    PINF
#define TRACK_PORT_OUT   PORTF
#define TRACK_PORT_DDR   DDRF

// port for network
#define NET_PORT_IN    PINE
#define NET_PORT_OUT   PORTF

// port for LEDs
#define LED_PORT         PORTB

const int PIN_COMMS_CLK  = 2;  // HIGH idle on IF1, LOW on QL; interrupt
const int PIN_COMMS_IN   = 4;
const int PIN_COMMS_OUT  = 6;
const int PIN_ERASE      = 12; // LOW active
const int PIN_READ_WRITE = 3;  // READ is HIGH; interrupt
const int PIN_WR_PROTECT = 8;  // LOW active

const int PIN_RUMBLE     = 9;
const int PIN_LED_WRITE  = 11;
const int PIN_LED_READ   = 10;

const int PIN_TRACK_1 = A3;
const int PIN_TRACK_2 = A5;

const int PIN_NET_IN       = 7; // NOTE these two pins must not use bit 7
const int PIN_NET_IN_BIT   = 6;
const int PIN_NET_OUT      = A2;
const int PIN_NET_OUT_BIT  = 5;

// --- pin masks --------------------------------------------------------------
const uint8_t MASK_COMMS_CLK  = B00000010;
const uint8_t MASK_COMMS_IN   = B00010000;
const uint8_t MASK_COMMS_OUT  = B10000000;
const uint8_t MASK_ERASE      = B01000000;
const uint8_t MASK_READ_WRITE = B00000001;
const uint8_t MASK_RECORDING  = MASK_ERASE | MASK_READ_WRITE;

const uint8_t MASK_LED_WRITE  = B10000000;
const uint8_t MASK_LED_READ   = B01000000;

const uint8_t MASK_NET_IN     = B01000000;
const uint8_t MASK_NET_OUT    = B00100000;

#endif

// --- common pin masks -------------------------------------------------------
const uint8_t MASK_TRACK_1     = B00010000;
const uint8_t MASK_TRACK_2     = B00000001;
const uint8_t MASK_BOTH_TRACKS = MASK_TRACK_1 | MASK_TRACK_2;

const uint8_t MASK_NET         = MASK_NET_IN | MASK_NET_OUT; // FIXME needed?

// --- LED behavior & rumble --------------------------------------------------
const bool ACTIVE = true;
const bool IDLE   = false;

uint8_t blinkCount  = 0;
uint8_t rumbleLevel = RUMBLE_LEVEL;

// --- tape format ------------------------------------------------------------
const int PREAMBLE_LENGTH   = 12;
const int HEADER_LENGTH_IF1 = 27;
const int RECORD_LENGTH_IF1 = 540;
const int HEADER_LENGTH_QL  = 28;
const int RECORD_LENGTH_QL  = 538;
const int RECORD_EXTRA_IF1  = 99; // during format, Interface 1 with V2 ROM
const int RECORD_EXTRA_QL   = 86; // and QLs send longer records

const int HEADER_LENGTH_CPM_MUX = 18;
const int RECORD_LENGTH_CPM_MUX = 527;

uint16_t headerLengthMux;
uint16_t recordLengthMux;
uint16_t sectorLengthMux;

// --- network format ---------------------------------------------------------
const int NET_HEADER_LENGTH = 8;

// indices of values in packet headers
const int NET_IX_NCIRIS     = 0;
const int NET_IX_NCSELF     = 1;
const int NET_IX_NCTYPE     = 4; // 0 for data, 1 for end of file
const int NET_IX_NCOBL      = 5;
const int NET_IX_NCDCS      = 6;
const int NET_IX_NCHCS      = 7;

// --- timer pre-loads --------------------------------------------------------
// values lower than 256 use a 256 pre-scaler (1 tick is 16us), values 256 and
// above use a 1024 pre-scaler (1 tick is 64us)
const int TIMER_COMMS          = 512 - 157; //   10 msec
const int TIMER_HEADER_GAP_IF1 = 256 - 234; // 3.75 msec
const int TIMER_HEADER_GAP_CPM = 256 - 171; // 2.75 msec
const int TIMER_HEADER_GAP_QL  = 256 - 225; // 3.60 msec

// --- drive select -----------------------------------------------------------
volatile uint8_t commsRegister   = 0;
volatile uint8_t commsClkCount   = 0;
volatile uint8_t activeDrive     = 0;
volatile uint8_t lastActiveDrive = 0;
volatile uint8_t driveOffset     = 0xff;
volatile uint8_t hwGroupStart    = 0;
volatile uint8_t hwGroupEnd      = 0;
volatile uint8_t maskHwOffset    = 0;

bool commsClkState;

const uint8_t DRIVE_STATE_UNKNOWN  = 0x80;
const uint8_t DRIVE_FLAG_LOADED    = 1;
const uint8_t DRIVE_FLAG_FORMATTED = 2;
const uint8_t DRIVE_FLAG_READONLY  = 4;
const uint8_t DRIVE_READABLE = DRIVE_FLAG_LOADED | DRIVE_FLAG_FORMATTED;

volatile uint8_t driveState = DRIVE_STATE_UNKNOWN;

// --- interrupt handler ------------------------------------------------------
typedef void (* TimerHandler)();
void setTimer(int preload, TimerHandler h);
void enableTimer(bool on, int preload, TimerHandler h);
TimerHandler timerHandler = NULL;

// --- sector buffer ----------------------------------------------------------
const uint16_t BUF_LENGTH = 10 +
	max(HEADER_LENGTH_IF1, HEADER_LENGTH_QL) +
	max(RECORD_LENGTH_IF1 + RECORD_EXTRA_IF1, RECORD_LENGTH_QL + RECORD_EXTRA_QL);
uint8_t buffer[BUF_LENGTH];

// --- message buffer ---------------------------------------------------------
uint8_t msgBuffer[4];

// --- boxed transmissions ----------------------------------------------------
const uint16_t BOX_A =   21;
const uint16_t BOX_B =  533;
const uint16_t BOX_C =  632;
const uint16_t BOX_D = 1024; // stop-box, not actually expected to be used
const uint16_t BOXES[] = {BOX_A, BOX_B, BOX_C, BOX_D};

const int MAX_BOX_PADDING = 32;
uint8_t fillBytes[MAX_BOX_PADDING];

uint16_t usedBoxEnd = 0;

// --- Microdrive interface type - Interface 1 or QL --------------------------
bool IF1 = true;
#define QL !IF1
bool confirmInterface = true;

// --- state flags ------------------------------------------------------------
volatile bool spinning    = false;
volatile bool quietDaemon = false;
volatile bool recording   = false;
volatile bool message     = false;
volatile bool headerGap   = false;
volatile bool calibration = false; // use the define setting at top to turn on!
volatile bool synced      = false;

const uint8_t NET_IDLE         = 0x00;
const uint8_t NET_FORWARD      = 0x01;
const uint8_t NET_REPLAY       = 0x02;
const uint8_t NET_REPLAY_RETRY = 0x04;
const uint8_t NET_BACKOFF      = 0x08;
const uint8_t NET_CUE_SILENCE  = 0x20;
const uint8_t NET_BLOCK        = 0x40;
const uint8_t NET_BLOCKED      = 0x80;

volatile uint8_t net        = 0;
volatile uint8_t netBackoff = 0;

const unsigned long NET_INITIAL_BACKOFF  =  750; // initial back-off in ms
const unsigned long NET_MAX_BACKOFF      = 3000; // max. back-off in ms
const unsigned long NET_MAX_REPLAY_PAUSE =  250; // max. replay pause in ms

volatile unsigned long netBlockStart       = 0;
volatile unsigned long netBackoffStart     = 0;
volatile unsigned long netCollisionBackoff = NET_INITIAL_BACKOFF;

// take a break after retrying replay of a packet for this many times
const uint8_t NET_REPLAY_BREAK_COUNT = 25;
const unsigned long NET_REPLAY_BREAK = 25; // length of replay break in ms
uint8_t netReplayRetries = 0;

uint16_t netPacketLength = 0;

volatile uint8_t pktIngress    = 0;			// network error stats
volatile uint8_t pktIngressErr = 0;
volatile uint8_t pktEgress     = 0;
volatile uint8_t pktEgressErr  = 0;

const uint8_t NET_RCV_ERROR_THRESHOLD = 3;
uint8_t netRcvErrors = 0;

const uint8_t  NET_PROFILE_LAST       = 3;	// network timing profiles
const uint8_t  NET_PROFILE_SPECTRUM   = 0;
const uint8_t  NET_PROFILE_QL         = 2;
const uint8_t  NET_PROFILE_CENTERCUE  = 0;
const uint8_t  NET_PROFILE_BIT_INPAK  = 1;
const uint8_t  NET_PROFILE_BIT_OUTPAK = 2;

const uint16_t netProfiles[][3] = {
	// entry format: { center cue, inpak bit delay, outpak bit delay }
	{ 18, 44, 43 }, // Spectrum (standard)
	{ 14, 44, 43 }, // Harlequin
	{ 12, 43, 42 }, // QL (standard, Minerva)
	{ 11, 43, 42 }  // QL (TK2)

// experimental profiles for finding TK2 sweet spot:
//	{ 18, 44, 43 }, // Spectrum bit time, Spectrum (standard)
//	{ 17, 44, 43 },
//	{ 16, 44, 43 },
//	{ 15, 44, 43 },
//	{ 14, 44, 43 },
//	{ 13, 44, 43 },
//	{ 12, 44, 43 },
//	{ 11, 44, 43 },
//	{ 10, 44, 43 },
//	{  9, 44, 43 },

//	{ 18, 43, 42 }, // QL bit time
//	{ 17, 43, 42 },
//	{ 16, 43, 42 },
//	{ 15, 43, 42 },
//	{ 14, 43, 42 },
//	{ 13, 43, 42 },
//	{ 12, 43, 42 }, // QL (standard, Minerva)
//	{ 11, 43, 42 }, // QL (TK2)
//	{ 10, 43, 42 },
//	{  9, 43, 42 }
};

uint16_t netCenterCue = netProfiles[0][NET_PROFILE_CENTERCUE];
uint16_t netBitInpak  = netProfiles[0][NET_PROFILE_BIT_INPAK];
uint16_t netBitOutpak = netProfiles[0][NET_PROFILE_BIT_OUTPAK];
volatile uint8_t netProfileIx = 0;

// --- daemon commands & flags ------------------------------------------------
const uint8_t PROTOCOL_VERSION = 6;

const char CMD_HELLO   = 'h';
const char CMD_VERSION = 'v';
const char CMD_PING    = 'P';
const char CMD_STATUS  = 's';
const char CMD_GET     = 'g';
const char CMD_PUT     = 'p';
const char CMD_VERIFY  = 'y';
const char CMD_MAP     = 'm';
const char CMD_DEBUG   = 'd';
const char CMD_RESYNC  = 'r';
const char CMD_CONFIG  = 'c';
const char CMD_NET     = 'n';
const char CMD_QUIET   = 'Q';

const char FLG_RING    = 'R';
const char FLG_NET     = 'N';

const char CMD_CONFIG_RUMBLE  = 'r';

const uint8_t CMD_NET_INGRESS      = 1;
const uint8_t CMD_NET_EGRESS       = 2;
const uint8_t CMD_NET_INGRESS_STAT = 11;
const uint8_t CMD_NET_EGRESS_STAT  = 12;

const uint8_t  CMD_LENGTH = 4;
const uint16_t PAYLOAD_LENGTH = BUF_LENGTH - CMD_LENGTH;

const char DAEMON_PING[]  = {CMD_PING, 'i', 'n', 'g'};
const char DAEMON_PONG[]  = {CMD_PING, 'o', 'n', 'g'};
const char DAEMON_HELLO[] = {CMD_HELLO, 'l' , 'o', 'd'};
const char IF1_HELLO[]    = {CMD_HELLO, 'l' , 'o', 'i'};
const char QL_HELLO[]     = {CMD_HELLO, 'l' , 'o', 'q'};

const uint8_t MASK_IF1 = 1;
const uint8_t MASK_QL  = 2;

const unsigned long DAEMON_TIMEOUT   =  5000;
const unsigned long RESYNC_THRESHOLD =  4500;
const unsigned long PING_INTERVAL    = 10000;

unsigned long lastPing = 0;

// ------------------------------------------------------------------ SETUP ---

//
void setup() {

	deactivateSignals();

	loadState();

	// FIXME: does this need deactivation?
	pinMode(PIN_COMMS_OUT, OUTPUT);
	digitalWrite(PIN_COMMS_OUT, LOW);

	// rumble & LEDs
	pinMode(PIN_RUMBLE, OUTPUT);
	pinMode(PIN_LED_WRITE, OUTPUT);
	pinMode(PIN_LED_READ, OUTPUT);
	ledRead(IDLE);
	ledWrite(IDLE);

	detectInterface(false, false, false);

	// open channel to daemon
	SERIAL_PORT.begin(BAUD_RATE, SERIAL_8N1);
	SERIAL_PORT.setTimeout(DAEMON_TIMEOUT);

	startupTests();

	// set up interrupts
	attachInterrupt(digitalPinToInterrupt(PIN_COMMS_CLK), commsClk, CHANGE);
	attachInterrupt(digitalPinToInterrupt(PIN_READ_WRITE), writeReq, FALLING);

	setupNetwork();
	rumbleGreet();

	// boxed transmissions need to pad payload with zeros to fill box
	for (uint8_t n = 0; n < MAX_BOX_PADDING; n++) {
		fillBytes[n] = 0;
	}
}

//
void startupTests() {
	if (!testBuffer()) {
		errorBlink(1, 2, 3); // bad RAM
	}
}

//
void remoteConfig(uint8_t item, uint8_t arg1, uint8_t arg2) {
	switch (item) {
		case CMD_CONFIG_RUMBLE:
			rumbleLevel = arg1;
			rumbleGreet();
			saveState();
			break;
	}
}

//
void setHWGroup(uint8_t start, uint8_t end) {
	if (start <= 8 && end <= 8 && start <= end) {
		hwGroupStart = start;
		hwGroupEnd = end;
		maskHwOffset = 1 << (start - 2);
	}
}

// ------------------------------------------------------------------- LOOP ---

void loop() {

	// FIXME: verify that serial is only accessed from main loop

	if (CALIBRATION) {
		calibrate(0x0f);
	}

	if (!synced) {
		driveOff();
		daemonSync();
	}

	debugFlush();
	ensureDriveState();

	if (spinning) {
		if (!handleQuietReq()) {
			return;
		}

		if (recording) {
			record();
		} else {
			replay();
		}

		if (blinkCount++ % 2 == 0) {
			if (recording) {
				ledRead(IDLE);
				ledWriteFlip();
			} else {
				ledWrite(IDLE);
				ledReadFlip();
			}
		}

		queueNextPing(1500);

	} else {
		// For signalling urgent state, the daemon may send a single-byte flag
		// if it has seen at least one ping after the last activity. On the
		// daemon side, this is done by locking and unlocking the conduit
		// accordingly. The adapter on the other hand will send a QUIET command
		// once activity resumes, which the daemon has to acknowledge with a
		// single-byte QUIET response. This allows the adapter to safely discard
		// any in-flight flags that may have been sent by the daemon, and
		// properly sync on the start of the response to its next command.
		daemonCheckFlag();

		if (net != NET_IDLE) {
			if (!handleQuietReq()) {
				return;
			}
			handleNetwork();

		} else if (needsPing()) {
			// Flag that activity on the local Sinclair network should be
			// blocked when detected, so that the client cannot start sending
			// as long as we're doing the ping. When this is flagged, the net
			// ISR will override the net activity, instead of receiving the
			// packet and interfering with serial communication. Simply blocking
			// right here will confuse the client if it is currently trying to
			// receive from net.
			flagNetworkBlock();

			if (daemonPing()) {
				// After a successful ping exchange, open a brief window during
				// which the daemon may send control commands (as opposed to
				// flags). The daemon must not send commands at any other time.
				daemonCheckControl();
			}
			unblockNetwork();
		}
	}
}

// ---------------------------------------------- Interface 1 / QL HANDLING ---

bool detectInterface(bool if1, bool ql, bool quickCheck) {

	bool prev = IF1;

	if1 = FORCE_IF1 ? true : if1;
	ql = FORCE_QL ? true : ql;

	if (if1) {
		IF1 = true;

	} else if (ql) {
		IF1 = false;

	} else {
		// Idle level of COMMS_CLK is HIGH for Interface 1, LOW for QL.
		// Sample COMMS_CLK line for two seconds to find out.

		// avoid floating input during detect
		pinMode(PIN_COMMS_CLK, INPUT_PULLUP);

		if (quickCheck) {
			IF1 = (CONTROL_PORT_IN & MASK_COMMS_CLK) != 0;

		} else {
			uint8_t high = 0, low = 0;
			for (uint8_t i = 0; i < 21; i++) {
				(CONTROL_PORT_IN & MASK_COMMS_CLK) == 0 ? low++ : high++;
				delay(100);
			}
			IF1 = high > low;
		}

		// restore to input without pull-up; whenever detecInterface is called,
		// COMMS_CLK input is configured that way; should that ever change, we
		// need to get current state above and then restore here
		pinMode(PIN_COMMS_CLK, INPUT);
	}

	// NOTE: Setting the net timing profile based on the detected Microdrive
	//       interface is not entirely correct, since there may be several,
	//       different machines chained to the network port of the OqtaDrive.
	//       However, most of the time, users will only have the one machine
	//       connected to the network port that's also using the Microdrive.
	//       So it's still better to employ this "educated guess", rather than
	//       having the initial profile always set to either Spectrum or QL.
	if (IF1) {
		if ((-1 < DRIVE_OFFSET_IF1) && (DRIVE_OFFSET_IF1 < 8)) {
			driveOffset = DRIVE_OFFSET_IF1;
		}
		headerLengthMux = HEADER_LENGTH_IF1 + 1;
		recordLengthMux = RECORD_LENGTH_IF1 + 1;
		commsClkState = true;
		netProfileIx = NET_PROFILE_SPECTRUM;

	} else {
		if ((-1 < DRIVE_OFFSET_QL) && (DRIVE_OFFSET_QL < 8)) {
			driveOffset = DRIVE_OFFSET_QL;
		}
		headerLengthMux = HEADER_LENGTH_QL + 1;
		recordLengthMux = RECORD_LENGTH_QL + 1;
		commsClkState = false;
		netProfileIx = NET_PROFILE_QL;
	}

	sectorLengthMux = headerLengthMux + recordLengthMux;

	return IF1 != prev;
}

// ----------------------------------------------------- READ/WRITE CONTROL ---

/*
	Check whether the WRITE or ERASE line is active (indicates recording),
	or an impending drive state change is indicated. Switches recording and
	spinning states accordingly, and returns true if any of the above is the
	case. This is used repeatedly while in replay mode to find out whether we
	need to bail out.

	Note: Any change in this function requires re-calibration of replay!

	TODO: Consider whether to consolidate this with checkReplayOrStop, since
	      most of the code is the same. We may not want to do this though for
	      timing reasons.
 */
bool checkRecordingOrStop() {

	uint8_t state = CONTROL_PORT_IN;

	// turn recording on, but never off here
	recording = recording || ((state & MASK_RECORDING) != MASK_RECORDING);

	// when recording, flip track mode here already
	// to give it more time to switch over
	if (recording) {
		setTracksToRecord();
	}

	// turn spinning off, but never on here; change in drive state is indicated
	// for IF1 by COMMS_CLK going active (i.e. LOW), and for QL by going inactive
	// (also LOW)
	bool prev = commsClkState;
	commsClkState = (state & MASK_COMMS_CLK) != 0;
	spinning = spinning && (!prev || commsClkState);

	return !spinning || recording;
}

/*
	Check whether both WRITE and ERASE lines are inactive (indicates replay),
	or an impending drive state change is indicated. Switches recording and
	spinning states accordingly, and returns true if any of the above is the
	case. This is used repeatedly while in record mode to find out whether we
	need to bail out.
 */
bool checkReplayOrStop() {

	uint8_t state = CONTROL_PORT_IN;

	// turn recording off, but never on here
	recording = recording && ((state & MASK_RECORDING) != MASK_RECORDING);

	if (!recording) {
		setTracksToReplay();
	}

	// turn spinning off, but never on here; change in drive state is indicated
	// for IF1 by COMMS_CLK going active (i.e. LOW), and for QL by going inactive
	// (also LOW)
	bool prev = commsClkState;
	commsClkState = (state & MASK_COMMS_CLK) != 0;
	spinning = spinning && (!prev || commsClkState);

	return !(spinning && recording);
}

// interrupt handler; switches tracks to record mode
void writeReq() {
	if (activeDrive > 0) {
		setTracksToRecord();
	}
}

// interrupt handler; signals the end of the header gap
void endHeaderGap() {
	headerGap = false;
}

/*
	Tracks in RECORD mode means the Arduino reads incoming data. I.e. when the
	Interface 1/QL wants to write to the Microdrive, the two track data pins
	need to be put into input mode.
 */
void setTracksToRecord() {
	TRACK_PORT_DDR &= (~MASK_BOTH_TRACKS);
	TRACK_PORT_OUT |= MASK_BOTH_TRACKS;
}

/*
	Tracks in REPLAY mode means the Arduino sends data. I.e. when the
	Interface 1/QL wants to read from the Microdrive, the two track data pins
	need to be put into	output mode.
 */
void setTracksToReplay() {
	TRACK_PORT_DDR |= MASK_BOTH_TRACKS;
	TRACK_PORT_OUT |= MASK_BOTH_TRACKS; // idle level is HIGH
}

//
void setWriteProtect(bool protect) {

	if (WR_PROTECT_BOOST) {
		// This is used when an additional PNP transistor is added to pin D6,
		// to overcome problems with write protect always being on (see README).
		if (protect) { // think inverted here
			// To signal that the cartridge is write protected, we must not
			// assert any voltage on the WR.PR line, so we need to keep the
			// transistor off. This is done by switching the pin to input
			// mode, i.e. no voltage, high impedance.
			pinMode(PIN_WR_PROTECT, INPUT);
		} else {
			// To signal that the cartridge is writable, we need to assert 9V
			// on WR.PR, so we output LOW on pin D6, to drive the transistor
			// open.
			pinMode(PIN_WR_PROTECT, OUTPUT);
			digitalWrite(PIN_WR_PROTECT, LOW);
		}

	} else {
		// no transistor, directly driving WR.PR with pin D6
		pinMode(PIN_WR_PROTECT, OUTPUT);
		digitalWrite(PIN_WR_PROTECT, protect ? LOW : HIGH);
	}
}

//
void activateSignals() {
	// control signals
	pinMode(PIN_READ_WRITE, INPUT_PULLUP);
	pinMode(PIN_ERASE, INPUT_PULLUP);
	pinMode(PIN_COMMS_CLK, INPUT_PULLUP);

	// This must not be set to INPUT_PULLUP. If there is a Microdrive upstream
	// of the adapter in the daisy chain, the pull-up resistor would feed into
	// that drive's COMMS_CLK output and confuse it.
	pinMode(PIN_COMMS_IN, INPUT);
}

// When drive is off, signal lines should be set to the least intrusive mode
// possible, to avoid interfering with any actual Microdrives that may be
// present. This is INPUT without pull-up, which for the ATmega328P has a
// typical input leakage current of 1uA.
void deactivateSignals() {
	// control signals
	pinMode(PIN_READ_WRITE, INPUT);
	pinMode(PIN_ERASE, INPUT);
	pinMode(PIN_COMMS_CLK, INPUT);
	pinMode(PIN_COMMS_IN, INPUT);
	pinMode(PIN_WR_PROTECT, INPUT);

	// tracks off
	TRACK_PORT_DDR &= (~MASK_BOTH_TRACKS);
	TRACK_PORT_OUT &= (~MASK_BOTH_TRACKS);
}

// ---------------------------------------------------------- DRIVE CONTROL ---

//
void driveOff() {
	stopTimer();
	deactivateSignals();
	ledWrite(IDLE);
	ledRead(IDLE);
	spinning = false;
	quietDaemon = false;
	recording = false;
	headerGap = false;
	rumble(false);
}

//
void driveOn() {
	activateSignals();
	setTracksToReplay();
	headerGap = false;
	driveState = DRIVE_STATE_UNKNOWN;
	recording = false;
	spinning = true;
	quietDaemon = true;
	rumble(true);
	// once we managed to turn on a drive, there's no more
	// need to confirm the interface
	confirmInterface = false;
}

// Active level of COMMS_CLK is LOW for Interface 1, HIGH for QL.
bool isCommsClk(uint8_t state) {
	return (state & MASK_COMMS_CLK) == (IF1 ? 0 : MASK_COMMS_CLK);
}

/*
	Interrupt handler for handling the 1 bit being pushed through the shift
	register formed by the up to eight actual Microdrives (daisy chain), to
	select the active drive. If the OqtaDrive adapter is connected directly to
	the Microdrive interface, commsRegister represents the complete shift
	register. If there are actual Microdrives present before the adapter, then
	commsRegister only represents the remainder of that shift register.
 */
void commsClk() {

	uint8_t d = CONTROL_PORT_IN;
	bool comms = (d & MASK_COMMS_IN) != 0;

	if (hwGroupStart == 1) {
		// immediately pass through COMMS when h/w drives are first in chain
		CONTROL_PORT_OUT = comms ? d | MASK_COMMS_OUT : d & ~MASK_COMMS_OUT;
	}

	if (isCommsClk(d)) {
		// When COMMS_CLK goes active, we increase the clock count, shift the
		// register by one, and add current COMMS state at the start.

		stopTimer();

		if ((driveOffset == 0xff) && QL && comms && (commsClkCount < 8)) {
			// When we see the 1 bit at COMMS_CLK going active, clock count is
			// the drive offset, with an offset of 0 meaning first drive in chain.
			driveOffset = commsClkCount;
		}

		commsClkCount++;
		commsRegister = commsRegister << 1;
		if (comms) {
			commsRegister |= 1;
		}

	} else {
		// COMMS_CLK going inactive triggers the shift register. If there are
		// h/w drives present further down the chain, we therefore sent them
		// the COMMS signal here, with the correct delay using commsRegister.
		if (hwGroupStart > 1) {
			CONTROL_PORT_OUT = (commsRegister & maskHwOffset) != 0 ?
				d | MASK_COMMS_OUT : d & ~MASK_COMMS_OUT;
		}
		setTimer(TIMER_COMMS, selectDrive);
	}
}

/*
	Called by timer when TIMER_COMMS expires. That is, if for a duration of
	TIMER_COMMS, there has been no change on the COMMS_CLK line, then the active
	drive has been selected, or all drives deselected, by Interface 1/QL.
 */
void selectDrive() {

	if (QL && isCommsClk(CONTROL_PORT_IN)) {
		// QL switches COMMS_CLK to HIGH and keeps it HIGH as long as it's
		// interested in reading more data. Should the timer fire when we're
		// still reading, we just discard it.
		return;
	}

	if (!synced) {
		return;
	}

	uint8_t last = activeDrive;
	activeDrive = 0;

	// A drive offset of 0xff means we don't know yet, and that also means
	// none of the virtual drives can possibly have been selected.
	if (driveOffset != 0xff) {
		for (uint8_t reg = IF1 ? commsRegister : commsRegister << driveOffset;
			reg > 0; reg >>= 1) {
			activeDrive++;
		}
	}

	debugMsg('C', 'K', commsClkCount);
	debugFlush();
	debugMsg('C', 'R', commsRegister);
	debugFlush();
	debugMsg('O', 'F', driveOffset);
	debugFlush();
	debugMsg('D', 'R', activeDrive);
	debugFlush();

	if (driveOffset != 0xff || commsClkCount > 7) {
		// As soon as the drive offset has been determined, we can reset comms
		// clock count each time a drive is selected. We can do this also if the
		// count goes beyond the maximum offset (7), which happens when
		// resetting the QL.
		commsClkCount = 0;
	}

	// avoid turning on a virtual drive when a h/w drive has been selected
	if (activeDrive <= driveOffset
		|| (hwGroupStart <= activeDrive && activeDrive <= hwGroupEnd)) {
		activeDrive = 0;
	}

	if (activeDrive == 0) {
		lastActiveDrive = last;
		driveOff();
	} else {
		lastActiveDrive = activeDrive;
		driveOn();
	}
}

/*
	Retrieve the drive state from the daemon, if it is still unknown.
	Otherwise, cached value is used.
 */
void ensureDriveState() {

	if (spinning && (driveState != DRIVE_STATE_UNKNOWN)) {
		return;
	}

	if (!spinning && (driveState == DRIVE_STATE_UNKNOWN)) {
		return;
	}

	// When a drive is turned off, selectDrive() may have been hit earlier then
	// this function (seen with QL + Minerva ROM), so activeDrive will already
	// be 0. Previous active drive is therefore saved in lastActiveDrive.
	uint8_t drive = spinning ? activeDrive :
		(activeDrive == 0 ? lastActiveDrive : activeDrive);

	daemonCmdArgs(CMD_STATUS, drive, spinning ? 1 : 0, 0, 0);

	if (spinning) {
		for (int r = 0; r < 400; r++) {
			if (SERIAL_PORT.available() > 0) {
				driveState = SERIAL_PORT.read();
				setWriteProtect(!isDriveWritable());
				return;
			}
			delay(5);
		}
		synced = false;

	} else {
		driveState = DRIVE_STATE_UNKNOWN;
	}
}

//
bool isDriveReadable() {
	return (driveState & DRIVE_READABLE) == DRIVE_READABLE;
}

//
bool isDriveWritable() {
	return (driveState & DRIVE_FLAG_READONLY) == 0;
}

// -------------------------------------------------------------- RECORDING ---

void record() {

	bool formatting = false;
	uint8_t blocks = 0;
	ledWrite(ACTIVE);

	do {
		daemonPendingCmd(CMD_PUT, activeDrive, 0);
		uint16_t read = receiveBlock();
		blocks++;

		if (blocks % 4 == 0) {
			ledWriteFlip();
		}

		if (usedBoxEnd > read) {
			SERIAL_PORT.write(fillBytes, usedBoxEnd - read); // pad to box end
			SERIAL_PORT.flush();
			// add (possibly) replaced bytes, may not have been used, but we
			// write those two bytes every time, which is more consistent;
			// final two bytes are the actual length of the contents in the box
			buffer[2] = read & 0xff;
			buffer[3] = read >> 8;
			SERIAL_PORT.write(buffer, 4);
			SERIAL_PORT.flush();
		}

		read += PREAMBLE_LENGTH; // preamble is not sent to daemon

		if (read < (IF1 ? HEADER_LENGTH_CPM_MUX : headerLengthMux)) {
			break; // nothing useful received
		} else if (read < (IF1 ? RECORD_LENGTH_CPM_MUX : recordLengthMux)) {
			formatting = true;   // headers are only written during format
			ledRead(IDLE);       // when that happens, we stay here
		}

	} while (formatting);

	if (formatting) {
		driveState = DRIVE_FLAG_LOADED | DRIVE_FLAG_FORMATTED;
	}

	ledWrite(IDLE);
	checkReplayOrStop();
}

/*
	Receive a block (header or record). Change in drive state is checked only
	while waiting for the start of the block. Once the block starts, no further
	checks are done. End of data from Interface 1/QL however is detected.

	We're reading both tracks simultaneously. Pin assignments are chosen such
	that one track is at bit position 0, the other at 4. 4 bits from each track
	are ORed into `d`, with a left shift before each OR.

                          --------------- track 1
                          |           --- track 2
           bit            |           |
      position:  7  6  5  4  3  2  1  0
    TRACK_PORT:  X  X  X  |  X  X  X  |   X = don't care
                   A N D  |           |
          MASK:  0  0  0  1  0  0  0  1
                    O R   |           |
             d: [ track 1 *][ track 2 *]  << before each OR

	`d` is then forwarded to the daemon over the serial line. This is repeated
	until the block (header or record) is done. The number of bytes read is
	returned. The receiving side takes care of demuxing the data, additionally
	considering that the tracks are shifted by 4 bits relative to one another.
 */
uint16_t receiveBlock() {

	noInterrupts();

	register uint8_t start = TRACK_PORT_IN & MASK_BOTH_TRACKS, end, bitCount, d, w;
	register uint16_t read = 0, ww;

	usedBoxEnd = 0;
	register uint8_t box = 0;
	register uint16_t boxEnd = BOXES[box];

	for (ww = 0xffff; ww > 0; ww--) {
		// sync on first bit change of block, on either track
		if (((TRACK_PORT_IN & MASK_BOTH_TRACKS) ^ start) != 0) {
			break;
		}
		// but don't wait forever, and bail out when activity on COMMS_CLK
		// indicates impending change of drive state
		if (checkReplayOrStop() || ww == 1) {
			SERIAL_REGISTER = ww == 1 ? 2 : 1; // cancel pending PUT command
			interrupts();
			return 0;
		}
	}

	// search for sync pattern, which is 10*'0' followed by 2*'ff'; we therefore
	// look for at least 24 consecutive zeros on both tracks, followed by eight
	// ones on at least one track.
	register uint8_t zeros = 0, ones = 0;
	while (zeros < 24 || ones < 8) {

		for (w = 0xff; (((TRACK_PORT_IN & MASK_BOTH_TRACKS) ^ start) == 0) && w > 0; w--);

		if (w == 0) { // could not sync
			SERIAL_REGISTER = 3; // cancel pending PUT command
			interrupts();
			return 0;
		}

		_delay_us(2.0); // short delay to make sure track state has settled
		start = TRACK_PORT_IN & MASK_BOTH_TRACKS;

		if (IF1) _delay_us(6.50); else _delay_us(4.50);

		if (((end = TRACK_PORT_IN & MASK_BOTH_TRACKS) ^ start) == 0) {
			if (ones > 0) {
				ones = 0;
				zeros = 1;
			} else {
				zeros++;
			}
		} else {
			ones++;
		}
		start = end;
	}

	SERIAL_REGISTER = 0; // complete pending PUT command to go ahead

	while (true) {

		d = 0;

		for (bitCount = 4; bitCount > 0; bitCount--) {

			// wait for start of cycle, or end of block
			// skipped when coming here for the first time
			for (w = 0x20; (((TRACK_PORT_IN & MASK_BOTH_TRACKS) ^ start) == 0) && w > 0; w--);

			if (w == 0) { // end of block
				usedBoxEnd = boxEnd;
				interrupts();
				return read;
			}

			_delay_us(2.0);  // short delay to make sure track state has settled
			start = TRACK_PORT_IN & MASK_BOTH_TRACKS;  //then take start reading
			                                        // and wait for end of cycle
			if (IF1) _delay_us(6.50); else _delay_us(4.50);
			// When a track has changed state compared to start of cycle at this
			// point, then it carries a 1 in this cycle, otherwise a 0.
			d = (d << 1) | ((end = TRACK_PORT_IN & MASK_BOTH_TRACKS) ^ start);   // store
			start = end;                               // prepare for next cycle
		}

		read++;

		if (read == boxEnd) {
			buffer[box] = d;      // save replaced byte
			box++;                // next box
			boxEnd = BOXES[box];
			SERIAL_REGISTER = 1;  // send continue flag over serial
		} else {
			SERIAL_REGISTER = d;  // send data byte over serial
		}
	}
}

// ----------------------------------------------------------------- REPLAY ---

/*
	Replay one sector. Switching over into record mode or stopping of drive is
	continuously monitored, and if detected, returns immediately.
 */
void replay() {

	if (checkRecordingOrStop() || !isDriveReadable()) {
		return;
	}

	daemonCmdArgs(CMD_GET, activeDrive, 0, 0, 0);

	unsigned long start = millis();
	uint16_t rcv = daemonRcv(0);
	if (rcv == 0) {
		synced = millis() - start < RESYNC_THRESHOLD;
		return;
	}

	// header
	uint8_t hLen = buffer[CMD_LENGTH];
	if (replayBlock(buffer + CMD_LENGTH + 1, hLen)) {
		return;
	}

	headerGap = true;
	if (timerEnabled()) { // COMMS_CLK timer may be active at this point
		stopTimer();
	}
	setTimer(IF1 ?
			(hLen < HEADER_LENGTH_IF1 ? TIMER_HEADER_GAP_CPM : TIMER_HEADER_GAP_IF1)
		: TIMER_HEADER_GAP_QL, endHeaderGap);

	while (headerGap) {
		if (checkRecordingOrStop()) {
			return;
		}
		_delay_us(20.0);
	}

	// record - this can be different lengths, due to for example extra bytes
	// being sent during format, or shorter records in CP/M
	replayBlock(buffer + CMD_LENGTH + 1 + hLen, rcv - hLen - 1);
}

/*
	Get a sector from daemon and immediately reflect it back. For reliability
	testing. FIXME: validate
 */
void verify() {
	daemonCmdArgs(CMD_GET,
		lowByte(sectorLengthMux), highByte(sectorLengthMux), ' ', 0);
	daemonRcv(sectorLengthMux);
	// send back for verification
	daemonCmdArgs(CMD_VERIFY,
		lowByte(sectorLengthMux), highByte(sectorLengthMux), ' ',
		sectorLengthMux);
}

/*
	Indefinitely replays the given pattern for checking wave form with
	oscilloscope.
 */
void calibrate(uint8_t pattern) {
	calibration = true;
	for (int ix = 0; ix < BUF_LENGTH; ix++) {
		buffer[ix] = pattern;
	}
	setTracksToReplay();
	while (true) {
		replayBlock(buffer, BUF_LENGTH);
	}
}

/*
	Replay a block of bytes to the Interface 1/QL. Switching over to recording
	and stopping of drive is periodically checked. If either of those was
	detected, replay stops and true is returned. If replay was completed,
	false is returned.
 */
bool replayBlock(uint8_t* buf, uint16_t len) {

	noInterrupts();

	register uint8_t bitCount, d, tracks = MASK_BOTH_TRACKS;

	// NOTE: To be fully precise, we would have to make below output operations
	//       only work on the two track pins, in order to avoid changing the
	//       network pins. This would change the timing though, and we'd need to
	//       re-calibrate. However, we're always writing 1 to the network pins
	//       here, which doesn't cause any problems, so we keep as is.

	for (; len > 0; len--) {
		d = *buf;
		for (bitCount = 4; bitCount > 0; bitCount--) {
			tracks = ~tracks;            // tracks always flip at start of cycle
			TRACK_PORT_OUT = tracks | ~MASK_BOTH_TRACKS;            // cycle end
			                                         // wait for middle of cycle
			if (IF1) _delay_us(5.40); else _delay_us(4.20);
			tracks = tracks ^ d;                   // flip track where data is 1
			TRACK_PORT_OUT = tracks | ~MASK_BOTH_TRACKS;// write out track flips
			                                            // wait for end of cycle
			if (IF1) _delay_us(2.35); else _delay_us(1.15);
			// note that calibration must not be a compile time constant,
			// otherwise we'd get different timing due to optimizations
			if (checkRecordingOrStop() && !calibration) {
				interrupts();
				return true;
			}
			d = d >> 1;
		}
		buf++;
	}

	// return tracks to idle level (HIGH) at cycle end
	TRACK_PORT_OUT |= MASK_BOTH_TRACKS;

	interrupts();
	return false;
}

// ------------------------------------------------------- SINCLAIR NETWORK ---

//
void handleNetwork() {

	uint16_t delay = 500;
	bool resume = false;
	bool more = false;

	switch (net) {
		case NET_REPLAY:
		case NET_REPLAY_RETRY:
			more = netReplayPacket();
			break;

		case NET_FORWARD:
			more = netForwardPacket();
			resume = netBackoff > 0;
			break;

		case NET_CUE_SILENCE:
			netCueOnSilence();
		default: // no untimely pings in case of error or back-off
			more = true;
	}

	if ((net == NET_BACKOFF) && !resume) { // check back-off expiration
		resume = ((millis() - netBackoffStart) > netCollisionBackoff);
	}

	if (resume) { // resume replay if forward took place or back-off timed out
		net = netBackoff;
		netBackoffStart = 0;
		netBackoff = 0;
		more = true;

	} else if (net == NET_BACKOFF) {
		delay = 1500; // longer ping delay if we're still in back-off
	}

	if (!more) { // when more packets are available, don't send stats
		netHandleStats();
	}

	// if there's more, push pack next ping; otherwise it's the end of a replay
	// or forward strand, which needs to be immediately followed by a ping to
	// unlock the conduit, so that net flags can be sent again more quickly
	queueNextPing(more ? delay : 0);
}

// retrieves one packet from the daemon and replays it to Interface 1/QL;
// returns true if more packets are available
bool netReplayPacket() {

	daemonCmdArgs(CMD_NET, CMD_NET_INGRESS,
		net == NET_REPLAY_RETRY ? 1 : 0, 0, 0);

	unsigned long start = millis();
	uint16_t rcv = daemonRcv(0);
	if (rcv < NET_HEADER_LENGTH) {
		synced = millis() - start < RESYNC_THRESHOLD;
		net = NET_IDLE;
		return false;
	}

	disableNetInputInterrupt();
	activateNetwork();

	uint8_t receiver = buffer[CMD_LENGTH + NET_IX_NCIRIS];
	uint8_t sender = buffer[CMD_LENGTH + NET_IX_NCSELF];
	bool sent = false;
	netBackoff = 0;

	while (!spinning) {
		if (!claimNetwork(sender)) { // collision, local station wants to send
			debugMsg('N', 'C', 21);
			debugFlush();
			netBackoffStart = millis();
			netCollisionBackoff *= 2; // double back-off time at each collision
			if (netCollisionBackoff > NET_MAX_BACKOFF) { // but limit length
				netCollisionBackoff = NET_MAX_BACKOFF;
			}
			netBackoff = 1;
			break;
		}

		_delay_us(71.4); // header gap, 271 T = 77.5us
		outpak(buffer + CMD_LENGTH, NET_HEADER_LENGTH);
		if (receiver) {
			if (!netReceiveACK()) {
				debugMsg('N', 'H', 21);
				debugFlush();
				break;
			}
			_delay_us(150.0);
		} else {
			_delay_us(166.5); // data block gap, 600 T = 171.6us for broadcast
		}

		outpak(
			buffer + CMD_LENGTH + NET_HEADER_LENGTH, rcv - NET_HEADER_LENGTH - 1);
		if (receiver && !netReceiveACK()) {
			debugMsg('N', 'B', 21);
			debugFlush();
			pktIngressErr++;
			break;
		}

		sent = true;
		break; // success
	}

	bool more = buffer[CMD_LENGTH + rcv - 1] > 0;

	if (sent) {
		net = more ? NET_REPLAY : NET_IDLE;
		// sent packet clears collision back-off, use half of initial since next
		// collision will double value
		netCollisionBackoff = NET_INITIAL_BACKOFF/2;
		netReplayRetries = 0;
		pktIngress++;
	} else {
		debugMsg('N', 'T', 21); // FIXME send failure to daemon
		debugFlush();
		net = NET_REPLAY_RETRY;
		netReplayRetries++;
	}

	if (netBackoff) {
		netBackoff = net;
		net = NET_BACKOFF;
	}

	deactivateNetwork();
	enableNetInputInterrupt();

	if (net == NET_REPLAY_RETRY && (netReplayRetries%NET_REPLAY_BREAK_COUNT == 0)) {
		delay(NET_REPLAY_BREAK);
	}

	return !sent || more;
}

// network input interrupt routine; fires on pin change
#if defined(__AVR_ATmega328P__)
ISR(PCINT1_vect) {
	PCIFR |= B00000010; // PCIF1

#elif defined(__AVR_ATmega32U4__)
void isr_net() {
	EIFR |= B01000000; // INTF6

#endif

	if (spinning) { // Microdrive outranks network
		return;
	}

	switch (net) {
		case NET_BLOCK: // prevent local stations from using network
			blockNetwork();
			return;

		case NET_IDLE:
		case NET_BACKOFF:
			// clear to receive a packet; recording the packet is done within
			// ISR due to timing constraints, upload to daemon however is taken
			// care of in main loop
			netRecordPacket();
			break;

		default: // any other value means not clear to receive
			return;
	}
}

// records one packet coming in from IF1/QL; called from net input ISR
void netRecordPacket() {

	disableNetInputInterrupt();

	netPacketLength = 0;
	int station = netReadSCOUT();
	if (station < 0) {
		enableNetInputInterrupt();
		return;
	}

	// Some programs such as LEC COPY, are doing "anonymous broadcasts", i.e.
	// they set sender to 0 in the packet, and also in the SCOUT. So we need to
	// allow this. Furthermore, QL TK2 and sGC/SMSQ/QXL have a totally different
	// SCOUT format, so using this is questionable anyway.
	//if (station == 0) {
	//	netResyncOnSilence();
	//	return;
	//}

	_delay_us(70.0);

	// ZX Net and QL (standard or Minerva) should be Low at this point. For a QL
	// with TK2, however the SCOUT will still be High here and directly lead into
	// the start bit of the first byte frame. Falling edge detection in inpak()
	// has a timeout of approx. 80us though. So if we see High here, we need to
	// wait a little more before starting to receive. So far, about 528us have
	// passed since the start of the SCOUT (including the 70us wait time above).
	// A TK2 SCOUT measures from start to end of first byte frame start bit 675us.
	// So we wait an additional 90us, leaving a little less than 60us to cover,
	// which inpak() can handle.
	if (NET_PORT_IN & MASK_NET_IN) {
		_delay_us(90.0);
	}

	uint16_t readH = inpak(buffer + CMD_LENGTH, NET_HEADER_LENGTH, 0); // header
	if (netRcvError(readH, NET_HEADER_LENGTH, 'H')) {
		netResyncOnSilence();
		return;
	}

	uint8_t receiver = buffer[CMD_LENGTH + NET_IX_NCIRIS];

	uint8_t want = buffer[CMD_LENGTH + NET_IX_NCHCS]; // header checksum
	// header checksum was added by inpak to total sum
	uint8_t sum = buffer[CMD_LENGTH + readH] - want;
	if (receiver && (sum != want)) {
		netRcvError(310, 0, 'h');
		netResyncOnSilence();
		return;
	}

	_delay_us(120.0);
	if (receiver > 8) { // FIXME make last local station configurable (see also below)?
		netSendACK();
	} else if (receiver > 0) {
		debugMsg('N', 'L', receiver);
		netResyncOnSilence(); // local traffic, wait for end of transmission
		return;
	}

 	// read block
 	uint16_t len = buffer[CMD_LENGTH + NET_IX_NCOBL];
	uint16_t readB = inpak(buffer + CMD_LENGTH + readH, len, 0);
	if (netRcvError(readB, len, 'B')) {
		netResyncOnSilence();
		return;
	}

	want = buffer[CMD_LENGTH + 6]; // data checksum
	sum = buffer[CMD_LENGTH + readH + readB];
	if ((receiver > 8) && (sum != want)) {
		netRcvError(310, 0, 'b');
		netResyncOnSilence();
		return;
	}

	_delay_us(120.0);
	if (receiver > 8) {
		netSendACK();
	}

	netPacketLength = readH + readB;
	if (receiver == 0 || receiver > 8) {
		net = NET_FORWARD;
		quietDaemon = true; // FIXME only at start of strand
	} else {
		net = NET_IDLE;
	}

	enableNetInputInterrupt();
	pktEgress++;
}

// forwards recorded packet to daemon, if a packet is present
bool netForwardPacket() {

	bool more = false;

	if (netPacketLength > 0) {
		// prevent local stations from sending until we've forwarded the packet
		flagNetworkBlock();
		daemonCmdArgs(CMD_NET, CMD_NET_EGRESS,
			netPacketLength & 0xff, netPacketLength >> 8, netPacketLength);
		more = buffer[CMD_LENGTH + NET_IX_NCTYPE] == 0;
		netPacketLength = 0;
		unblockNetwork();
	}

	net = NET_IDLE;
	return more;
}

//
void netHandleStats() {

	if (pktEgress + pktEgressErr  > 20) {
		daemonCmdArgs(CMD_NET, CMD_NET_EGRESS_STAT, pktEgress, pktEgressErr, 0);
		pktEgressErr = 0;
		pktEgress = 0;
	}

	if (pktIngress + pktIngressErr  > 20) {
		daemonCmdArgs(CMD_NET, CMD_NET_INGRESS_STAT, pktIngress, pktIngressErr, 0);
		pktIngressErr = 0;
		pktIngress = 0;
	}
}

//
bool netRcvError(uint16_t rcvd, uint16_t expected, uint8_t indicator) {

	if (rcvd == expected) {
		return false;
	}

	enableNetInputInterrupt();

	pktEgressErr++;
	netRcvErrors++;

	if (netRcvErrors > NET_RCV_ERROR_THRESHOLD) {
		netProfileIx++;
		if (netProfileIx > NET_PROFILE_LAST) {
			netProfileIx = 0;
		}
		netCenterCue = netProfiles[netProfileIx][NET_PROFILE_CENTERCUE];
		netBitInpak  = netProfiles[netProfileIx][NET_PROFILE_BIT_INPAK];
		netBitOutpak = netProfiles[netProfileIx][NET_PROFILE_BIT_OUTPAK];
		netRcvErrors = 0;
		debugMsg('N', 'P', netProfileIx & 0xff);
	} else {
		debugMsg('N', indicator, (rcvd < 300 ? rcvd : rcvd - 300) & 0xff);
	}

	return true;
}

// send a header or block acknowledgement
void netSendACK() {
	activateNetwork();
	buffer[0] = 1;
	outpak(buffer, 1);
	deactivateNetwork();
}

// wait for a header or block acknowledgement
bool netReceiveACK() {
	buffer[0] = 0;
	uint16_t ret = inpak(buffer, 1, 0x1fff); // timeout at approx. 3ms
	debugMsg('N', 'K', buffer[0]*100 + (ret > 300 ? ret - 300 : ret));
	debugFlush();
	return buffer[0] == 1;
}

// this merely flags that we need to re-sync on network silence; for calling
// from ISR; the actual sync will then be done from main loop
void netResyncOnSilence() {
	net = NET_CUE_SILENCE;
	enableNetInputInterrupt();
}

// find a network resting gap of approx. 3 ms
void netCueOnSilence() {

	disableNetInputInterrupt();

	const uint16_t counter = 550;
	for (register uint16_t ix = counter; ix > 0; ix--) {
		if (NET_PORT_IN & MASK_NET_IN) {
			ix = counter;
		}
		_delay_us(5.0);
	}

	net = NET_IDLE;
	enableNetInputInterrupt();
}

// this is supposed to be called as soon as the rising edge of the leader pulse
// for the SCOUT has been detected; after lead in time we check one more time
// whether network is active, if not, -1 is returned
int netReadSCOUT() {

	uint8_t station = 0;

	_delay_us(25.0);

	if ((NET_PORT_IN & MASK_NET_IN) == 0) {
		return -1; // net is inactive, not really a SCOUT
	}

	for (register uint8_t ix = 8; ix > 0; ix--) { // read SCOUT
		_delay_us(52.0);
		// similar to the ASM section in inpak()
		__asm__ __volatile__ (		// cycles
			"lsl  %0       \n"		// 1
			"sbis %2, 0x01 \n"		// 1 (no skip) / 2 (skip)
			"ori  %0, 0x01 \n"		// 1
			: "=r" (station) : "r" (station), "I" (_SFR_IO_ADDR(NET_PORT_IN)));
	}

	// activate only for debugging, disturbs timing between reading SCOUT and
	// start of header
	//debugMsg('N', 'S', station);
	//debugFlush();

	return station;
}

//
bool claimNetwork(uint8_t station) {

	// T = 0,000000286

	noInterrupts();

	// network resting check; 3ms + random
	for (register uint16_t ix = 600 + (uint16_t)random(100); ix > 0; ix--) {
		if (NET_PORT_IN & MASK_NET_IN) {
			interrupts();
			return false;
		}
		_delay_us(5.0);
	}

	// send SCOUT leader pulse, then negated station number, MSB first; each
	// pulse is 181 T = 51.714us; LOW on out pin drives transistor and yields
	// HIGH on network
	uint8_t bit = 0;
	for (register uint8_t ix = 9; ix > 0; ix--) {
		NET_PORT_OUT = bit ? MASK_NET_OUT : 0;
		_delay_us(38.0); // collision detection check after 136 T = 38.857us
		switch ((NET_PORT_IN & MASK_NET_IN) | bit) {
			case MASK_NET_IN | 0x80:
			case 0:
				interrupts();
				return false;
		}
		bit = station & 0x80;
		station <<= 1;
		_delay_us(12.7);
	}

	NET_PORT_OUT = MASK_NET_OUT; // return to LOW
	interrupts();
	return true;
}

// trying to find the last byte by checking for the shorter last stop period did
// not proof to be practical, timing was too tight; however, we always know how
// many bytes we want, so we always request exactly that
uint16_t inpak(uint8_t* buf, uint16_t len, uint16_t timeout) {

	register uint16_t ww = timeout > 0 ? timeout : 0xffff, read = 0;

	for (; ww > 0; ww--) { // sync on rising edge of leader pulse
		if (NET_PORT_IN & MASK_NET_IN) {
			break;
		}
		if (ww == 1) {
			return 301; // timed out waiting for leader pulse (approx. 25ms) FIXME reduce?
		}
	}

	// TODO _delay_loop_2 doc; 1 loop is 250ns

	register uint8_t d, ix;
	register uint16_t a = 0;

	while (read < len) {
		// at the start of each byte we need to sync on the falling edge of the
		// leader pulse (98 T = 28us) when coming here for the first time, or
		// the stop period of previous byte (145 T = 41.4us, final stop is
		// 86 T = 24.6us)

		_delay_loop_2(60); // make sure we're well into the leader/stop period

		// if net input is LOW at this point, we've reached end of block, but
		// did not receive enough data
		if (read > 0 && (NET_PORT_IN & MASK_NET_IN) == 0) {
			return 302; // insufficient data
		}

		// find falling edge; don't use 0xff for ix, produces more ASM code
		// waits max. 80us
		for (ix = 0xfe; (NET_PORT_IN & MASK_NET_IN) && ix > 0; ix--);

		// if net input is still HIGH at this point, that's an error
		if (NET_PORT_IN & MASK_NET_IN) {
			return 303; // discard data
		}

		// wait into center of start period (40 T = 11.4us); this gets auto
		// adjusted in netRcvError() if too many errors occur;
		// known good values are:
		//
		//		Spectrum  48k: 	<16-20>, default 18
		//		Harlequin 128: 	<14,15>, default 14
		//		QL:				<10,11>, default 10
		//
		_delay_loop_2(netCenterCue);

		d = 0;
		for (ix = 8; ix > 0; ix--) { // sample one byte, each bit also 40 T
			_delay_loop_2(netBitInpak);
			// To avoid integer promotion on d when right shifting, and to
			// prevent compiler surprises in the future, use ASM instead of:
			//
			//		d >>= 1;
			//		if (NET_PORT_IN & MASK_NET_IN) d |= 0x80;
			//
			// Time spent here is constant, independent of bit value.
			__asm__ __volatile__ (		// cycles
				"lsr  %0       \n"		// 1
				"sbic %2, %3   \n"		// 1 (no skip) / 2 (skip)
    			"ori  %0, 0x80 \n"		// 1
				: "=r" (d) // output
				: "r" (d), "I" (_SFR_IO_ADDR(NET_PORT_IN)), "M" (PIN_NET_IN_BIT));
		}

		*buf = d;
		a += d;
		buf++;
		read++;
	}

	*buf = a; // set checksum of received data after last byte
	return read;
}

//
void testNet() {

	activateNetwork();
	buffer[0] = 0xaa;
	buffer[1] = 0x55;
	buffer[2] = 0xff;
	buffer[3] = 0x00;
	buffer[4] = 0x01;
	buffer[5] = 0x01;

	for (uint8_t ix = 0; ix < 0xff; ix++) {
		outpak(buffer, 6);
		delay(1000);
	}

	deactivateNetwork();
}

// FIXME bail out when MD activity detected?
void outpak(uint8_t* buf, uint16_t len) {

	if (len == 0) {
		return;
	}

	noInterrupts();

	NET_PORT_OUT = 0; // leader pulse, 98 T = 28us
	_delay_loop_2(106);

	register uint8_t d, a;

	for (;;len--) {
		NET_PORT_OUT = MASK_NET_OUT; // start, LOW for 40 T = 11.4us
		_delay_loop_2(netBitOutpak);

		d = *buf;
		for (register uint8_t ix = 8; ix > 0; ix--) { // output byte, LSB first
			// To avoid integer promotion on d when right/left shifting, and to
			// prevent compiler surprises in the future, use ASM instead of:
			//
			//	NET_PORT_OUT = (d << 2) & MASK_NET_OUT;
			//	d >>= 1;
			//
			// The two NOP at the end combine with the delay that follows,
			// and allow for more fine grained tuning of the timing, since
			// _delay_loop_2 only supports increments of 250ns.
			//
			__asm__ __volatile__ (			// cycles
    			"     ldi  %2, %4   \n"		// 1
				"     lsr  %0       \n"		// 1
    			"     brcc out      \n"		// 1 (false), 2 (true)
    			"     ldi  %2, 0x00 \n"		// 1
    			"out: out  %3, %2   \n"		// 1
    			"     nop           \n"		// 1
    			"     nop           \n"		// 1
				: "=r" (d) // output
				: "r" (d), "r" (a), "I" (_SFR_IO_ADDR(NET_PORT_OUT)), "M" (MASK_NET_OUT)
			);
			_delay_loop_2(netBitOutpak); // each bit 40 T = 11.4us
		}

		// stop, HIGH for 145 T = 41.4us; readings with logic analyzer however
		// suggest a stop time of 31.7us
		NET_PORT_OUT = 0;
		if (len == 1) { // final stop is 86 T = 24.6us
			_delay_loop_2(94);
			NET_PORT_OUT = MASK_NET_OUT; // return to LOW
			interrupts();
			return;
		}
		_delay_loop_2(150);
		buf++;
	}
}

// TODO doc
void setupNetwork() {

	pinMode(PIN_NET_OUT, INPUT);
	pinMode(PIN_NET_IN, INPUT_PULLUP);

#if defined(__AVR_ATmega328P__)
	PCICR |= B00000010; // PCIE1 (bit in PCICR that enables port C)
#elif defined(__AVR_ATmega32U4__)
	attachInterrupt(digitalPinToInterrupt(PIN_NET_IN), isr_net, RISING);
#endif

	enableNetInputInterrupt();
}

// only net output needs to be activated
void activateNetwork() {
	NET_PORT_OUT = MASK_NET_OUT;
	pinMode(PIN_NET_OUT, OUTPUT);
}

// only net output needs to be deactivated
void deactivateNetwork() {
	pinMode(PIN_NET_OUT, INPUT);
}

// flag that network should be blocked if any activity from local stations is
// detected
void flagNetworkBlock() {
	net = NET_BLOCK;
}

// prevent local stations from sending by asserting HIGH on network
void blockNetwork() {

	disableNetInputInterrupt();

	if (net != NET_BLOCKED) {
		netBlockStart = micros();
		net = NET_BLOCKED;
		activateNetwork();
		NET_PORT_OUT = 0;
	}
	enableNetInputInterrupt();
}

// Allow local stations to send by not asserting HIGH on network. If needed,
// unblocking is delayed so that at least 500us will have elapsed since the
// start of an actual block, if any, has occurred after the call to
// flagNetworkBlock(). This is required since in the worst case, network has
// just been blocked before calling this function. We therefore need to delay
// unblocking for the length of a SCOUT to make sure the local station repeats
// claiming the network.
void unblockNetwork() {

	disableNetInputInterrupt();

	if (net == NET_BLOCKED) {
		unsigned long elapsed = micros() - netBlockStart;
		netBlockStart = 0;
		if (elapsed < 500) {
			_delay_loop_2((500 - elapsed) * 4);
		}
		NET_PORT_OUT = MASK_NET_OUT;
		deactivateNetwork();
	}

	net = NET_IDLE;
	enableNetInputInterrupt();
}

// enable interrupts on level change on network input pin
void enableNetInputInterrupt() {
#if defined(__AVR_ATmega328P__)
	PCIFR |= B00000010; // PCIF1
	PCMSK1 |= MASK_NET_IN;
#elif defined(__AVR_ATmega32U4__)
	EIFR |= B01000000;  // INTF6
	EIMSK |= B01000000; // INT6
#endif
}

// disable interrupts on level change on network input pin
void disableNetInputInterrupt() {
#if defined(__AVR_ATmega328P__)
	PCMSK1 &= (~MASK_NET_IN);
	PCIFR |= B00000010; // PCIF1
#elif defined(__AVR_ATmega32U4__)
	EIMSK &= B10111111; // INT6
	EIFR |= B01000000;  // INTF6
#endif
}

// --------------------------------------------------------- DEBUG MESSAGES ---

void debugMsg(uint8_t a, uint8_t b, uint8_t c) {
	msgBuffer[1] = a;
	msgBuffer[2] = b;
	msgBuffer[3] = c;
	message = true;
}

void debugFlush() {
	if (message) {
		msgBuffer[0] = CMD_DEBUG;
		SERIAL_PORT.write(msgBuffer, 4);
		SERIAL_PORT.flush();
		message = false;
	}
}

// --------------------------------------------------- DAEMON COMMUNICATION ---

//
void daemonSync() {

	driveState = DRIVE_STATE_UNKNOWN;

	if (LED_SYNC_WAIT) {
		ledRead(true);
		ledWrite(false);
	}

	while (true) {

		// before sending hello, make sure receive buffer is empty; any garbage
		// remaining in the buffer could potentially prevent alignment with the
		// hello response sent by the daemon
		while (SERIAL_PORT.available() > 0) {
			SERIAL_PORT.read();
		}

		daemonCmd((uint8_t*)(IF1 ? IF1_HELLO : QL_HELLO));

		if (daemonRcvAck(20, 100, (uint8_t*)DAEMON_HELLO)) {
			// send protocol & firmware version
			daemonCmdArgs(CMD_VERSION, PROTOCOL_VERSION, FIRMWARE_VERSION, 1, 0);
			// send config
			daemonCmdArgs(CMD_CONFIG, CMD_CONFIG_RUMBLE, rumbleLevel, 0, 0);
			// send h/w drive setup
			daemonHWGroup();
			lastPing = millis();
			synced = true;

			if (LED_SYNC_WAIT) {
				ledRead(IDLE);
				ledWrite(IDLE);
			}
			return;
		}

		if (LED_SYNC_WAIT) {
			ledReadFlip();
			ledWriteFlip();
		}
	}
}

//
void daemonCheckControl() {

	uint8_t arg1, arg2, arg3;

	while (daemonRcvCmd(5, 2)) {

		arg1 = buffer[CMD_LENGTH + 1];
		arg2 = buffer[CMD_LENGTH + 2];
		arg3 = buffer[CMD_LENGTH + 3];

		switch (buffer[CMD_LENGTH]) {

			case CMD_MAP:
				if (!HW_GROUP_LOCK) {
					setHWGroup(arg1, arg2);
					saveState();
				}
				daemonHWGroup();
				break;

			case CMD_CONFIG:
				remoteConfig(arg1, arg2, arg3);
				break;

			case CMD_RESYNC:
				detectInterface(
					(arg1 & MASK_IF1) != 0, (arg1 & MASK_QL) != 0, false);
				synced = false;
				return;
		}
	}
}

//
void daemonHWGroup() {
	daemonCmdArgs(CMD_MAP, hwGroupStart, hwGroupEnd, HW_GROUP_LOCK ? 1 : 0, 0);
}

// returns true if a pending quiet request could be processed successfully, or
// no quiet request was pending, false otherwise (request processing failed)
bool handleQuietReq() {
	if (quietDaemon) {
		quietDaemon = false;
		return daemonQuiet();
	}
	return true;
}

/*
	Notify daemon that we're entering an activity (Microdrive or network), and
	that it should be quiet from now on, i.e. not send anything unless asked to
	do so. The daemon needs to acknowledge this with a single flag byte. This
	allows the adapter to discard any possibly still in-flight flags recently
	sent by the daemon, which would be received before the acknowledge flag.

	FIXME validate time out
 */
bool daemonQuiet() {

	daemonCmdArgs(CMD_QUIET, 0, 0, 0, 0);

	for (int r = 0; r < 15; r++) {
		if (SERIAL_PORT.available() == 0) {
			delay(5);
		} else {
			if (SERIAL_PORT.read() == CMD_QUIET) {
				return true;
			}
			r = 0;
		}
	}

	debugMsg('E', 'Q', 0);
	debugFlush();
	synced = false;
	return false;
}

//
void daemonCheckFlag() {
	while (SERIAL_PORT.available() > 0) {
		switch (SERIAL_PORT.read()) {
			case FLG_NET:
				if (net == NET_IDLE) {
					net = NET_REPLAY;
					quietDaemon = true;
				}
				break;
			case FLG_RING:
				queueNextPing(0);
				break;
			default: // any other flag sent by the daemon is an error
				synced = false;
				break;
		}
	}
}

//
void queueNextPing(uint16_t delay) {
	lastPing = millis() + delay - PING_INTERVAL;
}

// returns true if a ping exchange is required
bool needsPing() {
	return synced && (millis() - lastPing > PING_INTERVAL);
}

// returns true if a successful ping was performed
bool daemonPing() {

	// If the interface hasn't been confirmed yet, quickly check whether we're
	// still seeing the same interface. This is mostly to work around an
	// Interface 1 quirk: after power on, the COMSS_CLK signal of the IF1
	// occasionally is 0V, while it should be 5V. The first Microdrive action
	// then corrects this. So, if we detect an interface different from what we
	// saw at startup, we cause a re-sync. At first successful drive activation,
	// we consider the interface confirmed and no longer perform this check.
	if (confirmInterface && detectInterface(false, false, true)) {
		synced = false;

	// we need to clear out any still pending flags before the ping
	} else if (daemonQuiet()) {
		// send PING command and check for daemon reply
		daemonCmd((uint8_t*)DAEMON_PING);
		synced = daemonRcvAck(15, 5, (uint8_t*)DAEMON_PONG);
		lastPing = millis();
		if (!synced) {
			for (int ix = 0; ix < CMD_LENGTH; ix++) {
				debugMsg('E', 'P', buffer[CMD_LENGTH + ix]);
				debugFlush();
			}
		}
	}

	return synced;
}

//
void daemonCmd(uint8_t cmd[]) {
	daemonCmdArgs(cmd[0], cmd[1], cmd[2], cmd[3], 0);
}

//
void daemonPendingCmd(uint8_t a, uint8_t b, uint8_t c) {
	buffer[0] = a;
	buffer[1] = b;
	buffer[2] = c;
	SERIAL_PORT.write(buffer, 3);
	SERIAL_PORT.flush();
}

//
void daemonCmdArgs(uint8_t cmd, uint8_t arg1, uint8_t arg2, uint8_t arg3,
	uint16_t bufferLen) {
	buffer[0] = cmd;
	buffer[1] = arg1;
	buffer[2] = arg2;
	buffer[3] = arg3;
	SERIAL_PORT.write(buffer, CMD_LENGTH + bufferLen);
	SERIAL_PORT.flush();
}

//
bool daemonRcvAck(uint8_t rounds, uint8_t wait, uint8_t exp[]) {
	if (daemonRcvCmd(rounds, wait)) {
		for (int ix = 0; ix < CMD_LENGTH; ix++) {
			if (buffer[CMD_LENGTH + ix] != exp[ix]) {
				return false;
			}
		}
		return true;
	}
	return false;
}

//
bool daemonRcvCmd(uint8_t rounds, uint8_t wait) {
	for (int r = 0; r < rounds; r++) {
		if (SERIAL_PORT.available() < CMD_LENGTH) {
			delay(wait);
		} else {
			return daemonRcv(CMD_LENGTH) == CMD_LENGTH;
		}
	}
	return false;
}

//
uint16_t daemonRcv(uint16_t bufferLen) {

	if (bufferLen == 0) { // unknown expected length, get from daemon
		if (daemonRcv(2) == 0) {
			return 0;
		};
		bufferLen = buffer[CMD_LENGTH]
			| (((uint16_t)buffer[CMD_LENGTH + 1]) << 8);
	}

	if (bufferLen > 0) {
		// don't overrun the buffer...
		uint16_t excess = bufferLen > PAYLOAD_LENGTH ? bufferLen - PAYLOAD_LENGTH : 0;
		uint16_t toRead = bufferLen - excess;
		if (SERIAL_PORT.readBytes(buffer + CMD_LENGTH, toRead) != toRead) {
			return 0;
		}
		// ...but still eat up all expected bytes coming in over serial
		uint8_t dummy[1];
		for (; excess > 0; excess--) {
			SERIAL_PORT.readBytes(dummy, 1);
		}
	}

	return bufferLen;
}

bool testBuffer() {
	return testBufferPattern(0xff)
		&& testBufferPattern(0x00)
		&& testBufferPattern(0xaa)
		&& testBufferPattern(0x55);
}

bool testBufferPattern(uint8_t p) {
	for (uint16_t ix = 0; ix < BUF_LENGTH; ix++) {
		buffer[ix] = p;
	}
	for (uint16_t ix = 0; ix < BUF_LENGTH; ix++) {
		if (buffer[ix] != p) {
			return false;
		}
	}
	return true;
}

// ------------------------------------------------------------------ TIMER ---

void setTimer(int preload, TimerHandler h) {
	enableTimer(true, preload, h);
}

void stopTimer() {
	enableTimer(false, 0, NULL);
}

#if defined(__AVR_ATmega328P__)

bool timerEnabled() {
	return TIMSK2 & (1<<TOIE2);
}

void enableTimer(bool on, int preload, TimerHandler h) {

	if (timerEnabled() == on) {
		return;
	}

	noInterrupts();
	timerHandler = h;

	if (on) {
		TCCR2A = 0;
		TCCR2B = 0;
		TIFR2 |= _BV(TOV2); // clear the overflow interrupt flag
		TCCR2B |= (1<<CS22) + (1<<CS21); // 256 pre-scaler
		if (preload < 256) { // fast
			TCNT2 = preload;
		} else { // slow
			TCNT2 = preload - 256;
			TCCR2B |= (1<<CS20); // extend pre-scaler to 1024
		}
		TIMSK2 |= (1<<TOIE2); // enable timer overflow interrupt
	} else {
		TIMSK2 &= (0<<TOIE2);
	}
	interrupts();
}

ISR(TIMER2_OVF_vect) {
	TimerHandler h = timerHandler;
	stopTimer();
	if (h != NULL) {
		h();
	}
}

#elif defined(__AVR_ATmega32U4__)

bool timerEnabled() {
	return TIMSK3 & (1<<TOIE3);
}

void enableTimer(bool on, int preload, TimerHandler h) {

	if (timerEnabled() == on) {
		return;
	}

	noInterrupts();
	timerHandler = h;

	if (on) {
		TCCR3A = 0;
		TCCR3B = 0;
		TIFR3 |= _BV(TOV3); // clear the overflow interrupt flag
		TCCR3B |= (1<<CS32); // 256 pre-scaler
		TCNT3H = 255;
		if (preload < 256) { // fast
			TCNT3L = preload;
		} else { // slow
			TCNT3L = preload - 256;
			TCCR3B |= (1<<CS30); // extend pre-scaler to 1024
		}
		TIMSK3 |= (1<<TOIE3); // enable timer overflow interrupt
	} else {
		TIMSK3 &= (0<<TOIE3);
	}
	interrupts();
}

ISR(TIMER3_OVF_vect) {
	TimerHandler h = timerHandler;
	stopTimer();
	if (h != NULL) {
		h();
	}
}

#endif

// -------------------------------------------------------------------- LED ---

void errorBlink(uint8_t a, uint8_t b, uint8_t c) {
	ledWrite(false);
	while (true) {
		ledBlink(a);
		ledBlink(b);
		ledBlink(c);
		delay(1500);
	}
}

void ledBlink(uint8_t count) {
	for (uint8_t c = count; c > 0; c--) {
		ledWrite(true);
		delay(250);
		ledWrite(false);
		delay(250);
	}
	delay(1000);
}

void ledReadFlip() {
	ledFlip(MASK_LED_READ);
}

void ledRead(bool active) {
	ledActivity(MASK_LED_READ, active, LED_RW_IDLE_ON);
}

void ledWriteFlip() {
	ledFlip(MASK_LED_WRITE);
}

void ledWrite(bool active) {
	ledActivity(MASK_LED_WRITE, active, LED_RW_IDLE_ON);
}

void ledActivity(uint8_t mask, bool active, bool idleOn) {
	(idleOn ? !active : active) ? ledOn(mask) : ledOff(mask);
}

void ledOn(uint8_t led_mask) {
	LED_PORT |= led_mask;
}

void ledOff(uint8_t led_mask) {
	LED_PORT &= (~led_mask);
}

void ledFlip(uint8_t led_mask) {
	LED_PORT ^= led_mask;
}

// ----------------------------------------------------------------- RUMBLE ---

void rumbleGreet() {
	if (rumbleLevel > 0) {
		rumble(true);
		delay(1500);
		rumble(false);
	}
}

void rumble(bool on) {
	if (on && (rumbleLevel == 0)) {
		return;
	}
	analogWrite(PIN_RUMBLE, on ? rumbleLevel : 0);
}

// ------------------------------------------------------------- STATE SAVE ---

struct state {
	uint8_t writes; // write count at EEPROM location, for leveled writes
	uint8_t hwGroupStart; // h/w drive mapping
	uint8_t hwGroupEnd;
	uint8_t rumbleLevel; // rumble motor level
};

void loadState() {
	state* s = &state{};
	if (loadFromEEPROM(s)) {
		debugMsg('S', 'L', 1);
	} else {
		debugMsg('S', 'L', 0);
		resetState(s);
	}
	if (!HW_GROUP_LOCK) {
		setHWGroup(s->hwGroupStart, s->hwGroupEnd);
	}
	rumbleLevel = s->rumbleLevel;
}

void saveState() {
	state* s = &state{};
	s->hwGroupStart = hwGroupStart;
	s->hwGroupEnd = hwGroupEnd;
	s->rumbleLevel = rumbleLevel;
	storeToEEPROM(s);
}

void resetState(struct state* s) {
	s->hwGroupStart = HW_GROUP_START;
	s->hwGroupEnd = HW_GROUP_END;
	s->rumbleLevel = RUMBLE_LEVEL;
}

// let's use good old QL checksum ;-)
uint16_t checksumState(struct state* s) {
	uint16_t a = 0x0f0f;
	uint8_t* p = (uint8_t*)s;
	for (uint8_t ix = 0; ix < sizeof(struct state); ix++) {
		a += *(p + ix);
	}
	return a;
}

// ----------------------------------------------------------------- EEPROM ---

const int EEPROM_START = sizeof(int); // start of EEPROM is used for index

void storeToEEPROM(struct state* s) {

	s->writes++;

	int ix;
	if (s->writes > 100) {
		ix = seekEEPROM(true);
		s->writes = 0;
	} else {
		ix = seekEEPROM(false);
	}

	uint16_t sum = checksumState(s);
	EEPROM.put(ix, sum);
	EEPROM.put(ix + sizeof(sum), *s);
}

bool loadFromEEPROM(struct state* s) {

	int ix = seekEEPROM(false);

	uint16_t sum;
	EEPROM.get(ix, sum);
	EEPROM.get(ix + sizeof(sum), *s);

	return (sum == checksumState(s));
}

int seekEEPROM(bool advance) {

	int ixR;
	EEPROM.get(0, ixR);

	int ixV = validateIndex(ixR);

	if (ixR != ixV) {
		EEPROM.put(0, ixV);
	}
	if (advance) {
		ixV = validateIndex(++ixV);
		EEPROM.put(0, ixV);
	}

	return ixV;
}

int validateIndex(int ix) {
	if (ix < EEPROM_START) {
		return EEPROM_START;
	} else {
		int last = ix + sizeof(uint16_t) + sizeof(state);
		if (last > E2END) {
			return EEPROM_START;
		}
	}
	return ix;
}
