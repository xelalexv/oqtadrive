# Installing On a *BananaPi M2 Zero*

## Before You Begin

|  **Important**  |
|-------------------------------------------|
| See the [notes in the main install guide](install.md#before-you-begin) for things to consider before starting! |

## The Easy Way...
Similar to the stand-alone variant or *QL* add-on with a *RaspberryPi Zero W*, there is a ready-made image that performs an automatic setup on first boot. This setup image is based on a self-built *Armbian* OS image, created using *Armbian*'s build system. The last officially released image for the *BananaPi M2 Zero* dates back to August 2021. To use the setup image, all you need to do is:

- [Download the latest image](https://oqtadrive.org/builds/images/latest-armbian-trunk_bananapim2zero-oqtadrive.img.xz) ([sha256sum](https://oqtadrive.org/builds/images/latest-armbian-trunk_bananapim2zero-oqtadrive.img.xz.sha256)) and write it to a suitable micro SD card, using an image writer, e.g. *gnome-disk-utility* on *Ubuntu*. You can also use the [*Raspberry Pi Imager*](https://www.raspberrypi.com/software/), which is available for *Linux*, *MacOS*, and *Windows*.

- Mount the card on your PC and edit file `/boot/armbian_first_run.txt`, adding the details of your WiFi (country, network name, and password).

    - Note for *Windows*: The card cannot be mounted directly under *Windows*. You need to either [mount it using *WSL 2*](https://devblogs.microsoft.com/commandline/access-linux-filesystems-in-windows-and-wsl-2/), or resort to third party tools, for example [*Linux File System for Windows*](http://www.paragon-software.com/home/linuxfs-windows/) (no affiliation).

- Check the config files `/home/pi/oqtadrive.makerc` and `/home/pi/oqtadrive/config.h`. If your setup differs from the standard stand-alone variant, for example when using an *Arduino Pro Mini* instead of a *Nano*, or a *Nano* with old boot loader, change the respective settings accordingly. In particular, if you're planning to use h/w *Microdrives*, make sure to set `HW_GROUP_LOCK` to `false` in `config.h`.

- Put the card in the *Pi*, place the *Pi* in the stand-alone, and boot it up. Now initial setup of *Armbian* OS is performed, followed by a full *OqtaDrive* installation.

The installation will take five to ten minutes, depending on download speed. When done, you should be able to access the web UI at `{IP address of Pi}:8888/`. Should the installation fail, simply restart the *Pi*. The setup will be retried on every boot until it completed successfully. You can access the *Pi* via `ssh` (user `pi`, password `oqtadrive`). *OqtaDrive* is also configured to use the repo feature. The repo folder is at `/home/pi/repo`. You can place your cartridge and snapshot files here, and organize them in sub-folders if you want.

**Important**:

- The image does not include a particular version of *OqtaDrive*. The latest version available at the time of installation will be downloaded and set up. This requires that *codeberg.org* is online during installation. Please check their [status page](https://status.codeberg.org/status/codeberg) before you start!

- If the installation fails repeatedly, shut down the *Pi*, remove the SD card and mount it on your PC. Check the installation logs `/root/setup.log` and `/home/pi/setup.log` for error messages.

## ...and the Hard Way
If you want to get familiar with all the steps involved to get *OqtaDrive* running on a *BananaPi M2 Zero*, then follow the steps described in the next sections.

## Preparing the *BananaPi*

- Download the *Armbian* image using the link above in the *The Easy Way...* section.

- Use an image writer (e.g. *gnome-disk-utility* on *Ubuntu*) to write the image to a suitable micro SD card.

- *Optional*: Use a partition editor such as *gparted* to increase the `root` partition to full size. For *Armbian* this is not needed. It will perform this step automatically on first boot.

- For letting the *Pi* access your wireless network, copy `/boot/armbian_first_run.txt.template` to `/boot/armbian_first_run.txt` on the SD card. Make these changes:

    ```
    FR_general_delete_this_file_after_completion=1
    FR_net_change_defaults=1
    FR_net_ethernet_enabled=0
    FR_net_wifi_enabled=1
    FR_net_wifi_ssid='<your SSID>'
    FR_net_wifi_key='<your password>'
    FR_net_wifi_countrycode='<country code, upper case>'
    ```

    You may want to create a backup copy of this file when done, since it will get deleted after the first boot.

- To enable the serial port, edit `/boot/armbianEnv.txt` and add this line:

    ```
    overlays=uart3
    ```

    If an `overlays=...` line already exists, just add `uart3` to its end, separated with a space.

- For accessing the board upon first boot, you have three options:

    + connect monitor and keyboard (requires USB OTG adapter)
    + connect via serial console (haven't tried this myself)
    + connect as `root` via `ssh`, initial password is `1234`; if this doesn't work, you can create file `/root/.ssh/authorized_keys` with your public ssh key in it

- Place the SD card in the *Pi*, pick your method of connecting, and boot it up. If you want to connect via `ssh`, check your wireless router to find out which IP address the Pi received, and log in, e.g. `ssh root@192.168.1.12`. The *Armbian* configurator will guide you through a config session for setting up `root` password and a standard user, and a few other basic settings. Once done, you're in a shell session as `root`.

- By default, the new user created in the previous step is already added to the `sudo` group, but still requires the password to be entered. For correct functioning of the *OqtaDrive* installation, we need to enable password-less `sudo` for this user. Invoke `visudo` and add the following line to the end, replacing `{user}` with the name of your user:

    ```
    {user} ALL=(ALL) NOPASSWD:ALL
    ```

- Reboot: `sudo reboot`

## Running the Installer
This works the same as given in the main install guide, but there are two differences:

- The `config.h` file is retrieved from a different URL:

    ```
    curl -fsSL https://codeberg.org/xelalexv/oqtadrive/raw/master/hack/unattended/bananapi/config.h -o oqtadrive/config.h
    ```

- In addition to the `PORT` environment variable, you also have to set `BAUD_RATE` to `500000` and `RESET_PIN` to `16` (`68` if you're using an old standalone PCB), so running a full install would look like this:

    ```
    PORT=/dev/ttyS3 BAUD_RATE=500000 RESET_PIN=16 make install patch_avrdude flash service_on
    ```

### Background
- The baud rate for the serial connection between daemon and adapter needs to be changed since the *BananaPi M2 Zero* does not support the standard 1 Mbps. The fastest speed that both the *Pi* and the *Arduino* support is 500 kbps. Despite not being ideal, this speed seems to be sufficient for normal operation of *OqtaDrive*, but has not been extensively tested yet. Feedback is welcome.
- The *BananaPi M2 Zero* uses a different GPIO chip and therefore different pin line numbering.
