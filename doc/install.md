# Install Guide (*Linux*)

This guide shows you how to set up *OqtaDrive* using the installation `Makefile`. At the current stage of development, this only supports *Linux*. It *may* work on *MacOS*, and possibly in the *Linux* sub-system on *Windows*, but has not been tested there at all. Contributions in this area are very welcome! For the hardware, we'll be using [Tom Dalby's stand-alone setup](https://tomdalby.com/other/oqtadrive.html), which is based on a *RaspberryPi Zero W*, version 1 or 2. If you've built only the adapter and want to run the daemon on your desktop, [have a look at this adapted guide](install_desktop.md). 

## Before You Begin

|  **Note**  |
|-------------------------------------------|
| Due to limited availability of the *RaspberryPi Zero W* in 2021 and 2022, we looked into alternatives and found that the *BananaPi M2 Zero* is (almost) a drop-in replacement for the standalone setup. Mechanically it perfectly fits onto the standalone PCB without any modifications. You just need to [follow slightly different steps for preparing it](install_bananapi.md). As of early 2023, the *RaspberryPi Zero W* is available again, but the *BananaPi* remains as an alternative. |
| Additionally, the *Linino One* board can be used as a standalone setup in itself, since it unifies a micro-controller and a *MIPS* CPU running *OpenWRT* ([install instructions here](install_linino.md)). |

|  **Important**  |
|-------------------------------------------|
| When picking an SD card for the installation, you need to choose a sufficiently fast one, which can be tricky. The speed numbers given on cards are usually most meaningful for writing & reading photos and videos, which are sequential reads/writes. Your best bet is to pick a card that's advertised as *Works well in a smartphone and speeds up your apps*, or something similar. This means that it can do random reads/writes fairly quickly. |
| When editing config files on *Windows*, make sure to use an editor that can save the file with *UNIX* line endings, i.e. LF only! Config files may not work properly when they contain CRLF *DOS* line endings. |

## Installation the Easy Way...
...if you've built the stand-alone variant or *QL* add-on with a *RaspberryPi Zero W* (version 1 or 2), at least. In this case you can use a ready-made image that performs an automatic setup on first boot. All you need to do is:

- [Download the latest image](https://oqtadrive.org/builds/images/latest-raspios-bullseye-armhf-lite-oqtadrive.img.xz) ([sha256sum](https://oqtadrive.org/builds/images/latest-raspios-bullseye-armhf-lite-oqtadrive.img.xz.sha256)) and write it to a suitable micro SD card, using an image writer, e.g. *gnome-disk-utility* on *Ubuntu*. You can also use the [*Raspberry Pi Imager*](https://www.raspberrypi.com/software/), which is available for *Linux*, *MacOS*, and *Windows*.

- Mount the card on your PC, find the `boot` partition, and edit file `wpa_supplicant.conf`, adding the details of your WiFi (country, network name, and password).

- Check the config files `/home/pi/oqtadrive.makerc` and `/home/pi/oqtadrive/config.h` on the `root` (not `boot`!) partition. If your setup differs from the standard stand-alone variant, for example when using an *Arduino Pro Mini* instead of a *Nano*, or a *Nano* with old boot loader, change the respective settings accordingly. In particular, if you're planning to use h/w *Microdrives*, make sure to set `HW_GROUP_LOCK` to `false` in `config.h`.

    - Note for *Windows*: While the `boot` partition can be mounted directly under *Windows*, this is not possible for the `root` partition. This one you either need to [mount using *WSL 2*](https://devblogs.microsoft.com/commandline/access-linux-filesystems-in-windows-and-wsl-2/), or resort to third party tools, for example [*Linux File System for Windows*](http://www.paragon-software.com/home/linuxfs-windows/) (no affiliation).

- Put the card in the *Pi*, place the *Pi* in the stand-alone, and boot it up. Now initial setup of *RaspberryPi OS* is performed, followed by a full *OqtaDrive* installation.

The installation will take five to ten minutes, depending on download speed. When done, you should be able to access the web UI at `{IP address of Pi}:8888/`. Should the installation fail, simply restart the *Pi*. The setup will be retried on every boot until it completed successfully. You can access the *Pi* via `ssh` (user `pi`, password `oqtadrive`). *OqtaDrive* is also configured to use the repo feature. The repo folder is at `/home/pi/repo`. You can place your cartridge and snapshot files here, and organize them in sub-folders if you want.

**Important**:

- The image does not include a particular version of *OqtaDrive*. The latest version available at the time of installation will be downloaded and set up. This requires that *codeberg.org* is online during installation. Please check their [status page](https://status.codeberg.org/status/codeberg) before you start!

- If the installation fails repeatedly, shut down the *Pi*, remove the SD card and mount it on your PC. Check the installation log `/home/pi/setup.log` located on the `root` partition for error messages.

## ...and the Hard Way
If you're using a different setup, or you insist on doing things the hard way, then follow the steps described in the next sections.

### Preparing the *RaspberryPi*

- [Download the *Raspberry Pi OS (Legacy)* image](https://www.raspberrypi.com/software/operating-systems/#raspberry-pi-os-legacy), pick the *Raspberry Pi OS (Legacy) Lite* version, since you won't be needing a fully-fledged desktop system. Don't use any of the newer *bookworm* based images, since they break our current installation workflow & setup tooling.

- Use an image writer (e.g. *gnome-disk-utility* on *Ubuntu*) to write the image to a suitable micro SD card.

- *Optional*: Use a partition editor such as *gparted* to increase the `root` partition to full size. For *RaspberryPi OS* this is not needed. It will perform this step automatically on first boot.

- Create file `ssh` in the `boot` partition on the SD card. This file enables login via *ssh*, and can be empty ([details](https://www.raspberrypi.com/documentation/computers/remote-access.html#ssh)).

- For letting the *Pi* access your wireless network, create file `wpa_supplicant.conf`, also in the `boot` partition. The contents of this file [is documented here](https://www.raspberrypi.com/documentation/computers/configuration.html#configuring-networking-2).

- A user needs to be configured (the default user has recently been removed from the *Raspberry Pi OS* images for security reasons). This is done by creating file `userconf.txt`, again in the `boot` partition. The content is a single line with `{user name}:{encrypted password}` (more details [here](https://www.raspberrypi.com/documentation/computers/configuration.html#configuring-a-user)). To encrypt your password, run this on a *Linux* system:

    ```
    echo 'mypassword' | openssl passwd -6 -stdin
    ```

- Place the SD card in the *Pi* and boot it up. Check your wireless router to find out which IP address it received, and log in via ssh, e.g. `ssh {user name}@192.168.1.12`.

- Edit `/boot/config.txt` to enable the serial port:

    `sudo nano /boot/config.txt`

    Look for the `[all]` section. If it doesn't exist, create it at the end of the file, then add `enable_uart=1`, so you get something similar to this:

    ```
    [all]
    enable_uart=1
    ```

- *Optional (but recommended)*: The WiFi module may go into power saving mode after a few minutes with no network activity. At least on my *Pi Zero W* it does that. This can be deactivate by editing `/etc/rc.local`:

    `sudo nano /etc/rc.local`

    Add the following line anywhere before the final `exit 0` line:
    ```
    /sbin/iwconfig wlan0 power off
    ```

- Reboot:

    `sudo reboot`

### Running the Installer
All steps in this section are performed on the target system, i.e. the *RaspberryPi* in our example.

#### The Short Version
For the impatient, here are all the steps that you would perform on the *Pi* setup after you installed the OS:

```
sudo apt install curl jq gawk strace gpiod
cd
curl -fsSL https://codeberg.org/xelalexv/oqtadrive/raw/master/hack/Makefile -o Makefile
mkdir oqtadrive
curl -fsSL https://codeberg.org/xelalexv/oqtadrive/raw/master/hack/unattended/raspberrypi/config.h -o oqtadrive/config.h

PORT=/dev/ttyS0 make install patch_avrdude flash service_on
```

*Note*: If in your setup, the *Arduino* board is connected via USB, drop `patch_avrdude` (details below).

#### The Long Version
And here's the same with a bit more background information:

- Install the *curl*, *jq*, *gawk*, *strace*, and *gpiod* OS packages, if they're not present. E.g. on *Debian* based systems such as *RasberryPi OS*, run:

    `sudo apt install curl jq gawk`

- Create and/or change into the folder where you want to install *OqtaDrive*. For the *Pi* setup, we're using `/home/pi`.

- Download the install `Makefile`:

    `curl -fsSL https://codeberg.org/xelalexv/oqtadrive/raw/master/hack/Makefile -o Makefile`

- Download the firmware config header file:

    ```
    mkdir oqtadrive
    curl -fsSL https://codeberg.org/xelalexv/oqtadrive/raw/master/hack/unattended/raspberrypi/config.h -o oqtadrive/config.h
    ```

    *Optional*: Make modifications to `config.h`, if your setup deviates from the standard configuration, i.e. if it's not the standalone setup. 

- Using the `Makefile`, download *OqtaDrive*'s `oqtactl` binary & firmware, and install the [*Arduino CLI*](https://github.com/arduino/arduino-cli) for compiling and flashing it:

    `make install`

    The installation of the *Arduino CLI* can take quite a bit, so some patience is required ;-)

- *Optional*: In our *RaspberryPi* setup, the serial connection between *Arduino* and *Pi* is done via GPIO pins, not USB. This requires applying a small patch to the *avrdude* flash program ([details](https://siytek.com/raspberry-pi-gpio-arduino/)). This only has to be run once: 

    `make patch_avrdude`

    If in your setup, the *Arduino* board is connected via USB, you can skip this.

- Now we're ready to compile & flash the firmware. Note that for this step it's important to specify the serial port device to which the adapter is connected. For the *Pi*, that's `/dev/ttyS0`. Compiling and flashing can again take quite a bit:

    `PORT=/dev/ttyS0 make flash`

- If you want to automatically start the *OqtaDrive* daemon whenever the system boots, you can enable it as a *systemd* service. This of course only works if your system is using *systemd* as its init system, which is the case with *RaspberryPi OS* on the *Pi*. Note that also in this step, it's necessary to specify the serial port device:

    `PORT=/dev/ttyS0 make service_on`

    To check on the state of the service, you can run:

    `systemctl status oqtadrive.service`

- If at some later point you want to upgrade to the latest *OqtaDrive* release, run:

    `make upgrade`

    This can however also be done from within the web UI.

The installer `Makefile` has a few more targets you can invoke, and environment variables you may specify for configuration. Simply run `make` to get an online help with detailed explanations.

## Hints for *Pi* Setup

- Make the IP address of the *Pi* static in your router if it supports this, so you don't have to look it up each time you want to `ssh` into it.
- For password-less login, you can also place your public *ssh* key in `/home/{user name}/.ssh/authorized_keys`.
- You can add the environment variables you want to set such as `PORT` to the `.bashrc` of your user, so they're already set up when you `ssh` into the *Pi*.
- If you've done a complete setup on a *Pi Zero W*, and you want to switch to a *Pi Zero 2 W*, you can essentially just take the micro SD card and plug it into the new board. There's one catch however: The *Zero 2 W* uses a different WiFi module, so WiFi setup gets repeated first time you boot it from the card. Only, the `wpa_supplicant.conf` file you placed in the `boot` partition during the initial setup is no longer there, because it got removed after the *Pi* completed WiFi setup. So before plugging the card into the new board, recreate `wpa_supplicant.conf` in the `boot` partition.
