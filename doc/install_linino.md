# Installing On a *Linino One*

## Before You Begin

|  **Important**  |
|-------------------------------------------|
| See the [notes in the main install guide](install.md#before-you-begin) for things to consider before starting! |

## Things to Consider About the *Linino One*
The *Linino One* is an interesting board in the *Arduino* family. It combines an *ATmega32U4* micro-controller with an *Atheros AR9331* SoC (*MIPS* core, 400 MHz, 64MB RAM, onboard WiFi), giving you the real time capabilities we know from other *Arduino* boards, along with a *Linux* system (*OpenWRT* to be precise) for connectivity. This makes it a great fit for *OqtaDrive*, where we need exactly those two aspects.

Sadly however, it was discontinued years ago, and there never was a replacement. I still felt it's interesting enough to get *OqtaDrive* running on this one. It also serves as a bottom-end reference, i.e. the minimum specs needed to run the *OqtaDrive* daemon. You may still be able to get a second hand or even a NOS board at a reasonable price. Just make sure it comes with the SD card add-on. But before you decide for the *Linino*, consider these drawbacks:

- Performance wise it can run the *OqtaDrive* daemon without problems, but booting is fairly slow (approx. 70s). Also, re-building the repo index may take a few minutes for really large repos (~10k files).

- The *Arduino* sketch cannot be built on the *Linino* itself, as is done on the other standalone setups. Instead, a fixed binary is included in the *OqtaDrive* release. This means that you cannot add a config header file with your own changes of default settings. Most of these settings however can be changed dynamically via command line or UI.

- There are currently no known limitations when running *OqtaDrive* on the *Linino One*, but *Linino* support should still be considered alpha.

### Relationship to *Arduino Yun Mini*
The *Linino One* is almost identical to the *Arduino Yun Mini*. Whether the *OqtaDrive* installation documented here can be used as is on a *Yun Mini* is however not clear. If anyone tries this out, feedback would be welcome.

## Preparing the *Linino One* Board
Similar to the stand-alone variant with a *RaspberryPi Zero W* or *BananaPi M2 Zero*, there is a ready-made image that performs an automatic setup. However, the image for the *Linino* is not a full OS image. It only contains resources needed for bootstrapping the installation. There are no longer any OS images available that we could build upon, so we use the version of *OpenWRT* that comes with the *Linino*. Even though this version is totally outdated and the package repos have long been gone, it's still good enough for our purposes. A few preparatory steps are required though, to make it work:

- Do the initial setup of the *Linino* as described in its documentation, to get it connected to your WiFi (web UI for base config should be at `http://192.168.240.1`, password `doghunter`). If your board has been used before and you want to start with factory settings, press the WiFi reset button (the one closest to the edge) for at least 30 seconds to do a factory reset.

- Once the *Linino* is connected to your WiFi, go to the *advanced configuration panel* and change, save & apply these settings:

    - set *System > System > Time Zone* accordingly, and make sure *Enable NTP client* is on
    - add the following line in *System > Startup > Local Startup*, right above the `exit 0` line:
        `/mnt/sda1/oqtadrive/bootstrap.sh`

    - optional: for easier *ssh* access, upload your *ssh* public key under *System > Administration*

### Notes on *ssh* Access
Since the *OpenWRT* version on the *Linino* dates from 2014, its *ssh* daemon still uses a by now deprecated key algorithm. We therefore need to tell our *ssh* client to accept this algorithm, or we won't be able to connect. On *Linux*, you can add this to your `~/.ssh/config` file, assuming the *Linino* is known under host name `linino` (you can also use its IP address here):

```ssh
Host linino
    User root
    KexAlgorithms +diffie-hellman-group1-sha1
    IdentityFile ~/.ssh/id_rsa
```

## Running the Installer

- [Download the latest image](https://oqtadrive.org/builds/images/latest-linino-one.img.xz) ([sha256sum](https://oqtadrive.org/builds/images/latest-linino-one.img.xz.sha256)) and write it to a suitable micro SD card, using an image writer, e.g. *gnome-disk-utility* on *Ubuntu*. You can also use the [*Raspberry Pi Imager*](https://www.raspberrypi.com/software/), which is available for *Linux*, *MacOS*, and *Windows*.
- Use a partition editor such as *gparted* to increase the partition to full size. Otherwise, it's going to be really small, with little space for cartridge files.
- Put the card in the *Linino* and boot it up or reboot. Now a full *OqtaDrive* installation is performed.

The installation will take five to ten minutes, depending on download speed. When done, you should be able to access the web UI at `{IP address of Linino}:8888/`. Should the installation fail, simply restart the *Linino*. The setup will be retried on every boot until it completed successfully. You can access the *Linino* via `ssh` (user `root`, with password you set up during initial prep). *OqtaDrive* is also configured to use the repo feature. The repo folder is at `/mnt/sda1/oqtadrive/repo`, or just `/oqtadrive` if you remove the SD card and mount it in your PC. You can place your cartridge and snapshot files here, and organize them in sub-folders if you want.

**Important**:

- The image does not include a particular version of *OqtaDrive*. The latest version available at the time of installation will be downloaded and set up. This requires that *codeberg.org* is online during installation. Please check their [status page](https://status.codeberg.org/status/codeberg) before you start!

- If the installation fails repeatedly, shut down the *Linino*, remove the SD card and mount it on your PC. Check the installation log `/home/pi/setup.log` for error messages.
