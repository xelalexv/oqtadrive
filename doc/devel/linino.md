# *OqtaDrive* on a *Linino One*

This is for tracking development work needed to make *OqtaDrive* run on a *Linino One*. This board is almost the same as an *Arduino Yun Mini*, but there are subtle differences. *OqtaDrive* on a *Yun* still needs to be tested. Additional work may be required to get it running there.

## TODO
- review service stop/start bracket around flash target
- block time critical APIs when drives are running

## Research

### Setup
https://www.heise.de/blog/Arduino-Yun-ger-3492183.html
https://forum.arduino.cc/t/upgrade-from-lininoos-to-openwrt-yun/313802

- FQBN: `arduino:avr:one`
- USB: `Bus 001 Device 019: ID 2a03:0001 dog hunter AG Linino ONE (bootloader)`
- for USB serial `opkg install kmod-usb-acm` may be required?

sources for MIPS binaries:

- with own fork of https://git.sr.ht/~fincham/static-binary-zoo
    - `bash`
    - `gawk`
    - `make`
    - 'unzip'

- `jq`: https://github.com/itchyny/gojq (need to compile for MIPS, works)

- other sources:
    - `bash`: https://github.com/darkerego/mips-binaries
    - `gawk` https://github.com/akpotter/embedded-toolkit
    - https://github.com/CyberDanube/medusa-embedded-toolkit


### Serial

- discussion about deactivating the serial bridge between *ATMega32U4* micro controller and *Atheros SoC* (MIPS CPU): https://jpmens.net/2013/09/23/understanding-arduino-yun-s-bridge/

    - comment out console on `ttySPI0` in `/etc/inittab` <-- no need to do this, but maybe on *Yun*, if it's using `/dev/ATH0` for the bridge
    - `echo 0 > /proc/sys/kernel/printk; wifi-live-or-reset` <-- no need to do this

- serial port on *OpenWRT* side is `/dev/ttyATH0`

    `stty -F /dev/ttyATH0 460800 cs8 -cstopb -parenb` can be used to configure, but OS does not allow specifying `500000` baud rate, however can set that from daemon binary

### Programming
compile on PC, upload `.hex` binary file to board, then run:
```
merge-sketch-with-bootloader.lua oqtadrive.ino.hex && run-avrdude oqtadrive.ino.hex
```

- https://forum.arduino.cc/t/upload-a-sketch-to-arduino-directly-from-linino/187699

### Timers
- https://medesign.seas.upenn.edu/index.php/Guides/MaEvArM-timers
- https://medesign.seas.upenn.edu/index.php/Guides/MaEvArM-timer0

--> timer 3 seems best

### Ports

| port   |   7    |   6     |   5     |   4   |   3   |   2    |   1   |   0   |
|--------|--------|---------|---------|-------|-------|--------|-------|-------|
| Port B |  PB7   |  PB6    |  PB5    |  PB4  |  PB3  |  PB2   |  PB1  | PB0   |
|        |  D11   |  D10    |  D9     |  D8   |       |        |       |       |
|        |  PWM   |  PWM    |  PWM    |       |  MISO |  MOSI  |  SCK  | RxLED |
|        |`LED.W` |`LED.R`  |`rumble` |`WR.PR`|       |        |       |       |
| Port C |  PC7   |  PC6    |         |       |       |        |       |       |
|        |  D13   |  D5     |         |       |       |        |       |       |
|        |  PWM   |  PWM    |         |       |       |        |       |       |
| Port D |  PD7   |  PD6    |  PD5    |  PD4  |  PD3  |  PD2   |  PD1  |  PD0  |
|        |  D6    |  D12    |         |  D4   |  D1*  |  D0*   |  D2*  |  D3*  |
|        |  PWM   |         | TxLED   |       |  Tx   |  Rx    |  SDA  |  PWM  |
|        |`CM.OUT`|`ERASE`  |         |`COMMS`|       |        |`CMCLK`| `R/W` |
| Port E |        |  PE6    |         |       |       |  PE2   |       |       |
|        |        |  D7*    |         |       |       |        |       |       |
|        |        |  Int6   |         |       |       |  HWB   |       |       |
|        |        |`NET.IN` |         |       |       |        |       |       |
| Port F |  PF7   |  PF6    |  PF5    |  PF4  |       |        |  PF1  |  PF0  |
|        |  A0    |  A1     |  A2     |  A3   |       |        |  A4   |  A5   |
|        |        |         |`NET.OUT`|`DATA1`|       |        |       |`DATA2`|

- https://docs.arduino.cc/hacking/hardware/PinMapping32u4

#### Interrupts

| pin | interrupt |
|-----|-----------|
| D0  | 2         |
| D1  | 3         |
| D2  | 1         |
| D3  | 0         |
| D7  | 4         |

- https://docs.arduino.cc/retired/boards/arduino-yun
- https://www.arduino.cc/reference/en/language/functions/external-interrupts/attachinterrupt/

#### Signal to Port Mapping

| signal | port |  *Nano*  | *Linino*  |
|--------|------|----------|-----------|
| CMCLK  | ctrl | D2   (5) | D2   (L7) |
| R/W    | ctrl | D3   (6) | D3   (L8) |
| COMMS  | ctrl | D4   (7) | D4   (L9) |
| ERASE  | ctrl | D5   (8) | D12 (L17) |
| WR.PR  | (any)| D6   (9) | D8  (L13) |
| CM.OUT | ctrl | D7  (10) | D6  (L11) |
| rumble |(any PWM)| D10 (13) |D9 (L14)|
| LED.R  | led  | D11 (14) | D10 (L15) |
| LED.W  | led  | D12 (15) | D11 (L16) |
| DATA2  | track| A0  (19) | A5   (R9) |
| DATA1  | track| A4  (23) | A3  (R11) |
| NET.IN | net  | A1  (20) | D7  (L12) |
| NET.OUT| net  | A2  (21) | A2  (R12) |

- control port (ctrl): Port D
- track port (track): Port F
- LED port (led): Port B
- net port: Port E (in) & F (out)
