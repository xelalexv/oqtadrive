# Boxed Transmission
Boxed transmission implements send/receive of variable size data, where the length of the data is not known when the transmission starts. However, lengths fall into typical categories (header, record, record with extra data), which can be used to put transmissions into boxes of known sizes. Last byte of a box indicates whether the transmission continues or is done.

## Common Lengths

Headers (no preamble, not transferred):

| type | length |
|------|--------|
| CP/M |  5     |
| IF1  | 15     |
| QL   | 16     |

Records (no preamble, not transferred):

| type | variant | length |
|------|---------|--------|
| CP/M | normal  | 514    |
| CP/M | format  | ???    | # FIXME
| IF1  | normal  | 528    |
| IF1  | format  | 627    |
| QL   | normal  | 526    |
| QL   | normal  | 612    |

Boxes:

| box | lengths        | box length | total box length | padding |
|-----|----------------|------------|------------------|---------|
| a   | 5, 15, 16      |   20       |  20 ( 21 muxed)  | 4 - 15  |
| b   | 514, 526, 528  |  512       | 532 (533 muxed)  | 4 - 18  |
| c   | 612, 627       |   99       | 631 (632 muxed)  | 4 - 19  |

With this, a block transmission is always one of *a*, *ab*, or *abc*. The byte(s) replaced by the flag byte at the end of block a and b are sent at the very end. For the sake of consistency and code simplicity, they are always appended, e.g. even for a transmission of type *a*, which has no byte replaced. The final two bytes are the actual length of the transmitted data contained in the box.
