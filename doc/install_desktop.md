# Install on a *Linux* Desktop

This guide shows you how to set up *OqtaDrive* using the installation `Makefile` on a *Linux* desktop system. The instructions were created for a recent *Ubuntu* system (20.04 or 22.04 should work), but can be easily adapted for other distros.

## Preparation
Decide where you want to place the *OqtaDrive* installation. For this guide, we'll assume `/home/{user}/oqtadrive`, with `{user}` being your user. Open a terminal, create this directory, and `cd` into it:

```
mkdir -p /home/{user}/oqtadrive
cd /home/{user}/oqtadrive
```

## Running the Installer

### The Short Version
For the impatient, here are all the steps that you would perform on your desktop:

```
sudo apt install curl jq gawk # if not yet installed
cd /home/{user}/oqtadrive
curl -fsSL https://codeberg.org/xelalexv/oqtadrive/raw/master/hack/Makefile -o Makefile
curl -fsSL https://codeberg.org/xelalexv/oqtadrive/raw/master/hack/unattended/desktop/oqtadrive.makerc -o oqtadrive.makerc
```

Have a look at `oqtadrive.makerc`, to see whether you need to make any changes according to your environment. Then start the installation:

```
make install flash service_on
```


### The Long Version
And here's the same with a bit more background information:

- Install the *curl*, *jq*, and *gawk* OS packages, if they're not present. E.g. on *Debian* based systems such as *Ubuntu*, run:

    `sudo apt install curl jq gawk`

- Change into the directory created above:

    `cd /home/{user}/oqtadrive`

- Download the install `Makefile`:

    `curl -fsSL https://codeberg.org/xelalexv/oqtadrive/raw/master/hack/Makefile -o Makefile`

- Download the installation config file, `desktop.makerc`:

    ```
    curl -fsSL https://codeberg.org/xelalexv/oqtadrive/raw/master/hack/unattended/desktop/oqtadrive.makerc -o oqtadrive.makerc
    ```

    Make modifications to `oqtadrive.makerc`, if your setup deviates from the standard configuration. 

- Using the `Makefile`, download *OqtaDrive*'s `oqtactl` binary & firmware, and install the [*Arduino CLI*](https://github.com/arduino/arduino-cli) for compiling and flashing it:

    `make install`

    The installation of the *Arduino CLI* can take quite a bit, so some patience is required ;-)

- If you want to make [specific adapter settings](../README.md#configuration), create directory `oqtadrive` in the installation directory and place a settings file `config.h` with your settings into it.

- Now we're ready to compile & flash the firmware. Note that for this step it's important that `oqtadrive.makerc` specifies the correct serial port device to which the adapter is connected. On an *Ubuntu* system, this is usually `/dev/ttyUSB0`:

    `make flash`

- If you want to automatically start the *OqtaDrive* daemon whenever the system boots, you can enable it as a *systemd* user service. This of course only works if your system is using *systemd* as its init system, which is the case for recent versions of *Ubuntu*:

    `make service_on`

    To check on the state of the service, you can run:

    `systemctl --user status oqtadrive.service`

- If at some later point you want to upgrade to the latest *OqtaDrive* release, run:

    `make upgrade`

    This can however also be done from within the web UI.

The installer `Makefile` has a few more targets you can invoke, and environment variables you may specify for configuration. Simply run `make` to get an online help with detailed explanations.

## Hints for *Desktop* Setup

- The serial device under which the adapter will appear on your system depends for one thing on the configuration of your system, but also on whether you're using any other USB serial devices, and if so, in what order you plug them in. To fix the adapter to a particular serial device, consider [setting up a `udev` rule](https://community.silabs.com/s/article/fixed-tty-device-assignments-in-linux-using-udev?language=en_US).
- Depending on your system, your user may need to be a member of the `dialout` group to access the serial device. Check the ownership of the device file: `ls -l /dev/{your device}`.
