# Troubleshooting Guide

## *Use the Log, Luke!*
Whenever you hit a problem, first thing to do is to check the daemon log. For troubleshooting, it's best to switch the daemon into `debug` log level. The most convenient way to do this is from the config tab in the web UI, where you can also easily retrieve the log. But you can also set the log level when starting the daemon:

```bash
LOG_LEVEL=debug oqtactl serve -d /dev/ttyUSB0 [other options]
```

If you're running the daemon as a `systemd` service, you need to edit the unit file accordingly, and restart the service. For many problems, the daemon log should already give you good hints about what's going on. It's also important when reporting an issue over at the project home at *Codeberg*.

## Check the Online Help
The `oqtactl` binary provides online help for all actions. Just run `oqtactl --help` to get a list of the available actions, and `oqtactl {action} --help` for finding out more about a particular action. Maybe there are additional options that you weren't aware of, and that can help you work around a problem.

## Make Sure *OqtaDrive* Correctly Recognizes Your Machine
When the *OqtaDrive* adapter starts up, it auto-detects what it's connected to, i.e. *Interface 1* or *QL*, and configures itself accordingly. This may fail in certain situations, so it's one of the first things to check. The daemon log and also the web UI show what machine has been detected.

In particular, after turning on the *Spectrum*, the adapter sometimes erroneously detects the *Interface 1* as a *QL*. The cause is that the `COMMS_CLK` signal of the *Interface 1* occasionally is at 0V after power on, while it should be 5V. The first *Microdrive* action taken then corrects this. To work around this problem, *OqtaDrive* rechecks the interface type at first drive activation and re-syncs with the correct type, if necessary. The first action will still fail, but the interface setting is now correct and all subsequent actions will succeed. So if you see your *Interface 1* being detected as a *QL*, run a `CAT 1`, and a few seconds later, the problem should be corrected.

You can however also change the detected interface by re-syncing the adapter, either via the web UI or `oqtactl resync`. Both methods also allow you to force a particular machine, via a set of radio buttons or the `--client` option. If you're always using the adapter with only one type of machine, you can also hard-code that in the adapter config. Check the config section at the top of `oqtadrive.ino`, and look for the `FORCE_IF1` and `FORCE_QL` settings. You can also force a particular machine with the `--client` option when starting the daemon. 

*Tip*: When using the adapter with a *Spectrum* + *Interface 1*, make sure to power up the adapter after or together with the *Spectrum*, but not before. Otherwise it will auto-detect a *QL*.

## Topics

### I cannot load this cartridge, keep getting `cartridge corrupted`
If loading a cartridge fails due to cartridge corruption (usually caused by incorrect check sums), try the `--repair` option of the `load` command. With this, *OqtaDrive* will try to repair the cartridge when loading.

### I see unstable behavior, keep getting `Microdrive not found`
Check the wiring that connects the adapter to the *Microdrive* port. Use ribbon cable, rather than separate wires, and keep the cable length below 5cm. Also, use all of the available six ground pins in the *Microdrive* interface to ensure low-resistance ground connection.

### I see unstable behavior with *QL*, in particular `format` is not working
This is very likely caused by a [factory fault](https://oqtadrive.org/09.background/ql-write-protect/) built into the *QL Microdrives*. It's related to diode `D22` on the head PCB of drive 1 and `D23` on drive 2 in *QLs* with issue 5 boards (`D31` and `D30` resp. for issue 6 boards). In many *QLs* with issue 5 boards, these diodes are missing, in those with issue 6 boards they are often installed in the wrong direction. These diodes are part of the write protection circuit. The fault has only an effect when write protection is on in an inactive drive, i.e. no cartridge or a cartridge with removed protection tab is in the drive. In this case, the inactive drive places extra load on the `READ/WRITE` line of the *Microdrive* bus, which can then cause erroneous behavior. A clear indicator for this problem is a non-working `format`.

A quick way to check whether your *QL* suffers from this fault is to place cartridges with protection tab present in all hardware drives and see whether that improves the situation. This is also a simple workaround, if you do not want to open up your *QL*. For a permanent solution, it's however best to add/correct the missing or incorrectly installed diodes. Alternatively, if you don't want to open up your *QL*, you can try adding a diode into the `R/W` line, with anode connected to the *Arduino* pin. This *may* solve the problem, but hasn't been confirmed yet. If you try this, let me know of the results.

### Some files take very long to load
Loading time for one thing depends on where the read position is located within the cartridge at the moment you start the loading. If it's right past the file you want to load, it will take a full round through the cartridge to retrieve it. This is a common property of a tape loop.

Another factor is the placement of the file's records in the cartridge. Many cartridge images you can download have terrible file organization. For example, a *Spectrum* or *QL* will always try to place the records of a file into non-adjacent sectors, i.e. such that there are other sectors in-between. When loading the file, this gives the machine more time to move a record to the right memory location after reading and then getting ready for the next record. On downloaded cartridge images you often see file records placed into adjacent sectors. When loading a file like this, the machine may miss records and then need several rounds through the cartridge to read all. You can use *OqtaDrive* to reorganize cartridges like this.

### I see drives at the wrong number
If you're accessing let's say drive 2, but get the content of drive 3, then the *adapter offset* may be wrong. This offset is the number of actual hardware drives present in the drive chain, upstream of the adapter. For the *QL*, this offset is by default auto-detected. When connecting the adapter for example to the external *Microdrive* port of a stock *QL*, it should be two. Auto-detect however may fail. For *Interface 1*, auto-detect is not possible and defaults to `0`. You can explicitly set a fixed offset in the config section at the top of `oqtadrive.ino`, settings `DRIVE_OFFSET_QL` and `DRIVE_OFFSET_IF1`.

Note further that using *OqtaDrive* with a *QL* that has a *vMap* installed will put the drives managed by *OqtaDrive* in the wrong position. *OqtaDrive* is not compatible with *vMap*.

### I keep getting `drive write protected` when formatting or writing from my *Spectrum*
If your cartridges seem to always be write protected, you may need to install the `WR.PR` boost addition to the circuit (see project README for details, and discussion in issue #23). Note that after adding it, you need to activate the write protect boost in the config of the adapter firmware and flash the adapter to upload the firmware with the new setting. The setting is preferably added to the `config.h` config header file. On a standalone *OqtaDrive* for example, this is located in `/home/pi/oqtadrive/config.h`. Add the following line:

`#define WR_PROTECT_BOOST true`

Alternatively, you can change the setting directly in `oqtadrive.ino`, it's already contained there and set to `false`. However, automated upgrades will overwrite this file!

### I reset my *Spectrum*/*QL*, but the adapter LED keeps flashing
This usually happens when resetting while a drive is active. The reason is that in the case of a reset on the *Spectrum* or *QL* side, the drive is never 'officially' deactivated by the host machine, so *OqtaDrive* keeps spinning the drive. You need to reset the adapter as well, either via web UI or `oqtactl resync --reset`, or quite simply via the *Arduino*'s own reset button. If you've installed the adapter inside the case of a *Spectrum*, *Interface 1*, or *QL*, consider hooking up the reset line of the *Nano* with the machine's reset line.

### I don't get any meaningful results from *repo* search
First, make sure you're using the right search term. The web UI has a help tool tip about this. Repo search may actually be doing what you asked. If that doesn't help, try rebuilding the search index. Also note that it is not recommended to place the repo & search index onto a volume with a *FAT*-type file system. More details [here](repo.md).

### The web UI doesn't work in my browser
Check whether you have any ad blocker, content blocker, or malware scanner extensions installed and active in your browser. If so, try to temporarily deactivate them. If this solves the issue, check whether these add-ons allow defining exemptions, and add the address of your *OqtaDrive* there.

### I want to check a new feature in the web UI, but it doesn't work as expected
Your browser may be holding on to older versions of the web pages and *JavaScript* code. The web UI uses *cache busting* to avoid this problem, but depending on the used browser, this may not always work. Try clearing the browser cache.

### I cannot flash the adapter, getting `not in sync` from `avrdude`
You likely have an *Arduino Nano* with old boot loader. In this case, you need to choose the right board option. How you do this depends on how you flash the firmware. In the *Arduino* IDE, look for board options. There should be a setting for old boot loader. For the *OqtaDrive* installer and the prepared images, there's config option `FQBN`, which you need to set to `arduino:avr:nano:cpu=atmega328old`. As a shorthand, you can also set config item `OLD_NANO=y`. The installer however also has a retry logic for the *Nano*: If flashing fails and the config is not set to old boot loader, it will retry flashing for old boot loader, and the other way around. It's still recommended to set the config right, so flashing succeeds the first time.
