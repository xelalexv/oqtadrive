# Cartridge Repository

You can store your collection of cartridges, `Z80`/`SNA` snapshots, and `TAP` files in a so-called *repo* on the daemon host. This is essentially just a designated folder where you place them. When set up, you can load files from this repo with `repo://{path}` references, where `{path}` is the path to the desired file, relative to the repo folder. A search index for the files in the repo is maintained automatically (see below), so searching through a collection with even thousands of files is quick.

## Setup

**Note:** When installing *OqtaDrive* using an installation image, the repo is set up automatically during the installation. You do not have to follow below steps in that case.

1. Choose or create a root folder for the repo. Anywhere is fine as long as the user under which you run the daemon has access to this location.

    - I **strongly advise against using a *FAT* file system** for the location of the repo! It may negatively impact the functioning of the search index (see below).

2. Copy your files into the folder. Any supported input format is fine. You can freely organize your files in sub-folders.

    - To transfer files from a remote machine, have a look at `scp` and/or `rsync`.

3. When starting the daemon with `oqtactl serve`, point it to the repo folder with the `--repo` or `-r` option, to make it aware of the repo. If you're running the daemon as a `systemd` service, you need to edit the unit file accordingly, and restart the service.

You can now use `oqtactl search` from anywhere on your network to search for files in the repo, and use a result when loading, e.g. `oqtactl load -i repo://a/b/pacman.z80`. Search & load is also supported in the web UI.

## Remotely Accessing the Repo Folder
While the daemon is running with the repo feature active, the repo folder can be accessed via the *OqtaDrive* API endpoint, using any *WebDAV* capable client. The file managers of all major operating systems have that capability. Just point it to your *OqtaDrive* daemon's endpoint -look for an option such as *Connect to server* or similar- and manage the files in the repo from your desktop machine. Here's the link you need to provide:

| OS              | link                                |
|-----------------|-------------------------------------|
| Linux           | `dav://{IP address}:8888/webdav`    |
| MacOS & Windows | `http://{IP address}:8888/webdav`   |

There is also a *WebDAV* client included in *OqtaDrive*'s web UI, which can be opened from the *Repo* tab via the *Manage* link.

## Search Index
For quick search results, in particular incremental search in the web UI, the daemon automatically creates an index of the file names in the repo, and keeps track of any file changes (addition, removal, rename, move). Should you ever experience any problems with search, you can rebuild the index either from the repo tab in the web UI, or with `oqtactl search --reindex`. Alternatively, you can also directly delete the index after stopping the daemon. It is located in the daemon's working directory, named `repo.index`. The daemon will recreate it upon restart.

### Searching in the *ZXDB*
The *Repo* tab in the web UI as well as the `oqtactl search` command also allow you to search the online [ZXDB](https://zxinfo.dk). The search field on the *Repo* tab has a radio button to switch between your own repo and *ZXDB*, and the `search` command has a `--source` option for that. Note that searching in *ZXDB* is not using any of your local data, but is purely done at the server side at *zxinfo.dk*. In the web UI, the search results are presented in the same result list as if searching in your local repo. When selecting an item from the list however, a *Details* link is added to the load dialog. This link takes you to the details page of the selected software title, if you want to get more information, read reviews, or see screenshots. When confirming the load dialog, the title is retrieved from the corresponding download site.

### Trash Folder
If there are files in the repo that you don't want to appear in search results, but also don't want to delete just yet, you can move them to the `trash` folder at the root of the repo. You can do this for example from the search result list, by selecting an item and then clicking the trash can button. You can then decide at a later time what to do with those files, e.g. manage them via *WebDAV*. Anything in or underneath the `trash` folder is excluded from searches. If the folder doesn't exist, *OqtaDrive* will create it upon start.
