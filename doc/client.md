# Controlling *OqtaDrive* from *Spectrum* and *QL*

Part of the *OqtaDrive* control actions that can be run from web UI and command line interface (CLI, i.e. `oqtactl`), can also be run from *Spectrum* and *QL* (client). This is achieved by writing the intended command into a designated file, which is picked up by the daemon and executed. Information about the result of a command and about *OqtaDrive* state in general can also be read by the client from dedicated files, but requires the use of a *control cartridge* (see below).

## The Control File `oqctl`
Whenever a file named `oqctl` is created in any drive, the daemon attempts to interpret the content as a control command and executes it. Afterwards, the file gets deleted by the daemon. This is done because the *Microdrive* file system of the *Spectrum* does not allow modifying an existing file. Writing another command to `oqctl` would therefore require to erase it first.

This is how you can write a command to an `oqctl` file:

#### *Spectrum*
```
OPEN #4;"m";{drive};"oqctl": PRINT #4;"{command to run}": CLOSE #4
```

Note that it is not required to use `#4`, you can pick a different stream, but do not use the preallocated internal streams `#0` through `#3`.

#### *QL*
```
open_new #4,mdv{drive}_oqctl: print #4,'{command to run}': close #4
```

Again, a different stream can be used, but stay clear of internal ones.

This is admittedly quite a bit to type, but has the advantage that it can be done with standard functionality contained in any ROM, and does not require loading additional software into the client.

## Supported Commands
These are the commands that can be written to `oqctl`. They are simplified variants of the corresponding `oqtactl` CLI commands. Note that each command also has a shorthand. For details on conventions for file locations, see the section below the table:

| *command*                    | *description*                                |
|------------------------------|----------------------------------------------|
|`load\|ld {drive} {file}`     | load cartridge file into drive               |
|`save\|sv {drive} [{file}]`   | save cartridge from drive to file; overwrites file if existing; when omitting file, cartridge is saved into the file from which it has been loaded or to which it has been previously saved |
|`rename\|rn {drive} {name}`   | rename cartridge in drive                    |
|`reorg\|ro {drive}`           | reorganize cartridge in drive                |
|`unload\|ul {drive} [force]`  | eject cartridge from drive; when `force` is used, modifications are not saved, i.e. all changes since last save are discarded |
|`map\|mp off\|{{start} {end}}`| set mapping of hardware drives, or turn off  |
|`resync\|rs [if1\|ql]`        | resync daemon and adapter, optionally setting client type |
|`config\|cf {r\|rumble {level}}`| set a config item (currently only rumble level)|
|`wifi {ssid}[:{passphrase}]`  | set WiFi network (for standalone variant)    |
|`poweroff`                    | shut down daemon host (for standalone variant)|

#### About File Locations & Automatic Write-Back
Commands taking a file argument can only access files that are located in the [*repo*](repo.md), so having this feature activated is a prerequisite. Furthermore, these files need to be located underneath the *client path*, which is located underneath the repo root folder, and provides separate sub-folders for *Spectrum* and *QL*:

- *Spectrum*: `client/if1`
- *QL*: `client/ql`

On a standalone variant for example, the full path for *Spectrum* would be `/home/pi/repo/client/if1`. Underneath these locations, additional sub-folders may be created. Here's an example for loading a file located in sub-folders:

```
load 1 foo/bar/test.mdr
```

Whenever a modified cartridge needs to be ejected from a drive, either because another cartridge gets loaded, or it is explicitly ejected, the cartridge is automatically written back to the location from which it was loaded or to which it was previously saved. This is however only done if that location is underneath the client path. Otherwise, the command causing the eject will fail to prevent loss of data.

A special case is formatting a cartridge that was loaded from the client path. After format, the cartridge is no longer associated with that location, and no automatic write back is performed for it. This prevents accidentally overwriting the original source.

**Example**

Let's say from the *Spectrum*, we load `foo.mdr` into drive 1. We then save a file to drive 1, and after that upload `bar.mdr` via the web UI into drive 1. Since the modified cartridge currently present in drive 1 was loaded from `client/if1/foo.mdr`, it is automatically written back into that location, and `bar.mdr` is placed into drive 1.

If we now save a file to drive 1, and from the *Spectrum* try to load `foo.mdr` again, this will fail. The cartridge in drive 1 is modified, but since it was uploaded via the web UI, there's no path to which it can be written. The user needs to explicitly decide where the cartridge should be saved using the `save` command.

#### Note on WiFi Setup
Setting a WiFi network is intended to be used with the standalone variants. It leverages either the `raspi-config` command (*RaspberryPi*), the `nmcli` command (*BananaPi*), or the `uci` command (*Linino One*), depending on which one is available. It *may* work for other systems as well, if they use *NetworkManager*. The `raspi-config` and `nmcli` commands **add** the provided WiFi network to the list of known networks. Information for previously used networks is kept, and the daemon host will connect based on what network is currently available. If several networks are available at the same time, it's up to the daemon host to choose one, for example based on signal strength. *OqtaDrive*'s `wifi` command cannot force the usage of a particular network in this case. The `uci` command on the other hand **replaces** the currently set WiFi network with the new one, so if you want to switch back, you have to set WiFi again.

## Control Cartridges
The idea behind a *control cartridge* is to (temporarily) set aside a drive of your choice for additional functionality and improved usability of client control. Any cartridge with name `oqtactl` is automatically considered a control cartridge by the daemon. This changes the cartridge in the following way:

- On the *Spectrum*, the length is shortened, currently to 32 sectors, to reduce access times. On the *QL*, unused records are dropped for speed up, since shortening the cartridge is more involved there.
- Control cartridges contain these special files, created & maintained by the daemon:
    + `drv` - current drive list
    + `cfg` - current *OqtaDrive* configuration; this includes also the IP address of the daemon host and a QR code containing the URL of the web UI for quick access
    + `res` - result of most recent command
    + `man` - a short man page for quick reference
- When a drive with a control cartridge starts, the daemon will always cue it to a position that reduces access time for loading the special files.

To display a special file on screen, run this command:

#### *Spectrum*
```
MOVE "m";{drive};"{file}" TO #2
```

#### *QL*
```
copy mdv{drive}_{file},con_
```

The control file `oqctl` can of course be written to a control cartridge. Due to its access speed up, it's actually desirable to do so. Writing `oqctl` will be considerably faster compared to a standard cartridge. To free a drive that currently holds a control cartridge, simply eject it or load a different cartridge.
