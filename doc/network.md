# *Sinclair Network*

|  **Note**  |
|-------------------------------------------|
| *Sinclair Network* with *OqtaDrive* is still an &alpha;-feature. While essentials already work, this is still under heavy development. Several assumptions have been made for the current implementation. Their validity now needs to be tested and confirmed. As a result, things may still change considerably. |
| The current implementation has been tested on various *Spectrums* as well as on *QLs* with standard ROM and *Minerva*. For *QL* with *TK2*, there appear to be problems, which however may be attributed to *TK2* running in contended memory. We're currently analyzing this and will adapt accordingly if needed in a later release. |

By adding the optional *network* part (1 transistor + 4 resistors, as shown in the schematic) to the adapter, you can enable your *OqtaDrive* to act as an *Internet* gateway for your *Spectrum* and *QL*. Once fitted, you connect it to one of the network ports of your *Interface 1* or *QL*. You will then be able to communicate over the *Internet* with other *Spectrums* and *QLs* anywhere in the world. By chaining several *Spectrums* or *QLs* together in the usual *Sinclair Network* way, *OqtaDrive* can act as the gateway for all of them (currently limited to eight machines).

All the commands using the *Sinclair Network* can be used transparently over this connection, just as if the other stations were connected directly to your machines. No modifications to your *Spectrum* or *QL* are necessary, and no additional software is required. So you could simply do `SAVE *"n";24` on your *Spectrum* for example, and have a friend do `LOAD *"n";18` to transfer your current *BASIC* program to them (about station numbers see below).

## Network Hub
To communicate with other *Spectrums* and *QLs*, you first need to log in your *OqtaDrive* to a *network hub*, via web UI or command line. The hub is a server that acts as the router for the *Sinclair Network* traffic. By having this central component, you do not need to open up your own network for incoming traffic. You only need to make sure your *OqtaDrive* can reach the hub.

### Operating a Hub
Anyone can operate a network hub. This functionality is provided by the `oqtactl` binary. You need to place this on a server machine, and enable it in the machine's startup system, e.g. *systemd*. See the command help `oqtactl hub --help` for details on available settings. The hub does not require an *HTTP* web server such as *Apache*, only a port on which it can listen & serve.

#### Hub Registration
While a hub can be run without requiring user logins, this is strictly meant for testing purposes only, in a private network, e.g. your home network. For security reasons, I strongly advise to require user registration for publicly reachable hubs. Technically access is controlled via an *htpasswd* file that needs to be place into the working directory in which the hub gets started. This way users can provide the hub operator with their credentials as an *htpasswd* entry during registration. The registration process itself is out of scope for the hub and needs to be organized separately.

## Station Numbers
To simplify the setup of the network, the current implementation distinguishes between local and public station numbers, or *addresses*:

| address | usage                                                  |
|---------|--------------------------------------------------------|
| 0       | broadcast; reaches all local stations and additionally gets forwarded to all stations currently connected to the used network hub |
| 1 - 8   | local stations; traffic between these stations will not leave the local network |
| 9 - 255 | public stations; members in the network use addresses from this range to communicate with each other; some of these addresses may be reserved by the hub |

### Network Address Translation (*NAT*)
When connecting to the hub, you select the number of addresses you require, depending on how many machines you have chained to the network port of your *OqtaDrive*. The hub then assigns a set of addresses to you, which your *OqtaDrive* uses for *network address translation*. This is something that's common in *IP* networks, e.g. your home network, and essentially works the same here. You therefore do not need to use the public addresses directly on your machines. Instead, you configure them to use local addresses 1 through 8 consecutively. *OqtaDrive* then maps the public addresses received from the hub, in ascending order, to stations 1 through 8.

**Example**: You have three machines chained to your *OqtaDrive*, so you need to configure them to use local addresses 1, 2, and 3. Which machine gets which local address is not important, as long as all addresses from 1 through the number of machines, i.e. 3 in this example, are in use. Upon login, the hub assigns for example addresses 18, 23, and 24 to you. *OqtaDrive* then maps these addresses in this order to local addresses 1, 2, and 3.

When communicating with a remote station, you use its public address. You can check on all clients logged in to the hub and their addresses in the web UI and from CLI. In the traffic that leaves your local network (a.k.a *egress* traffic), *OqtaDrive* then replaces the local sender address with your corresponding public address, so that the remote station can receive it. Likewise, in all incoming traffic (a.k.a *ingress* traffic), the destination address, i.e. your public address, is replaced with the corresponding local address, so that your machine can receive it.

The advantage of *NATing* is that when configuring your local machines, you always just go through the range of local addresses starting with 1, and you never have to check which public addresses actually got assigned to you. Most of the time, however you will only have one machine. In that case, you don't even have to configure it at all, since station number 1 is the default after power on/reset.

### Special Addresses
The following addresses are currently used by the hub to provided additional services. This list is very likely to change as work on the network feature progresses.

| address | service                                                         |
|---------|-----------------------------------------------------------------|
| 63      | chat service: provides a group chat, used by the `chat` program |
| 64      | echo service: bounces received packets back to the sender with a delay of 2 seconds; used by the `echo` program for testing network connectivity        |

## Sample Programs
There are currently three *BASIC* programs available for the *Spectrum* to demonstrate the network feature:

- `echo` - When starting this program, it keeps sending a test packet to the hub's echo service, receives the bounced back packet and compares it with what was sent. This is useful for testing your network connection.
- `chat` - This is a group chat program, simple but fully functional. It lets all users logged in to the hub chat together, using the hub's chat service.
- `net game` - This is the number guessing game from the original *Sinclair Microdrive* demo cartridge for the *Spectrum*. It was slightly adapted to account for the way in which station numbers are used.

These programs are made available as a cartridge image. TODO where?

## Design Decisions & Limitations

### *Microdrive* Priority
The emulated *Microdrives* are always handled with highest priority. When connecting only one machine to the *OqatDrive* network port, this is of little concern since *Microdrive* and network would never be used concurrently from this single machine. However, when more machines are chained together, those machines cannot use the network as long as the first machine is operating *Microdrives*.

### Header & Block Acknowledgements
The *Sinclair Network* specifies that a receiving station has to acknowledge the header and block of each packet it has received. If the sending station does not receive an acknowledgement from the receiver, it will resend the header or block until it succeeds. The deadline for acknowledgements is approx. 3ms. This requirement is difficult to match in our setup. Even though the communication between *OqtaDrives* and the hub uses highly efficient *WebSocket* connections, common round trip times are still in the range of 10 to 20ms. Latency can vary a lot depending on the used network connection, quality of *WiFi*, distance to hub, and many other factors.

In the current implementation, outgoing packets are therefore directly acknowledged by the *OqtaDrive* on the sending side, on behalf of the receiving station. The *OqtaDrive* on the receiving end then accordingly takes care of repeating headers and blocks as needed, should the receiving station not be ready yet. To avoid deadlocks, repetition is limited to 30 seconds. If a packet is not acknowledged by then, it gets discarded along with the remaining packets belonging to the same transmission. While this approach to acknowledgement is an important difference in the timing behavior of the network, it does not seem to be relevant in practical use. However, there may be existing applications for which this could pose a problem.

### Mixing Original *Spectrums*, *Harlequins*, And *QLs* In Same Local Network
The *Harlequin* line of *Spectrum* clones as well as the *QL* exhibit slightly different timing behavior. The *OqtaDrive* adapter automatically adjusts its configuration to handle this. However, if machines with different timing are chained to an *OqtaDrive*, this adjustment is done repeatedly, depending on which machine is using the network. This works, but may somewhat slow down network traffic.
