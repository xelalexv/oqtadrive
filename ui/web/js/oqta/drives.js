/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2021, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

//
function buildList(drives) {

    var driveList = document.getElementById('driveList');

    var fc = document.createElement('input');
    fc.className = 'custom-file-input';
    fc.id = 'fc';
    fc.type = 'file';
    fc.accept = '.mdr,.MDR,.mdv,.MDV,.mdi,.MDI,.z80,.Z80,.sna,.SNA,.tap,.TAP,.zip,.ZIP,.gz,.GZ,.gzip,.GZIP,.7z,.7Z';
    fc.style = 'display:none;';
    fc.onclick = function() {
        this.value = null;
    };
    fc.onchange = function() {
        var name = this.files[0].name;
        var drive = this.dataset.drive;
        var cmp = getFormatCompressor(name);
        upload(drive, getName(name), cmp.format, cmp.compressor, this.files[0], false);
    };
    driveList.appendChild(fc);

    for (var i = 1; i <= drives.length; i++) {

        var row = document.createElement('div');
        row.className = 'row';
        driveList.appendChild(row);

        var div = document.createElement('div');                 // drive button
        div.className = 'col-3 btn-drive-container mt-1 mb-1';

        var bt = document.createElement('img');
        bt.className = 'btn btn-default btn-drive shadow-none mt-1 mb-0';
        bt.id = 'bt' + i;
        bt.src = 'drive.svg';
        bt.alt = i;
        bt.width = 72;
        bt.onclick = function() {
            var drive = this.id.substring(2);
            if (!loadSelectedSearchItem(drive)) {
                var fc = document.getElementById('fc');
                fc.dataset.drive = drive;
                fc.click();
            }
        };
        configureButton(bt, drives[i-1]);
        div.appendChild(bt);
        var divInner = document.createElement('div');
        divInner.className = 'btn-drive-label';
        divInner.innerHTML = i;
        div.appendChild(divInner);

        row.appendChild(div);

        div = document.createElement('div');                   // cartridge name
        div.id = 'name' + i;
        div.className = 'col-6 mt-1';
        div.style = "font-size: 0.75rem; cursor: pointer;";
        div.align = 'left';

        var divH = document.createElement('div'); // header
        divH.id = div.id + 'hd';
        divH.style = "font-size: 1.0rem;";
        div.appendChild(divH);

        var sub = document.createElement('p'); // sub-line
        sub.id = div.id + 'sub';
        sub.className = "text-white-50";
        div.appendChild(sub);

        setNameAndSub(div, drives[i-1]);
        div.onclick = function() {
            showFiles(this.id.substring(4),
                this.dataset.name, this.dataset.format, this.dataset.status);
        };
        row.appendChild(div);

        div = document.createElement('div');                     // drive status
        div.className = 'col-3 mt-3';
        var it = document.createElement('i');
        it.id = 'it' + i;
        setStatusIcon(it, drives[i-1]);
        div.appendChild(it);
        row.appendChild(div);
    }
}

//
function updateDrives(data) {
    updateClient(data.client);
    updateList(data.drives);
}

//
function updateClient(cl) {

    if (cl == "") {
        return;
    }

    lAuto = document.getElementById('lAuto');
    lIF1 = document.getElementById('lIF1');
    lQL = document.getElementById('lQL');

    var s = "connected";
    switch (cl) {
        case '<unknown>':
            s = 'disconnected';
            lIF1.classList.remove("text-success");
            lQL.classList.remove("text-success");
            break;
        case 'Interface 1':
            lIF1.classList.add("text-success");
            lQL.classList.remove("text-success");
            break;
        case 'QL':
            lIF1.classList.remove("text-success");
            lQL.classList.add("text-success");
            break;
    }

    document.getElementById('clientIcon').className = getStatusIcon(s);
}

//
function updateList(drives) {

    if (drives == null) {
        return;
    }

    for (var i = 1; i <= drives.length; i++) {
        var d = drives[i-1];
        setStatusIcon(document.getElementById('it' + i), d);
        setNameAndSub(document.getElementById('name' + i), d);
        configureButton(document.getElementById('bt' + i), d);
    }
}

//
function indicateLoading(drive) {

    document.getElementById('bt' + drive).disabled = true;
    document.getElementById('it' + drive).className = getStatusIcon('loading');

    var div = document.getElementById('name' + drive);
    var divH = div.querySelector('#' + div.id + 'hd');
    if (divH != null) {
        divH.innerHTML = '&lt; loading &gt;';
    }
    var sub = div.querySelector('#' + div.id + 'sub');
    if (sub != null) {
        sub.innerHTML = 'just a moment...';
    }
}

//
function upload(drive, name, format, compressor, data, isRef) {

    if (disconnected()) {
        return
    }

    indicateLoading(drive);

    var path = `/drive/${drive}?type=${format}&compressor=${compressor}&repair=true&name=`
        + encodeURIComponent(name);

    if (isRef) {
        path += "&ref=true"
    }

    var status

    fetch(path, {
        method: 'PUT',
        body: data
    }).then(
        response => {
            status = response.status;
            return response.text();
    }).then(
        data => {
            if (status != 200) {
                userAlert("Upload Failed", data);
            }
    }).catch(
        err => console.log('error: ' + err)
    );
};

//
function resetClient() {

    if (disconnected()) {
        return;
    }

    var client = '';
    if (document.getElementById('rbIF1').checked) {
        client = '&client=if1'
    } else if (document.getElementById('rbQL').checked) {
        client = '&client=ql'
    };

    fetch(`/resync?reset=true${client}`, {method: 'PUT'}).then(
        function(){}
    ).then(
        success => console.log(success)
    ).catch(
        err => console.log('error: ' + err)
    );
}

//
function configureButton(bt, data) {
    bt.disabled = (data.status == 'busy' || data.status == 'hardware');
}

//
function setNameAndSub(div, data) {

    var h = '';
    var s = '';

    if (data.name != '' || data.status != 'busy') {
        if (data.formatted) {
            h = data.name;
            if (data.control) {
                var icon = getStatusIcon('control');
                h += `&nbsp; <i class="${icon}"></i>`;
            }
        } else if (data.status == 'hardware') {
            h = '-';
            s = '&lt; h/w drive &gt;';
        } else {
            h = '-';
            s = '&lt; unformatted &gt;';
        }
    }

    div.dataset.name = data.name;
    div.dataset.status = data.status;

    var divH = div.querySelector('#' + div.id + 'hd');
    if (divH != null) {
        if (h != '') {
            divH.innerHTML = h;
        } else if (divH.innerHTML == '') {
            divH.innerHTML = '-';
        }
    }

    var sub = div.querySelector('#' + div.id + 'sub');
    if (sub != null) {
        var cl = data.client;
        if (cl == 'Interface 1') {
            cl = 'Spectrum';
        }
        if (cl != '') {
            var spcl = data.specials;
            if (spcl != '') {
                spcl = `, ${spcl}`;
            }
            var stats = '';
            if (data.fsSupport) {
                var free = Math.floor((data.sectors-data.used)/2);
                stats = `, ${data.files} files, ${free}kb`;
            }
            s = `${cl}${spcl}${stats}`;
        }
    }

    if (s != '') {
        sub.innerHTML = s;
    } else if (sub.innerHTML == '') {
        sub.innerHTML = '-';
    }

    div.dataset.format = data.format
}

//
function setStatusIcon(it, data) {

    var s = data.status;

    if (data.status == 'idle') {
        if (data.modified) {
            s = 'modified';
        } else if (data.writeProtected) {
            s = 'writeProtected';
        } else if (!data.formatted) {
            s = 'unformatted';
        }
    }

    it.className = getStatusIcon(s);
}

//
function poweroff() {
    userConfirm("Power Off",
        "This will power off the OqtaDrive host system. Proceed?", "",
        function(confirmed) {
            if (confirmed && !disconnected()) {
                doPoweroff();
            }
        });
}

//
function doPoweroff() {
    fetch("/shutdown?command=poweroff", {
        method: 'PUT'
    }).then(
        response => response.text()
    ).then(
        data => {
            userAlert("Power Off", data);
    }).catch(
        err => console.log('error: ' + err)
    );
}
