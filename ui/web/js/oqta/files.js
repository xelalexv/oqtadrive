/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2021, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

//
function showFiles(drive, name, format, status) {

    fetch(`/drive/${drive}/list?annotate=true`, {
        headers: {
            'Content-Type': 'text/plain'
        }
    }).then(
        response => response.text()
    ).then(
        data => updateFileList(drive, name, format, status, data)
    ).catch(
        err => console.log('error: ' + err)
    );

    showTab('files');
}

//
function updateFileList(drive, name, format, status, data) {

    var pre = document.getElementById('fileList');
    pre.innerHTML = `drive ${drive}: ` + data.trim();

    var off = data.startsWith('no cartridge in drive') || status == 'hardware';

    var bt = document.getElementById('btSave');
    bt.name = drive;
    bt.disabled = off;

    var a = document.getElementById('aSave');
    a.href = `/drive/${drive}`;
    var n = name.replace(/[^a-z0-9]/gi, '_');
    a.download = `${n}.${format}`;

    bt = document.getElementById('btReorg');
    bt.name = drive;
    bt.disabled = off;
    bt.dataset.format = format;
    bt.dataset.status = status;
    bt.dataset.name = name;

    bt = document.getElementById('btRename');
    bt.name = drive;
    bt.disabled = off;
    bt.dataset.format = format;
    bt.dataset.name = name;

    bt = document.getElementById('btFormat');
    bt.name = drive;
    bt.disabled = status == 'hardware';
    bt.dataset.format = format;
    bt.dataset.name = name;

    bt = document.getElementById('btUnload');
    bt.name = drive;
    bt.disabled = off;
}

//
function resetFileList(drive, name, format, data) {

    var div = document.getElementById('fileList');
    div.innerHTML = '';
    var bt = document.getElementById('btSave');
    bt.disabled = true;
    bt = document.getElementById('btRename');
    bt.disabled = false;
    bt = document.getElementById('btFormat');
    bt.disabled = false;
    bt = document.getElementById('btUnload');
    bt.disabled = true;
}

//
function operateDrive(drive, cartridge, action) {

    var path = `/drive/${drive}/${action}?force=true`;
    var note = 'Unsaved changes will be lost!';

    switch (action) {

        case 'save':
            if (!disconnected()) {
                document.getElementById('aSave').click();
            }
            break;

        case 'reorg':
            userConfirm("Reorganize cartridge?",
                "The files on the cartridge will be rewritten to produce an optimized layout.", "",
                function(confirmed) {
                    if (confirmed) {
                        operateDriveDo(drive, action, 'PUT', path);
                    }
                });
            break;

        case 'rename':
            userInput("Rename cartridge?", "", "cartridge name", cartridge,
                function(confirmed, name) {
                    if (confirmed) {
                        operateDriveDo(
                            drive, action, 'PUT', path + `&name=${name}`);
                    }
                });
            break;

        case 'format':
            userInput("Format cartridge?", note, "cartridge name", cartridge,
                function(confirmed, name) {
                    if (confirmed) {
                        operateDriveDo(
                            drive, action, 'PUT', path + `&name=${name}`);
                    }
                });
            break;

        case 'unload':
            userConfirm("Unload cartridge?", note, "",
                function(confirmed) {
                    if (confirmed) {
                        operateDriveDo(drive, action, 'GET', path);
                    }
                });
            break;
    }
}

//
function operateDriveDo(drive, action, method, path) {

    if (disconnected()) {
        return;
    }

    fetch(path, {
        method: method,
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(
        response => {
            status = response.status;
            return response.text();
    }).then(
        data => {
            if (status != 200) {
                var a = action.charAt(0).toUpperCase() + action.substr(1);
                userAlert(`${a} Failed`, data);
                return;
            }
            switch (action) {
                case 'unload':
                case 'rename':
                case 'format':
                    showTab('drives');
                    resetFileList();
                    break;
                case 'reorg':
                    userAlert('Reorganized cartridge', data);
                    bt = document.getElementById('btReorg');
                    showFiles(bt.name,
                        bt.dataset.format, bt.dataset.format, bt.dataset.status)
                    break;
            }
    }).catch(
        err => console.log('error: ' + err)
    );
}
