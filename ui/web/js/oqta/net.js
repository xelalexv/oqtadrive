/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2024, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

//
function setupNet() {

    getNetConfig();

    var drop = document.getElementById('address-count');
    for (var i = 1; i <= 8; i++) {
        opt = document.createElement('option');
        opt.selected = i == 1;
        opt.appendChild(document.createTextNode(i));
        drop.appendChild(opt);
    }
}

//
function resetNet() {
    updateNet({hub: ''});
}

//
function updateNet(data) {

    if (data == null) {
        return;
    }

    var tfHub = document.getElementById('hub-url');
    var state = false;

    if (data.hub != '') {
        state = true;
        if (data.hub != tfHub.value) {
            tfHub.value = data.hub;
        }
    } else {
        console.log('resetting hub connection state');
    }

    var drop = document.getElementById('address-count');
    if (data.addresses != null && data.addresses.length > 0) {
        drop.value = data.addresses.length;
    }
    drop.disabled = state;

    document.getElementById('cbHubConnect').checked = state;
    document.getElementById('userList').innerText = state ?
        data.description.trim() : "not connected";
}

//
function hubUrl(src) {

    var url = 'wss://hub.oqtadrive.org:8443/port/';

    switch (src) {
    case 'test':
        break;
    default:
        url = '';
        break;
    }

    document.getElementById('hub-url').value = url;
}

//
function hubConnect() {

    if (disconnected()) {
        resetNet();
        return;
    }

    var cb = document.getElementById('cbHubConnect');
    var addresses = document.getElementById('address-count').value;
    var data = '';

    if (cb.checked) {
        var hub = document.getElementById('hub-url').value;
        if (hub == '') {
            userAlert("No Hub URL", "Please select or enter a hub URL.");
            resetNet();
            return;
        }

        var proto = 'wss://';
        var ix = hub.indexOf('://');
        if (ix > 0) {
            ix += 3;
            proto = hub.substring(0, ix);
            hub = hub.substring(ix);
        }

        var user = document.getElementById('hub-user').value;
        var pass = document.getElementById('hub-pass').value;
        data = proto;
        if (user != '') {
            data += user;
            if (pass != '') {
                data += ':' + pass;
            }
            data += '@';
        }
        data += hub;
    }

    fetch(`/network/config?hub=true&addresses=${addresses}`, {
        method: 'POST',
        body: data
    }).then(
        response => response.text()
    ).then(
        data => {
            userAlert("Network Hub", data);
            getNetConfig();
    }).catch(
        err => console.log('error: ' + err)
    );
}

//
function getNetConfig() {
    fetch('/network/config', {
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(
        response => response.json()
    ).then(
        data => updateNet(data)
    ).catch(
        err => console.log('error: ' + err)
    );
}
