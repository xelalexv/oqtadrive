/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2021, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

//
const MaxSearchItems = 500;
const PageSize = 10;

//
var keyupStack = [];

//
function setupSearch() {

    var keyword = document.getElementById('repo-search');
    keyword.addEventListener('keyup', function(evt) {
        keyupStack.push(1);
        setTimeout(function() {
            keyupStack.pop();
            if (keyupStack.length === 0 &&
                (evt.keyCode == 13 || getSearchSource() == "repo")) {
                search(this.value.toString(), MaxSearchItems, PageSize);
            }
        }.bind(this), 600);
    });

    var button = document.getElementById('btSearch');
    button.onclick = function() {
        if (isURL(keyword.value)) {
            searchItemSelected(keyword.value, '');
        } else {
            search(keyword.value.toString(), MaxSearchItems, PageSize);
        }
    };

    var cb = document.getElementById('cbSearchTap');
    cb.checked = false;

    button = document.getElementById('rbSrcRepo');
    button.checked = true;
}

//
function withTAP() {
    var cb = document.getElementById('cbSearchTap');
    return cb != null && cb.checked;
}

//
function getSearchSource() {
    return document.querySelector('input[name="search-source"]:checked').value;
}

//
function search(term, items, page) {

    if (term.length < 2 || isURL(term) || disconnected()) {
        return;
    }

    var src = getSearchSource();
    var excl = withTAP() ? "" : "tap";

    var modInst;
    if (src == 'zxdb') {
        modInst = userAlert("Searching ZXDB",
            "This can take a moment, please wait...", true);
    }

    fetch(`/search?items=${items}&page=${page}&source=${src}&exclude=${excl}&term=`
        + encodeURIComponent(term), {
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(
        response => {
            status = response.status;
            if (status == 200) {
                return response.json();
            }
            return response.text();
    }).then(
        data => {
            if (modInst != null) {
                modInst.hide();
            }
            if (status == 200) {
                updateSearchResults(data);
            } else {
                userAlert("Search failed", data);
            }
        }
    ).catch(
        err => console.log('error: ' + err)
    );
}

//
function getSearchPage(id, start, page) {

    fetch(`/search?id=${id}&start=${start}&page=${page}`, {
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(
        response => response.json()
    ).then(
        data => updateSearchResults(data)
    ).catch(
        err => console.log('error: ' + err)
    );
}

//
function isURL(term) {
    return term.startsWith('http://') || term.startsWith('https://')
}

//
function updateIndexStatus(state) {
    if (state == null || state.reindex == null) {
        return;
    }
    document.getElementById('indexLabel').innerHTML = state.reindex;
}

//
function updateSearchResults(data) {

    var list = document.getElementById('search-results');
    list.textContent = null;

    var li = document.createElement('li');
    li.className = "list-group-item text-white bg-dark";

    var hits = "";
    if (data.total > data.available) {
        hits = data.total + " total hits, showing first " + data.available;
    } else {
        hits = data.total + " hits";
    }

    updatePagination(data);

    li.appendChild(document.createTextNode(hits));
    list.appendChild(li);

    data.hits.forEach(function(l) {

        if (l == null) {
            return;
        }

        li = document.createElement('li');
        li.className = "list-group-item text-white bg-transparent d-flex justify-content-start align-items-start";
        li.style = "border: white;";
        li.dataset.searchItem = l.ref;
        li.dataset.itemDetails = l.details;
        li.onclick = function() {
            searchItemSelected(this.dataset.searchItem, this.dataset.itemDetails);
        }

        var divH = document.createElement('div');
        divH.className = "text-white";
        divH.style = "font-size: 1.0rem;";
        divH.appendChild(document.createTextNode(l.label));

        var div = document.createElement('div');
        div.className = "ms-2 me-auto w-100 text-white-50";
        div.style = "font-size: 0.75rem; cursor: pointer;";

        var span = document.createElement('span');
        span.className = "badge bg-secondary rounded-pill align-middle mt-2";
        span.innerText = l.format;

        div.appendChild(divH);
        div.appendChild(document.createTextNode(l.sub));

        li.appendChild(div);
        li.appendChild(span);

        list.appendChild(li);
    });
}

//
function updatePagination(data) {

    var paging = data.available > PageSize
    var nav = document.getElementById('search-pages');

    if (paging) {
        updatePageStepper('aSearchFirst', 'first', data);
        updatePageStepper('aSearchRewind', 'rewind', data);
        updatePageStepper('aSearchPrev', 'prev', data);
        updatePageStepper('aSearchNext', 'next', data);
        updatePageStepper('aSearchForward', 'forward', data);
        updatePageStepper('aSearchLast', 'last', data);
        updatePageNumber(data);
        nav.className = null;
    } else {
        nav.className = "d-none";
    }
}

//
function updatePageStepper(linkId, step, data) {

    var a = document.getElementById(linkId);
    var jump = 5;
    var s = 0;

    switch (step) {
    case 'first':
        break;
    case 'rewind':
        s = data.start - (jump * PageSize);
        if (s < 0) {
            s = 0;
        }
        break;
    case 'prev':
        s = data.start - PageSize;
        break;
    case 'next':
        s = data.start + PageSize;
        break;
    case 'forward':
        s = data.start + (jump * PageSize);
        if (s > data.available) {
            s = Math.floor(data.available / PageSize) * PageSize;
        }
        break;
    case 'last':
        s = (Math.ceil(data.available / PageSize) - 1) * PageSize;
        break;
    }

    if (s < 0 || s > data.available) {
        a.dataset.searchId = 0;
        a.dataset.pageStart = 0;
        a.onclick = null;
    } else {
        a.dataset.searchId = data.id;
        a.dataset.pageStart = s;
        a.onclick = actionSearchPage;
    }
}

//
function updatePageNumber(data) {

    var pages = Math.floor(data.available / PageSize) + 1;
    var p = Math.floor(data.start / PageSize) + 1;
    var a = document.getElementById('aPageNumber');

    if (0 < p && p <= pages) {
        a.innerHTML = p;
    } else {
        a.innerHTML = ".";
    }
}

//
function actionSearchPage() {
    if (this.dataset.searchId > 0) {
        getSearchPage(
            this.dataset.searchId, this.dataset.pageStart, PageSize);
    }
}

//
function searchItemSelected(item, details) {

    var action = '';
    var more = '';
    if (item.startsWith('repo://')) {
        action = 'trash';
        more = ' To exclude the cartridge from future searches, click the trash can icon. ' +
            'This will move it into the repo trash folder.'
    }

    userConfirmAction("Load cartridge?",
        "Confirm & click the drive symbol of the drive into which you want to load." + more,
        details, action, function(confirmed) {
            selectedSearchItem = item;
            if (confirmed === true) {
                showTab('drives');
            } else if (confirmed === action) {
                moveToTrash(item);
            } else {
                selectedSearchItem = "";
            }
        });
}

//
function moveToTrash(item) {

    userConfirm("Remove cartridge?",
        "Move this cartridge to the repo trash folder?", '',
        function(move) {
            if (move && !disconnected()) {
                fetch('/repo/trash', {
                    method: 'POST',
                    body: item
                }).then(
                    response => response.text()
                ).then(
                    data => {
                        userAlert("Removed", data);
                }).catch(
                    err => console.log('error: ' + err)
                );
            }
        });
}

//
function loadSelectedSearchItem(drive) {

    if (selectedSearchItem == "") {
        return false;
    }

    var fc = getFormatCompressor(selectedSearchItem);
    upload(drive, getName(selectedSearchItem), fc.format, fc.compressor,
        selectedSearchItem, true);
    selectedSearchItem = "";

    return true;
}

//
function reindex() {
    userConfirm("Rebuild search index",
        `This will delete the current search index and rebuild a new one. Proceed?`, '',
        function(confirmed) {
            if (confirmed && !disconnected()) {
                fetch('/reindex', {
                    method: 'POST'
                }).then(
                    response => response.text()
                ).then(
                    data => {
                        userAlert("Rebuild search index", data);
                }).catch(
                    err => console.log('error: ' + err)
                );
            }
        });
}
