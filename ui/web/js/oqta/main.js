/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2021, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

//
var statusIcons = {
    'empty':          'bi-x-lg',
    'idle':           'bi-app',
    'busy':           'bi-caret-right-square',
    'hardware':       'bi-gear',
    'unformatted':    'bi-hr',
    'writeProtected': 'bi-lock',
    'modified':       'bi-app-indicator',
    'connected':      'bi-plug-fill',
    'disconnected':   'bi-plug',
    'loading':        'bi-hourglass-split',
    'locked':         'bi-lock',
    'unlocked':       'bi-unlock',
    'control':        'bi-tools',
    'trash':          'bi-trash3',
};

//
function getStatusIcon(s) {
    return statusIcons[s];
}

//
async function subscribe() {

    while (true) {

        var path = '/watch';
        if (!connected) {
            path = "/status";
        }

        try {
            let response = await fetch(path, {
                headers: {
                    'Content-Type': 'application/json'
                }
            });

            console.log("watch ended with code ", response.status);
            failCount = 0;

            switch (response.status) {
                case 408:  // request timeout (Chrome)
                case 504:  // gateway timeout (Firefox)
                    break; // long poll timed out --> ok
                case 200:
                    if (!connected) {
                        console.log("not connected");
                        window.location.reload();
                        return;
                    }
                    let data = await response.json();
                    if (data != null) {
                        updateDrives(data);
                        updateNet(data.network);
                        updateConfig(data.state);
                        updateIndexStatus(data.state);
                    }
                    break;
                default:
                    console.log("disconnecting due to status code");
                    if (connected) {
                        resetNet();
                    }
                    connected = false;
                    break;
            }

        } catch (err) {
            console.log("error fetching: ", err);
            failCount++;
            if (failCount > 3) {
                console.log("disconnecting due to repeated fetch errors");
                if (connected) {
                    resetNet();
                }
                connected = false;
            }
        }

        if (!connected) {
            await new Promise(resolve => setTimeout(resolve, 5000));
        }
    }
}

//
function disconnected() {
    if (!connected) {
        userAlert("Not connected", "The OqtaDrive daemon seems to be down.");
        return true;
    }
    return false;
}

//
function getFormatCompressor(file) {
    var compressor = getCompressor(file);
    if (compressor != '') {
        file = removeExtension(file);
    }
    var format = getFormat(file);
    return {
        "format": format,
        "compressor": compressor
    };
}

//
function getFormat(file) {
    var ext = getExtension(file);
    switch (ext) {
        case 'mdv':
        case 'mdvt':
        case 'mdi':
        case 'mdr':
        case 'z80':
        case 'sna':
        case 'tap':
            return ext;
    }
    return '';
}

//
function getCompressor(file) {
    var ext = getExtension(file);
    switch (ext) {
        case 'gz':
        case 'gzip':
        case 'zip':
        case '7z':
            return ext;
    }
    return '';
}

//
function getExtension(file) {
    var lastDot = file.lastIndexOf('.');
    return file.substring(lastDot + 1).toLowerCase();
}

//
function removeExtension(file) {
    var lastDot = file.lastIndexOf('.');
    return file.substring(0, lastDot);
}

//
function getName(path) {
    var lastSlash = path.lastIndexOf('/');
    var name = path.substring(lastSlash + 1);
    var firstDot = name.indexOf('.');
    return name.substring(0, firstDot);
}

//
function userConfirm(title, question, details, callback) {
    userConfirmAction(title, question, details, '', callback)
}

//
function userConfirmAction(title, question, details, action, callback) {

    var mod = document.getElementById('modal-confirm');
    mod.querySelector('.modal-title').textContent = title;
    var body = mod.querySelector('.modal-body');
    body.textContent = question;
    body.align = "left";

    var a = document.getElementById('aConfirmDetails');
    if (details) {
        a.innerHTML = 'Details';
        a.href = details;
    } else {
        a.innerHTML = '';
        a.href = '';
    }

    var b = document.getElementById('bConfirmAction');
    if (action) {
        document.getElementById('confirmAction').className = getStatusIcon(action);
        b.classList.remove('invisible');
        b.classList.add('visible');
        b.onclick = function() {
            modInst.hide();
            callback(action);
        }
    } else {
        b.classList.remove('visible');
        b.classList.add('invisible');
        b.onclick = function(){};
    }

    var modInst = new bootstrap.Modal(mod, null);

    mod.querySelector('.btn-primary').onclick = function() {
        modInst.hide();
        callback(true);
    };
    mod.querySelector('.btn-secondary').onclick = function() {
        modInst.hide();
        callback(false);
    };

    modInst.show();
}

//
function userInput(title, question, placeholder, value, callback) {

    var mod = document.getElementById('modal-input');
    mod.querySelector('.modal-title').textContent = title;
    var body = mod.querySelector('.modal-body');
    body.align = "left";
    var text = body.querySelector('#modalInputText');
    text.textContent = question;

    var modInst = new bootstrap.Modal(mod, null);
    var input = body.querySelector('#modalInput');
    input.placeholder = placeholder;
    input.value = value;
    input.onkeypress = function(event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            modInst.hide();
            callback(true, input.value.toString());
        }
    }

    mod.querySelector('.btn-primary').onclick = function() {
        modInst.hide();
        callback(true, input.value.toString());
    };
    mod.querySelector('.btn-secondary').onclick = function() {
        modInst.hide();
        callback(false, '');
    };

    mod.addEventListener('shown.bs.modal', function (e) {
        input.focus();
    });
    modInst.show();
}

//
function userAlert(title, message, timeCritical) {

    var mod = document.getElementById('modal-alert');
    mod.querySelector('.modal-title').textContent = title;

    if (timeCritical) {
        // workaround for https://github.com/twbs/bootstrap/issues/25008
        mod.classList.remove('fade');
    } else {
        mod.classList.add('fade');
    }

    var body = mod.querySelector('.modal-body');
    body.textContent = message;
    body.align = "left";

    var modInst = new bootstrap.Modal(mod, null);

    mod.querySelector('.btn-primary').onclick = function() {
        modInst.hide();
    };

    modInst.show();
    return modInst;
}

//
async function showTab(t) {
    var triggerEl = document.querySelector(`a[data-bs-target="#${t}"]`);
    bootstrap.Tab.getOrCreateInstance(triggerEl).show();
}

// ----------------------------------------------------------------------------

//
var popoverTriggerList = [].slice.call(
    document.querySelectorAll('[data-bs-toggle="popover"]'))
var popoverList = popoverTriggerList.map(function (popoverTriggerEl) {
    return new bootstrap.Popover(popoverTriggerEl)
})

//
var selectedSearchItem = "";
var connected = true;
var failCount = 0;

// FIXME make more compact

//
fetch('/list', {
    headers: {
        'Content-Type': 'application/json'
    }
}).then(
    response => response.json()
).then(
    data => buildList(data)
).catch(
    err => console.log('error: ' + err)
);

//
fetch('/status', {
    headers: {
        'Content-Type': 'application/json'
    }
}).then(
    response => response.json()
).then(
    data => updateClient(data.client)
).catch(
    err => console.log('error: ' + err)
);

//
document.getElementById('btClient').onclick = function() {
    resetClient();
};

var bt = document.getElementById('btSave');
bt.onclick = function() {
    operateDrive(this.name, this.dataset.name, 'save');
};
bt.disabled = true;

bt = document.getElementById('btReorg');
bt.onclick = function() {
    operateDrive(this.name, this.dataset.name, 'reorg');
};
bt.disabled = true;

bt = document.getElementById('btRename');
bt.onclick = function() {
    operateDrive(this.name, this.dataset.name, 'rename');
};
bt.disabled = true;

bt = document.getElementById('btFormat');
bt.onclick = function() {
    operateDrive(this.name, this.dataset.name, 'format');
};
bt.disabled = true;

bt = document.getElementById('btUnload');
bt.onclick = function() {
    operateDrive(this.name, this.dataset.name, 'unload');
};
bt.disabled = true;

//
getVersion();
getDriveMapping();
getRumbleLevel();
setupSearch();
setupNet();
setupConfig();
subscribe();
