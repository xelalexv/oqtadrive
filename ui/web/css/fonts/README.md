# Attributions

## Sinclair Logo Font
The FontStruction “Sinclair logo” (https://fontstruct.com/fontstructions/show/245226) by Michał Nowak is licensed under a Creative Commons Attribution Share Alike license (http://creativecommons.org/licenses/by-sa/3.0/).
