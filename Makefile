#
#   OqtaDrive - Sinclair Microdrive emulator
#   Copyright (c) 2021, Alexander Vollschwitz
#
#   This file is part of OqtaDrive.
#
#   OqtaDrive is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   OqtaDrive is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
#

.DEFAULT_GOAL := help
SHELL = /bin/bash

REPO = oqtadrive
OQTADRIVE_RELEASE = 0.7.1
OQTADRIVE_VERSION := $(shell git describe --always --tag --dirty)

ROOT = $(shell pwd)
SKETCH_DIR := $(ROOT)/arduino
SKETCH := $(SKETCH_DIR)/oqtadrive.ino
BUILD_OUTPUT = _build
BINARIES = $(BUILD_OUTPUT)/bin
ISOLATED_PKG = $(BUILD_OUTPUT)/pkg
ISOLATED_CACHE = $(BUILD_OUTPUT)/cache
UI_BASE = $(ROOT)/ui/web

ARDUINO_CLI_IMAGE = xelalex/oqtadrive-arduino-cli
GO_IMAGE = golang:1.22.4-bookworm@sha256:96788441ff71144c93fc67577f2ea99fd4474f8e45c084e9445fe3454387de5b
JSMINIFY_IMAGE = tdewolff/minify:v2.20.34@sha256:e0258f91b41d07609f0746e8e8117868a6da63de51afcc23f5e9eedfec72acb3

# unattended setup image build
IMG_MOUNT = $(ROOT)/$(BUILD_OUTPUT)/mnt
DEFAULT_USER = pi
DEFAULT_PASSWORD = oqtadrive

## env
# You can set the following environment variables when calling make:
#
#	${ITL}VERBOSE=y${NRM}	get detailed output
#
#	${ITL}ISOLATED=y${NRM}	When using this with the build target, the build will be isolated in the
#			sense that local caches such as ${DIM}\${GOPATH}/pkg${NRM} and ${DIM}~/.cache${NRM} will not be
#			mounted into the container. Instead, according folders underneath the
#			configured build folder are used. These folders are removed when running
#			${DIM}make clean${NRM}. That way you can force a clean build/test, where all
#			dependencies are retrieved & built inside the container.
#
#	${ITL}CROSS=y${NRM}		When using this with the build target, ${ITL}MacOS${NRM} & ${ITL}Windows${NRM} binaries
#			are also built.
#

VERBOSE ?=
ifeq ($(VERBOSE),y)
    MAKEFLAGS += --trace
else
    MAKEFLAGS += -s
endif

ISOLATED ?=
ifeq ($(ISOLATED),y)
    CACHE_VOLS = -v $(shell pwd)/$(ISOLATED_PKG):/go/pkg -v $(shell pwd)/$(ISOLATED_CACHE):/.cache
else
    CACHE_VOLS = -v $(GOPATH)/pkg:/go/pkg -v /home/$(USER)/.cache:/.cache
endif

PORT ?=
ifeq ($(PORT),)
	ARDUINO_CLI_ARGS = --clean --verify --export-binaries
	ifeq ($(DEBUG),y)
		ARDUINO_CLI_ARGS += --build-path /oqtadrive/oqtadrive/build --build-property "build.extra_flags=-save-temps=obj -fverbose-asm"
	endif
	TTY_VOL =
else
	ARDUINO_CLI_ARGS = --clean --upload --port $(PORT)
	TTY_VOL = -v $(PORT):$(PORT)
endif

FQBN ?= arduino:avr:nano

export

#
#
#

.PHONY: help
help:
#	show this help
#
	$(call utils, synopsis) | more


.PHONY: run
run:
#	run the daemon with Go on host; set ${DIM}DEVICE${NRM} to serial device
#
	go run cmd/oqtad/main.go serve --device=$(DEVICE)


.PHONY: tests
tests: prep
#	run tests
#
	$(call utils, run_tests)
	rm -f $(BUILD_OUTPUT)/coverage.zip
	zip --junk-paths $(BUILD_OUTPUT)/coverage.zip \
		$(BUILD_OUTPUT)/coverage.out $(BUILD_OUTPUT)/coverage.html


.PHONY: imgarduino
imgarduino:
#	build the ${ITL}Arduino CLI${NRM} image required for compiling adapter firmware
#
ifeq ($(shell docker images --digests --quiet $(ARDUINO_CLI_IMAGE)),)
	echo "building Arduino CLI image..."
	docker build --build-arg BRANCH=$(BRANCH) -t $(ARDUINO_CLI_IMAGE) \
		-f ./hack/arduino-cli.Dockerfile .
	echo "image build done"
endif


.PHONY: imgsetup_linino
imgsetup_linino: img_set
#	build the unattended setup image for ${ITL}Linino One${NRM}
#
	$(call img, prepare_image_linino "$(IMG)" "$(IMG_MOUNT)")


.PHONY: imgsetup_raspberrypi
imgsetup_raspberrypi: mount_prep
#	build the unattended setup image for standalone with ${ITL}RaspberryPi${NRM}
#
	echo "adding unattended setup"
	$(call img, mount_img "$(IMG)" '^[ ]*1.+lba$$' "$(IMG_MOUNT)") # boot
	$(call img, prepare_boot_raspberrypi \
		"$(IMG_MOUNT)" "$(ROOT)/hack/unattended/raspberrypi")
	$(call img, umount_img "$(IMG_MOUNT)")
	$(call img, mount_img "$(IMG)" '^[ ]*2.+ext4$$' "$(IMG_MOUNT)") # root
	$(call img, prepare_root "$(IMG_MOUNT)" \
		"$(ROOT)/hack/unattended/raspberrypi")
	$(call img, prepare_root_raspberrypi "$(IMG_MOUNT)" \
		"$(ROOT)/hack/unattended/raspberrypi")
	$(call img, umount_img "$(IMG_MOUNT)")
	echo "done"


.PHONY: imgsetup_bananapi
imgsetup_bananapi: mount_prep
#	build the unattended setup image for standalone with ${ITL}BananaPi${NRM}
#
	echo "adding unattended setup"
	$(call img, mount_img "$(IMG)" '^[ ]*1.+ext4$$' "$(IMG_MOUNT)")
	$(call img, prepare_boot_bananapi "$(IMG_MOUNT)/boot" \
		"$(ROOT)/hack/unattended/bananapi")
	$(call img, prepare_root "$(IMG_MOUNT)" \
		"$(ROOT)/hack/unattended/bananapi")
	$(call img, prepare_root_bananapi "$(IMG_MOUNT)" \
		"$(ROOT)/hack/unattended/bananapi")
	$(call img, umount_img "$(IMG_MOUNT)")
	echo "done"


.PHONY: mount_prep
mount_prep: prep img_set img_exists #

.PHONY: img_set
img_set: #
ifeq ($(IMG),)
	$(error No image specified in IMG!)
	exit 1
endif

.PHONY: img_exists
img_exists: #
	[[ -f "$(IMG)" ]] || { \
		echo "image file does not exist"; \
		exit 1; \
	}


.PHONY: firmware
firmware: imgarduino
#	compile the adapter firmware; set ${DIM}FQBN${NRM} to select board type (defaults to ${DIM}arduino:avr:nano${NRM});
#	set ${DIM}PORT${NRM} to port of connected adapter to actually upload instead of just compiling, e.g.:
# 
#	    ${DIM}FQBN=arduino:avr:pro PORT=/dev/ttyUSB0 make firmware${NRM}
# 
#	common FQBN:
# 
#	    ${DIM}arduino:avr:nano${NRM}
#	    ${DIM}arduino:avr:nano:cpu=atmega328old${NRM}
#	    ${DIM}arduino:avr:pro${NRM}
#
	echo "building firmware for FQBN $(FQBN)..."
	docker run --rm -ti --privileged -v "$(SKETCH_DIR):/oqtadrive/oqtadrive" \
		$(TTY_VOL) $(ARDUINO_CLI_IMAGE) \
		./arduino/arduino-cli compile $(ARDUINO_CLI_ARGS) \
			--fqbn $(FQBN) /oqtadrive/oqtadrive


.PHONY: release
release: clean ui linino_hex build
#	build all artifacts needed for a release, i.e the ${DIM}oqtactl${NRM} binary,
#	packed UI resources, and hex firmware for ${ITL}Linino${NRM}
#


.PHONY: build
build: prep
#	build the ${DIM}oqtactl${NRM} binary
#
	rm -f $(BINARIES)/oqtactl
	$(call utils, build_binary oqtactl linux amd64 keep)
ifneq ($(CROSS),)
	$(call utils, build_binary oqtactl linux 386)
	$(call utils, build_binary oqtactl linux arm)
	$(call utils, build_binary oqtactl linux arm64)
	$(call utils, build_binary oqtactl linux mips)
	$(call utils, build_binary oqtactl darwin amd64)
	$(call utils, build_binary oqtactl darwin arm64)
	$(call utils, build_binary oqtactl windows amd64)
endif
	cd $(BINARIES); \
		touch ui.zip oqtadrive.linino.hex; \
		sha256sum oqtactl*_*.zip ui.zip oqtadrive.linino.hex > checksums.txt

	[[ -L $(BINARIES)/oqtactl ]] || \
		( cd $(BINARIES); ln -s oqtactl_$(OQTADRIVE_RELEASE)_linux_amd64 oqtactl )


.PHONY: linino_hex
linino_hex: #
	cp arduino/oqtadrive.ino arduino/oqtadrive.ino.bak
	sed -i 's;^//\(#include "config/linino.h"\)$$;\1;' arduino/oqtadrive.ino
	make firmware FQBN=arduino:avr:one
	mv -f arduino/oqtadrive.ino.bak arduino/oqtadrive.ino
	cp arduino/build/arduino.avr.one/oqtadrive.ino.hex \
		_build/bin/oqtadrive.linino.hex


.PHONY: ui
ui: prep
#	pack the UI resources
#
	$(call utils, minify_js drives.js files.js repo.js net.js config.js main.js)
	$(call utils, cache_bust)
	rm -f $(BINARIES)/ui.zip
	zip --recurse-paths $(BINARIES)/ui.zip ui \
		-x 'ui/web/js/oqta/*' 'ui/web/cover.css' '*/.gitignore'



.PHONY: prep
prep: #
	mkdir -p $(BINARIES) $(ISOLATED_PKG) $(ISOLATED_CACHE) $(IMG_MOUNT)


.PHONY: clean
clean:
#	clean up
#
	[[ ! -d $(BUILD_OUTPUT) ]] || chmod -R u+w $(BUILD_OUTPUT)
	rm -rf $(BUILD_OUTPUT)/*
	rm -f $(UI_BASE)/js/oqta.min.*.js $(UI_BASE)/cover.*.css


#
# helper functions
#
utils = ./hack/devenvutil.sh $(1)
img = ./hack/imgutil.sh $(1)
