/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2023, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package util

import (
	"context"
)

// -
func NewLock(acquired, released func(bool)) *Lock {
	ret := &Lock{
		lock:     make(chan bool, 1),
		acquired: acquired,
		released: released,
	}
	if acquired == nil {
		ret.acquired = func(bool) {}
	}
	if released == nil {
		ret.released = func(bool) {}
	}
	return ret
}

// -
type Lock struct {
	lock     chan bool
	acquired func(bool)
	released func(bool)
}

// -
func (l *Lock) Acquire(ctx context.Context) bool {
	select {
	case l.lock <- true:
		l.acquired(true)
		return true
	case <-ctx.Done():
		l.acquired(false)
		return false
	}
}

// -
func (l *Lock) Release() bool {
	select {
	case <-l.lock:
		l.released(true)
		return true
	default:
		l.released(false)
		return false
	}
}

// -
func (l *Lock) IsLocked() bool {
	return len(l.lock) > 0
}
