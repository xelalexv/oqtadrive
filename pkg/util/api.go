/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2021, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package util

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"

	"github.com/fsnotify/fsnotify"
	log "github.com/sirupsen/logrus"
	htpasswd "github.com/tg123/go-htpasswd"
)

// -
const EnvCredentials = "OQTADRIVE_CREDENTIALS"
const DefaultPort = 8888

// -
func ApiCall(address, method, path string, json bool,
	body io.Reader) (io.ReadCloser, error) {

	parts := strings.SplitN(address, ":", 2)
	host := parts[0]
	var port string
	if len(parts) > 1 {
		port = parts[1]
	} else {
		port = fmt.Sprintf("%d", DefaultPort)
	}

	client := &http.Client{}
	req, err := http.NewRequest(
		method, fmt.Sprintf("http://%s:%s%s", host, port, path), body)
	if err != nil {
		return nil, err
	}

	if json {
		req.Header.Add("Content-Type", "application/json")
		req.Header.Add("Accept", "application/json")
	} else {
		req.Header.Add("Content-Type", "text/plain")
		req.Header.Add("Accept", "text/plain")
	}

	if creds := os.Getenv(EnvCredentials); creds != "" {
		parts := strings.SplitN(creds, ":", 2)
		user := parts[0]
		pass := ""
		if len(parts) > 1 {
			pass = parts[1]
		}
		req.SetBasicAuth(user, pass)
	}

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	if 200 <= resp.StatusCode && resp.StatusCode <= 299 {
		return resp.Body, nil
	}

	defer resp.Body.Close()

	msg := "API call failed, no further details, could not read server response"
	if bytes, err := ioutil.ReadAll(resp.Body); err == nil {
		msg = string(bytes)
	}

	return nil, fmt.Errorf("%s", msg)
}

// -
func SetHeaders(h http.Header, json bool) {
	if json {
		h.Set("Content-Type", "application/json; charset=UTF-8")
	} else {
		h.Set("Content-Type", "text/plain; charset=UTF-8")
	}
}

// -
func HandleError(e error, statusCode int, w http.ResponseWriter) {
	SetHeaders(w.Header(), false)
	w.WriteHeader(statusCode)
	if _, err := w.Write([]byte(fmt.Sprintf("%v\n", e))); err != nil {
		log.Errorf("problem writing error: %v", err)
	}
}

// -
func NewHTPASSWD(file string, watch bool) (*HTPASSWD, error) {

	if info, err := os.Stat(file); err != nil {
		return nil, err
	} else if info.Mode()&077 != 0 {
		log.Warnf("permissions of credentials file %s too liberal: %v",
			file, info.Mode())
	}

	h, err := htpasswd.New(file, htpasswd.DefaultSystems, nil)
	if err != nil {
		return nil, err
	}

	var w *DirWatcher
	if watch {
		if w, err = NewDirWatcher(file); err != nil {
			return nil, err
		}
		if err = w.Start(0, func(evt fsnotify.Event) error {
			if evt.Op == fsnotify.Write {
				log.WithField("file", file).Info("reloading htpasswd file")
				return h.Reload(nil)
			}
			return nil
		}, nil); err != nil {
			return nil, err
		}
	}

	return &HTPASSWD{htpasswd: h, watcher: w}, nil
}

// -
func NewUserOnlyAuth() *HTPASSWD {
	return &HTPASSWD{}
}

// -
type HTPASSWD struct {
	htpasswd *htpasswd.File
	watcher  *DirWatcher
}

// -
func (h *HTPASSWD) Stop() {
	if h.watcher != nil {
		h.watcher.Stop()
	}
}

// -
func (h *HTPASSWD) Auth(inner http.Handler) http.Handler {

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		logger := log.WithFields(log.Fields{
			"remote": r.RemoteAddr,
			"method": r.Method,
			"path":   r.RequestURI,
			"agent":  r.UserAgent()})

		u, p, ok := r.BasicAuth()

		var err error
		if p, err = url.QueryUnescape(p); err != nil {
			logger.Warn("improper password escape")
			HandleError(fmt.Errorf("improper password escape"),
				http.StatusUnauthorized, w)
		}

		if !ok {
			logger.Warn("anonymous login tried")
			HandleError(fmt.Errorf("no anonymous logins"),
				http.StatusUnauthorized, w)
			return
		}

		if ok && (h.htpasswd == nil || h.htpasswd.Match(u, p)) {
			inner.ServeHTTP(w, r)
			return
		}

		logger.WithField("user", u).Warn("failed authentication")
		time.Sleep(2 * time.Second)

		w.Header().Set("WWW-Authenticate",
			`Basic realm="restricted", charset="UTF-8"`)
		HandleError(fmt.Errorf("invalid username or password"),
			http.StatusUnauthorized, w)
	})
}
