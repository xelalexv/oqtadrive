/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2022, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package util

import (
	"fmt"
	"net"
	"net/http"
	"strings"

	"golang.org/x/mod/semver"
)

// -
const OqtaDriveAgent = "oqtadrive"

// -
func GetIPAddresses() []net.IP {

	// if we can egress, then the outbound IP is our best bet
	if ip, err := getOutboundIP(); ip != nil && err == nil {
		return []net.IP{ip}
	}

	// if we're confined to the LAN, we need to collect addresses from all
	// interfaces and sort out obvious non-candidates
	var ret []net.IP

	if ifaces, err := net.Interfaces(); err == nil {

		for _, i := range ifaces {

			if i.Flags&net.FlagUp == 0 || i.Flags&net.FlagLoopback != 0 ||
				strings.HasPrefix(i.Name, "docker") ||
				strings.HasPrefix(i.Name, "crc") {
				continue
			}

			addrs, err := i.Addrs()
			if err != nil || len(addrs) == 0 {
				continue
			}

			for _, a := range addrs {
				if ip := parseIP(a.String()); ip != nil {
					if ip.IsUnspecified() || ip.IsLoopback() || ip.IsMulticast() ||
						ip.IsLinkLocalMulticast() || ip.IsLinkLocalUnicast() ||
						ip.IsInterfaceLocalMulticast() {
						continue
					}
					ret = append(ret, ip)
				}
			}
		}
	}

	return ret
}

// -
func parseIP(ip string) net.IP {
	if ix := strings.Index(ip, "/"); ix > -1 {
		ip = ip[:ix]
	}
	return net.ParseIP(ip)
}

// -
func getOutboundIP() (net.IP, error) {
	conn, err := net.Dial("udp", "8.8.8.8:80")
	if err != nil {
		return nil, err
	}
	defer conn.Close()
	return conn.LocalAddr().(*net.UDPAddr).IP, nil
}

// -
func AddOqtaDriveAgent(r *http.Request) {
	if r != nil && r.Header.Get("User-Agent") == "" {
		r.Header.Add("User-Agent",
			fmt.Sprintf("%s/%s", OqtaDriveAgent, OqtaDriveVersion))
	}
}

// -
func GetOqtaDriveVersion(r *http.Request) (version, build string, ok bool) {

	if r != nil && r.Header != nil {
		p := strings.Split(r.Header.Get("User-Agent"), "/")
		if len(p) == 2 && p[0] == OqtaDriveAgent {
			if p = strings.SplitN(p[1], "-", 2); len(p) > 1 {
				build = p[1]
			}
			version = p[0]
			ok = semver.IsValid(fmt.Sprintf("v%s", version))
		}
	}

	return
}

// -
func CompareVersions(a, b string) int {
	return semver.Compare(fmt.Sprintf("v%s", a), fmt.Sprintf("v%s", b))
}
