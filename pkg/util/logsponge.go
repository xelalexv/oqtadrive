/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2022, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package util

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"time"
)

// -
func NewLogSponge(maxMinutes, maxBytes int) *LogSponge {

	if maxMinutes < 1 {
		maxMinutes = 5
	}

	if maxBytes < 16*1024 {
		maxBytes = 16 * 1024
	}

	return &LogSponge{
		lck:      NewLock(nil, nil),
		bufs:     make([]bytes.Buffer, maxMinutes),
		maxBytes: maxBytes / maxMinutes,
		start:    time.Now(),
	}
}

// -
type LogSponge struct {
	bufs     []bytes.Buffer
	tip      int
	maxBytes int
	start    time.Time
	lck      *Lock
}

// -
func (s *LogSponge) lock(ctx context.Context) bool {
	return s.lck.Acquire(ctx)
}

// -
func (s *LogSponge) unlock() {
	s.lck.Release()
}

// -
func (s *LogSponge) Write(p []byte) (n int, err error) {

	ctx, cancel := context.WithTimeout(context.Background(), 25*time.Millisecond)
	defer cancel()

	if s.lock(ctx) {
		defer s.unlock()

		b := &s.bufs[s.tip]
		n, err = b.Write(p)
		t := time.Now()

		if b.Len() > s.maxBytes || t.Sub(s.start) > time.Minute {
			s.tip = (s.tip + 1) % len(s.bufs)
			s.bufs[s.tip].Reset()
			s.start = t
		}
	} else {
		n = len(p)
	}

	return
}

// -
func (s *LogSponge) Fprint(ctx context.Context, w io.Writer) error {
	if s.lock(ctx) {
		defer s.unlock()
		n := len(s.bufs)
		for ix := 1; ix <= n; ix++ {
			b := &s.bufs[(s.tip+ix)%n]
			if _, err := fmt.Fprintf(w, "%s", b.Bytes()); err != nil {
				return err
			}
		}
	}
	return nil
}
