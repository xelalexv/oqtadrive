/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2022, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package util

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
)

//
func NewAnnotation(key string, value interface{}) *Annotation {
	return &Annotation{Key: key, Val: value}
}

//
type Annotation struct {
	Key string      `json:"key"`
	Val interface{} `json:"value"`
}

//
func (a *Annotation) Value() interface{} {
	return a.Val
}

//
func (a *Annotation) IsBool() bool {
	_, ok := a.Val.(bool)
	return ok
}

//
func (a *Annotation) Bool() bool {
	if v, ok := a.Val.(bool); ok {
		return v
	}
	return false
}

//
func (a *Annotation) IsInt() bool {
	_, ok := a.Val.(int)
	return ok
}

//
func (a *Annotation) Int() int {
	if v, ok := a.Val.(int); ok {
		return v
	}
	return 0
}

//
func (a *Annotation) IsString() bool {
	_, ok := a.Val.(string)
	return ok
}

//
func (a *Annotation) String() string {
	if a.Val == nil {
		return ""
	}
	if v, ok := a.Val.(string); ok {
		return v
	}
	return fmt.Sprintf("%v", a.Val)
}

//
type Annotations map[string]*Annotation

//
func (a Annotations) Annotate(key string, value interface{}) *Annotation {
	an := NewAnnotation(key, value)
	a[key] = an
	return an
}

//
func (a Annotations) HasAnnotation(key string) bool {
	_, ok := a[key]
	return ok
}

//
func (a Annotations) GetAnnotation(key string) *Annotation {
	if an, ok := a[key]; ok {
		return an
	}
	return &Annotation{}
}

//
func (a Annotations) RemoveAnnotations(keys ...string) {
	for _, k := range keys {
		delete(a, k)
	}
}

//
func (a Annotations) RemoveAnnotation(key string) *Annotation {
	ret := a.GetAnnotation(key)
	delete(a, key)
	return ret
}

//
func (a Annotations) AddAnnotations(all Annotations) {
	for k, v := range all {
		a[k] = v
	}
}

//
func (a Annotations) MarshalAnnotations(file string) error {

	fd, err := os.Create(file)
	if err != nil {
		return err
	}
	defer fd.Close()

	m := make(Annotations)

	for k, v := range a {
		if v.IsString() || v.IsInt() || v.IsBool() {
			m[k] = v
		}
	}

	data, err := json.Marshal(m)
	if err != nil {
		return err
	}

	_, err = fd.Write(data)
	return err
}

//
func (a Annotations) UnmarshalAnnotations(file string) error {

	fd, err := os.Open(file)
	if err != nil {
		if !os.IsNotExist(err) {
			return err
		}
		return nil
	}
	defer fd.Close()

	data, err := io.ReadAll(fd)
	if err != nil {
		return err
	}

	return json.Unmarshal(data, &a)
}
