/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2022, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package util

import (
	"bufio"
	"encoding/binary"
	"fmt"
	"golang.org/x/text/unicode/norm"
	"io"
	"strings"
)

// -
func ReadUInt16(r io.Reader, bigEndian bool) (int, error) {

	var order binary.ByteOrder = binary.LittleEndian
	if bigEndian {
		order = binary.BigEndian
	}

	var ret uint16
	if err := binary.Read(r, order, &ret); err != nil {
		return -1, err
	}
	return int(ret), nil
}

// -
func WriteUInt16s(w io.Writer, i []int, bigEndian bool) (int, error) {
	sum := 0
	for _, v := range i {
		s, err := WriteUInt16(w, v, bigEndian)
		sum += s
		if err != nil {
			return sum, err
		}
	}
	return sum, nil
}

// -
func WriteUInt16(w io.Writer, i int, bigEndian bool) (int, error) {
	var order binary.ByteOrder = binary.LittleEndian
	if bigEndian {
		order = binary.BigEndian
	}
	if err := binary.Write(w, order, uint16(i)); err != nil {
		return 0, err
	}
	return 2, nil
}

// NOTE: This only works correctly as long as r delivers byte sized characters!
func StreamTranslate(w io.Writer, r io.Reader, bufSize, wrap int,
	translate func(string, rune) string) error {

	if bufSize < 1 {
		bufSize = 128
	}

	b := make([]byte, bufSize)
	var sb strings.Builder
	var trans string
	l := 0

	for {
		if n, err := io.ReadFull(r, b); n > 0 {

			left := '\n'
			if len(trans) > 0 {
				left = rune(trans[len(trans)-1])
			}

			trans = translate(string(b[:n]), left)

			if wrap == 0 {
				if _, e := w.Write([]byte(trans)); e != nil {
					return e
				}

			} else {
				var it norm.Iter
				it.InitString(norm.NFKD, trans)
				for !it.Done() {
					if l == wrap {
						sb.WriteRune('\n')
						if _, e := w.Write([]byte(sb.String())); e != nil {
							return e
						}
						sb.Reset()
						l = 0
					}
					sb.Write(it.Next())
					l++
				}
			}

		} else if err != nil {
			if err == io.EOF || err == io.ErrUnexpectedEOF {
				if sb.Len() > 0 {
					_, err = w.Write([]byte(sb.String()))
					return err
				}
				return nil
			}
			return err
		}
	}
}

// -
func ToScreenWidth(lines string, w int, newlines bool) (string, error) {

	if w < 1 {
		return "", fmt.Errorf("screen width too narrow")
	}

	format := fmt.Sprintf("%%-%ds", w)

	s := bufio.NewScanner(strings.NewReader(lines))
	var b strings.Builder

	for s.Scan() {
		l := s.Text()
		if len(l) > w {
			l = strings.Join(strings.Fields(l), " ")
		}
		b.WriteString(fmt.Sprintf(format, l))
		if newlines {
			b.WriteRune('\n')
		}
	}

	return b.String(), s.Err()
}
