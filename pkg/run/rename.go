/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2023, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package run

import (
	"fmt"
	"io/ioutil"
)

//
func NewRename() *Rename {

	r := &Rename{}
	r.Runner = *NewRunner(
		`rename -d|--drive {drive} -n|--name {name} [-a|--address {address}]

Client short form:
  rename|rn {drive} {name}`,
		"rename a cartridge in a drive",
		`
Use the rename command to rename a cartridge in a drive.`,
		"", runnerHelpEpilogue, r.Run)

	r.AddBaseSettings()
	r.AddSetting(&r.Drive, "drive", "d", "", nil, "drive number (1-8)", true)
	r.AddSetting(&r.Name, "name", "n", "", nil, "new cartridge name", true)

	return r
}

//
type Rename struct {
	//
	Runner
	//
	Drive int
	Name  string
}

//
func (r *Rename) Run() error {

	r.ParseSettings()

	if err := validateDrive(r.Drive); err != nil {
		return err
	}

	if r.Name == "" {
		return fmt.Errorf("you need to specify a non-empty name")
	}

	resp, err := r.apiCall("PUT",
		fmt.Sprintf("/drive/%d/rename?name=%s", r.Drive, r.Name), false, nil)
	if err != nil {
		return err
	}
	defer resp.Close()

	msg, err := ioutil.ReadAll(resp)
	if err != nil {
		return err
	}

	fmt.Printf("%s", msg)
	return nil
}
