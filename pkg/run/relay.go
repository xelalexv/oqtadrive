/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2022, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package run

import (
	"fmt"
	"strings"
	"time"
	"unicode"

	log "github.com/sirupsen/logrus"

	"codeberg.org/xelalexv/oqtadrive/pkg/control"
	"codeberg.org/xelalexv/oqtadrive/pkg/daemon"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/base"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/format/helper"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/fs"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/if1"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/ql"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/types"
	"codeberg.org/xelalexv/oqtadrive/pkg/repo"
	"codeberg.org/xelalexv/oqtadrive/pkg/run/info"
	"codeberg.org/xelalexv/oqtadrive/pkg/util"
)

//
const IF1ControlCartridgeLength = 32

// listens for cartridges being formatted, if a cartridge was formatted with the
// control cartridge name, additional setup is done on the cartridge
func ControlCartFormatListener(drive int, c *base.Cartridge,
	fc *types.FileChange) error {

	log.WithFields(log.Fields{
		"drive": drive,
		"name":  fc.Name,
		"op":    fc.Op}).Debug("control cart format listener")
	if fc.Op == types.OpFormatted {
		handleControlCart(c, true)
	}
	return nil
}

// listens for drive start and load
type ControlCartDriveListener struct {
	address string
	daemon  *daemon.Daemon
	// to be precise, this would have to be maintained per drive,
	// but globally is good enough
	lastInfoWrite time.Time
}

//
func (dl *ControlCartDriveListener) Change(
	c *base.Cartridge, dc *daemon.DriveChange) error {

	log.WithFields(log.Fields{
		"drive": dc.Drive, "change": dc.State,
	}).Debug("control card drive listener")

	switch dc.State {

	// if the started drive contains a control cartridge, an ls file with
	// the current drive list is created on that cartridge
	case daemon.DriveStarted:
		if control.IsControlCart(c) {
			dl.writeInfoFiles(c)
		}

	// prevent writing info files to soon after stopping; for IF1, this causes
	// a problem during formatting when the drive is briefly stopped between
	// phase 1 & 2
	case daemon.DriveStopped:
		dl.lastInfoWrite = time.Now()

	// listens for cartridges being loaded, if the loaded cartridge has the
	// control cartridge name, additional setup is done on the cartridge
	case daemon.DriveLoaded:
		handleControlCart(c, true)
	}

	return nil
}

//
func (dl *ControlCartDriveListener) writeInfoFiles(c *base.Cartridge) {

	if time.Since(dl.lastInfoWrite) < 3*time.Second {
		if c.Client() == types.QL {
			c.SeekToStart()
			c.RewindAccessIxBy(2, true)
		}
		return
	}
	dl.lastInfoWrite = time.Now()

	if s := getSectors(c, 1); len(s) == 0 || c.IsFormatting() ||
		(c.Client() == types.IF1 && // IF1 is formatting, so hands off
			(s[0].Record().Flags() == if1.RecordFlagLastRecord &&
				s[0].Record().Length() == 0)) {
		return
	}

	// config
	var b strings.Builder
	ret, err := apiCall(dl.address, "GET", "/map", false, nil)
	if err != nil {
		ret = "couldn't get drive mapping"
	}
	b.WriteString(ret)
	b.WriteRune('\n')

	ret, err = apiCall(dl.address, "GET", "/config", false, nil)
	if err != nil {
		ret = "couldn't get config"
	}
	b.WriteString(ret)
	b.WriteRune('\n')

	ret, err = apiCall(dl.address, "GET", "/version?nolatest=true", false, nil)
	if err != nil {
		ret = "couldn't get version"
	} else {
		ret = strings.Replace(ret, "protocol", "proto", 1)
		ret = strings.Replace(ret, "firmware", "fw", 1)
	}
	b.WriteString(ret)
	b.WriteString(fmt.Sprintf("\nlatest:          %s", info.LatestRelease()))

	var qrIF1 []byte
	var qrQL []byte
	var qrQLbin []byte

	if ips := info.IPAddresses(); len(ips) > 0 {
		qrIF1 = info.WebUIQR(true, false)
		qrQL = info.WebUIQR(false, false)
		qrQLbin = info.WebUIQR(false, true)
		b.WriteString(fmt.Sprintf("\n\nIP: %s", ips[0].String()))
	} else {
		b.WriteString("\n\nIP: -")
	}

	config := b.String()

	// drive list
	b.Reset()
	b.WriteRune('\n')
	lst, _ := control.GetCartridges(dl.daemon)
	for drive, stat := range lst {
		b.WriteString(fmt.Sprintf("%d %-28s\n", drive+1, stat.String(true)))
	}
	b.WriteString("\n\n\n")
	drives := b.String()

	if c.Client() == types.IF1 {
		err = writeInfoFilesIF1(c, config, drives, qrIF1)
	} else {
		err = writeInfoFilesQL(c, config, drives, qrQL, qrQLbin)
	}
	if err != nil {
		log.Errorf("failed writing info files: %v", err)
	}
}

//
func writeInfoFilesQL(c *base.Cartridge, config, drives string, webUIQR,
	webUIQRbin []byte) error {

	// config
	config = fmt.Sprintf(
		"%s\n\nTo see the web UI QR-code,\nload & run qr.bas", config)
	cfg, _ := util.ToScreenWidth(fmt.Sprintf("%s\n\n\n", config), 36, true)
	if err := writeFile(c, "cfg", ql.CONFIG, []byte(cfg)); err != nil {
		log.Errorf("failed writing config info file: %v", err)
	}

	// drive list
	drv, _ := util.ToScreenWidth(drives, 36, true)
	if err := writeFile(c, "drv", ql.CONFIG, []byte(drv)); err != nil {
		log.Errorf("failed writing drives file: %v", err)
	}

	// QR code program
	if err := writeFile(c, "qr.bas", ql.BASIC, webUIQR); err != nil {
		log.Errorf("failed writing QR code program: %v", err)
	}

	// media change
	if err := ql.FIXME_ChangeMedia(c); err != nil {
		log.Errorf("failed changing media number: %v", err)
	}

	c.SeekToStart()
	c.RewindAccessIxBy(2, true)
	return nil
}

//
func writeInfoFilesIF1(c *base.Cartridge, config, drives string,
	webUIQR []byte) error {

	cfg, _ := util.ToScreenWidth(
		fmt.Sprintf("%s\n%s", config, webUIQR), 32, false)
	if err := writeFile(c, "cfg", if1.PRINT, []byte(cfg)); err != nil {
		return fmt.Errorf("error writing config file: %v", err)
	}

	drv, _ := util.ToScreenWidth(drives, 32, false)
	if err := writeFile(c, "drv", if1.PRINT, []byte(drv)); err != nil {
		return fmt.Errorf("error writing drive list: %v", err)
	}

	c.SeekToSector(IF1ControlCartridgeLength, true)
	return nil
}

//
func addManual(c *base.Cartridge) error {

	var typ fs.FileType
	var man string
	var err error

	if c.Client() == types.IF1 {
		typ = if1.PRINT
		man, err = util.ToScreenWidth(info.ManIF1, 32, false)
	} else {
		typ = ql.BASIC
		man, err = util.ToScreenWidth(info.ManQL, 36, true)
	}

	if err != nil {
		return err
	}
	return writeFile(c, "man", typ, []byte(man))
}

//
func writeFile(c *base.Cartridge, name string, typ fs.FileType,
	data []byte) error {

	f, err := c.FS().Open(name, fs.OVERWRITE, typ)
	if err != nil {
		return fmt.Errorf("error opening file: %v", err)
	}

	if _, err = f.Write(data); err != nil {
		return fmt.Errorf("error writing file: %v", err)
	}

	if err = f.Close(); err != nil {
		return fmt.Errorf("error closing file: %v", err)
	}

	return nil
}

//
func getSectors(c *base.Cartridge, count int) []types.Sector {
	var ret []types.Sector
	for ix := 0; ix < count; ix++ {
		if s := c.GetSector(); s != nil {
			if r := s.Record(); r != nil {
				ret = append(ret, s)
			}
		}
		c.AdvanceAccessIx(true)
	}
	return ret
}

//
func handleControlCart(cart *base.Cartridge, addManPage bool) {
	if control.IsControlCart(cart) {
		if cart.Client() == types.QL {
			// reduced length does not work for QL without modifying the sector
			// map (maybe not even then); we will use headers only mode instead,
			// which in turn doesn't work for Spectrum
			log.Info("setting headers only mode for control cartridge")
			cart.Annotate(daemon.AnnoHeadersOnly, true)
		} else {
			log.WithField("length", IF1ControlCartridgeLength).
				Info("reducing length of control cartridge")
			cart.Annotate(base.AnnoReducedLength, IF1ControlCartridgeLength)
		}
		cart.FS().Delete("res", false)
		if addManPage {
			addManual(cart)
		}
	}
}

//
type ControlListener struct {
	address    string
	repository string
}

//
func (cl *ControlListener) Change(drive int, cart *base.Cartridge,
	fc *types.FileChange) (err error) {

	logger := log.WithFields(
		log.Fields{"drive": drive, "name": fc.Name, "op": fc.Op})

	if (fc.Op != types.OpCreated && fc.Op != types.OpModified) ||
		fc.Name != control.ControlFile {
		logger.Debug("control listener - nothing to do")
		return nil
	}

	logger.Debug("control listener - handling file")

	locked := true
	var cmd string

	defer func() {
		if !locked { // need to lock again for writing result file
			if !daemon.LockCartridge(cart, 5*time.Millisecond) {
				return
			}
		}
		if err := writeResFile(cart, cmd, err); err != nil {
			log.Warnf("error writing result file: %v", err)
		}
	}()

	fsys := cart.FS()
	f, err := fsys.Open(control.ControlFile, fs.READ, fs.ASCII)
	if err != nil {
		return fmt.Errorf("error opening control file: %v", err)
	}

	var buf strings.Builder
	if err := f.Render(&buf); err != nil {
		return fmt.Errorf("error reading control file: %v", err)
	}

	cmd = strings.TrimLeftFunc(
		strings.TrimRight(buf.String(), "\n"), unicode.IsSpace)
	if len(cmd) > 0 && cmd[0] == '-' { // FIXME Spectrum specific
		cmd = cmd[1:]
	}

	var p []string
	purge := false

	if strings.HasPrefix(cmd, "wifi ") {
		// argument is `ssid:password` (password optional); since password may
		// have trailing space, we may only trim space on left
		p = []string{"wifi",
			strings.TrimLeftFunc(strings.TrimPrefix(cmd, "wifi "),
				unicode.IsSpace)}
		if ix := strings.IndexRune(cmd, ':'); ix > -1 {
			cmd = fmt.Sprintf("%s:<redacted>", cmd[:ix]) // don't log password
		}
		purge = true
	} else {
		p = strings.Split(strings.TrimSpace(cmd), " ")
	}

	if len(p) == 0 {
		return fmt.Errorf("empty control command")
	}

	log.Infof("control command: %s", cmd)
	if ok, err := fsys.Delete(control.ControlFile, purge); err != nil {
		log.Debugf("error deleting control file: %v", err)
	} else {
		log.Debugf("deleted control file: %v", ok)
	}

	if err := helper.AutoSave(drive, cart, false); err != nil {
		return fmt.Errorf("auto-saving drive %d failed: %v", drive, err)
	}

	clientPath := repo.ClientFolderIF1
	if cart.Client() == types.QL {
		clientPath = repo.ClientFolderQL
	}

	cart.Unlock() // need to unlock in case the command uses this cartridge
	locked = false

	return newRelayCmd(cl.address, clientPath, p[0], p[1:]).run()
}

//
func writeResFile(c *base.Cartridge, cmd string, err error) error {

	log.WithFields(log.Fields{
		"command": cmd, "error": err}).Debug("writing result file")

	var b strings.Builder
	b.WriteString(cmd)
	b.WriteRune('\n')

	if err != nil {
		b.WriteString(err.Error())
		b.WriteRune('\n')
	} else {
		b.WriteString("OK\n")
	}

	if c.Client() == types.IF1 {
		res, _ := util.ToScreenWidth(b.String(), 32, false)
		return writeFile(c, "res", if1.PRINT, []byte(res))
	} else {
		res, _ := util.ToScreenWidth(b.String(), 36, true)
		// FIXME media change?
		return writeFile(c, "res", ql.CONFIG, []byte(res))
	}
}
