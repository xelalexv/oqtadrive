/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2023, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package run

import (
	"fmt"
	"io/ioutil"
	"strconv"
)

//
func NewFormat() *Format {

	f := &Format{}
	f.Runner = *NewRunner(
		"format -d|--drive {drive} -n|--name {name} [-f|--force] [-a|--address {address}]",
		"format a cartridge in a drive",
		`
Use the format command to format a cartridge in a drive. The type of cartridge,
i.e. Spectrum or QL, will not be changed. If the drive is empty, a new cartridge
will be inserted. The type will be that of the currently connected client. If no
client is connected, the type will be Spectrum.`,
		"", runnerHelpEpilogue, f.Run)

	f.AddBaseSettings()
	f.AddSetting(&f.Drive, "drive", "d", "", nil, "drive number (1-8)", true)
	f.AddSetting(&f.Name, "name", "n", "", nil, "cartridge name", true)
	f.AddSetting(&f.Force, "force", "f", "", false,
		`force formatting modified cartridge, this is equivalent to
discarding the changes, all data will be lost!`, false)

	return f
}

//
type Format struct {
	//
	Runner
	//
	Drive int
	Name  string
	Force bool
}

//
func (f *Format) Run() error {

	f.ParseSettings()

	if err := validateDrive(f.Drive); err != nil {
		return err
	}

	if f.Name == "" {
		return fmt.Errorf("you need to specify a non-empty name")
	}

	resp, err := f.apiCall("PUT",
		fmt.Sprintf("/drive/%d/format?name=%s&force=%s",
			f.Drive, f.Name, strconv.FormatBool(f.Force)), false, nil)
	if err != nil {
		return err
	}
	defer resp.Close()

	msg, err := ioutil.ReadAll(resp)
	if err != nil {
		return err
	}

	fmt.Printf("%s", msg)
	return nil
}
