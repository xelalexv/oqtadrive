/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2023, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package run

import (
	"fmt"
	"io/ioutil"
)

//
func NewReorg() *Reorg {

	r := &Reorg{}
	r.Runner = *NewRunner(
		`reorg -d|--drive {drive} [-a|--address {address}]

Client short form:
  reorg|ro {drive}`,
		"reorganize a cartridge in a drive",
		`
Use the reorg command to optimize the allocation of files for a cartridge in a drive.`,
		"", runnerHelpEpilogue, r.Run)

	r.AddBaseSettings()
	r.AddSetting(&r.Drive, "drive", "d", "", nil, "drive number (1-8)", true)

	return r
}

//
type Reorg struct {
	//
	Runner
	//
	Drive int
}

//
func (r *Reorg) Run() error {

	r.ParseSettings()

	if err := validateDrive(r.Drive); err != nil {
		return err
	}

	resp, err := r.apiCall("PUT",
		fmt.Sprintf("/drive/%d/reorg", r.Drive), false, nil)
	if err != nil {
		return err
	}
	defer resp.Close()

	msg, err := ioutil.ReadAll(resp)
	if err != nil {
		return err
	}

	fmt.Printf("%s", msg)
	return nil
}
