/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2023, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package run

import (
	"fmt"
	"os"

	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/base"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/format"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/if1"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/ql"
)

// -
func createCartridge(name, typ, specials string) (*base.Cartridge, error) {

	var cart *base.Cartridge

	switch typ {
	case "mdr":
		if specials == if1.SpecialsCPM {
			cart = if1.NewCartridgeCPM()
		} else {
			cart = if1.NewCartridge()
		}
	case "mdv":
		cart = ql.NewCartridge()
	default:
		return nil, fmt.Errorf(
			"only mdr and mdv are supported as output formats")
	}

	if err := cart.Format(name); err != nil {
		return nil, err
	}
	return cart, nil
}

// -
func readCartridge(file string) (*base.Cartridge, error) {

	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	_, typ, comp := format.SplitNameTypeCompressor(file)

	rd, err := format.NewCartReader(f, comp)
	if err != nil {
		return nil, err
	}

	if typ == "" {
		typ = rd.Type()
	}

	form, err := format.NewFormat(typ, rd.Prefix())
	if err != nil {
		return nil, err
	}

	return form.Read(rd, false, false, nil)
}
