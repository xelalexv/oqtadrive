/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2024, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package run

import (
	"fmt"
	"io"
	"io/ioutil"
	"strings"
)

// -
func NewNetwork() *Network {

	n := &Network{}
	n.Runner = *NewRunner(
		`network [-a|--address {address}] [-b|--hub {hub URL}] [-c|--count {address count}]
          [-d|--disconnect]`,
		"manage connection to Sinclair Network hub",
		`
Use the network command for connecting to/disconnecting from Sinclair Network hubs.
When no arguments are given, the current network config state is output.`,
		"", runnerHelpEpilogue, n.Run)

	n.AddBaseSettings()
	n.AddSetting(&n.Hub, "hub", "b", "", "", "hub URL", false)
	n.AddSetting(&n.User, "user", "u", "", "", "user for login", false)
	n.AddSetting(&n.Pass, "password", "p", "", "", "password for login", false)
	n.AddSetting(&n.Count, "count", "c", "", 1,
		"number of required addresses", false)
	n.AddSetting(&n.Disconnect, "disconnect", "d", "", false,
		"disconnect from hub if currently connected", false)

	return n
}

// -
type Network struct {
	Runner
	//
	Hub        string
	User       string
	Pass       string
	Count      int
	Disconnect bool
}

// -
func (n *Network) Run() error {

	n.ParseSettings()

	if n.Disconnect && n.Hub != "" {
		return fmt.Errorf("don't specify a hub when disconnecting")
	}

	method := "GET"
	url := "/network/config"
	var in io.Reader

	if n.Disconnect || n.Hub != "" {

		method = "POST"

		if n.Disconnect {
			url = fmt.Sprintf("%s?hub=true", url)

		} else {
			var auth string
			if n.User != "" {
				if n.Pass != "" {
					n.Pass = ":" + n.Pass
				}
				auth = fmt.Sprintf("%s%s@", n.User, n.Pass)
			}
			url = fmt.Sprintf("%s?hub=true&addresses=%d", url, n.Count)
			proto := "wss://"
			if ix := strings.Index(n.Hub, "://"); ix > 0 {
				proto = n.Hub[:ix+3]
				n.Hub = n.Hub[ix+3:]
			}
			in = strings.NewReader(fmt.Sprintf("%s%s%s", proto, auth, n.Hub))
		}
	}

	resp, err := n.apiCall(method, url, false, in)

	if err != nil {
		return err
	}
	defer resp.Close()

	msg, err := ioutil.ReadAll(resp)
	if err != nil {
		return err
	}

	fmt.Printf("\n%s\n", msg)
	return nil
}
