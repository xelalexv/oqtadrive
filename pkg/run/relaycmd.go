/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2022, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package run

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/url"
	"strings"

	log "github.com/sirupsen/logrus"

	"codeberg.org/xelalexv/oqtadrive/pkg/repo"
	"codeberg.org/xelalexv/oqtadrive/pkg/util"
)

//
func newRelayCmd(address, clientPath, action string, args []string) *relayCmd {
	return &relayCmd{
		address:    address,
		clientPath: clientPath,
		action:     action,
		args:       args,
	}
}

//
type relayCmd struct {
	address    string
	action     string
	clientPath string
	args       []string
	runOpts    []string
}

//
func (cmd *relayCmd) run() error {

	switch cmd.action {

	case "load", "ld": // load|ld {drive} {file}
		return cmd.load()

	case "save", "sv": // save|sv {drive} {file}
		return cmd.save()

	case "rename", "rn": // rename|rn {drive} {name}
		return cmd.rename()

	case "reorg", "ro": // reorg|ro {drive}
		return cmd.reorg()

	case "unload", "ul": // unload|ul {drive} [force]
		return cmd.unload()

	case "map", "mp": // map|mp off|{{start} {end}}
		return cmd.mp()

	case "resync", "rs": // resync|rs [if1|ql]
		// TODO check why queuing first control command times out
		return cmd.resync()

	case "config", "cf": // config|cf {r|rumble {level}}| ...others
		return cmd.config()

	case "wifi": // wifi {ssid}[:{passphrase}]
		return cmd.wifi()

	case "poweroff":
		return cmd.poweroff()
	}

	return fmt.Errorf("unknown action: %s", cmd.action)
}

//
func (cmd *relayCmd) load() error {

	if err := cmd.hasMinArgs(2); err != nil {
		return err
	}

	cmd.addOpts("-r", "-d", cmd.arg(0),
		"-i", fmt.Sprintf("%s%s%s", repo.PrefixRepo, cmd.clientPath, cmd.arg(1)))
	return cmd.exec(&NewLoad().Runner)
}

//
func (cmd *relayCmd) save() error {

	if err := cmd.hasMinArgs(1); err != nil {
		return err
	}

	ref := ""
	if len(cmd.args) > 1 {
		ref = fmt.Sprintf("%s%s%s", repo.PrefixRepo, cmd.clientPath, cmd.arg(1))
	}

	msg, err := cmd.apiCall("PUT",
		fmt.Sprintf("/drive/%s/save", cmd.arg(0)), strings.NewReader(ref))
	if err != nil {
		return err
	}
	log.Info(msg)
	return nil
}

//
func (cmd *relayCmd) rename() error {

	if err := cmd.hasMinArgs(2); err != nil {
		return err
	}

	name := cmd.arg(1)
	// collect additional args in case name has spaces
	for ix := 2; ix < len(cmd.args); ix++ {
		name = fmt.Sprintf("%s %s", name, cmd.arg(ix))
	}
	cmd.addOpts("-d", cmd.arg(0), "-n", url.QueryEscape(name))
	return cmd.exec(&NewRename().Runner)
}

//
func (cmd *relayCmd) reorg() error {

	if err := cmd.hasMinArgs(1); err != nil {
		return err
	}

	cmd.addOpts("-d", cmd.arg(0))
	return cmd.exec(&NewReorg().Runner)
}

//
func (cmd *relayCmd) unload() error {

	if err := cmd.hasMinArgs(1); err != nil {
		return err
	}

	cmd.addOpts("-d", cmd.arg(0))

	if len(cmd.args) > 1 {
		if cmd.arg(1) != "force" {
			return fmt.Errorf("invalid option: %s", cmd.arg(1))
		}
		cmd.addOpts("-f")
	}

	return cmd.exec(&NewUnload().Runner)
}

//
func (cmd *relayCmd) mp() error {

	if err := cmd.hasMinArgs(1); err != nil {
		return err
	}

	cmd.addOpts("-y")

	if cmd.arg(0) == "off" {
		cmd.addOpts("-o")

	} else {
		if err := cmd.hasMinArgs(2); err != nil {
			return err
		}
		cmd.addOpts("-s", cmd.arg(0), "-e", cmd.arg(1))
	}

	return cmd.exec(&NewMap().Runner)
}

//
func (cmd *relayCmd) resync() error {
	cmd.addOpts("-r")
	if len(cmd.args) > 0 {
		cmd.addOpts("-c", cmd.arg(0))
	}
	return cmd.exec(&NewResync().Runner)
}

//
func (cmd *relayCmd) config() error {

	if len(cmd.args) > 0 {

		switch cmd.arg(0) {

		case "rumble", "r":
			if len(cmd.args) > 1 {
				cmd.addOpts("-r", cmd.arg(1))
			}
		}

		if len(cmd.runOpts) > 0 {
			return cmd.exec(&NewConfig().Runner)
		}
	}
	return nil
}

//
func (cmd *relayCmd) wifi() error {

	if err := cmd.hasMinArgs(1); err != nil {
		return err
	}

	p := strings.SplitN(cmd.arg(0), ":", 2)
	ssid := p[0]
	pass := ""
	if len(p) > 1 {
		pass = p[1]
	}

	log.WithField("ssid", ssid).Info("setting WiFi")

	msg, err := cmd.apiCall("PUT",
		fmt.Sprintf("/wifi?ssid=%s", url.QueryEscape(ssid)),
		strings.NewReader(pass))
	if err != nil {
		return err
	}

	log.WithField("ssid", ssid).Info(msg)
	return nil
}

//
func (cmd *relayCmd) poweroff() error {

	log.Info("powering off daemon host")

	msg, err := cmd.apiCall("PUT", "/shutdown?command=poweroff", nil)
	if err != nil {
		return err
	}
	log.Info(msg)
	return nil
}

//
func (cmd *relayCmd) hasMinArgs(min int) error {
	if len(cmd.args) < min {
		return fmt.Errorf(
			"not enough arguments, got %d, want %d", len(cmd.args), min)
	}
	return nil
}

//
func (cmd *relayCmd) arg(ix int) string {
	if 0 <= ix && ix < len(cmd.args) {
		return cmd.args[ix]
	}
	return ""
}

//
func (cmd *relayCmd) addOpts(opts ...string) {
	cmd.runOpts = append(cmd.runOpts, opts...)
}

//
func (cmd *relayCmd) exec(run *Runner) error {
	cmd.runOpts = append(cmd.runOpts, "-a", cmd.address)
	log.Debugf("relay command run options: %v", cmd.runOpts)
	return run.Execute(cmd.runOpts)
}

//
func (cmd *relayCmd) apiCall(method, path string, body io.Reader) (string, error) {
	return apiCall(cmd.address, method, path, false, body)
}

//
func apiCall(address, method, path string, json bool, body io.Reader) (string, error) {

	resp, err := util.ApiCall(address, method, path, json, body)
	if err != nil {
		return "", err
	}
	defer resp.Close()

	msg, err := ioutil.ReadAll(resp)
	if err != nil {
		return "", err
	}
	return strings.TrimSpace(string(msg)), nil
}
