/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2021, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package run

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/url"
	"os"
	"strings"
)

//
const DefaultMaxItems = 100

//
func NewSearch() *Search {

	s := &Search{}
	s.Runner = *NewRunner(
		`search [-a|--address {address}] -t|--term {search term} [-i|--items {max results}]
         [-s|--source {search source}] [-p|--tap]
  search [-a|--address {address}] -r|--reindex`,
		"search for cartridges in daemon repo, or rebuild the search index",
		`
Use the search command to find cartridges in the daemon's repository, if enabled.`,
		"", runnerHelpEpilogue, s.Run)

	s.AddBaseSettings()
	s.AddSetting(&s.Term, "term", "t", "", nil,
		"search term; used to search through the cartridge file names", false)
	s.AddSetting(&s.Items, "items", "i", "", DefaultMaxItems,
		"max number of search results to return", false)
	s.AddSetting(&s.Reindex, "reindex", "r", "", false,
		"rebuild the search index, precludes all other options", false)
	s.AddSetting(&s.Tap, "tap", "p", "", false,
		"include TAP files in search results", false)
	s.AddSetting(&s.Source, "source", "s", "", "repo",
		`source to search; "repo" or "zxdb"`, false)

	return s
}

//
type Search struct {
	Runner
	//
	Term    string
	Source  string
	Items   int
	Tap     bool
	Reindex bool
}

//
func (s *Search) Run() error {

	s.ParseSettings()

	if s.Reindex {
		if s.Term != "" || s.Items != DefaultMaxItems {
			return fmt.Errorf("reindex must not be used with any other options")
		}
		resp, err := s.apiCall("POST", "/reindex", false, nil)
		if err != nil {
			return err
		}
		defer resp.Close()
		msg, err := ioutil.ReadAll(resp)
		if err != nil {
			return err
		}

		fmt.Printf("%s\n", msg)
		return nil
	}

	if s.Term == "" {
		return fmt.Errorf("no search term given")
	}

	src := "repo"
	switch strings.ToLower(s.Source) {
	case "", "repo":
	case "zxdb":
		src = "zxdb"
	default:
		return fmt.Errorf("unknown search source: %s", s.Source)
	}

	exclude := "tap"
	if s.Tap {
		exclude = ""
	}

	resp, err := s.apiCall("GET",
		fmt.Sprintf("/search?items=%d&source=%s&exclude=%s&term=%s",
			s.Items, src, exclude, url.QueryEscape(s.Term)),
		false, nil)
	if err != nil {
		return err
	}
	defer resp.Close()

	fmt.Println()
	if _, err := io.Copy(os.Stdout, resp); err != nil {
		return err
	}

	return nil
}
