/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2021, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package run

import (
	"bufio"
	"fmt"
	"io"
	"net/url"
	"os"
	"path"
)

// -
func NewDump() *Dump {

	d := &Dump{}
	d.Runner = *NewRunner(
		`dump [-d|--drive {drive}] [-f|--file {file}] [-i|--input {file}]
       [-o|--output {file}] [-a|--address {address}]`,
		"dump cartridge & files from file or daemon", `
Use the dump command to output a hex dump for a complete cartridge or a file on
the cartridge. The cartridge to use can be either from one of the drives in the
daemon, or from a file.`,
		"", `- When dumping files from a CP/M  file system,  the user defaults to  user 0. To
  access files of a particular user, use the following path notation:

    /{user id}/{filename}

`+runnerHelpEpilogue, d.Run)

	d.AddBaseSettings()
	d.AddSetting(&d.Input, "input", "i", "", nil, "cartridge input file", false)
	d.AddSetting(&d.Drive, "drive", "d", "", 1, "drive number (1-8)", false)
	d.AddSetting(&d.File, "file", "f", "", nil, `file on cartridge to dump; see below about accessing
files of particular users on CP/M file systems`, false)
	d.AddSetting(&d.Output, "output", "o", "", nil,
		`output file for dumping raw content of selected file;
use '-' for storing under same name in current working
directory; using --output implies --file`, false)

	return d
}

// -
type Dump struct {
	//
	Runner
	//
	Drive  int
	Input  string
	File   string
	Output string
}

// -
func (d *Dump) Run() error {

	d.ParseSettings()

	var out io.Writer = os.Stdout
	var raw bool

	if d.Output != "" {
		raw = true

		if d.File == "" {
			return fmt.Errorf(
				"setting output file implies selecting a file to dump")
		}

		if d.Output == "-" {
			d.Output = d.File
			if dir, file := path.Split(d.File); dir != "" {
				d.Output = file
			}
		}

		f, err := os.Create(d.Output)
		if err != nil {
			return err
		}
		defer CloseOrDie(f)

		bOut := bufio.NewWriter(f)
		defer FlushOrDie(bOut)

		out = bOut
	}

	if d.Input != "" {
		cart, err := readCartridge(d.Input)
		if err != nil {
			return err
		}
		return cart.Dump(out, d.File, raw)
	}

	if err := validateDrive(d.Drive); err != nil {
		return err
	}

	resp, err := d.apiCall("GET",
		fmt.Sprintf("/drive/%d/dump?file=%s&raw=%v",
			d.Drive, url.QueryEscape(d.File), raw), false, nil)
	if err != nil {
		return err
	}
	defer resp.Close()

	_, err = io.Copy(out, resp)
	return err
}
