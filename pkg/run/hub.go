/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2024, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package run

import (
	"fmt"

	"codeberg.org/xelalexv/oqtadrive/pkg/zxnet/hub"
)

// -
const PortHTTP = 8080
const PortHTTPS = 8443
const PathTLSKey = "./tls/tls.key"
const PathTLSCert = "./tls/tls.crt"

// -
func NewHub() *Hub {

	h := &Hub{}
	h.Runner = *NewRunner(`hub [-p|--port {port}] [-a|--addresses {max user addresses}]
      [-c|--cert {TLS certificate file} -k|--key {TLS private key}]
      [-l|--liberal] [-i|--insecure] [-w|--webhook {webhook URL}]`,
		"start hub server for managing Sinclair Network traffic", `
Use the hub command to start a hub server for managing Sinclair Network traffic
between OqtaDrive stations.

This is still work in progress and highly experimental!
`,
		"", runnerHelpEpilogue, h.Run)

	h.AddSetting(&h.Port, "port", "p", "", 0, "endpoint port; defaults to "+
		fmt.Sprintf("%d", PortHTTPS)+" for HTTPS, "+fmt.Sprintf("%d", PortHTTP)+
		" for HTTP", false)

	h.AddSetting(&h.MaxAddresses, "addresses", "a", "", hub.DefaultMaxAddressesPerPort,
		"max. number of addresses a user may request", false)

	h.AddSetting(&h.Liberal, "liberal", "l", "", false,
		`do not require users to register, allows login without
password and with any username; use only for testing in
local networks!`, false)
	h.AddSetting(&h.Insecure, "insecure", "i", "", false,
		`run hub with plain HTTP endpoint; use only for testing
in local networks!`, false)

	h.AddSetting(&h.Cert, "cert", "c", "", "",
		`path and name of TLS certificate in PEM format; defaults
to ./tls/tls.crt`, false)
	h.AddSetting(&h.Key, "key", "k", "", "",
		`path and name of TLS private key in PEM format; defaults
to ./tls/tls.key`, false)

	h.AddSetting(&h.Webhook, "webhook", "w", "OQTADRIVE_WEBHOOK", "",
		`Slack web hook URL for hub admin notifications
`, false)

	return h
}

// -
type Hub struct {
	//
	Runner
	//
	Insecure     bool
	Liberal      bool
	Port         int
	Cert         string
	Key          string
	Webhook      string
	MaxAddresses int
}

// -
func (h *Hub) Run() error {

	h.ParseSettings()

	if h.Port <= 0 {
		h.Port = PortHTTPS
		if h.Insecure {
			h.Port = PortHTTP
		}
	}

	if h.Cert == "" {
		h.Cert = PathTLSCert
	}
	if h.Key == "" {
		h.Key = PathTLSKey
	}

	return hub.NewHub(h.Insecure, h.Liberal, h.Port, h.MaxAddresses,
		h.Cert, h.Key, h.Webhook).Serve()
}
