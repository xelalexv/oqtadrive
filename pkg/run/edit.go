/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2023, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package run

import (
	"bufio"
	"fmt"
	"os"
	"path/filepath"

	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/base"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/format"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/format/qdos"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/fs"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/if1"
)

// -
func NewEdit() *Edit {

	e := &Edit{}
	e.Runner = *NewRunner(
		"edit [-i|--input {file}] -o|--output {file} [-p|--operation {add}] [-c|--cpm] {files...}",
		"edit cartridge", `
Use the edit command to add & remove files to/from a cartridge.
This is still work in progress!`,
		"", runnerHelpEpilogue, e.Run)

	e.AddBaseSettings()
	e.AddSetting(&e.Input, "input", "i", "", "",
		`cartridge input file to edit; if not specified, a new
cartridge  will be  created,  with type  according to
output file`, false)
	e.AddSetting(&e.Output, "output", "o", "", nil, "cartridge output file", true)
	e.AddSetting(&e.Operation, "operation", "p", "", "add", "edit operation", false)
	e.AddSetting(&e.CPM, "cpm", "c", "", false,
		`when editing a new cartridge,  create in CP/M format;
ignored when editing a cartridge from input file`, false)

	return e
}

// -
type Edit struct {
	//
	Runner
	//
	Input     string
	Output    string
	Operation string
	CPM       bool
}

// -
func (e *Edit) Run() error {

	e.ParseSettings()

	switch e.Operation {
	case "", "add":
	default:
		return fmt.Errorf(
			"invalid operation '%s' - valid operations are: add", e.Operation)
	}

	var cart *base.Cartridge
	var err error

	name, typ, comp := format.SplitNameTypeCompressor(e.Output)
	if comp != "" {
		return fmt.Errorf("compressed output is not supported")
	}

	if e.Input == "" {
		var specials string
		if e.CPM {
			if typ != "mdr" {
				return fmt.Errorf("CP/M not supported for selected output")
			}
			fmt.Println("creating CP/M cartridge")
			specials = if1.SpecialsCPM
		}
		cart, err = createCartridge(name, typ, specials)

	} else {
		if e.CPM {
			return fmt.Errorf(
				"ignoring --cpm flag when using cartridge from input file")
		}
		cart, err = readCartridge(e.Input)
	}

	if err != nil {
		return err
	}

	fs := cart.FS()

	for _, a := range e.Args {
		if filepath.Ext(a) == ".zip" {
			processQDOS(cart, a)
			continue
		}

		var f string
		var t string

		if parts := filepath.SplitList(a); len(parts) == 0 {
			continue
		} else {
			a = parts[0]
			f = filepath.Base(a)
			if len(parts) > 1 {
				t = parts[1]
			}
		}

		fmt.Printf("adding file '%s'\n", f)
		data, err := os.ReadFile(a)
		if err != nil {
			return fmt.Errorf("error reading file %s: %v", a, err)
		}
		if err = addFile(fs, f, t, data, nil); err != nil {
			return err
		}
	}

	if e.Input != "" {
		if err := cart.Rename(name); err != nil {
			return err
		}
	}

	out, err := os.Create(e.Output)
	if err != nil {
		return err
	}
	defer CloseOrDie(out)

	form, err := format.NewFormat(typ, nil)
	if err != nil {
		return err
	}
	return form.Write(cart, out, true, nil)
}

// -
func addFile(fsys fs.FileSystem, name, typ string, data, header []byte) error {

	fd, err := fsys.Open(name, fs.WRITE, fsys.ParseFileType(typ))
	if err != nil {
		return fmt.Errorf("error opening target file: %v", err)
	}

	if len(header) > 0 {
		fmt.Println("setting header")
		fd.SetHeader(header)
	}

	if _, err = fd.Write(data); err != nil {
		return fmt.Errorf("error writing target file: %v", err)
	}
	return fd.Close()
}

// -
func processQDOS(cart *base.Cartridge, zip string) error {

	fmt.Printf("adding from ZIP file '%s'\n", zip)

	q := qdos.QDOS{Cart: cart}
	f, err := os.Open(zip)
	if err != nil {
		return err
	}
	defer CloseOrDie(f)

	return q.Load(bufio.NewReader(f))
}
