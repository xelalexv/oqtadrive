/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2022, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/
package info

import (
	"bytes"
	"fmt"

	"codeberg.org/xelalexv/oqtadrive/pkg/util"
)

//
var qlQRbin = []byte{
	0x00, 0x00, 0x00, 0x00, 0x60, 0x00, 0x00, 0x0e, 0x00, 0x00,
	0x4a, 0xfb, 0x00, 0x07, 0x6f, 0x71, 0x74, 0x61, 0x71, 0x72,
	0x61, 0x00, 0x00, 0x2e, 0x34, 0x3a, 0x00, 0x48, 0x20, 0x56,
	0x42, 0x87, 0x43, 0xfb, 0x78, 0x42, 0x70, 0x2e, 0x12, 0x3c,
	0x00, 0x07, 0x4e, 0x43, 0x50, 0x87, 0x51, 0xca, 0xff, 0xf0,
	0x70, 0x01, 0x36, 0x3c, 0xff, 0xff, 0x4e, 0x43, 0x70, 0x05,
	0x72, 0xff, 0x42, 0x83, 0x4e, 0x41, 0x60, 0xf6, 0x43, 0xfa,
	0x00, 0x0c, 0x34, 0x78, 0x00, 0xc6, 0x4e, 0x92, 0x2c, 0x88,
	0x4e, 0x75, 0x02, 0x02, 0x00, 0x07, 0x02, 0x00, 0x01, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x12, 0x34, 0x56, 0x78,
}

//
func qlQRProgBIN(qr string) ([]byte, error) {

	var b bytes.Buffer
	b.WriteByte(0)
	b.WriteByte(0)

	width := 6
	height := 4
	xStart := 10
	x := xStart
	y := 10
	blk := 0
	count := 0

	for _, c := range qr + "\n" {

		switch c {

		case '\n':
			if blk > 0 {
				if _, err := util.WriteUInt16s(
					&b, []int{x - blk, height, blk, y}, true); err != nil {
					return nil, err
				}
				count++
				blk = 0
			}
			x = xStart
			y += height
			continue

		case 'w':
			if blk == 0 {
				blk = x
			}

		default:
			if blk > 0 {
				if _, err := util.WriteUInt16s(
					&b, []int{x - blk, height, blk, y}, true); err != nil {
					return nil, err
				}
				count++
				blk = 0
			}
		}
		x += width
	}

	data := b.Bytes()
	if len(data) > 1 {
		count-- // FIXME correct?
		data[0] = byte(count >> 8)
		data[1] = byte(count)
	}

	return append(qlQRbin, data...), nil
}

//
const qlQRbasic = `100 PAPER 0:CLS
110 PRINT "IP: %s"
120 RESTORE
130 x=10
140 y=32
150 REPeat loop
160 IF EOF:EXIT loop
170 READ b
180 IF b=-1:x=10:y=y+4:NEXT loop
190 FOR j=1 TO 8
200 IF (b MOD 2)=1:BLOCK 6,4,x,y,7
210 x=x+6
220 b=b DIV 2
230 NEXT j
240 END REPeat loop
250 DATA `

//
func qlQRProgBASIC(ip, qr string) []byte {

	var b bytes.Buffer
	b.WriteString(fmt.Sprintf(qlQRbasic, ip))

	bits := 0
	blk := 0

	for _, c := range qr {
		if c == '\n' {
			if bits > 0 {
				blk >>= 8 - bits
				b.WriteString(fmt.Sprintf("%d,", blk))
				bits = 0
				blk = 0
			}
			b.WriteString("-1,")
		} else {
			if blk >>= 1; c == 'w' {
				blk |= 0x80
			}
			if bits++; bits == 8 {
				b.WriteString(fmt.Sprintf("%d,", blk))
				bits = 0
				blk = 0
			}
		}
	}

	b.WriteString("0\n")
	return b.Bytes()
}
