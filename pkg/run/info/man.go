/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2023, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/
package info

//
const ManIF1 = `Client Control

Get drive list:
 MOVE "m";{drive};"drv" TO #2

Get config:
 MOVE "m";{drive};"cfg" TO #2

Run command:
 OPEN #4;"m";{drive};"oqctl":
 PRINT #4;"{command}": CLOSE #4

Different stream can be used,
but stay away from internal
streams #0 through #3.

Supported commands:
 load | ld {drive} {file}
 save | sv {drive} [{file}]
 rename|rn {drive} {name}
 reorg|ro  {drive}
 unload|ul {drive} [force]
 map | mp  off|{{start} {end}}
 resync|rs [if1|ql]
 config|cf {r|rumble {level}}
 wifi {ssid}[:{passphrase}]
 poweroff

`

//
const ManQL = `Client Control

Get drive list:
 copy mdv{drive}_drv,con_

Get config:
 copy mdv{drive}_cfg,con_

Run command:
 open_new #4,mdv{drive}_oqctl:
  print #4,'{command}': close #4

Different stream can be used, but
stay away from internal streams
#0 through #3.

Supported commands:
 load | ld   {drive} {file}
 save | sv   {drive} [{file}]
 rename | rn {drive} {name}
 reorg | ro  {drive}
 unload | ul {drive} [force]
 map | mp    off|{{start} {end}}
 resync | rs [if1|ql]
 config | cf {r|rumble {level}}
 wifi {ssid}[:{passphrase}]
 poweroff

`
