/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2022, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/
package info

import (
	"fmt"
	"net"
	"strings"

	"github.com/mdp/qrterminal/v3"
)

//
func generateQRCodes(ip, address string) (if1, qlBasic, qlBin []byte) {

	qr := generateQRCode(ip, address, false)
	if len(qr) > 32 {
		qr = qr[32:] // remove black bar at top
	}
	if1 = []byte(qr)

	qr = generateQRCode(ip, address, true)
	qlBasic = qlQRProgBASIC(ip, qr)

	var err error
	if qlBin, err = qlQRProgBIN(qr); err != nil {
		qlBin = nil
	}

	return if1, qlBasic, qlBin
}

//
func generateQRCode(ip, address string, ql bool) string {

	var b strings.Builder
	cfg := qrterminal.Config{
		HalfBlocks:     !ql,
		BlackChar:      "\x8f", // Spectrum specific codes
		WhiteChar:      "\x80", //
		BlackWhiteChar: "\x83", //
		WhiteBlackChar: "\x8c", //
		QuietZone:      3,
		Level:          qrterminal.L,
		Writer:         &b,
	}

	if ql {
		cfg.BlackChar = "b"
		cfg.WhiteChar = "w"
		cfg.QuietZone = 0
	}

	port := "8888"
	if _, p, err := net.SplitHostPort(address); err == nil {
		port = p
	}
	qrterminal.GenerateWithConfig(
		fmt.Sprintf("http://%s:%s", ip, port), cfg)
	return b.String()
}
