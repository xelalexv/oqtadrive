/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2022, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package info

import (
	"net"
	"sync"
	"time"

	log "github.com/sirupsen/logrus"

	"codeberg.org/xelalexv/oqtadrive/pkg/control"
	"codeberg.org/xelalexv/oqtadrive/pkg/util"
)

// -
const keyLatestRelease = "latestRelease"
const keyIPAddresses = "ipAddresses"
const keyQRIF1 = "webUIQRif1"
const keyQRQL = "webUIQRql"
const keyQRQLbin = "webUIQRqlBIN"

// -
var config *sync.Map

// -
func init() {
	config = &sync.Map{}
}

// -
func RefreshConfig(address string, checkLatestRelease bool) time.Duration {

	log.Debug("refreshing config info")

	ips := util.GetIPAddresses()
	oldIPs := IPAddresses()

	if len(ips) == 0 {
		if len(oldIPs) > 0 { // clean up if we lost our IPs
			setConfigItem(keyIPAddresses, ips)
			setConfigItem(keyQRIF1, "")
			setConfigItem(keyQRQL, "")
			setConfigItem(keyQRQLbin, "")
		}
		return 10 * time.Second
	}

	// only get latest release once during a run, and only if requested
	if checkLatestRelease {
		if r := LatestRelease(); r == "" || r == control.UnknownRelease {
			setConfigItem(keyLatestRelease, control.GetLatestRelease())
		}
	}

	// regenerate QR code only if we got a new IP address
	if newIP := ips[0].String(); len(oldIPs) == 0 || newIP != oldIPs[0].String() {
		setConfigItem(keyIPAddresses, ips)
		setQRCodes(newIP, address)
	}

	return time.Minute
}

// -
func IPAddresses() []net.IP {
	if v, ok := config.Load(keyIPAddresses); ok {
		if ips, ok := v.([]net.IP); ok {
			return ips
		}
	}
	return []net.IP{}
}

// -
func LatestRelease() string {
	return getConfigItemString(keyLatestRelease)
}

// -
func WebUIQR(if1, bin bool) []byte {
	if if1 {
		return getConfigItemBytes(keyQRIF1)
	}
	if bin {
		return getConfigItemBytes(keyQRQLbin)
	}
	return getConfigItemBytes(keyQRQL)
}

// -
func getConfigItemString(key string) string {
	if v := getConfigItem(key); v != nil {
		if str, ok := v.(string); ok {
			return str
		}
	}
	return ""
}

// -
func getConfigItemBytes(key string) []byte {
	if v := getConfigItem(key); v != nil {
		if b, ok := v.([]byte); ok {
			return b
		}
	}
	return nil
}

// -
func getConfigItem(key string) interface{} {
	if v, ok := config.Load(key); ok {
		return v
	}
	return nil
}

// -
func setConfigItem(key string, i interface{}) {
	config.Store(key, i)
}

// -
func setQRCodes(ip, address string) {
	if1, ql, qlbin := generateQRCodes(ip, address)
	setConfigItem(keyQRIF1, if1)
	setConfigItem(keyQRQL, ql)
	setConfigItem(keyQRQLbin, qlbin)
}
