/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2021, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package run

import (
	"fmt"
	"io"

	"codeberg.org/xelalexv/oqtadrive/pkg/daemon"
	"codeberg.org/xelalexv/oqtadrive/pkg/util"
)

// -
const (
	runnerHelpPrologue = ""
	runnerHelpEpilogue = `- Elements noted in  curly braces {}  are mandatory and need to be replaced with
  appropriate values, omitting the curly braces.  Elements in square brackets []
  are optional and replaced with desired values when needed, omitting the square
  brackets. Pipe symbols | denote mutually exclusive elements.

- When an option can be set via environment variable, the variable name is given
  in parenthesis  at the end of  the option's explanation.  Note however that an
  option, when specified overrides an environment variable.

- Boolean options (flags) without a parameter mean 'true'. To unset, specify the
  flag  with a  parameter  'false', e.g.  '--foo=false' or  '-f=false' (note the
  equal sign).

- If the API endpoint is password protected, set user credentials in environment
  variable  OQTADRIVE_CREDENTIALS,  in  format '{user}:{password}', for allowing
  commands to access the API.
`
)

/*
NewRunner creates a base runner for commands to use. The parameters are passed
to the base command wrapped by this runner.
*/
func NewRunner(use, short, long, helpPrologue, helpEpilogue string,
	exec func() error) *Runner {
	return &Runner{
		Command: *NewCommand(
			use, short, long, helpPrologue, helpEpilogue, exec),
	}
}

// -
type Runner struct {
	//
	Command
	//
	Address string
}

// -
func (r *Runner) AddBaseSettings() {
	// Implementation Note: This cannot be included in NewRunner, but rather has
	// to be called from the top level command type. Otherwise, we will confuse
	// Cobra/Viper and the settings will not be filled with their values.
	r.AddSetting(&r.Address, "address", "a", "OQTADRIVE_ADDRESS",
		fmt.Sprintf(":%d", util.DefaultPort),
		`listen address and port of daemon's API server,
format: {host}[:{port}]`, false)
}

// -
func (r *Runner) apiCall(method, path string, json bool,
	body io.Reader) (io.ReadCloser, error) {
	return util.ApiCall(r.Address, method, path, json, body)
}

// -
func validateDrive(d int) error {
	if d < 1 || d > daemon.DriveCount {
		return fmt.Errorf(
			"invalid drive number: %d; valid numbers are 1 through %d",
			d, daemon.DriveCount)
	}
	return nil
}
