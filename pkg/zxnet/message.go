/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2024, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package zxnet

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"strings"
	"time"
)

// -
type MessageOption func(m *Message, buf *bytes.Buffer) (*bytes.Buffer, error)

// -
func Pack(iris, self int, payload string) ([]*Message, error) {

	if payload == "" {
		return nil, nil
	}

	ret := make([]*Message, len(payload)/MaxDataLength+1)

	for ix, _ := range ret {
		s := ix * MaxDataLength
		e := min(s+MaxDataLength, len(payload))
		p, err := NewPacket(iris, self, ix, ix == len(ret)-1, []byte(payload[s:e]))
		if err != nil {
			return nil, err
		}
		ret[ix], err = NewMessage(WithCommand(CmdForward), WithBytes(p.Raw()))
		if err != nil {
			return nil, err
		}
	}

	return ret, nil
}

// -
func NewMessage(opts ...MessageOption) (*Message, error) {

	msg := &Message{created: time.Now()}
	var buf *bytes.Buffer
	var err error

	for _, opt := range opts {
		if buf, err = opt(msg, buf); err != nil {
			return nil, fmt.Errorf("error creating message: %v", err)
		}
	}

	if buf != nil {
		msg.raw = buf.Bytes()
	}
	return msg, nil
}

// -
func FromRaw(r []byte) MessageOption {
	return func(m *Message, buf *bytes.Buffer) (*bytes.Buffer, error) {
		if buf != nil {
			return nil,
				fmt.Errorf("creating message form raw does not allow buffer")
		}
		if len(r) < CommandLength {
			return nil, fmt.Errorf("not a message, too short: %d", len(r))
		}
		m.raw = r
		cmd, err := parseCommand(m.CommandBytes())
		if err != nil {
			return nil, fmt.Errorf("invalid command in message: %v", m.Command())
		}
		m.command = cmd
		return nil, nil
	}
}

// -
func Blank(length int) MessageOption {
	return func(m *Message, buf *bytes.Buffer) (*bytes.Buffer, error) {
		m.raw = make([]byte, CommandLength+length)
		return nil, nil
	}
}

// -
func WithCommand(cmd Command) MessageOption {
	return func(m *Message, buf *bytes.Buffer) (*bytes.Buffer, error) {
		m.command = cmd
		var err error
		if buf, err = m.chooseRawOrBuffer(buf); err == nil {
			if buf == nil {
				copy(m.raw, cmd.Bytes())
			} else {
				_, err = buf.Write(cmd.Bytes())
			}
		}
		return buf, err
	}
}

// -
func WithPayload(p any) MessageOption {
	return func(m *Message, buf *bytes.Buffer) (*bytes.Buffer, error) {
		if len(m.raw) > 0 {
			return buf, fmt.Errorf("raw message must not be set when encoding payload")
		}
		buf, _ = m.chooseRawOrBuffer(buf)
		return buf, gob.NewEncoder(buf).Encode(p)
	}
}

// -
func WithBytes(b []byte) MessageOption {
	return func(m *Message, buf *bytes.Buffer) (*bytes.Buffer, error) {
		var err error
		if buf, err = m.chooseRawOrBuffer(buf); err == nil {
			if buf == nil {
				copy(m.raw[CommandLength:], b)
			} else {
				_, err = buf.Write(b)
			}
		}
		return buf, err
	}
}

// -
type Message struct {
	command Command
	raw     []byte
	created time.Time
}

// -
func (m *Message) chooseRawOrBuffer(buf *bytes.Buffer) (*bytes.Buffer, error) {
	if len(m.raw) > 0 && buf != nil {
		return nil, fmt.Errorf("must not use raw and buffer at the same time")
	}
	if len(m.raw) > 0 {
		return nil, nil
	}
	if buf == nil {
		buf = new(bytes.Buffer)
	}
	return buf, nil
}

// -
func (m *Message) Created() time.Time {
	return m.created
}

// -
func (m *Message) Expired(d time.Duration) bool {
	return m.created.Add(d).Before(time.Now())
}

// -
func (m *Message) Command() Command {
	return m.command
}

// -
func (m *Message) CommandBytes() []byte {
	return m.raw[:CommandLength]
}

// -
func (m *Message) CommandStr() string {
	return strings.TrimSpace(string(m.CommandBytes()))
}

// -
func (m *Message) Payload() []byte {
	return m.raw[CommandLength:]
}

// -
func (m *Message) DecodePayload(t any) error {

	buf := bytes.NewBuffer(m.Payload())
	dec := gob.NewDecoder(buf)

	if err := dec.Decode(t); err != nil {
		return fmt.Errorf("error decoding payload: %v", err)
	}
	return nil
}

// -
func (m *Message) Raw() []byte {
	return m.raw
}
