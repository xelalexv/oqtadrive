/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2024, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package hub

import (
	"bytes"
	"context"
	"crypto/tls"
	"fmt"
	"net/http"
	"sort"

	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
	"github.com/kylelemons/godebug/pretty"
	log "github.com/sirupsen/logrus"

	"codeberg.org/xelalexv/oqtadrive/pkg/util"
	"codeberg.org/xelalexv/oqtadrive/pkg/zxnet"
	"codeberg.org/xelalexv/oqtadrive/pkg/zxnet/chat"
)

// -
const (
	PathWebSocket = "/port/"

	FirstAddress = 9
	LastAddress  = 240

	DefaultMaxAddressesPerPort = 5
	MinimumOqtaDriveVersion    = "0.7.0"

	SvcChat    = 63
	SvcEcho    = 64
	SvcEchoAlt = 62
)

// -
func NewHub(insecure, liberal bool, port, maxAddresses int,
	cert, key, notifyWebhook string) *Hub {

	h := &Hub{
		insecure:      insecure,
		liberal:       liberal,
		httpPort:      port,
		cert:          cert,
		key:           key,
		notifyWebhook: notifyWebhook,
		maxAddresses:  maxAddresses,
	}

	h.setup()
	return h
}

// -
type Hub struct {
	//
	insecure     bool
	liberal      bool
	httpPort     int
	cert         string
	key          string
	maxAddresses int
	//
	notifyWebhook string
	htpasswd      *util.HTPASSWD
	users         map[string][]*port
	addresses     []*port
	services      map[int]service
	//
	lck *util.Lock
}

// -
func (h *Hub) setup() {

	h.users = make(map[string][]*port)
	h.addresses = make([]*port, 256)
	h.services = make(map[int]service)
	h.lck = util.NewLock(nil, nil)

	h.services[SvcEcho] = &svcEcho{}
	h.services[SvcEchoAlt] = h.services[SvcEcho]
	h.services[SvcChat] = &svcChat{chat: chat.NewChat(h, SvcChat)}
}

// -
func (h *Hub) Serve() (err error) {

	router := mux.NewRouter().StrictSlash(true)
	addHTTPRoute(router, "status", "GET", "/", h.status)

	router.PathPrefix(PathWebSocket).Handler(
		requestLogger(http.HandlerFunc(h.wsPort), "port"))

	srv := &http.Server{
		Addr:    fmt.Sprintf(":%d", h.httpPort),
		Handler: router,
	}

	if !h.insecure {
		srv.TLSConfig = getServerTLSConfig()
		srv.TLSNextProto = make(
			map[string]func(*http.Server, *tls.Conn, http.Handler), 0)
	}

	if h.liberal {
		router.Use(util.NewUserOnlyAuth().Auth)
	} else {
		if h.htpasswd, err = util.NewHTPASSWD("./htpasswd", true); err != nil {
			return err
		}
		defer h.htpasswd.Stop()
		router.Use(h.htpasswd.Auth)
	}

	log.WithFields(log.Fields{
		"port":     h.httpPort,
		"liberal":  h.liberal,
		"insecure": h.insecure}).Info("Sinclair Network hub starting endpoint")

	if h.insecure {
		err = srv.ListenAndServe()
	} else {
		err = srv.ListenAndServeTLS(h.cert, h.key)
	}
	if err != nil && err != http.ErrServerClosed {
		return err
	}
	return nil
}

// -
func (h *Hub) AdminNotify(service, text string) error {

	if h.notifyWebhook == "" {
		return nil
	}

	logger := log.WithField("service", service)
	logger.Info("sending admin notification")

	client := &http.Client{}
	msg := fmt.Sprintf(`{"text": "%s"}`, text)
	req, err := http.NewRequest("POST",
		h.notifyWebhook, bytes.NewBuffer([]byte(msg)))
	if err != nil {
		return err
	}

	if resp, err := client.Do(req); err != nil {
		logger.Errorf("error sending admin notification: %v", err)
		return err
	} else {
		return resp.Body.Close()
	}
}

// -
func (h *Hub) status(w http.ResponseWriter, req *http.Request) {
	w.Write([]byte("This is your friendly Sinclair Network hub\n"))
}

// -
func (h *Hub) wsPort(w http.ResponseWriter, r *http.Request) {

	log.Info("user connecting")

	usr, _, ok := r.BasicAuth()
	if !ok {
		util.HandleError(fmt.Errorf("no user"), http.StatusUnauthorized, w)
		return
	}

	ver, bld, ok := util.GetOqtaDriveVersion(r)
	if !ok {
		util.HandleError(fmt.Errorf("unsupported agent"), http.StatusForbidden, w)
		return
	}

	if bld != "" {
		// not official release, e.g. development build, expecting exact match
		if v := fmt.Sprintf("%s-%s", ver, bld); v != util.OqtaDriveVersion {
			util.HandleError(fmt.Errorf(
				"hub runs development build %s, you're running %s, not compatible",
				util.OqtaDriveVersion, v), http.StatusForbidden, w)
			return
		}
	} else if util.CompareVersions(ver, MinimumOqtaDriveVersion) < 0 {
		util.HandleError(
			fmt.Errorf("unsupported client version, upgrade to %s or higher",
				MinimumOqtaDriveVersion), http.StatusForbidden, w)
		return
	}

	up := websocket.Upgrader{
		CheckOrigin: func(r *http.Request) bool {
			return true // FIXME origin check?
		},
	}
	c, err := up.Upgrade(w, r, nil)
	if err != nil {
		log.Errorf("websocket upgrade: %v", err)
		return
	}
	defer c.Close()

	logger := log.WithFields(log.Fields{"user": usr, "remote": r.RemoteAddr})
	logger.Info("user connected")

	h.AdminNotify("hub", fmt.Sprintf("user %s logged in to hub", usr))

	p := newPort(h, usr, fmt.Sprintf("v%s", ver), c)
	h.addPort(usr, p)

	defer func() {
		if e := recover(); e != nil {
			logger.Errorf("closing port due to unrecoverable error: %v", e)
		}
		h.removePort(usr, p)

		logger.Info("user disconnected")
		h.broadcastUserList("user disconnect")

		h.AdminNotify("hub", fmt.Sprintf("user %s logged out from hub", usr))
	}()

	p.listen() // blocks until port is closed
}

// -
func (h *Hub) portsByUser(u string) []*port {
	h.lock()
	defer h.unlock()
	return h.users[u]
}

// -
func (h *Hub) portByAddress(a int) *port {
	if a <= 0 || a >= len(h.addresses) {
		return nil
	}
	h.lock()
	defer h.unlock()
	return h.addresses[a]
}

// -
func (h *Hub) PortForAddress(a int) zxnet.Port {
	port := h.portByAddress(a)
	if port == nil {
		return nil // explicitly return Nil interface
	}
	return port
}

// -
func (h *Hub) broadcastUserList(reason string) {
	log.WithField("reason", reason).Info("broadcasting user list")
	msg, err := h.userList()
	if err != nil {
		log.Errorf("cannot broadcast user list: %v", err)
		return
	}
	h.broadcast(msg, nil)
}

// -
func (h *Hub) broadcast(msg *zxnet.Message, origin *port) {

	if msg == nil {
		return
	}

	h.lock()
	defer h.unlock()

	for _, ports := range h.users {
		for _, p := range ports {
			if p != nil && p != origin {
				if !p.queueMessage(msg) {
					log.WithField("user", p.user).Warn(
						"destination port full, dropping broadcast message")
				}
			}
		}
	}
}

// -
func (h *Hub) addPort(usr string, p *port) {
	h.lock()
	defer h.unlock()
	h.users[usr] = append(h.users[usr], p)
}

// -
func (h *Hub) removePort(usr string, port *port) {

	h.lock()
	defer h.unlock()

	ports := h.users[usr]

	for ix, p := range ports {
		if p == port {
			h.svcDisconnect(usr, p.addresses())
			h.freeAddresses(usr, p.addresses())
			if l := len(ports) - 1; l == 0 {
				delete(h.users, usr)
			} else {
				ports[ix] = ports[l]
				ports[l] = nil
				h.users[usr] = ports[:l]
			}
			break
		}
	}
}

// user list encoded in message for putting on the wire
func (h *Hub) userList() (*zxnet.Message, error) {

	h.lock()
	defer h.unlock()

	lst := make([]*User, len(h.users))
	ix := 0
	for user, ports := range h.users {
		u := &User{Name: user}
		for _, p := range ports {
			u.Addresses = append(u.Addresses, p.addresses()...)
		}
		sort.Ints(u.Addresses)
		lst[ix] = u
		ix++
	}

	return zxnet.NewMessage(
		zxnet.WithCommand(zxnet.CmdUserList), zxnet.WithPayload(lst))
}

// -
func (h *Hub) assignAddresses(p *port, count int) error {

	if p == nil {
		return nil
	}

	addr := p.addresses()
	need := count - len(addr)
	if need == 0 {
		return nil
	}

	h.lock()
	defer h.unlock()

	if need < 0 { // de-assign surplus addresses
		p.setAddresses(addr[:count])
		h.freeAddresses(p.user, addr[count:])

	} else if need > 0 { // assign missing number of addresses

		available := h.maxAddresses
		for _, p := range h.users[p.user] {
			available -= len(p.addresses())
		}
		if need = min(need, available); need < 1 {
			return fmt.Errorf("address quota exceeded for user %s", p.user)
		}

		var a []int
		for ix := FirstAddress; ix <= LastAddress; ix++ {
			if h.addresses[ix] == nil {
				h.addresses[ix] = p
				a = append(a, ix)
				if len(a) == need {
					log.WithField("user", p.user).Infof("assigned addresses %v", a)
					addr = append(addr, a...)
					sort.Ints(addr)
					p.setAddresses(addr)
					break
				}
			}
		}
	}

	return nil
}

// -
func (h *Hub) freeAddresses(usr string, addr []int) {
	log.WithField("user", usr).Infof("freed addresses %v", addr)
	for _, a := range addr {
		if FirstAddress <= a && a <= LastAddress {
			h.addresses[a] = nil
		}
	}
}

// -
func (h *Hub) svcDisconnect(usr string, addr []int) {
	for _, svc := range h.services {
		svc.disconnect(addr)
	}
}

// -
func (h *Hub) lock() {
	ctx, cancel := context.WithCancel(context.Background())
	h.lck.Acquire(ctx)
	cancel()
}

// -
func (h *Hub) unlock() {
	h.lck.Release()
}

// -
func addHTTPRoute(r *mux.Router, name, method, pattern string,
	handler http.HandlerFunc) {
	r.Methods(method).
		Path(pattern).
		Name(name).
		Handler(requestLogger(handler, name))
}

// -
func requestLogger(inner http.Handler, name string) http.Handler {

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		log.WithFields(log.Fields{
			"remote": r.RemoteAddr,
			"method": r.Method,
			"path":   r.RequestURI,
		}).Infof("REQ BEGIN | %s", name)

		pretty.Print(r.Header)

		inner.ServeHTTP(w, r)

		log.WithFields(log.Fields{
			"remote": r.RemoteAddr,
			"method": r.Method,
			"path":   r.RequestURI,
		}).Infof("REQ END   | %s", name)

		log.Info()
	})
}

// -
func getServerTLSConfig() *tls.Config {

	return &tls.Config{
		MinVersion: tls.VersionTLS12,
		CurvePreferences: []tls.CurveID{
			tls.CurveP521, tls.CurveP384, tls.CurveP256},
		PreferServerCipherSuites: true,
		CipherSuites: []uint16{
			tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA,
			tls.TLS_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_RSA_WITH_AES_256_CBC_SHA,
		},
	}
}
