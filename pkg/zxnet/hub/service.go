/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2024, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package hub

import (
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"

	"codeberg.org/xelalexv/oqtadrive/pkg/zxnet"
	"codeberg.org/xelalexv/oqtadrive/pkg/zxnet/chat"
	"codeberg.org/xelalexv/oqtadrive/pkg/zxnet/sttp"
)

// -
type service interface {
	// FIXME support multi-message requests; we need to collect all
	// packets that belong to the request first
	handle(req *zxnet.Message) ([]*zxnet.Message, error)

	disconnect(stations []int)
}

// -
func packetFromMessage(m *zxnet.Message) (*zxnet.Packet, error) {
	return zxnet.NewPacketFromRaw(m.Payload())
}

// -
func serviceLogger(svc string, p *zxnet.Packet) *log.Entry {
	return log.WithFields(
		log.Fields{"service": svc, "from": p.Self(), "to": p.IRIS()})
}

// -
type svcEcho struct{}

// -
func (s *svcEcho) handle(m *zxnet.Message) ([]*zxnet.Message, error) {

	p, err := packetFromMessage(m)
	if err != nil {
		return nil, err
	}
	logger := serviceLogger("echo", p)
	logger.Debug("echoing packet")

	self := p.Self()
	iris := p.IRIS()

	p.SetIRIS(self)
	p.SetSelf(iris)
	p.FixHeaderChecksum()
	time.Sleep(2 * time.Second)

	return []*zxnet.Message{m}, nil
}

// -
func (s *svcEcho) disconnect(stations []int) {}

// -
type svcChat struct {
	chat *chat.Chat
}

// -
func (s *svcChat) handle(m *zxnet.Message) ([]*zxnet.Message, error) {

	p, err := packetFromMessage(m)
	if err != nil {
		return nil, err
	}

	logger := serviceLogger("chat", p)
	logger.Debug("handling chat request")

	self := int(p.Self())
	iris := int(p.IRIS())

	resp := &sttp.Response{}
	req, err := sttp.NewRequest(self, string(p.Data()))
	if err == nil {
		err = s.chat.Handle(req, resp)
	}
	if err != nil {
		return nil, fmt.Errorf("error handling chat request: %v", err)
	}

	var ret []*zxnet.Message
	if resp.Data() != "" {
		ret, err = zxnet.Pack(self, iris, resp.Data())
	}

	if err != nil {
		return nil, fmt.Errorf("dropping chat response: %v", err)
	}
	return ret, nil
}

// -
func (s *svcChat) disconnect(stations []int) {
	s.chat.Disconnect(stations)
}
