/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2024, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package hub

import (
	"context"
	"fmt"
	"strings"

	"github.com/gorilla/websocket"
	log "github.com/sirupsen/logrus"

	"codeberg.org/xelalexv/oqtadrive/pkg/zxnet"
)

// -
func newPort(hub *Hub, user, version string, conn *websocket.Conn) *port {
	return &port{
		hub:     hub,
		user:    user,
		version: version,
		conn:    conn,
		input:   make(chan *zxnet.Message, 32)}
}

// -
type port struct {
	hub     *Hub
	user    string
	version string
	addr    []int
	conn    *websocket.Conn
	//
	input chan *zxnet.Message
}

// -
func (p *port) addresses() []int {
	return p.addr
}

// -
func (p *port) setAddresses(a []int) {
	p.addr = a
}

// -
func (p *port) listen() {

	ctx, cancel := context.WithCancel(context.Background())

	go func() { // egress
	Loop:
		for {
			select {
			case <-ctx.Done():
				break Loop
			case msg := <-p.input:
				p.conn.WriteMessage(websocket.BinaryMessage, msg.Raw())
			}
		}
		log.WithField("user", p.user).Debug("port receiver routine stopping")
	}()

	for { // ingress
		mt, data, err := p.conn.ReadMessage()

		if mt == websocket.CloseMessage || err != nil {
			// "close 1005 (no status)" can be ignored
			if err != nil && strings.Index(err.Error(), "close 1005") == -1 {
				log.Errorf("websocket read: mt = %d, %v", mt, err)
			}
			break
		}

		if err := p.handleMessage(mt, data); err != nil {
			log.Errorf("error handling message: %v", err)
			break
		}
	}

	cancel()
}

// -
func (p *port) handleMessage(typ int, raw []byte) error {

	msg, err := zxnet.NewMessage(zxnet.FromRaw(raw))
	if err != nil {
		return err
	}

	log.WithFields(log.Fields{
		"command": msg.CommandStr(), "user": p.user}).Info("received command")

	switch msg.Command() {

	case zxnet.CmdPing:
		if msg, err = zxnet.NewMessage(
			zxnet.Blank(0), zxnet.WithCommand(zxnet.CmdPong)); err == nil {
			return p.conn.WriteMessage(typ, msg.Raw())
		}
		return err

	case zxnet.CmdForward:
		p.forward(msg)
		return nil

	case zxnet.CmdAssignAddresses:
		var count int
		msg.DecodePayload(&count)
		if err = p.hub.assignAddresses(p, count); err != nil {
			return fmt.Errorf("error assigning addresses: %v", err)
		}
		resp, err := zxnet.NewMessage(
			zxnet.WithCommand(zxnet.CmdAssignAddresses),
			zxnet.WithPayload(p.addr))
		if err != nil {
			return err
		}
		if err = p.conn.WriteMessage(typ, resp.Raw()); err != nil {
			return err
		}
		p.hub.broadcastUserList("address assign")
		return nil

	case zxnet.CmdUserList:
		resp, err := p.hub.userList()
		if err != nil {
			return err
		}
		return p.conn.WriteMessage(typ, resp.Raw())
	}

	return fmt.Errorf("unknown command: %s", msg.CommandStr())
}

// -
func (p *port) forward(msg *zxnet.Message) {

	if msg == nil {
		return
	}

	pkt, err := packetFromMessage(msg)
	if err != nil {
		log.Errorf("dropping invalid packet: %v", err)
		return
	}

	iris := int(pkt.IRIS())
	self := int(pkt.Self())
	logger := log.WithFields(log.Fields{"from": self, "to": iris})

	if 0 < iris && iris < FirstAddress {
		logger.Debug("dropping non-routeable packet")
		return
	}

	// check for impersonation and anonymous directed packets, but allow
	// anonymous broadcast
	if iris > 0 && self == 0 {
		logger.Debug("dropping anonymous packet")
		return
	}
	if self > 0 && p.hub.portByAddress(self) != p {
		logger.Debug("dropping packet with address impersonation")
		return
	}

	sent := true

	if iris == 0 { // broadcast
		p.hub.broadcast(msg, p)

	} else if svc := p.hub.services[iris]; svc != nil {
		// handled by hub service, response goes back to requester
		// retrieving services needs to be synchronized if we make them dynamic
		if m, err := svc.handle(msg); err != nil {
			logger.Warnf("error handling service request: %v", err)
		} else {
			sent = p.queueMessages(m)
		}

	} else { // normal forward
		if dest := p.hub.portByAddress(iris); dest == nil {
			logger.Debug("dropping packet to non-assigned address")
		} else {
			sent = dest.queueMessage(msg)
		}
	}

	if !sent {
		logger.Warn("destination port full, dropping packet")
	}
}

// -
func (p *port) QueuePayload(iris, self int, payload string) (bool, error) {
	if p == nil {
		return false, nil
	}
	msg, err := zxnet.Pack(iris, self, payload)
	if err != nil {
		return false, err
	}
	return p.queueMessages(msg), nil
}

// -
func (p *port) queueMessages(msg []*zxnet.Message) bool {
	for _, m := range msg {
		if !p.queueMessage(m) {
			return false
		}
	}
	return true
}

// -
func (p *port) queueMessage(msg *zxnet.Message) bool {
	select {
	case p.input <- msg:
		return true
	default:
		return false
	}
}
