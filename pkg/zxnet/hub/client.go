/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2024, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package hub

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strings"
	"sync/atomic"
	"time"

	"github.com/gorilla/websocket"
	log "github.com/sirupsen/logrus"

	"codeberg.org/xelalexv/oqtadrive/pkg/util"
	"codeberg.org/xelalexv/oqtadrive/pkg/zxnet"
)

// -
var certPool *x509.CertPool

// -
func NewClient() *Client {
	ret := &Client{
		ingress: make(chan *zxnet.Message, 32),
		egress:  make(chan *zxnet.Message, 32),
		control: make(chan *control),
		status:  make(chan error),
	}
	ret.reset()
	return ret
}

// -
type control struct {
	value    string
	argument int
}

// -
type Client struct {
	//
	hubUrl atomic.Value
	conn   *websocket.Conn
	//
	ingress chan *zxnet.Message
	egress  chan *zxnet.Message
	control chan *control
	status  chan error
	cancel  context.CancelFunc
	notify  func()
	mrum    *zxnet.Message
	//
	addresses atomic.Value
	users     atomic.Value
	update    atomic.Value
	ping      time.Time
}

// -
func (c *Client) reset() {

	c.addresses.Store([]int{})
	c.users.Store([]*User{})
	c.hubUrl.Store("")
	c.conn = nil

	for len(c.ingress) > 0 {
		<-c.ingress
	}
	for len(c.egress) > 0 {
		<-c.egress
	}

	c.mrum = nil
	c.updated()
}

// -
func (c *Client) Run(notify func()) {

	if c.notify = notify; notify == nil {
		c.notify = func() {}
	}

	go func() { // sender & control handler

		ticker := time.NewTicker(2 * time.Second)
		defer ticker.Stop()

		for {
			select {
			case ctrl := <-c.control:
				if c.doControl(ctrl) {
					return
				}

			case cmd := <-c.egress:
				if err := c.send(cmd); err != nil {
					log.Errorf("error sending message: %v", err)
				}

			case <-ticker.C:
				if len(c.ingress) > 0 { // remind daemon of waiting packets
					notify()
				}
				// if c.conn != nil {
				// 	c.Send(NewEmptyMessage(CmdPing, 0))
				// 	c.ping = time.Now()
				// 	log.Info("PING to hub")
				// }
			}
		}
	}()

	log.Info("hub client started")
}

// -
func (c *Client) Stop() error {
	c.control <- &control{value: "!EXIT"}
	return <-c.status
}

// FIXME decide what to set here, server or full URL
func (c *Client) Open(url string, addresses int) error {
	c.control <- &control{value: url, argument: addresses}
	return <-c.status
}

// -
func (c *Client) Close() error {
	c.control <- &control{value: ""}
	return <-c.status
}

// -
func (c *Client) HubURL() string {
	return c.hubUrl.Load().(string)
}

// -
func (c *Client) Update() time.Time {
	return c.update.Load().(time.Time)
}

// -
func (c *Client) updated() {
	c.update.Store(time.Now())
}

// -
func (c *Client) Addresses() []int {
	return c.addresses.Load().([]int)
}

// -
func (c *Client) Users() []*User {
	return c.users.Load().([]*User)
}

// -
func (c *Client) Next() (*zxnet.Message, bool) {
	select {
	case n := <-c.ingress:
		c.mrum = n
		return n, len(c.ingress) > 0
	default:
		return nil, false
	}
}

// -
func (c *Client) Repeat() (*zxnet.Message, bool) {
	return c.mrum, len(c.ingress) > 0
}

// -
func (c *Client) Send(msg *zxnet.Message) bool {
	select {
	case c.egress <- msg:
		return true
	default:
		return false
	}
}

// -
func (c *Client) doControl(ctrl *control) bool {

	var err error
	var exit bool

	switch ctrl.value {

	case "":
		c.close()

	case "!EXIT":
		c.close()
		log.Info("hub client exiting")
		exit = true

	default:
		c.close()
		err = c.open(ctrl)
	}

	c.status <- err
	return exit
}

// -
func (c *Client) open(ctrl *control) error {

	dial := websocket.Dialer{
		HandshakeTimeout: 45 * time.Second,
		TLSClientConfig:  getClientTLSConfig(),
	}

	req := http.Request{Header: http.Header{}}
	util.AddOqtaDriveAgent(&req)
	hubUrl := ctrl.value

	u, err := url.Parse(hubUrl)
	if err != nil {
		return err
	}

	if u.User != nil {
		creds := strings.SplitN(u.User.String(), ":", 2)
		user := creds[0]
		var pass string
		if len(creds) > 1 {
			pass = creds[1]
		}
		hubUrl = fmt.Sprintf("%s://%s%s", u.Scheme, u.Host, u.Path)
		req.SetBasicAuth(user, pass)
	}

	var res *http.Response

	if c.conn, res, err = dial.Dial(hubUrl, req.Header); err != nil {
		if res != nil && res.StatusCode >= 300 && res.StatusCode <= 399 {
			var redirect *url.URL
			redirect, err = res.Location()
			if err == nil {
				switch strings.ToLower(redirect.Scheme) {
				case "http":
					redirect.Scheme = "ws"
				case "https":
					redirect.Scheme = "wss"
				}
				log.Infof("following redirect: %s", redirect.String())
				c.conn, _, err = dial.Dial(redirect.String(), nil)
			}
		}

		if err != nil {
			if res != nil {
				if b, e := io.ReadAll(io.LimitReader(res.Body, 256)); e == nil {
					err = fmt.Errorf("%s (%d)",
						strings.TrimSpace(string(b)), res.StatusCode)
				}
			} else {
				err = fmt.Errorf("error dialing websocket: %v", err)
			}
			c.conn = nil
		}
	}

	if err == nil && c.conn != nil {
		log.WithField("hub", hubUrl).Info("opened connection to hub")
		c.hubUrl.Store(hubUrl)
		c.updated()

		var ctx context.Context
		ctx, c.cancel = context.WithCancel(context.Background())
		c.receive(ctx, c.conn)

		msg, err := zxnet.NewMessage(
			zxnet.WithCommand(zxnet.CmdAssignAddresses),
			zxnet.WithPayload(ctrl.argument))
		if err != nil {
			err = fmt.Errorf("error requesting addresses: %v", err)
		} else {
			c.Send(msg)
		}
	}

	return err
}

// -
func (c *Client) close() {

	if c.cancel != nil {
		c.cancel()
		c.cancel = nil
	}

	if c.conn == nil {
		return
	}

	log.WithField("hub", c.hubUrl).Info("closing connection to hub")

	if err := c.conn.WriteMessage(websocket.CloseMessage, []byte{}); err != nil {
		log.Errorf("error sending close message to hub: %v", err)
	}
	if err := c.conn.Close(); err != nil {
		log.Errorf("error closing hub connection: %v", err)
	}

	c.reset()
}

// -
func (c *Client) send(msg *zxnet.Message) error {

	if c.conn == nil {
		return nil
	}

	if msg.Command() == zxnet.CmdForward {
		pkt, err := zxnet.NewPacketFromRaw(msg.Payload())
		if err != nil {
			return fmt.Errorf("dropping invalid egress packet: %v", err)
		}

		if iris := pkt.IRIS(); 0 < iris && iris < FirstAddress {
			log.WithField("to", pkt.IRIS()).Warn(
				"dropping non-routeable egress packet")
			return nil
		}

		if !c.nat(pkt, true) {
			return nil
		}
	}

	return c.conn.WriteMessage(websocket.BinaryMessage, msg.Raw())
}

// -
func (c *Client) nat(pkt *zxnet.Packet, egress bool) bool {

	addr := c.Addresses()
	var a int
	var l string

	if egress {
		a = int(pkt.Self())
		l = "dropping egress packet, cannot NAT"
	} else {
		a = int(pkt.IRIS())
		l = "dropping ingress packet, cannot NAT"
	}

	if a > 0 { // not a broadcast, need to NAT
		if egress {
			if a--; a >= len(addr) {
				log.WithField("address", a).Warn(l)
				return false
			}
			pkt.SetSelf(byte(addr[a]))

		} else {
			var ix, aa int
			for ix, aa = range addr {
				if a == aa {
					break
				}
			}
			if ix >= len(addr) {
				log.WithField("address", a).Warn(l)
				return false
			}
			pkt.SetIRIS(byte(ix + 1))
		}
		pkt.FixHeaderChecksum()
	}

	return true
}

// FIXME get rid of ctx, closing conn will trip the read and exit go routine
func (c *Client) receive(ctx context.Context, conn *websocket.Conn) {

	go func() {
	Loop:
		for {
			select {
			case <-ctx.Done():
				log.Infof("stopped receiving from hub")
				return

			default:
				mt, rcv, err := conn.ReadMessage()
				if mt == websocket.CloseMessage || err != nil {
					// "close 1005 (no status)" can be ignored
					if err != nil && strings.Index(err.Error(), "close 1005") == -1 {
						log.Errorf("websocket read: mt = %d, %v", mt, err)
					}
					break Loop // full stop
				}
				c.dispatch(rcv)
			}
		}

		log.Warn("disconnecting from network hub after error")
		c.Close()
	}()
}

// -
func (c *Client) dispatch(raw []byte) {

	if msg, err := zxnet.NewMessage(zxnet.FromRaw(raw)); err != nil {
		log.Errorf("received invalid message: %v", err)

	} else {
		switch msg.Command() {

		case zxnet.CmdPong:
			log.WithField("duration", time.Now().Sub(c.ping)).Info("PONG from hub")

		case zxnet.CmdForward:
			if pkt, err := zxnet.NewPacketFromRaw(msg.Payload()); err != nil {
				log.Errorf("dropping invalid ingress packet: %v", err)
			} else if c.nat(pkt, false) {
				select {
				case c.ingress <- msg:
					if len(c.ingress) == 1 { // start of new strand
						c.notify()
					}
				default:
					log.Warn("dropping ingress packet")
				}
			}

		case zxnet.CmdAssignAddresses:
			log.Debug("receiving assigned addresses")
			var addr []int
			if err := msg.DecodePayload(&addr); err != nil {
				log.Errorf("error getting assigned addresses: %v", err)
			} else {
				c.addresses.Store(addr) // order matters
				c.updated()
			}

		case zxnet.CmdUserList:
			log.Debug("receiving user list")
			var list []*User
			if err := msg.DecodePayload(&list); err != nil {
				log.Errorf("error getting user list: %v", err)
			} else {
				c.users.Store(list) // order matters
				c.updated()
			}

		default:
			log.Errorf("received unknown command from hub: %s", msg.CommandStr())
		}
	}
}

// -
func getClientTLSConfig() *tls.Config {
	ret := &tls.Config{
		InsecureSkipVerify: false,
	}
	if certPool != nil {
		ret.RootCAs = certPool
	}
	return ret
}
