/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2024, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package sttp

import (
	"fmt"
	"net/url"
	"strconv"
	"strings"
)

// -
const (
	VerbLength = 3

	VerbGET  = "GET"
	VerbPUT  = "PUT"
	VerbPOST = "POST"
)

// -
func NewRequest(station int, r string) (*Request, error) {
	ret := &Request{station: station}
	if err := ret.parse(r); err != nil {
		return nil, err
	}
	return ret, nil
}

// -
type Request struct {
	station int
	verb    string
	path    string
	params  url.Values
	data    string
}

// -
func (r *Request) parse(req string) error {

	parts := strings.SplitN(req, ",", 2)

	r.verb = parts[0]
	switch r.verb {
	case VerbGET, VerbPUT, VerbPOST:
	default:
		return fmt.Errorf("unknown verb: %s", r.verb)
	}

	if len(parts) > 1 {
		parts = strings.SplitN(parts[1], "#", 2)
		u, err := url.Parse(strings.ReplaceAll(parts[0], "\r", ""))
		if err != nil {
			return fmt.Errorf("invalid request: %v", err)
		}
		r.path = u.Path
		r.params = u.Query()
		if len(parts) > 1 {
			r.data = parts[1]
		}
	}

	return nil
}

// -
func (r *Request) Station() int {
	return r.station
}

// -
func (r *Request) Verb() string {
	return r.verb
}

// -
func (r *Request) Path() string {
	return r.path
}

// -
func (r *Request) Param(key string) string {
	return r.params.Get(key)
}

// -
func (r *Request) Has(key string) bool {
	return r.params.Has(key)
}

// -
func (r *Request) IsFlagSet(key string) bool {
	return r.params.Get(key) == "true"
}

// -
func (r *Request) Data() string {
	return r.data
}

// -
func (r *Request) IntParam(key string, def int) (int, error) {
	if val := r.Param(key); val == "" {
		if def > -1 {
			return def, nil
		}
		return -1, fmt.Errorf("int parameter not set: %s", key)
	} else if ret, err := strconv.Atoi(val); err != nil {
		return def, err
	} else {
		return ret, nil
	}
}

// -
type Response struct {
	data string
}

// -
func (r *Response) Data() string {
	return r.data
}

// -
func (r *Response) SetData(d string) {
	r.data = d
}
