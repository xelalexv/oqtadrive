/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2024, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package sttp

import (
	"fmt"
)

// -
type Handler func(req *Request, resp *Response) error

// -
func NewRouter() *Router {
	return &Router{handlers: make(map[string]Handler)}
}

// -
type Router struct {
	handlers map[string]Handler
}

// -
func (r *Router) Handle(req *Request, resp *Response) error {

	id := handlerID(req.Verb(), ensureTrailingSlash(req.Path()))
	if h := r.handlers[id]; h != nil {
		return h(req, resp)
	}
	return nil // FIXME 404?
}

// -
func (r *Router) AddRoute(verb, path string, h Handler) {
	r.handlers[handlerID(verb, ensureTrailingSlash(path))] = h
}

// -
func handlerID(verb, path string) string {
	return fmt.Sprintf("%s@%s", verb, path)
}

// -
func ensureTrailingSlash(p string) string {
	if p == "" {
		return "/"
	}
	if p[len(p)-1] == '/' {
		return p
	}
	return p + "/"
}
