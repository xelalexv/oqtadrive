/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2023, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package zxnet

import (
	"fmt"

	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/raw"
)

// -
const HeaderLength = 8
const MaxDataLength = 255

// -
var headerIndex = map[string][2]int{
	"NCIRIS": {0, 1}, // IRIS station number
	"NCSELF": {1, 1}, // own station number
	"NCNUMB": {2, 2}, // block number
	"NCTYPE": {4, 1}, // 0 for data, 1 for end of file
	"NCOBL":  {5, 1}, // buffer length
	"NCDCS":  {6, 1}, // data checksum
	"NCHCS":  {7, 1}, // header checksum
	"header": {0, 7},
}

// -
type Packet struct {
	header  *raw.Block
	rawData []byte
}

// -
func NewPacketFromRaw(data []byte) (*Packet, error) {

	if len(data) < HeaderLength+1 {
		return nil, fmt.Errorf("not enough packet data")
	}

	ret := &Packet{
		header:  raw.NewBlock(headerIndex, data[:HeaderLength]),
		rawData: data,
	}
	return ret, ret.validate()
}

// -
func NewPacket(iris, self, number int, eof bool, data []byte) (*Packet, error) {

	if len(data) == 0 {
		return nil, fmt.Errorf("no packet data")
	}

	if len(data) > MaxDataLength {
		return nil, fmt.Errorf("packet data too long")
	}

	p := &Packet{rawData: make([]byte, HeaderLength+len(data))}
	p.header = raw.NewBlock(headerIndex, p.rawData[:HeaderLength])
	copy(p.rawData[HeaderLength:], data)

	p.SetIRIS(byte(iris))
	p.SetSelf(byte(self))
	p.SetNumber(number)
	p.SetEOF(eof)
	p.SetLen(byte(len(data)))
	p.FixDataChecksum() // order matters
	p.FixHeaderChecksum()

	return p, nil
}

// -
func (p *Packet) Self() byte {
	return p.header.GetByte("NCSELF")
}

// -
func (p *Packet) SetSelf(s byte) {
	p.header.SetByte("NCSELF", s)
}

// -
func (p *Packet) IRIS() byte {
	return p.header.GetByte("NCIRIS")
}

// -
func (p *Packet) SetIRIS(i byte) {
	p.header.SetByte("NCIRIS", i)
}

// -
func (p *Packet) Number() int {
	return p.header.GetInt("NCNUMB")
}

// -
func (p *Packet) SetNumber(n int) {
	p.header.SetInt("NCNUMB", n)
}

// -
func (p *Packet) EOF() bool {
	return p.header.GetByte("NCTYPE") != 0
}

// -
func (p *Packet) SetEOF(eof bool) {
	if eof {
		p.header.SetByte("NCTYPE", 1)
	} else {
		p.header.SetByte("NCTYPE", 0)
	}
}

// -
func (p *Packet) Len() byte {
	return p.header.GetByte("NCOBL")
}

// -
func (p *Packet) SetLen(l byte) {
	p.header.SetByte("NCOBL", l)
}

// -
func (p *Packet) Header() []byte {
	return p.header.Data
}

// -
func (p *Packet) Data() []byte {
	return p.rawData[HeaderLength:]
}

// -
func (p *Packet) Raw() []byte {
	return p.rawData
}

// -
func (p *Packet) IsParent(c *Packet) bool {
	return p != nil && c != nil && p.Self() == c.Self() &&
		p.IRIS() == c.IRIS() && c.Number() > p.Number()
}

// -
func (p *Packet) headerChecksum() byte {
	return p.header.GetByte("NCHCS")
}

// -
func (p *Packet) setHeaderChecksum(c byte) {
	p.header.SetByte("NCHCS", c)
}

// -
func (p *Packet) calculateHeaderChecksum() byte {
	return byte(p.header.Sum("header"))
}

// -
func (p *Packet) FixHeaderChecksum() {
	p.setHeaderChecksum(p.calculateHeaderChecksum())
}

// -
func (p *Packet) dataChecksum() byte {
	return p.header.GetByte("NCDCS")
}

// -
func (p *Packet) setDataChecksum(c byte) {
	p.header.SetByte("NCDCS", c)
}

// -
func (p *Packet) calculateDataChecksum() byte {
	sum := 0
	for _, v := range p.Data() {
		sum += int(v)
	}
	return byte(sum)
}

// -
func (p *Packet) FixDataChecksum() {
	p.setDataChecksum(p.calculateDataChecksum())
}

// -
func (p *Packet) validate() error {
	if p.headerChecksum() != p.calculateHeaderChecksum() {
		return fmt.Errorf("header checksum incorrect")
	}
	if p.dataChecksum() != p.calculateDataChecksum() {
		return fmt.Errorf("data checksum incorrect")
	}
	return nil
}
