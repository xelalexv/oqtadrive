/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2024, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package chat

import (
	"time"
)

// -
type client int

// -
func (c client) String() string {
	switch c {
	case ClientSpectrum:
		return "spectrum"
	case ClientQL:
		return "ql"
	}
	return ""
}

// -
func (c client) linebreak() byte {
	switch c {
	case ClientSpectrum:
		return '\x0d'
	case ClientQL:
		return '\x0a'
	}
	return '\x0d'
}

const (
	ClientSpectrum client = iota
	ClientQL
	ClientUnknown
)

// -
type subscription struct {
	station      int
	user         string
	client       client
	active       bool
	lastId       int
	lastDelivery time.Time
}
