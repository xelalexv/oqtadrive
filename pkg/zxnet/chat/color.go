/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2024, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package chat

import (
	"strings"
)

// -
const (
	ColorBlack = iota
	ColorBlue
	ColorRed
	ColorMagenta
	ColorGreen
	ColorCyan
	ColorYellow
	ColorWhite
	ColorTransparent
)

// -
var defaultColor = (&color{paper: ColorTransparent, ink: ColorWhite}).string()

var invalidColorCombos = []*color{
	&color{paper: ColorRed, ink: ColorMagenta},
	&color{paper: ColorMagenta, ink: ColorRed},
	&color{paper: ColorGreen, ink: ColorCyan},
	&color{paper: ColorCyan, ink: ColorGreen},
}

// -
type color struct {
	paper byte
	ink   byte
}

// -
func (c *color) byte() byte {
	return (c.paper << 4) | c.ink
}

// -
func (c *color) string() string {
	var sb strings.Builder
	sb.WriteByte(0x10)
	sb.WriteByte(c.ink)
	sb.WriteByte(0x11)
	sb.WriteByte(c.paper)
	return sb.String()
}

// -
func (c *color) advance() {
	c.paper = ((c.paper + 1) % ColorWhite) + 1
	for {
		if c.ink = ((c.ink + 2) % ColorWhite) + 1; c.isValid() {
			break
		}
	}
}

// -
func (c *color) equals(that *color) bool {
	return c != nil && that != nil && c.ink == that.ink && c.paper == that.paper
}

// -
func (c *color) isValid() bool {
	if c.ink == c.paper {
		return false
	}
	for _, inv := range invalidColorCombos {
		if c.equals(inv) {
			return false
		}
	}
	return true
}
