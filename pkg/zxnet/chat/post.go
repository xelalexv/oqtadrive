/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2024, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package chat

import (
	"fmt"
	"strings"
	"time"

	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/if1"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/ql"
)

// -
func init() {
	postSanitizer = strings.NewReplacer(
		"\"", "'",
		"\n", " ",
		"\x0a", " ",
		"\x0d", " ",
	)

	asciiToSpec = strings.NewReplacer(
		"^", "'",
		"`", "'",
		"Ä", "Ae",
		"Ö", "Oe",
		"Ü", "Ue",
		"ä", "ae",
		"ö", "oe",
		"ü", "ue",
		"ß", "ss",
	)

	asciiToQL = strings.NewReplacer(
		"`", "'",
	)
}

// -
var postSanitizer *strings.Replacer
var asciiToSpec *strings.Replacer
var asciiToQL *strings.Replacer

// -
func sanitizeMessage(m string) string {
	return postSanitizer.Replace(m)
}

// -
func mapToSpec(r rune) rune {
	if r < '\x20' || r > '\x7e' {
		return ' '
	}
	return r
}

// -
func mapToQL(r rune) rune {
	if r < '\x20' || r > '\x7e' {
		return ' '
	}
	return r
}

// -
func newPost(id, station int, user string, client client, msg, color string) *post {

	p := &post{
		id:      id,
		date:    time.Now(),
		station: station,
		user:    user,
		color:   color,
	}

	msg = sanitizeMessage(msg)

	switch client {

	case ClientSpectrum:
		p.msgSpec = msg
		p.msgASCII = strings.TrimSpace(if1.Translate(msg, ' '))
		p.msgQL = strings.Map(mapToQL, asciiToQL.Replace(p.msgASCII))

	case ClientQL:
		p.msgQL = msg
		p.msgASCII = strings.TrimSpace(ql.Translate(msg))
		p.msgSpec = strings.Map(mapToSpec, asciiToSpec.Replace(p.msgASCII))

	default:
		p.msgASCII = msg
		p.msgSpec = strings.Map(mapToSpec, asciiToSpec.Replace(msg))
		p.msgQL = strings.Map(mapToQL, asciiToQL.Replace(msg))
	}

	return p
}

// -
type post struct {
	id       int
	date     time.Time
	station  int
	user     string
	msgSpec  string
	msgQL    string
	msgASCII string
	color    string
}

// -
func (p *post) String(c client) string {

	switch c {
	case ClientSpectrum:
		return fmt.Sprintf("%s%s@%d%s %s",
			p.color, p.user, p.station, defaultColor, p.msgSpec)
	case ClientQL:
		return fmt.Sprintf("%s@%d > %s", p.user, p.station, p.msgQL)
	}
	return fmt.Sprintf("%s@%d %s", p.user, p.station, p.msgASCII)
}
