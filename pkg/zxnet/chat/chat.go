/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2024, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package chat

import (
	"context"
	"fmt"
	"regexp"
	"sort"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"

	"codeberg.org/xelalexv/oqtadrive/pkg/util"
	"codeberg.org/xelalexv/oqtadrive/pkg/zxnet"
	"codeberg.org/xelalexv/oqtadrive/pkg/zxnet/sttp"
)

// -
const (
	MaxPostsOnDelivery = 8
	MinUserLength      = 4
	MaxUserLength      = 8

	WelcomeSpectrum = "Welcome!  To post, start" +
		"typing your message. Enter /help" +
		"to see  help on  slash commands." +
		"          no anonymity          " +
		"     you own your own words      "

	WelcomeQL = "Welcome! To post, start " +
		"typing your message. Enter /help " +
		"to see  help on slash commands. " +
		"No anonymity, you own your own words."

	ErrInvalidUser       = -10
	ErrUnsupportedClient = -11
)

// -
func NewChat(h zxnet.Hub, address int) *Chat {

	c := &Chat{address: address, hub: h, lck: util.NewLock(nil, nil)}
	c.clearSubs()

	r := sttp.NewRouter()
	r.AddRoute(sttp.VerbPUT, "/sub", c.hSubscribe)
	r.AddRoute(sttp.VerbGET, "/msg", c.hGetPost)
	r.AddRoute(sttp.VerbPUT, "/msg", c.hPutPost)

	c.router = r
	c.userColors = make(map[string]string)
	c.nextColor = &color{paper: ColorBlue, ink: ColorYellow}
	c.nameCheck = regexp.MustCompile("^[a-zA-Z0-9_]*$")
	return c
}

// -
type Chat struct {
	//
	address int
	hub     zxnet.Hub
	router  *sttp.Router
	//
	count      int
	posts      []*post
	subs       map[int]*subscription
	nextColor  *color
	userColors map[string]string
	//
	nameCheck *regexp.Regexp
	lck       *util.Lock
}

// -
func (c *Chat) Handle(req *sttp.Request, resp *sttp.Response) error {
	if err := c.lock(); err != nil {
		return err
	}
	defer c.unlock()
	return c.router.Handle(req, resp)
}

// -
func (c *Chat) Disconnect(stations []int) {

	log.WithField("stations", stations).Info("handling disconnect")
	if c.lock() != nil {
		return
	}
	defer c.unlock()

	for _, s := range stations {
		if sub, ok := c.subs[s]; ok && sub != nil {
			log.WithFields(log.Fields{
				"user": sub.user, "station": s}).Info("removing chat subscription")
			delete(c.subs, s)
		}
	}
}

// -
func (c *Chat) lock() error {
	if !c.lck.Acquire(context.Background()) {
		return fmt.Errorf("could not lock chat")
	}
	return nil
}

// -
func (c *Chat) unlock() {
	c.lck.Release()
}

// -
func (c *Chat) hGetPost(req *sttp.Request, resp *sttp.Response) error {

	id, err := req.IntParam("id", -1)
	if err != nil {
		return err
	}

	cl, ok := c.getClient(req, resp)
	if !ok {
		return nil
	}

	if p := c.post(id); p != nil {
		log.WithFields(log.Fields{
			"station": req.Station(),
			"id":      id,
			"client":  cl.String()}).Info("message get")
		resp.SetData(toPayload(id, 0, cl, p.String(cl)))
	}
	return nil
}

// -
func (c *Chat) hPutPost(req *sttp.Request, resp *sttp.Response) error {

	cl, ok := c.getClient(req, resp)
	if !ok {
		return nil
	}

	usr, ok := c.getUser(req, resp, cl)
	if !ok {
		return nil
	}

	data := req.Data()
	if data == "" {
		return nil
	}

	logger := log.WithFields(log.Fields{
		"station": req.Station(), "user": usr, "client": cl.String()})

	if data[0] == '/' {
		if data = strings.TrimSpace(data); data != "" {
			data = data[1:]
			logger.WithField("command", data).Info("slash command")
			resp.SetData(toPayload(-1, 0, cl, c.hSlashCommand(data)...))
		}
		return nil
	}

	logger.Info("message posted")
	p := c.addPost(req.Station(), cl, usr, req.Data())
	col, ok := c.userColors[usr]
	if !ok {
		col = c.nextColor.string()
		c.userColors[usr] = col
		c.nextColor.advance()
	}
	p.color = col

	last, _ := req.IntParam("last", -1)
	c.createOrUpdateSub(req.Station(), cl, usr, last).active = true
	c.deliverSubs()

	c.hub.AdminNotify("chat", p.String(ClientUnknown))

	return nil
}

// -
func (c *Chat) addPost(station int, client client, usr, msg string) *post {
	p := newPost(c.count, station, usr, client, msg, defaultColor)
	c.posts = append(c.posts, p)
	c.count++
	return p
}

// -
func (c *Chat) servicePost(msg string) *post {
	return newPost(-1, c.address, "chat", ClientUnknown, msg, defaultColor)
}

// -
func (c *Chat) hSlashCommand(cmd string) []string {

	var ret []string

	switch cmd {
	case "users":
		ret = append(ret, " ", "currently active users:")
		for s, sub := range c.subs {
			if sub.active {
				for ix := len(c.posts) - 1; ix >= 0; ix-- {
					if p := c.posts[ix]; p.station == s {
						ret = append(ret, fmt.Sprintf("%s@%d", p.user, s))
						break
					}
				}
			}
		}
		if len(ret) > 2 {
			sort.Strings(ret[2:])
		}
		ret = append(ret, " ")
	case "help":
		ret = []string{" ", "available slash commands:",
			"/users - list of active users", " "}
	}

	return ret
}

// -
func (c *Chat) hSubscribe(req *sttp.Request, resp *sttp.Response) error {

	cl, ok := c.getClient(req, resp)
	if !ok {
		return nil
	}

	usr, ok := c.getUser(req, resp, cl)
	if !ok {
		return nil // FIXME add user to subscription?
	}

	last, _ := req.IntParam("last", -1)
	s := req.Station()
	init := req.IsFlagSet("init")

	log.WithFields(log.Fields{
		"station": s,
		"user":    usr,
		"last":    last,
		"init":    init,
		"client":  cl.String()}).Info("subscribing")

	sub := c.createOrUpdateSub(s, cl, usr, last)
	c.deliverSub(sub, init)
	sub.active = true

	if init {
		c.hub.AdminNotify("chat", fmt.Sprintf("user %s appeared in chat", usr))
	}

	return nil
}

// -
func (c *Chat) createOrUpdateSub(
	station int, client client, user string, last int) *subscription {

	sub := c.subs[station]

	if sub != nil {
		sub.client = client // FIXME: ?
		// avoid double delivery: when sub delivery and user post happen in
		// close proximity, we don't update latest in sub
		if sub.lastDelivery.Add(15 * time.Second).Before(time.Now()) {
			sub.lastId = last
		}
	} else {
		sub = &subscription{
			station: station,
			user:    user,
			client:  client,
			active:  true,
			lastId:  last,
		}
		c.subs[station] = sub
	}
	return sub
}

// -
func (c *Chat) deliverSubs() {
	for _, s := range c.subs {
		if s.active {
			c.deliverSub(s, false)
			s.lastDelivery = time.Now()
			s.active = false
		}
	}
}

// -
func (c *Chat) clearSubs() {
	c.subs = make(map[int]*subscription)
}

// -
func (c *Chat) deliverSub(s *subscription, init bool) {

	latest := c.latestPostID()
	logger := log.WithFields(
		log.Fields{"station": s.station, "last": s.lastId, "latest": latest})

	port := c.hub.PortForAddress(s.station)
	if port == nil {
		logger.Info("no port for station, removing sub")
		delete(c.subs, s.station)
		return
	}

	logger.Debug("delivering sub")

	start := s.lastId
	if start > -1 {
		start++
	}
	sent, err := port.QueuePayload(
		s.station, c.address, c.collectPosts(start, -1, init, s.client))

	if err == nil {
		if sent {
			logger.Debug("sent message")
			s.lastId = latest
			return
		}
		err = fmt.Errorf("could not queue message in port")
	}
	if err != nil {
		logger.Warnf("dropping subscription delivery: %v", err)
	}
}

// start & end are inclusive
func (c *Chat) collectPosts(start, end int, init bool, client client) string {

	log.Debugf("collecting posts, from %d to %d", start, end)

	var posts []*post
	if init { // initial subscribe
		w := WelcomeSpectrum
		if client == ClientQL {
			w = WelcomeQL
		}
		posts = append(posts, c.servicePost(w))
	}

	if len(c.posts) == 0 {
		log.Debug("no posts")
		posts = append(posts, c.servicePost("no posts yet"))

	} else {
		if start < 0 {
			start = c.posts[0].id
		}
		if end < 0 {
			end = c.latestPostID()
		}

		if end-start > MaxPostsOnDelivery {
			cut := end - start - MaxPostsOnDelivery
			start = end - MaxPostsOnDelivery + 1
			posts = append(posts,
				c.servicePost(fmt.Sprintf("skipping %d older posts", cut)))
		}

		for id := start; id <= end; id++ {
			p := c.post(id)
			if p == nil {
				continue
			}
			posts = append(posts, p)
		}
	}

	var sb strings.Builder
	for ix, p := range posts {
		sb.WriteString(
			toPayload(p.id, len(posts)-ix-1, client, p.String(client)))
	}
	return sb.String()
}

// -
func (c *Chat) getUser(req *sttp.Request, resp *sttp.Response, cl client) (
	string, bool) {

	usr := req.Param("usr")
	var err string

	if len(usr) < MinUserLength {
		err = fmt.Sprintf(
			"user name '%s' too short, min. %d characters", usr, MinUserLength)
	}
	if len(usr) > MaxUserLength {
		err = fmt.Sprintf(
			"user name '%s' too long, max. %d characters", usr, MaxUserLength)
	}
	if !c.nameCheck.MatchString(usr) {
		err = fmt.Sprintf("user name '%s' contains invalid characters", usr)
	}

	if err != "" {
		log.WithFields(log.Fields{
			"station": req.Station(), "user": usr}).Infof("invalid user: %v", err)
		resp.SetData(toPayload(ErrInvalidUser, 0, cl, err))
		return "", false
	}

	return usr, true
}

// -
func (c *Chat) getClient(req *sttp.Request, resp *sttp.Response) (client, bool) {

	p := strings.ToLower(strings.TrimSpace(req.Param("client")))
	var cl client

	switch p {
	case "sp", "":
		cl = ClientSpectrum
	case "ql":
		cl = ClientQL
	default:
		log.WithFields(log.Fields{
			"station": req.Station(),
			"client":  cl.String()}).Info("unsupported client")
		resp.SetData(toPayload(ErrUnsupportedClient, 0, ClientUnknown,
			fmt.Sprintf("unsupported client '%s'", p)))
		return cl, false
	}

	return cl, true
}

// -
func (c *Chat) latestPostID() int {
	if len(c.posts) == 0 {
		return -1
	}
	return c.posts[len(c.posts)-1].id
}

// -
func (c *Chat) post(id int) *post {

	if len(c.posts) == 0 {
		return nil
	}

	if id -= c.posts[0].id; id < 0 || id >= len(c.posts) {
		return nil
	}
	return c.posts[id]
}

// -
func toPayload(id, more int, cl client, msg ...string) string {

	var sb strings.Builder
	lb := cl.linebreak()

	for _, m := range msg { // all message lines, each terminated with 0x0d
		if m != "" {
			sb.WriteString(m)
			if m[len(m)-1] != lb {
				sb.WriteByte(lb)
			}
		}

	}
	sb.WriteByte(lb) // final line break to mark end of message lines
	sb.WriteString(fmt.Sprintf("%d%c%d%c", id, lb, more, lb))

	return sb.String()
}
