/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2024, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package zxnet

import (
	"fmt"
	"strings"
)

// -
const CommandLength = 16

// -
type Command int

const (
	CmdNone Command = iota
	CmdForward
	CmdAssignAddresses
	CmdUserList
	CmdPing
	CmdPong
)

const (
	None            = ""
	Forward         = "forward"
	AssignAddresses = "addrassign"
	UserList        = "users"
	Ping            = "ping"
	Pong            = "pong"
)

// -
func (t Command) Bytes() []byte {
	return []byte(t.String())
}

// -
func (t Command) String() string {
	r := None
	switch t {
	case CmdForward:
		r = Forward
	case CmdAssignAddresses:
		r = AssignAddresses
	case CmdUserList:
		r = UserList
	case CmdPing:
		r = Ping
	case CmdPong:
		r = Pong
	}
	return fmt.Sprintf("%-16s", r)
}

// -
func parseCommand(cmd []byte) (Command, error) {

	if len(cmd) != CommandLength {
		return CmdNone, fmt.Errorf("not a command, wrong length: %d", len(cmd))
	}

	r := CmdNone
	switch strings.TrimSpace(string(cmd)) {
	case Forward:
		r = CmdForward
	case AssignAddresses:
		r = CmdAssignAddresses
	case UserList:
		r = CmdUserList
	case Ping:
		r = CmdPing
	case Pong:
		r = CmdPong
	}
	return r, nil
}
