/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2022, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package repo

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"path/filepath"
)

//
func NewFileSource(path string) *FileSource {
	return &FileSource{path: path}
}

//
type FileSource struct {
	path   string
	file   *os.File
	reader io.Reader
}

//
func (fs *FileSource) open() error {
	if fs.file != nil {
		return fmt.Errorf("already open")
	}
	var err error
	if fs.file, err = os.Open(fs.path); err != nil {
		return err
	}
	fs.reader = bufio.NewReader(fs.file)
	return nil
}

//
func (fs *FileSource) Read(p []byte) (n int, err error) {
	return fs.reader.Read(p)
}

//
func (fs *FileSource) Close() error {
	return fs.file.Close()
}

//
func NewFileTarget(path string) *FileTarget {
	return &FileTarget{path: path}
}

//
type FileTarget struct {
	path   string
	file   *os.File
	writer *bufio.Writer
}

//
func (ft *FileTarget) open() error {
	if ft.file != nil {
		return fmt.Errorf("already open")
	}
	if p := filepath.Dir(ft.path); p != "." && p != string(filepath.Separator) {
		if err := os.MkdirAll(p, 0755); err != nil {
			return err
		}
	}
	var err error
	if ft.file, err = os.Create(ft.path); err != nil {
		return err
	}
	ft.writer = bufio.NewWriter(ft.file)
	return nil
}

//
func (ft *FileTarget) Write(p []byte) (n int, err error) {
	return ft.writer.Write(p)
}

//
func (ft *FileTarget) Close() error {
	ft.writer.Flush()
	return ft.file.Close()
}
