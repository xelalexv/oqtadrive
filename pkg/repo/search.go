/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2021, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package repo

import (
	"fmt"
	"path/filepath"
	"regexp"
	"strings"

	"github.com/blevesearch/bleve/v2"
	log "github.com/sirupsen/logrus"

	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/format"
)

// -
var bracketsReplacer *strings.Replacer
var wordSplit *regexp.Regexp

// -
func init() {
	bracketsReplacer = strings.NewReplacer("(", " ", ")", ",", "[", " ", "]", ",")
	wordSplit = regexp.MustCompile(`([a-z])([A-Z0-9])`)
}

// -
type SearchResult struct {
	Hits      []*Hit `json:"hits"`
	Start     int    `json:"start"`
	Total     uint64 `json:"total"`
	Available uint64 `json:"available"`
	ID        uint64 `json:"id"`
}

// -
type Hit struct {
	Label   string `json:"label"`
	Sub     string `json:"sub"`
	Ref     string `json:"ref"`
	Format  string `json:"format"`
	Details string `json:"details"`
}

func (h *Hit) Render() string {
	var b strings.Builder
	if h.Label != "" {
		b.WriteString(h.Label)
		if h.Sub != "" {
			b.WriteRune(' ')
			b.WriteString(h.Sub)
		}
		if h.Format != "" {
			b.WriteString(fmt.Sprintf(" (%s)", strings.ToUpper(h.Format)))
		}
		b.WriteRune('\n')
	}
	if h.Ref != "" {
		b.WriteString(h.Ref)
		b.WriteRune('\n')
	}
	if b.Len() > 2 {
		return b.String()
	}
	return ""
}

// -
func (i *Index) Search(term string, excludedFormats map[string]bool, max int) (
	*SearchResult, error) {

	if i.stopped {
		return nil, fmt.Errorf("cannot search, index stopped")
	}

	term = strings.TrimSpace(term)
	if term == "" {
		return nil, fmt.Errorf("no search term")
	}

	log.Debugf("searching for '%s'", term)
	query := bleve.NewQueryStringQuery(term)
	search := bleve.NewSearchRequestOptions(query, max+1, 0, false)
	res, err := i.index.Search(search)
	if err != nil {
		return nil, err
	}

	l := len(res.Hits)
	if l > max {
		l = max
	}

	ret := &SearchResult{
		Hits:      make([]*Hit, l),
		Available: uint64(l),
		Total:     res.Total,
	}

	for ix, h := range res.Hits {
		if ix >= l {
			break
		}

		name, typ, _ := format.SplitNameTypeCompressor(h.ID)
		if !format.IsSupportedFormat(typ) || excludedFormats[typ] ||
			i.isInTrash(h.ID) {
			ret.Total--
			continue
		}

		label, sub := SplitLabelSub(name)
		path := fmt.Sprintf("in: %s", filepath.Dir(h.ID))

		if sub == "" {
			sub = path
		} else {
			sub = fmt.Sprintf("%s %s", sub, path)
		}

		ret.Hits[ix] = &Hit{
			Label:  label,
			Sub:    sub,
			Ref:    PrefixRepo + h.ID,
			Format: strings.ToUpper(typ),
		}
	}

	return ret, nil
}

// -
func SplitLabelSub(name string) (label, sub string) {

	start := len(name)
	if ix := strings.Index(name, "("); ix > -1 && ix < start {
		start = ix
	}
	if ix := strings.Index(name, "["); ix > -1 && ix < start {
		start = ix
	}

	label = wordSplit.ReplaceAllString(name[:start], "$1 $2")
	sub = bracketsReplacer.Replace(name[start:])

	return label, sub
}
