/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2023, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package zxdb

import (
	"encoding/json"
	"fmt"
	"io"
	"strings"

	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/format"
	"codeberg.org/xelalexv/oqtadrive/pkg/repo"
)

//
const ZXInfoBaseURLDetails = "https://zxinfo.dk/details"

//
func unmarshalResult(r io.ReadCloser, excluded map[string]bool) (
	*repo.SearchResult, error) {

	dec := json.NewDecoder(r)
	res := &SearchResult{}
	if err := dec.Decode(res); err != nil {
		return nil, err
	}

	ret := &repo.SearchResult{Hits: []*repo.Hit{}}

	for _, h := range res.Hits.Hits {

		if h.Source == nil {
			continue
		}

		sub := fmt.Sprintf("%s, %s", h.Source.Genre, h.Source.shortMachineType())
		if h.Source.Score.Stars > 0.0 {
			sub = fmt.Sprintf("%s, \u2605%1.1f", sub, h.Source.Score.Stars)
		}

		for _, rel := range h.Source.Releases {
			if len(rel.Files) == 0 {
				continue
			}

			for _, f := range rel.Files {

				frmt := f.extractFormat()

				if format.IsSupportedFormat(frmt) && !excluded[frmt] {

					name, _, _ := format.SplitNameTypeCompressor(f.Path)
					lbl, sb := repo.SplitLabelSub(name)

					if sb != "" {
						sb = fmt.Sprintf("%s %s", sb, sub)
					} else {
						sb = sub
					}

					if rel.Year > 0 {
						sb = fmt.Sprintf("%d, %s", rel.Year, sb)
					}

					h := &repo.Hit{
						Label:   lbl,
						Sub:     sb,
						Ref:     fmt.Sprintf("%s:/%s", repo.RefSchemaZXDB, f.Path),
						Format:  strings.ToUpper(frmt),
						Details: fmt.Sprintf("%s/%s", ZXInfoBaseURLDetails, h.ID),
					}
					ret.Hits = append(ret.Hits, h)
				}
			}
		}
	}

	ret.Total = uint64(len(ret.Hits))
	ret.Available = ret.Total

	return ret, nil
}

//
type SearchResult struct {
	Hits *Hits `json:"hits"`
}

//
type Hits struct {
	Total *Total `json:"total"`
	Hits  []*Hit `json:"hits"`
}

//
type Total struct {
	Value    int    `json:"value"`
	Relation string `json:"relation"`
}

//
type Hit struct {
	ID     string  `json:"_id"`
	Source *Source `json:"_source"`
}

//
type Source struct {
	Title        string     `json:"title"`
	Genre        string     `json:"genre,omitempty"`
	MachineType  string     `json:"machineType,omitempty"`
	Availability string     `json:"availability"`
	Releases     []*Release `json:"releases,omitempty"`
	Score        *Score     `json:"score"`
}

func (s *Source) shortMachineType() string {
	parts := strings.Split(s.MachineType, " ")
	return parts[len(parts)-1]
}

//
type Release struct {
	Year  int     `json:"yearOfRelease,omitempty"`
	Files []*File `json:"files,omitempty"`
}

//
type File struct {
	Path   string `json:"path"`
	Format string `json:"format"`
}

//
type Score struct {
	Stars float32 `json:"score"`
	Votes int     `json:"votes"`
}

//
func (f *File) extractFormat() string {
	if ix1 := strings.LastIndex(f.Format, "("); ix1 > -1 {
		ix1++
		if ix2 := strings.LastIndex(f.Format, ")"); ix2 > ix1 {
			return strings.ToLower(f.Format[ix1:ix2])
		}
	}
	return ""
}
