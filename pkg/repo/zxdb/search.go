/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2023, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package zxdb

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"

	"codeberg.org/xelalexv/oqtadrive/pkg/repo"
	"codeberg.org/xelalexv/oqtadrive/pkg/util"
)

// -
func Search(term string, excludedFormats map[string]bool, max int) (
	*repo.SearchResult, error) {

	resp, err := apiCall(fmt.Sprintf(
		"search?contenttype=SOFTWARE&machinetype=ZXSPECTRUM"+
			"&availability=Available&mode=full&titlesonly=%v&size=%d&query=%s",
		false, max, url.QueryEscape(term)))
	if err != nil {
		return nil, err
	}

	defer resp.Close()

	return unmarshalResult(resp, excludedFormats)
}

// -
func apiCall(path string) (io.ReadCloser, error) {

	client := &http.Client{
		Timeout: 8 * time.Second,
		Transport: &http.Transport{
			ResponseHeaderTimeout: 8 * time.Second,
		},
	}

	req, err := http.NewRequest(http.MethodGet,
		fmt.Sprintf("https://api.zxinfo.dk/v3/%s", path), nil)
	if err != nil {
		return nil, err
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", "application/json")
	util.AddOqtaDriveAgent(req)

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	if 200 <= resp.StatusCode && resp.StatusCode <= 299 {
		return resp.Body, nil
	}

	defer resp.Body.Close()

	msg := "ZXDB API call failed, no further details, could not read server response"
	if bytes, err := ioutil.ReadAll(resp.Body); err == nil {
		msg = string(bytes)
	}

	return nil, fmt.Errorf("%s", msg)
}
