/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2021, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package repo

import (
	"fmt"
	"io"
	"path/filepath"
	"strings"

	log "github.com/sirupsen/logrus"
)

//
const (
	RefSchemaRepo  = "repo"
	RefSchemaZXDB  = "zxdb"
	RefSchemaHttp  = "http"
	RefSchemaHttps = "https"

	PrefixRepo = RefSchemaRepo + "://"
	PrefixZXDB = RefSchemaZXDB + "://"

	RefOptWriteBack = "write-back"

	ClientFolderIF1 = "client/if1/"
	ClientFolderQL  = "client/ql/"

	PrefixWoS  = "/pub/sinclair/"
	BaseURLWoS = "https://archive.org/download/World_of_Spectrum_June_2017_Mirror/World%20of%20Spectrum%20June%202017%20Mirror.zip/World%20of%20Spectrum%20June%202017%20Mirror/sinclair/"

	PrefixSpecComp  = "/zxdb/sinclair/"
	BaseURLSpecComp = "https://spectrumcomputing.co.uk/zxdb/sinclair/"
)

//
func zxdbURLfromPath(p string) string {
	if strings.HasPrefix(p, PrefixZXDB) {
		p = p[len(PrefixZXDB)-1:]
		if strings.HasPrefix(p, PrefixWoS) {
			return BaseURLWoS + p[len(PrefixWoS):]
		}
		if strings.HasPrefix(p, PrefixSpecComp) {
			return BaseURLSpecComp + p[len(PrefixSpecComp):]
		}
	}
	return ""
}

//
type Reference struct {
	//
	Schema  string
	Path    string
	Options map[string]string
	Source  io.ReadCloser
	Target  io.WriteCloser
}

//
func (r *Reference) RequiresWriteBack() bool {
	if r == nil {
		return false
	}
	_, ok := r.Options[RefOptWriteBack]
	return ok
}

//
func Resolve(ref, repo string, write, open bool) (*Reference, error) {

	log.WithFields(log.Fields{
		"reference":  ref,
		"repository": repo,
		"write":      write,
	}).Debug("resolving ref")

	r, err := ParseReference(ref)
	if err != nil {
		return nil, fmt.Errorf("invalid reference: %v", err)
	}

	switch r.Schema {

	case RefSchemaRepo:
		if repo == "" {
			return nil, fmt.Errorf("cartridge repository is not enabled")
		}
		if write {
			ft := NewFileTarget(filepath.Join(repo, r.Path))
			if r.Target = ft; open {
				err = ft.open()
			}

		} else {
			fs := NewFileSource(filepath.Join(repo, r.Path))
			if r.Source = fs; open {
				err = fs.open()
			}
		}

	case RefSchemaZXDB:
		ref = zxdbURLfromPath(ref)
		fallthrough
	case RefSchemaHttp, RefSchemaHttps:
		if write {
			return nil, fmt.Errorf("http(s) schema does not support writing")
		}
		if repo != "" {
			log.Warnf("repo setting ignored for http(s) schema")
		}
		hs := NewHTTPSource(ref)
		if r.Source = hs; open {
			err = hs.open()
		}

	default:
		return nil, fmt.Errorf("invalid reference: %s", ref)
	}

	if err != nil {
		return nil, err
	}

	log.Debugf("resolved reference: %v", r)
	return r, nil
}

//
func IsReference(ref string) bool {
	return len(strings.SplitN(ref, "://", 2)) == 2
}

//
func ParseReference(ref string) (*Reference, error) {

	ret := &Reference{}

	parts := strings.SplitN(ref, "://", 2)
	if len(parts) < 2 {
		return nil, fmt.Errorf("not a reference")
	}

	ret.Options = make(map[string]string)
	ret.Schema = parts[0]

	parts = strings.SplitN(parts[1], "?", 2)

	path := parts[0]
	ret.Path = filepath.FromSlash(path)

	if strings.HasPrefix(path, ClientFolderIF1) ||
		strings.HasPrefix(path, ClientFolderQL) {
		ret.Options[RefOptWriteBack] = "true"
	}

	if len(parts) > 1 {
		for _, opt := range strings.Split(parts[1], "&") {
			if kv := strings.SplitN(opt, "=", 2); len(kv) > 1 {
				ret.Options[kv[0]] = kv[1]
			} else {
				ret.Options[kv[0]] = "true"
			}
		}
	}

	switch ret.Schema {
	case RefSchemaRepo, RefSchemaZXDB, RefSchemaHttp, RefSchemaHttps:
	default:
		return nil, fmt.Errorf("unsupported reference schema: %s", ret.Schema)
	}

	return ret, nil
}
