/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2021, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package microdrive

import (
	"fmt"

	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/base"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/if1"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/ql"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/types"
)

//
func NewCartridge(cl types.Client) (*base.Cartridge, error) {

	switch cl {

	case types.IF1:
		return if1.NewCartridge(), nil

	case types.QL:
		return ql.NewCartridge(), nil

	default:
		return nil, fmt.Errorf("unsupported client type for cartridge: %d", cl)
	}
}

//
func NewSector(h types.Header, r types.Record) (types.Sector, error) {
	return types.NewSector(h, r)
}

//
func NewHeader(cl types.Client, data []byte, raw bool) (types.Header, error) {

	switch cl {

	case types.IF1:
		return if1.NewHeader(data, raw)

	case types.QL:
		return ql.NewHeader(data, raw)

	default:
		return nil, fmt.Errorf("unsupported client type for header: %d", cl)
	}
}

//
func NewRecord(cl types.Client, data []byte, raw bool) (types.Record, error) {

	switch cl {

	case types.IF1:
		return if1.NewRecord(data, raw)

	case types.QL:
		return ql.NewRecord(data, raw)

	default:
		return nil, fmt.Errorf("unsupported client type for record: %d", cl)
	}
}
