/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2021, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package raw

// lookup table for fast bit reversion
var revert []byte

//
func init() {
	revert = make([]byte, 256)
	for ix := range revert {
		revert[ix] = RevertNibblesSlow(byte(ix))
	}
}

/*
	Mux takes plain readable data bytes and transforms them into muxed data
	bytes that can be sent to the adapter for replay. Note that a mux-demux
	round-trip will not yield the same plain data, i.e.

		data -> mux -> demux -> data'

	This is because for replay, the bit order needs to be the reverse of the
	order in which the bytes were recorded.

	For the QL, track 1 (DATA1) is ahead of track 2 (DATA2), just the opposite
	of IF1. With invert set to true, muxing will be done as appropriate for the
	QL, i.e. tracks are switched.
*/
func Mux(data []byte, invert bool) []byte {

	if len(data) == 0 {
		return []byte{}
	}

	buf := make([]byte, len(data)+1)

	r := 0
	if invert {
		r = 1
	}

	for ix := 0; ix < len(data); {
		d := data[ix]
		if ix%2 == r {
			buf[ix] = buf[ix] | (d & 0x0f)
			ix++
			buf[ix] = buf[ix] | (d >> 4)
		} else {
			buf[ix] = buf[ix] | (d << 4)
			ix++
			buf[ix] = buf[ix] | (d & 0xf0)
		}
	}

	return buf
}

/*
	Demux takes raw bytes recorded by the adapter and transforms them into plain
	readable data.

	For the QL, track 1 (DATA1) is ahead of track 2 (DATA2), just the opposite
	of IF1. With invert set to true, demuxing will be done as appropriate for
	the QL, i.e. tracks are switched.

	Note that raw gets modified during demux, and contains the raw data ready
	for replay.
*/
func Demux(raw []byte, invert bool) []byte {

	if len(raw) <= 1 {
		return []byte{}
	}

	for ix, rw := range raw {
		raw[ix] = revert[rw]
	}

	data := make([]byte, len(raw)-1)

	r := 0
	if invert {
		r = 1
	}

	cur := raw[0]

	for ix, nxt := range raw[1:] {
		if ix%2 == r {
			data[ix] = (cur & 0x0f) | (nxt << 4) // low nibbles
		} else {
			data[ix] = (cur >> 4) | (nxt & 0xf0) // high nibbles
		}
		cur = nxt
	}

	// zero out unneeded nibbles at start end end; depending on situation,
	// high or low nibble needs to be removed
	var s, e byte
	switch r*2 + len(raw)%2 {
	case 0:
		s = 0x0f
		e = 0x0f
	case 1:
		s = 0x0f
		e = 0xf0
	case 2:
		s = 0xf0
		e = 0xf0
	case 3:
		s = 0xf0
		e = 0x0f
	}

	raw[0] &= s
	raw[len(raw)-1] &= e

	return data
}

//
func RevertNibbles(b byte) byte {
	return revert[b]
}

// needed in init() for building reversion table
func RevertNibblesSlow(b byte) byte {
	var ret byte
	for x := 0; ; x++ {
		ret = (ret | 0x88) & (b | 0x77)
		if x == 3 {
			return ret
		}
		b <<= 1
		ret >>= 1
	}
}
