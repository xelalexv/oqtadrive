/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2021, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package format

import (
	"fmt"
	"io"
	"os"

	log "github.com/sirupsen/logrus"

	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/base"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/if1"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/raw"
	"codeberg.org/xelalexv/oqtadrive/pkg/util"
)

// -
var nullPadding []byte = make([]byte, 128)

// MDR is a reader/writer for MDR format
// MDR files contain the sectors in replay order.
// -
type MDR struct{}

// -
func NewMDR() *MDR {
	return &MDR{}
}

// -
func (m *MDR) Read(in io.Reader, strict, repair bool,
	p util.Params) (*base.Cartridge, error) {

	cart := if1.NewCartridge()
	if n, ok := p.GetString("name"); ok {
		cart.Annotate(base.AnnoNameHint, n)
	}

	r := 0

	// TODO: possibly add switch to reassign or keep order from MDR file?
	for ; r < cart.Size(); r++ {

		header := make([]byte, 27)
		ix := raw.CopySyncPattern(header)

		read, err := io.ReadFull(in, header[ix:])
		if err != nil {
			if err == io.ErrUnexpectedEOF {
				if read == 1 {
					cart.SetWriteProtected(header[ix] > 0)
				} else {
					log.Warnf("expected one final byte, but got %d", read)
					cart.SetWriteProtected(false)
				}
				break
			}
			return nil, err
		}

		if header[ix] == 0 && r >= 192 { // FIXME CP/M check & constant
			log.WithField("index", r).Debug("stopping at first excess sector")
			break
		}

		record := make([]byte, 540)
		ix = raw.CopySyncPattern(record)
		if _, err := io.ReadFull(in, record[ix:]); err != nil {
			return nil, err
		}

		hd, err := if1.NewHeader(header, false)
		if err != nil && repair {
			if e := hd.FixChecksum(); e != nil {
				log.Warnf("cannot fix checksum of header at index %d: %v", r, e)
			} else {
				log.Debugf("fixed checksum of header at index %d", r)
				err = nil
			}
		}
		if err != nil {
			msg := fmt.Sprintf("defective header at index %d: %v", r, err)
			if strict {
				return nil, fmt.Errorf(msg)
			}
			log.Warn(msg)
		}

		rec, err := if1.NewRecord(record, false)
		if err != nil && repair {
			if e := rec.FixChecksums(); e != nil {
				log.Warnf("cannot fix checksums of record at index %d: %v", r, e)
			} else {
				log.Debugf("fixed checksums of record at index %d", r)
				err = nil
			}
		}
		if err != nil {
			msg := fmt.Sprintf("defective record at index %d: %v", r, err)
			if strict {
				return nil, fmt.Errorf(msg)
			}
			log.Warn(msg)
		}

		sec, err := microdrive.NewSector(hd, rec)
		if err != nil {
			msg := fmt.Sprintf("defective sector at index %d: %v", r, err)
			if strict {
				return nil, fmt.Errorf(msg)
			} else {
				log.Warn(msg)
			}
		}

		cart.SetNextSector(sec)

		if log.IsLevelEnabled(log.TraceLevel) {
			sec.Emit(os.Stdout)
		}
	}

	if repair {
		RepairOrder(cart)
	}

	cart.SeekToStart()
	cart.RewindAccessIx(true)

	log.Debugf("%d sectors loaded", r)
	cart.SetModified(false)

	return cart, nil
}

// -
func (m *MDR) Write(cart *base.Cartridge, out io.Writer, repair bool,
	p util.Params) error {

	rng := cart.Range()
	var count int

	for ; ; count++ {
		ix, sec := rng.Next()
		if ix == -1 {
			break
		}

		if repair {
			if err := sec.Header().FixChecksum(); err != nil {
				return err
			}
		}

		dmx := sec.Header().Demuxed()
		if _, err := out.Write(dmx[raw.SyncPatternLength:]); err != nil {
			return err
		}

		// need to pad for CP/M headers
		if d := if1.HeaderLength - len(dmx); d > 0 {
			if err := writeNullPadding(d, out); err != nil {
				return err
			}
		}

		rec := sec.Record()
		dmx = rec.Demuxed()

		if len(dmx) > if1.RecordLength {
			// When formatting with an Interface 1 with V2 ROM, records
			// contain extra data for testing more of the tape area, and
			// are therefore longer than they would be with V1 ROM. The last
			// phase of FORMAT overwrites these long records with standard
			// ones, but some (in particular sector 254) may be left over.
			// Reduce to normal length when saving.
			log.Debugf(
				"reducing long FORMAT record in sector %d (index %d), length %d",
				sec.Index(), ix, len(dmx))
			r, _ := if1.NewRecord(dmx[:if1.RecordLength], false)
			if err := r.FixChecksums(); err != nil {
				return err
			}
			dmx = r.Demuxed()

		} else if repair {
			if err := rec.FixChecksums(); err != nil {
				return err
			}
		}

		if _, err := out.Write(dmx[raw.SyncPatternLength:]); err != nil {
			return err
		}

		// need to pad for CP/M records
		if d := if1.RecordLength - len(dmx); d > 0 {
			if err := writeNullPadding(d, out); err != nil {
				return err
			}
		}
	}

	log.WithField("count", count).Debug("sectors written")

	if c := cart.Size() - count; c > 0 {
		log.WithField("missing", c).Debug("padding cartridge to size")

		cpm := cart.Specials() == if1.SpecialsCPM
		var hdDmx []byte
		if !cpm { // copy one live header as template for padding
			d := cart.GetNextSector().Header().Demuxed()
			hdDmx = make([]byte, len(d))
			copy(hdDmx, d)
		}

		for c = count; c < cart.Size(); c++ {
			var dmx []byte
			if cpm {
				dmx = if1.NewBlankHeader(c, false).Demuxed()
			} else {
				// for non-CP/M cartridges with missing sectors, such as the
				// highly irregular ones created by the FUSE emulator, padded
				// sectors need to have proper standard headers, or loading
				// will stop prematurely, thinking it's a CP/M cartridge
				hdDmx[raw.SyncPatternLength] = 1
				hdDmx[raw.SyncPatternLength+1] = byte(c)
				hd, _ := if1.NewHeader(hdDmx, false)
				hd.FixChecksum()
				dmx = hd.Demuxed()
			}
			if _, err := out.Write(dmx[raw.SyncPatternLength:]); err != nil {
				return err
			}
			dmx = if1.NewBlankRecord(0, false).Demuxed()
			if _, err := out.Write(dmx[raw.SyncPatternLength:]); err != nil {
				return err
			}
		}
	}

	var wp byte = 0x00
	if cart.IsWriteProtected() {
		wp = 0xff
	}
	if _, err := out.Write([]byte{wp}); err != nil {
		return err
	}

	return nil
}

// -
func writeNullPadding(count int, out io.Writer) error {
	if count > len(nullPadding) {
		return fmt.Errorf("excessive padding, missing %d bytes", count)
	}
	_, err := out.Write(nullPadding[:count])
	return err
}
