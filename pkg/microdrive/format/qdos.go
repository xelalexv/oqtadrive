/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2023, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package format

import (
	"fmt"
	"io"

	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/base"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/format/qdos"
	"codeberg.org/xelalexv/oqtadrive/pkg/util"
)

// QDOS is a format for loading QDOS ZIP archives. It is an asymmetrical format
// in the sense that it reads QDOS ZIP files, but writes MDVs.
type QDOS struct {
}

// -
func NewQDOS() *QDOS {
	return &QDOS{}
}

// -
func (q *QDOS) Read(in io.Reader, strict, repair bool, p util.Params) (
	cart *base.Cartridge, err error) {

	defer func() {
		if e := recover(); e != nil {
			cart = nil
			err = fmt.Errorf("unrecoverable error during QDOS conversion: %v", e)
		}
	}()

	name, _ := p.GetString("name")
	if cart, err = qdos.Load(in, name); err != nil {
		return nil, err
	}

	if repair {
		RepairOrder(cart)
	}

	cart.SetModified(false)
	cart.SeekToStart()
	cart.RewindAccessIx(true)

	return cart, nil
}

// -
func (q *QDOS) Write(cart *base.Cartridge, out io.Writer, repair bool,
	p util.Params) error {
	return NewMDV(fMDVmdv).Write(cart, out, repair, p)
}
