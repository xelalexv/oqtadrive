/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2023, Alexander Vollschwitz

   This file is part of OqtaDrive.

   The TAP loading code is based on work copyright (c) 2023 Tom Dalby,
   ported from C to Go by Alexander Vollschwitz.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package qdos

import (
	"fmt"
	"io"
	"time"

	log "github.com/sirupsen/logrus"

	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/base"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/ql"
)

//
type QDOS struct {
	//
	name string
	Cart *base.Cartridge
}

//
func (q *QDOS) SetName(n string) {
	if n == "" {
		n = "QDOSonMDV"
	}
	q.name = base.ToCartridgeName(n)
}

// reads QDOS archive and converts it into a cartridge on the fly
func Load(in io.Reader, name string) (*base.Cartridge, error) {

	start := time.Now()

	q := &QDOS{}
	q.SetName(name)
	q.Cart = ql.NewCartridge()
	if err := q.Cart.Format(q.name); err != nil {
		return nil, err
	}

	if err := q.Load(in); err != nil {
		return nil, fmt.Errorf("error loading QDOS archive: %v", err)
	}

	log.WithField("duration", time.Since(start)).Debug("loaded QDOS archive")

	return q.Cart, nil
}
