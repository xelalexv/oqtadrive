/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2023, Alexander Vollschwitz

   This file is part of OqtaDrive.

   The TAP loading code is based on work copyright (c) 2023 Tom Dalby,
   ported from C to Go by Alexander Vollschwitz.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package qdos

import (
	log "github.com/sirupsen/logrus"
)

/*
	see:
		https://github.com/xXorAa/qlzip/blob/05b0da1819d93c30d46aec2310d0f59a9c411321/qdos/qdos.c#L113
		http://justsolve.archiveteam.org/wiki/ZIP#Extensible_data_fields

	IDs:
	0x5455 extended timestamp
	0xfb4a SMS/QDOS
	0xfd4a SMS/QDOS (alternative)

*/
const shortIdDate = 0x5455
const shortIdHeader = 0xfb4a
const shortIdHeaderAlt = 0xfd4a
const longIdHeader = "QDOS02\x00\x00"

const dateLength = 5
const headerLength = 72

//
//
func newExtra(ex []byte) *extra {

	log.Debugf("decoding extra: %v", ex)
	ret := &extra{}

	for len(ex) > 4 {

		sId := int(ex[0]) + int(ex[1])*256
		length := int(ex[2]) + int(ex[3])*256

		ex = ex[4:]
		if len(ex) < length {
			break
		}

		switch sId {

		case shortIdHeader, shortIdHeaderAlt:
			if length == headerLength && string(ex[:8]) == longIdHeader {
				log.Debug("found header")
				ret.header = ex[8 : 8+64]
			}

		case shortIdDate:
			if length == dateLength {
				log.Debug("found date")
				ret.date = ex[:length]
			}

		default:
			log.WithField("id", sId).Debug("non-relevant extra data")
		}

		ex = ex[length:]
	}

	return ret
}

//
type extra struct {
	header []byte
	date   []byte
}

//
func (ex *extra) hasHeader() bool {
	return ex != nil && len(ex.header) > 0
}

//
func (ex *extra) hasDate() bool {
	return ex != nil && len(ex.date) > 0
}
