/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2023, Alexander Vollschwitz

   This file is part of OqtaDrive.

   The TAP loading code is based on work copyright (c) 2023 Tom Dalby,
   ported from C to Go by Alexander Vollschwitz.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package qdos

import (
	"archive/zip"
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"strings"

	log "github.com/sirupsen/logrus"

	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/fs"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/ql"
)

//
func (q *QDOS) Load(in io.Reader) error {

	data, err := ioutil.ReadAll(in)
	if err != nil {
		return err
	}

	zr, err := zip.NewReader(bytes.NewReader(data), int64(len(data)))
	if err != nil {
		return err
	}
	if len(zr.File) == 0 {
		return fmt.Errorf("empty zip archive")
	}

	fsys := q.Cart.FS()

	// validate capacity
	stats, _, err := fsys.Ls()
	if err != nil {
		return err
	}
	uncomp := 0
	for _, f := range zr.File {
		uncomp += ((int(f.UncompressedSize+ql.FileHeaderLength)/fs.FileBlockLength + 1) *
			fs.FileBlockLength) // adjusted to block size
	}
	// we're ignoring the possible increase of the directory here, so for a few
	// edge cases, this check may be too optimistic
	if uncomp/fs.FileBlockLength > stats.Free() {
		return fmt.Errorf(
			"content of archive exceeds cartridge capacity: need %dkB, have %dkB",
			uncomp/1024, stats.Free()/2)
	}

	for _, f := range zr.File {
		log.WithField("name", f.Name).Debug("adding entry")
		rd, err := f.Open()
		if err != nil {
			return err
		}

		d, err := ioutil.ReadAll(rd)
		if err != nil {
			return err
		}
		rd.Close()

		if err = addFile(fsys, f.Name, "", d, newExtra(f.Extra)); err != nil {
			return err
		}
	}

	return nil
}

//
func addFile(fsys fs.FileSystem, name, typ string, data []byte, ex *extra) error {

	fd, err := fsys.Open(canonicalName(name), fs.WRITE, fsys.ParseFileType(typ))
	if err != nil {
		return fmt.Errorf("error opening target file: %v", err)
	}

	if ex.hasHeader() {
		fd.SetHeader(ex.header)
	}

	if _, err = fd.Write(data); err != nil {
		return fmt.Errorf("error writing target file: %v", err)
	}
	return fd.Close()
}

//
func canonicalName(n string) string {
	return strings.ReplaceAll(n, ".", "_")
}
