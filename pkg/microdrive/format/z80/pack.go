/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2021, Alexander Vollschwitz

   This file is part of OqtaDrive.

   The Z80toMDR code is based on Z80onMDR_Lite, copyright (c) 2021 Tom Dalby,
   ported from C to Go by Alexander Vollschwitz. For the original C code, refer
   to:

        https://github.com/TomDDG/Z80onMDR_lite

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package z80

import (
	"fmt"
	"sync"

	log "github.com/sirupsen/logrus"

	formutil "codeberg.org/xelalexv/oqtadrive/pkg/microdrive/format/util"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/if1"
)

// total bytes: 392 (0x188)
var readme = "\x00\x00\x88\x01\xfb" + // CLS, 1
	// total PRINT+characters 201, needs to stay constant
	":\xf5\"--------------------------------\"" + // 36
	":\xf5\"Snapshot->Microdrive Conversion\"" + // 35
	":\xf5\"created with OqtaDrive\"" + // 26
	":\xf5\"--------------------------------\"" + // 36
	":\xf5\"                           \"" + // 31
	":\xf5\"https://oqtadrive.org           \":" + // 37
	//
	"\xf9\xc0\x30\x0e\x00\x00\xe2\x5d\x00\x0d\x27\x0f\x96\x00" + // 190
	"\xea\x21\x47\x5e\x11\x7b\x5a\x18\x06\xcd\x1f\x5e\x23\x10\xfa\x7e" +
	"\x23\x47\x04\x28\xfe\xfe\x20\x38\xf0\x4f\xe6\xe0\x07\x07\x07\xfe" +
	"\x07\x20\x02\x86\x23\xc6\x02\x47\xe5\x79\xe6\x1f\xc6\x40\x6e\x67" +
	"\xcd\x1f\x5e\xeb\xcd\x21\x5e\xeb\x10\xf6\xe1\x23\x18\xd1\x7e\x12" +
	"\x7a\xfe\x58\x38\x07\x07\x07\x07\xee\x82\x57\xc9\x3c\x57\xe6\x07" +
	"\xc0\xaa\x1f\x1f\x1f\xc6\x4f\x57\x13\x7b\xe6\x1f\xfe\x00\xc0\x7b" +
	"\xc6\x1b\x5f\xd0\x14\xc9\x01\x38\x00\xb0\x7b\xfa\x12\x7b\x08\x3a" +
	"\x01\x03\x07\x0f\x1f\x3f\x7f\xff\xfa\x1b\x7c\x00\x16\xd0\x7f\xfa" +
	"\x1b\x9c\x00\x34\xd0\x7f\xfa\x1b\xbc\x00\x25\xd0\x7f\xfa\x1b\xdc" +
	"\x00\x2f\xd0\x7f\xff\x0d\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc" +
	"\x00\x00\x62\x00\xfd\x30\x0e\x00\x00\x0f\x61\x00\x3a\xe7\xb0\x00"

// -
func (s *snapshot) pack() error {

	s.cart = if1.NewCartridge()
	if err := s.cart.Format(s.name); err != nil {
		return err
	}

	var wg sync.WaitGroup

	// screen
	scr := &formutil.Part{}
	wg.Add(1)

	packScreen := func(main []byte) {
		scr.Data = make([]byte, 6912+55+36)
		scr.Length = s.launcher.compress(
			main, scr.Data[len(scrLoad):], 6912, true, "0")
		scr.Length += copy(scr.Data, scrLoad) // add m/c
		scr.File = fmt.Sprintf("%-10s", "0")
		scr.Start = 32201
		scr.Param = 0xffff
		scr.Type = if1.CODE
		log.WithField("size", scr.Length).Debug("screen file")
	}

	go func() {
		defer func() {
			scr.Err = formutil.IsPanic(recover(), "packing screen")
		}()
		defer wg.Done()
		packScreen(s.main)
	}()

	// otek pages
	var pg1 *formutil.Part
	var pg3 *formutil.Part
	var pg4 *formutil.Part
	var pg6 *formutil.Part
	var pg7 *formutil.Part

	if s.launcher.isOtek() {

		lenData := 16384 + 129 + len(unpack)

		// page 1
		pg1 = &formutil.Part{}
		wg.Add(1)

		go func() {
			defer func() {
				pg1.Err = formutil.IsPanic(recover(), "packing page file 1")
			}()
			defer wg.Done()
			pg1.Data = make([]byte, lenData)
			pg1.Length = s.launcher.compress(
				s.main[s.bank[4]:], pg1.Data[len(unpack):], 16384, false, "1")
			copy(pg1.Data, unpack) // add in unpacker
			pg1.Length += len(unpack)
			pg1.Start = 32256 - len(unpack)
			pg1.Param = 0xffff
			pg1.File = fmt.Sprintf("%-10d", 1)
			pg1.Type = if1.CODE
			log.WithField("size", pg1.Length).Debug("page file 1")
		}()

		// page 3
		pg3 = &formutil.Part{}
		wg.Add(1)

		go func() {
			defer func() {
				pg3.Err = formutil.IsPanic(recover(), "packing page file 3")
			}()
			defer wg.Done()
			pg3.Data = make([]byte, lenData)
			pg3.Data[0] = 0x13
			pg3.Length = s.launcher.compress(
				s.main[s.bank[6]:], pg3.Data[1:], 16384, false, "2") + 1
			pg3.Start = 32255 // don't need to replace the unpacker, just the page number
			pg3.Param = 0xffff
			pg3.File = fmt.Sprintf("%-10d", 2)
			pg3.Type = if1.CODE
			log.WithField("size", pg3.Length).Debug("page file 3")
		}()

		// page 4
		pg4 = &formutil.Part{}
		wg.Add(1)

		go func() {
			defer func() {
				pg4.Err = formutil.IsPanic(recover(), "packing page file 4")
			}()
			defer wg.Done()
			pg4.Data = make([]byte, lenData)
			pg4.Data[0] = 0x14
			pg4.Length = s.launcher.compress(
				s.main[s.bank[7]:], pg4.Data[1:], 16384, false, "3") + 1
			pg4.File = fmt.Sprintf("%-10d", 3)
			pg4.Start = 32255
			pg4.Param = 0xffff
			pg4.Type = if1.CODE
			log.WithField("size", pg4.Length).Debug("page file 4")
		}()

		// page 6
		pg6 = &formutil.Part{}
		wg.Add(1)

		go func() {
			defer func() {
				pg6.Err = formutil.IsPanic(recover(), "packing page file 6")
			}()
			defer wg.Done()
			pg6.Data = make([]byte, lenData)
			pg6.Data[0] = 0x16
			pg6.Length = s.launcher.compress(
				s.main[s.bank[9]:], pg6.Data[1:], 16384, false, "4") + 1
			pg6.File = fmt.Sprintf("%-10d", 4)
			pg6.Start = 32255
			pg6.Param = 0xffff
			pg6.Type = if1.CODE
			log.WithField("size", pg6.Length).Debug("page file 6")
		}()

		// page 7
		pg7 = &formutil.Part{}
		wg.Add(1)

		go func() {
			defer func() {
				pg7.Err = formutil.IsPanic(recover(), "packing page file 7")
			}()
			defer wg.Done()
			pg7.Data = make([]byte, lenData)
			pg7.Data[0] = 0x17
			pg7.Length = s.launcher.compress(
				s.main[s.bank[10]:], pg7.Data[1:], 16384, false, "5") + 1
			pg7.File = fmt.Sprintf("%-10d", 5)
			pg7.Start = 32255
			pg7.Param = 0xffff
			pg7.Type = if1.CODE
			log.WithField("size", pg7.Length).Debug("page file 7")
		}()
	}

	// runner & main
	main := &formutil.Part{}
	run := &formutil.Part{}
	wg.Add(1)

	// The hidden launcher modifies main, so we need to give it its own copy to
	// ensure we don't disturb the packing of the other parts above, which can
	// happen in parallel on multi-core systems.
	mainCp := make([]byte, len(s.main))
	copy(mainCp, s.main)

	go func() {
		defer func() {
			main.Err = formutil.IsPanic(recover(), "packing main and runner")
		}()
		defer wg.Done()
		main.Data = make([]byte, s.launcher.mainSize()+319+287)

		delta := 3
		dgap := 0

		for {
			main.Err = s.launcher.byteSeriesScan(mainCp, delta, dgap)
			if main.Err != nil {
				return
			}
			// up to the full size - delta
			main.Length = s.launcher.compress(
				mainCp[s.launcher.startPos():], main.Data[287:],
				s.launcher.mainSize()-delta, false, "M")
			dgap = s.launcher.decompress(
				main.Data[287:], main.Length, s.launcher.mainSize())
			delta += dgap
			if delta > BGap {
				main.Err = fmt.Errorf(
					"cannot compress main block, delta too large: %d > %d",
					delta, BGap)
				return
			}
			if dgap < 1 {
				break
			}
		}

		maxSize := 40624 // 0x6110 onwards
		var adder int
		adder, main.Err = s.launcher.getAdder(delta, main.Length, maxSize)
		if main.Err != nil {
			return
		}
		main.Length += adder

		s.launcher.flushMain(main.Data, mainCp, adder, delta)
		main.Data = main.Data[287-adder:]
		main.Start = 65536 - main.Length
		main.Param = 0xffff
		main.File = fmt.Sprintf("%-10s", "M")
		main.Type = if1.CODE
		log.WithFields(log.Fields{
			"size": main.Length, "delta": delta}).Debug("main file")

		// run file
		s.launcher.flushRun(run)
		run.File = fmt.Sprintf("%-10s", "run")
		run.Start = 23813
		run.Param = 0
		run.Type = if1.BASIC
		log.WithField("size", run.Length).Debug("run file")
	}()

	wg.Wait()

	// if stack is within screen, we need to repack the screen or we're
	// missing the launcher
	if 0 < s.launcher.stackPos() && s.launcher.stackPos() <= 23296 {
		scr = &formutil.Part{}
		packScreen(mainCp)
	}

	rdm := &formutil.Part{}
	rdm.File = fmt.Sprintf("%-10s", "-oqtadrive")
	rdm.Data = []byte(readme)
	rdm.Start = 23813
	rdm.Length = len(rdm.Data)
	log.WithField("size", rdm.Length).Debug("readme file")

	parts := []*formutil.Part{run, scr, pg1, pg3, pg4, pg6, pg7, main, rdm}
	fs := s.cart.FS()

	for _, p := range parts {
		if p != nil {
			if err := p.AddToCartridge(fs); err != nil {
				return err
			}
		}
	}

	return nil
}
