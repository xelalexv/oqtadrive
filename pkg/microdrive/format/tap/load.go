/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2023, Alexander Vollschwitz

   This file is part of OqtaDrive.

   The TAP loading code is based on work copyright (c) 2023 Tom Dalby,
   ported from C to Go by Alexander Vollschwitz.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package tap

import (
	"fmt"
	"io"
	"strings"

	log "github.com/sirupsen/logrus"

	formutil "codeberg.org/xelalexv/oqtadrive/pkg/microdrive/format/util"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/if1"
)

// -
func (t *tap) load(in io.Reader) error {

	var pBasic []*formutil.Part
	var pMC []*formutil.Part

	infoTxt := ""
	count := 0
	var basic byte
	var lineNum byte = 10
	header := false

	autorun := 0
	basicPos := 0
	length := 0
	len2 := 0
	p1 := 0
	p2 := 0

	fName := make([]string, 17)
	main48k := make([]byte, 49152)
	basicInfo := make([]byte, 1024) //1024 bytes max

	for {
		bytesRead, err := io.ReadFull(in, main48k[:3])
		if err != nil {
			if err == io.EOF {
				break
			}
			return fmt.Errorf("error reading prefix from TAP file: %v", err)
		}

		if bytesRead == 3 {
			length = int(main48k[1])*256 + int(main48k[0])

			if main48k[2] == 0 { // header
				header = true
				bytesRead, err := io.ReadFull(in, main48k[:18])
				if err != nil {
					return fmt.Errorf(
						"error reading header from TAP file: %v", err)
				}
				if bytesRead != 18 {
					return fmt.Errorf("unexpected end of TAP file")
				}
				basic = main48k[0]
				for i := 0; i < 10; i++ {
					if main48k[i+1] < 32 {
						fName[count] += "?"
					} else {
						fName[count] += string(main48k[i+1])
					}
				}
				len2 = int(main48k[12])*256 + int(main48k[11])
				p1 = int(main48k[14])*256 + int(main48k[13])
				p2 = int(main48k[16])*256 + int(main48k[15])
				// main48k[17] is checksum

			} else { // data
				if !header {
					len2 = length - 2
					basic = 0x03      // default to code if headerless
					p1 = 65536 - len2 // load code at top of memory
					p2 = 32768        // standard for code
					fName[count] = fmt.Sprintf("%02d:HL     ", count+1)

				} else {
					if length-2 != len2 {
						log.Warnf(
							"length mismatch - header says %d, prefix %d, using prefix length",
							len2, length-2)
						len2 = length - 2
					}
					if count > 0 { // check for dupe filename
						i := 0
						for {
							if fName[count] == fName[i] {
								fName[count] = fmt.Sprintf("%-8s%02d",
									strings.TrimSpace(fName[count]), count)
								i = count
							}
							i++
							if i >= count {
								break
							}
						}
					}

					if basic == 0 { // write to MD
						autorun = p1
						p1 = 23813 // 23813 for MD, 23755 for TAP
						p2 = 32768 // remove autostart
					}
				}

				if len2 > 49151 {
					// should be no way a data block can be this large
					return fmt.Errorf("data block in TAP file too large")
				}

				// +1 for checksum
				bytesRead, err := io.ReadFull(in, main48k[:len2+1])
				if err != nil {
					return err
				}

				if bytesRead != len2+1 {
					return fmt.Errorf("unexpected end of TAP file")
				}

				p := &formutil.Part{}
				p.Length = len2
				p.Data = make([]byte, len2)
				copy(p.Data, main48k[:len2])
				p.Start = p1
				p.Param = p2
				p.File = fName[count]
				p.Type = if1.FileType(int(basic))

				if basic == 0 {
					pBasic = append(pBasic, p)
				} else {
					pMC = append(pMC, p)
				}

				switch basic {

				case 0:
					if autorun < 32768 {
						infoTxt = fmt.Sprintf(
							"\"%s\" LINE %d", fName[count], autorun)
					} else {
						infoTxt = fmt.Sprintf("\"%s\"", fName[count])
					}

				case 1:
					infoTxt = fmt.Sprintf("\"%s\" DIM A()", fName[count])

				case 2:
					infoTxt = fmt.Sprintf("\"%s\" DIM A$()", fName[count])

				default:
					if !header {
						infoTxt = fmt.Sprintf("\"%s\" CODE ???,%d",
							fName[count], len2)
					} else {
						if p1 == 16384 && len2 == 6912 {
							infoTxt = fmt.Sprintf("\"%s\" SCREEN$", fName[count])
						} else {
							infoTxt = fmt.Sprintf("\"%s\" CODE %d,%d",
								fName[count], p1, len2)
						}
					}
				}

				lineLen := len(infoTxt)

				basicInfo[basicPos] = 0x00
				basicPos++
				basicInfo[basicPos] = lineNum
				basicPos++
				basicInfo[basicPos] = byte(lineLen) + 2
				basicPos++
				basicInfo[basicPos] = 0x00
				basicPos++
				basicInfo[basicPos] = 0xea
				basicPos++

				for i := 0; i < lineLen; i++ {
					basicInfo[basicPos] = infoTxt[i]
					basicPos++
				}

				basicInfo[basicPos] = 0x0d
				basicPos++
				lineNum += 10
				count++

				header = false // reset to default
			}

		} else {
			break
		}

		if count >= 16 {
			break
		}
	}

	info := &formutil.Part{}
	info.Length = basicPos
	info.Data = make([]byte, basicPos)
	copy(info.Data, basicInfo[:basicPos])
	info.Start = 32768
	info.Param = basicPos
	info.File = fmt.Sprintf("%-10s", "(Info)")
	info.Type = if1.BASIC

	pBasic = append([]*formutil.Part{info}, pBasic...)

	fs := t.cart.FS()

	for ix, p := range append(pBasic, pMC...) {
		if ix == len(pBasic) { // FIXME use reduced length?
			// make some room for the `run` the user will create
			log.WithField("count", 24).Debug("adding space before M/C sections")
			t.cart.AdvanceAccessIxBy(24, false)
		}

		if p != nil {
			if err := p.AddToCartridge(fs); err != nil {
				return err
			}
		}
	}

	return nil
}
