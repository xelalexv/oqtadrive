/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2021, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package util

import (
	"bufio"
	"fmt"

	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/fs"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/if1"
)

//
func Fill(r *bufio.Reader, target []byte, indexes []int) error {
	for _, ix := range indexes {
		if ix < 0 || ix >= len(target) {
			return fmt.Errorf("fill index out of range: %d", ix)
		}
		var err error
		if target[ix], err = r.ReadByte(); err != nil {
			return err
		}
	}
	return nil
}

//
func Adjust(target []byte, ix int, val int) error {
	if ix < 0 || ix >= len(target) {
		return fmt.Errorf("adjustment index out of range: %d", ix)
	}
	target[ix] = byte(int(target[ix]) + val)
	return nil
}

//
func IsPanic(err interface{}, action string) error {
	if err != nil {
		return fmt.Errorf("unrecoverable error while %s: %v", action, err)
	}
	return nil
}

//
type Part struct {
	File   string
	Data   []byte
	Length int
	Start  int
	Param  int
	Type   if1.FileType
	Err    error
}

//
func (p *Part) AddToCartridge(fsys fs.FileSystem) error {

	fd, err := fsys.Open(p.File, fs.WRITE, p.Type)
	if err != nil {
		return err
	}

	fh := if1.FileHeader{
		Type:    p.Type,
		Length:  p.Length,
		Start:   p.Start,
		Program: 0xffff,
		Line:    0xffff,
	}

	if p.Type == if1.BASIC {
		fh.Program = fh.Length
		fh.Line = p.Param
	}
	fd.SetHeader(fh.Bytes())

	if _, err = fd.Write(p.Data[:p.Length]); err != nil {
		return err
	}
	return fd.Close()
}
