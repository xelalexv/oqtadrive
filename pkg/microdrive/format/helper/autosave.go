/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2021, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package helper

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"time"

	log "github.com/sirupsen/logrus"

	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/base"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/format"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/if1"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/types"
)

// -
const (
	FlagModified       = 0x01
	FlagWriteProtected = 0x02
	AutoSaveVersion    = 1

	ixVersion = 0
	ixClient  = 1
	ixFlags   = 2
)

// -
func AutoSave(drive int, cart *base.Cartridge, repair bool) error {

	if cart == nil || !cart.IsFormatted() || cart.IsAutoSaved() {
		return nil
	}

	start := time.Now()
	logger := log.WithField("drive", drive)
	logger.WithField("repair", repair).Info("auto-saving")

	fm, err := format.NewFormat(cart.Client().DefaultFormat(), nil)
	if err != nil {
		return err
	}

	_, file, err := autoSavePath(drive, true)
	if err != nil {
		return err
	}

	tmp := fmt.Sprintf("%s_", file)

	fd, err := os.OpenFile(tmp, os.O_CREATE|os.O_RDWR, 0644)
	if err != nil {
		return err
	}

	out := bufio.NewWriter(fd)

	preamble := make([]byte, 3)

	var flags byte = 0
	if cart.IsModified() {
		flags |= FlagModified
	}
	if cart.IsWriteProtected() {
		flags |= FlagWriteProtected
	}

	preamble[ixVersion] = AutoSaveVersion
	preamble[ixClient] = byte(cart.Client())
	preamble[ixFlags] = flags

	if err := writeRaw(preamble, out); err != nil {
		return err
	}

	if err := fm.Write(cart, out, repair, nil); err != nil {
		return err
	}

	if err := out.Flush(); err != nil {
		return err
	}

	if err := fd.Sync(); err != nil {
		return err
	}

	if err := fd.Close(); err != nil {
		return err
	}

	if err := os.Rename(tmp, file); err != nil {
		return err
	}

	if err := AutoSaveAnnotations(drive, cart); err != nil {
		logger.Warnf("error auto-saving annotations: %v", err)
	}

	cart.SetAutoSaved(true)
	cart.SeekToStart()
	cart.RewindAccessIx(true)

	logger.WithField("duration", time.Now().Sub(start)).Info("auto-save done")
	return nil
}

// -
func AutoSaveAnnotations(drive int, cart *base.Cartridge) error {
	_, file, err := autoSavePath(drive, false)
	if err != nil {
		return err
	}
	return cart.MarshalAnnotations(annotationsFile(file))
}

// -
func AutoLoad(drive int, repair bool) (*base.Cartridge, error) {

	logger := log.WithField("drive", drive)
	logger.WithField("repair", repair).Info("loading auto-save")

	_, file, err := autoSavePath(drive, false)
	if err != nil {
		return nil, err
	}

	fd, err := os.Open(file)
	if err != nil {
		if !os.IsNotExist(err) {
			return nil, err
		}
		logger.Info("no auto-save file")
		return nil, nil
	}
	defer fd.Close()

	in := bufio.NewReader(fd)

	preamble, err := readRaw(in, 64)
	if err != nil {
		return nil, fmt.Errorf("error reading preamble: %v", err)
	}

	if preamble[ixVersion] != AutoSaveVersion {
		return nil, fmt.Errorf(
			"incompatible auto-save version, want %d, got %d",
			AutoSaveVersion, preamble[ixVersion])
	}

	cl := types.Client(preamble[ixClient])
	fm, err := format.NewFormat(cl.DefaultFormat(), nil)
	if err != nil {
		return nil, err
	}

	cart, err := fm.Read(in, true, repair, nil)
	if err != nil {
		return nil, err
	}

	if err := cart.UnmarshalAnnotations(annotationsFile(file)); err != nil {
		logger.Warnf("error reading auto-saved annotations: %v", err)
	}

	if n := cart.GetAnnotation(base.AnnoNameHint).String(); n != "" {
		if cart.Specials() == if1.SpecialsCPM {
			log.WithField("name", n).Debug(
				"setting name from name hint for CP/M cartridge")
			cart.SetName(n)
		}
	}

	cart.SetModified(preamble[ixFlags]&FlagModified != 0)
	cart.SetWriteProtected(preamble[ixFlags]&FlagWriteProtected != 0)
	cart.SetAutoSaved(true)

	return cart, nil
}

// -
func AutoRemove(drive int) error {

	_, file, err := autoSavePath(drive, false)
	if err != nil {
		return err
	}

	if err := os.Remove(file); err != nil {
		if !os.IsNotExist(err) {
			return err
		}
	}

	if err := os.Remove(annotationsFile(file)); err != nil {
		if !os.IsNotExist(err) {
			return err
		}
	}

	log.WithField("drive", drive).Debug("removed auto-save")
	return nil
}

// -
func autoSavePath(drive int, create bool) (string, string, error) {

	home, err := os.UserHomeDir()
	if err != nil {
		return "", "", err
	}

	dir := filepath.Join(home, ".oqtadrive", fmt.Sprintf("%d", drive))

	if create {
		if err := os.MkdirAll(dir, 0755); err != nil {
			return "", "", err
		}
	}

	return dir, filepath.Join(dir, "cart"), nil
}

// -
func annotationsFile(autoSaveFile string) string {
	return fmt.Sprintf("%s_annotations.json", autoSaveFile)
}

// -
func readRaw(in io.Reader, maxLen int) ([]byte, error) {

	buf := []byte{0, 0}
	if _, err := in.Read(buf); err != nil {
		return nil, err
	}

	length := int(buf[0]) + 256*int(buf[1])

	if length > maxLen {
		return nil, fmt.Errorf("max length %d, but have %d", maxLen, length)
	}

	ret := make([]byte, length)
	if _, err := in.Read(ret); err != nil {
		return nil, err
	}

	return ret, nil
}

// -
func writeRaw(data []byte, out io.Writer) error {

	buf := []byte{byte(len(data) % 256), byte((len(data) >> 8))}

	if _, err := out.Write(buf); err != nil {
		return err
	}

	if _, err := out.Write(data); err != nil {
		return err
	}

	return nil
}
