/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2023, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package format

import (
	"bufio"
	"bytes"
	"compress/flate"
	"encoding/binary"
	"fmt"
	"io"
	"os"

	log "github.com/sirupsen/logrus"

	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/base"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/if1"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/raw"
	"codeberg.org/xelalexv/oqtadrive/pkg/util"
)

// -
const ixMDVTVersion = 0
const ixMDVTFlags = 2
const ixMDVTSecLen = 4
const ixMDVTSecCount = 6

const flagMDVTWrProtect = 0x01
const flagMDVTCompressed = 0x02
const flagMDVTConverted = 0x04
const flagMDVTUnformatted = 0x08

var mdvtPreamble = []byte("MDVT")

// -
func isMDVT(prefix []byte) bool {
	if len(prefix) < len(mdvtPreamble) {
		return false
	}
	return bytes.Compare(mdvtPreamble, prefix[:len(mdvtPreamble)]) == 0
}

// MDVT is a reader/writer for MDV files produced by the JSpeccy emulator.
// Cartridges are always written in MDR format.
type MDVT struct{}

// -
func NewMDVT() *MDVT {
	return &MDVT{}
}

// -
func (m *MDVT) Read(in io.Reader, strict, repair bool, p util.Params) (*base.Cartridge, error) {

	cart := if1.NewCartridge()
	if n, ok := p.GetString("name"); ok {
		cart.Annotate(base.AnnoNameHint, n)
	}

	bin := bufio.NewReader(in)
	var err error

	pre := make([]byte, len(mdvtPreamble))
	if _, err = bin.Read(pre); err != nil {
		return nil, err
	}
	if !isMDVT(pre) {
		return nil, fmt.Errorf("not an MDVT file")
	}

	hdLen := make([]byte, 4)
	if _, err = io.ReadFull(bin, hdLen); err != nil {
		return nil, err
	}

	hd := make([]byte, binary.LittleEndian.Uint32(hdLen))
	if _, err = io.ReadFull(bin, hd); err != nil {
		return nil, err
	}

	flags := binary.LittleEndian.Uint16(hd[ixMDVTFlags : ixMDVTFlags+2])
	if flags&flagMDVTUnformatted != 0 {
		return cart, nil
	}

	ver := fmt.Sprintf("%d.%d", hd[ixMDVTVersion], hd[ixMDVTVersion+1])
	secLen := int(binary.LittleEndian.Uint16(hd[ixMDVTSecLen : ixMDVTSecLen+2]))
	secCount := int(binary.LittleEndian.Uint16(hd[ixMDVTSecCount : ixMDVTSecCount+2]))
	compressed := flags&flagMDVTCompressed != 0

	log.WithFields(log.Fields{
		"version":       ver,
		"header length": len(hd),
		"sector count":  secCount,
		"sector length": secLen,
		"flags":         flags,
		"compressed":    compressed,
	}).Debug("MDVT format")

	if err = skipField(bin, "creator"); err != nil {
		return nil, err
	}
	if err = skipField(bin, "description"); err != nil {
		return nil, err
	}
	if err = skipField(bin, "comment"); err != nil {
		return nil, err
	}

	bin.Discard(2) // FIXME not sure what these two bytes are

	data := make([]byte, secLen*secCount)

	if compressed {
		if err = deflate(bin, data); err != nil {
			return nil, fmt.Errorf("error deflating: %v", err)
		}
		log.Debug("deflated")

	} else if _, err = io.ReadFull(bin, data); err != nil {
		// FIXME reading uncompressed data has not been tested
		return nil, fmt.Errorf("error reading data: %v", err)
	}

	// The start of data may not be aligned to a sector start, so we need to
	// find the first header sync pattern. Using the flag byte to distinguish
	// between header and record is tricky. Standard headers have 0 bit set, but
	// CP/M records are flagged with 0xff. Standard records have 0 bit reset,
	// but CP/M headers are flagged with 0. This flag value may however also be
	// found in standard records :-/ We need to find the first and second sync
	// patterns and then look at the distance to decide.
	start := 0
	next := 0
	if start = raw.FindSyncPattern(data, start); start == -1 {
		return nil, fmt.Errorf("no initial sync pattern in data")
	}

	var cpm bool

	for {
		if next = raw.FindSyncPattern(data, start+raw.SyncPatternLength); next == -1 {
			return nil, fmt.Errorf("no more sync pattern in data")
		}

		if next-start < if1.RecordLengthCPM {
			// distance is shorter than record --> header
			if flag := data[start+raw.SyncPatternLength]; flag == 0 {
				log.Debug("found CP/M header")
				cpm = true
			} else {
				log.Debugf("found standard header, flags: %b", flag)
			}
			break
		}
		start = next
	}

	if start > 0 { // align data to first header
		log.WithField("offset", start).Debug("aligning data")
		b := make([]byte, start)
		copy(b, data[0:start])
		copy(data, data[start:])
		copy(data[len(data)-len(b):], b)
	}

	r := 0

	for ix := 0; ; { // create sectors

		header := data[ix : ix+27]
		if ix = raw.FindSyncPattern(data, ix+27); ix == -1 {
			return nil, fmt.Errorf("record missing")
		}

		if cpm && r >= if1.SectorCountCPM {
			log.WithField("index", r).Debug("stopping at first excess sector")
			break
		}

		record := data[ix : ix+540]

		hd, err := if1.NewHeader(header, false)
		if err != nil && repair {
			if e := hd.FixChecksum(); e != nil {
				log.Warnf("cannot fix checksum of header at index %d: %v", r, e)
			} else {
				log.Debugf("fixed checksum of header at index %d", r)
				err = nil
			}
		}
		if err != nil {
			msg := fmt.Sprintf("defective header at index %d: %v", r, err)
			if strict {
				return nil, fmt.Errorf(msg)
			}
			log.Warn(msg)
		}

		rec, err := if1.NewRecord(record, false)
		if err != nil && repair {
			if e := rec.FixChecksums(); e != nil {
				log.Warnf("cannot fix checksums of record at index %d: %v", r, e)
			} else {
				log.Debugf("fixed checksums of record at index %d", r)
				err = nil
			}
		}
		if err != nil {
			msg := fmt.Sprintf("defective record at index %d: %v", r, err)
			if strict {
				return nil, fmt.Errorf(msg)
			}
			log.Warn(msg)
		}

		sec, err := microdrive.NewSector(hd, rec)
		if err != nil {
			msg := fmt.Sprintf("defective sector at index %d: %v", r, err)
			if strict {
				return nil, fmt.Errorf(msg)
			} else {
				log.Warn(msg)
			}
		}

		cart.SetNextSector(sec)
		r++

		if log.IsLevelEnabled(log.TraceLevel) {
			sec.Emit(os.Stdout)
		}

		if ix = raw.FindSyncPattern(data, ix+540); ix == -1 {
			break
		}
	}

	if repair {
		RepairOrder(cart)
	}

	cart.SeekToStart()
	cart.RewindAccessIx(true)

	log.Debugf("%d sectors loaded", r)
	cart.SetModified(false)

	return cart, nil
}

// -
func (m *MDVT) Write(cart *base.Cartridge, out io.Writer, repair bool,
	p util.Params) error {
	return NewMDR().Write(cart, out, repair, p)
}

// -
func skipField(bin *bufio.Reader, field string) error {

	l, err := bin.ReadByte()
	if err == nil && l > 0 {
		f := make([]byte, int(l))
		if _, err = bin.Read(f); err == nil {
			log.WithFields(log.Fields{
				"field": field,
				"value": string(f),
			}).Debug("skipping field")
		}
	}
	return err
}

// -
func deflate(in io.Reader, buf []byte) error {

	fr := flate.NewReader(in)
	defer fr.Close()

	for read := 0; ; {
		r, err := fr.Read(buf[read:])
		read += r
		if r == 0 || err == io.EOF {
			if read < len(buf) {
				return fmt.Errorf("not enough data, want %d, got %d", len(buf), read)
			}
			return nil
		}
		if err != nil {
			return fmt.Errorf("error deflating: %v", err)
		}
	}
}
