/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2022, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package format

import (
	"archive/zip"
	"bufio"
	"bytes"
	"compress/gzip"
	"fmt"
	"io"
	"io/ioutil"
	"path/filepath"
	"strings"

	"github.com/bodgit/sevenzip"

	log "github.com/sirupsen/logrus"
)

// -
func NewCartReader(r io.ReadCloser, compressor string) (*CartReader, error) {

	log.WithField("compressor", compressor).Debug("cartridge reader requested")

	var ret *CartReader
	var err error

	switch compressor {

	case "gzip", "gz":
		ret, err = getGZipReader(r)

	case "zip":
		ret, err = getZipReader(r, false)

	case "7z":
		ret, err = getZipReader(r, true)

	case "":
		ret = &CartReader{readCloser: r}

	default:
		err = fmt.Errorf("unsupported compressor")
	}

	ret.bufReader = bufio.NewReader(ret.readCloser)
	if ret.prefix, err = ret.bufReader.Peek(16); err != nil {
		err = fmt.Errorf("cannot peek prefix: %v", err)
	}

	if err != nil {
		return nil, err
	}

	log.WithFields(log.Fields{
		"compressor": ret.compressor,
		"name":       ret.name,
		"type":       ret.typ}).Debug("cartridge reader created")

	return ret, nil
}

// -
type CartReader struct {
	readCloser io.ReadCloser
	bufReader  *bufio.Reader
	//
	name       string
	typ        string
	compressor string
	prefix     []byte
}

// -
func (r *CartReader) Read(p []byte) (n int, err error) {
	return r.bufReader.Read(p)
}

// -
func (r *CartReader) Close() error {
	r.bufReader = nil
	return r.readCloser.Close()
}

// -
func (r *CartReader) Name() string {
	return r.name
}

// -
func (r *CartReader) Type() string {
	return r.typ
}

// -
func (r *CartReader) Compressor() string {
	return r.compressor
}

// -
func (r *CartReader) Prefix() []byte {
	return r.prefix
}

// -
func getGZipReader(r io.ReadCloser) (*CartReader, error) {

	gzr, err := gzip.NewReader(r)
	if err != nil {
		return nil, err
	}

	ret := &CartReader{readCloser: gzr}
	ret.name, ret.typ, _ = SplitNameTypeCompressor(gzr.Name)
	ret.compressor = "gzip"

	return ret, nil
}

// -
func getZipReader(r io.ReadCloser, zip7 bool) (*CartReader, error) {

	var sponge bytes.Buffer
	size, err := io.Copy(&sponge, r)
	if err != nil {
		return nil, err
	}
	r.Close()

	ret := &CartReader{}

	if zip7 {
		zr, err := sevenzip.NewReader(bytes.NewReader(sponge.Bytes()), size)
		if err != nil {
			return nil, err
		}
		if len(zr.File) == 0 {
			return nil, fmt.Errorf("empty 7-zip archive")
		}

		var file *sevenzip.File
		for _, file = range zr.File {
			n, t, _ := SplitNameTypeCompressor(file.Name)
			if isBonaFideType(file.Name, n, t, "7z", ret) {
				ret.readCloser, err = file.Open()
				break
			}
		}

		if err == nil && ret.typ == "" {
			return nil, fmt.Errorf(
				"no cartridge image file found in 7-zip archive")
		}

	} else {
		zr, err := zip.NewReader(bytes.NewReader(sponge.Bytes()), size)
		if err != nil {
			return nil, err
		}
		if len(zr.File) == 0 {
			return nil, fmt.Errorf("empty zip archive")
		}

		ret.typ = "qdos"
		var file *zip.File
		for _, file = range zr.File {
			n, t, _ := SplitNameTypeCompressor(file.Name)
			if isBonaFideType(file.Name, n, t, "zip", ret) {
				ret.readCloser, err = file.Open()
				break
			}
		}

		if err == nil && ret.typ == "qdos" {
			log.Debug("handling zip archive as QDOS zip")
			ret.readCloser = ioutil.NopCloser(bytes.NewReader(sponge.Bytes()))
		}
	}

	if err != nil {
		return nil, err
	}

	return ret, nil
}

// -
func isBonaFideType(file, name, typ, comp string, cr *CartReader) bool {
	if typ != "qdos" && IsSupportedFormat(typ) {
		log.WithField("file", file).Debug(
			"found bona fide cartridge file in archive")
		cr.typ = typ
		cr.name = name
		cr.compressor = comp
		return true
	}
	return false
}

// -
func SplitNameTypeCompressor(file string) (name, typ, compressor string) {

	_, n := filepath.Split(file)

Loop:
	for {
		prev := n
		ext := filepath.Ext(n)
		n = strings.TrimSuffix(n, ext)
		ext = strings.ToLower(strings.TrimPrefix(ext, "."))

		switch ext {

		case "mdr", "mdv", "mdvt", "mdi", "z80", "sna", "tap":
			typ = ext

		case "gz", "gzip", "zip", "7z":
			compressor = ext

		default:
			name = prev
			break Loop
		}
	}

	return name, typ, compressor
}
