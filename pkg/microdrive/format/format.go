/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2021, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package format

import (
	"fmt"
	"io"
	"strings"

	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/base"
	"codeberg.org/xelalexv/oqtadrive/pkg/util"
)

// Reader interface for reading in a cartridge
type Reader interface {
	// when setting strict, invalid sectors are discarded
	Read(in io.Reader, strict, repair bool, p util.Params) (*base.Cartridge, error)
}

// Writer interface for writing out a cartridge
type Writer interface {
	Write(cart *base.Cartridge, out io.Writer, repair bool, p util.Params) error
}

// ReadWriter interface for reading/writing a cartridge
type ReadWriter interface {
	Reader
	Writer
}

// pass nil for `prefix` to determine format solely based on `typ`
func NewFormat(typ string, prefix []byte) (ReadWriter, error) {
	rw, _, err := getFormat(typ, false, prefix)
	return rw, err
}

// -
func IsSupportedFormat(typ string) bool {
	_, ok, _ := getFormat(typ, true, nil)
	return ok
}

// -
func getFormat(typ string, justCheck bool, prefix []byte) (
	ReadWriter, bool, error) {

	ok := true

	switch strings.ToLower(typ) {

	case "mdr":
		if !justCheck {
			return NewMDR(), true, nil
		}

	case "mdvt":
		if !justCheck {
			return NewMDVT(), true, nil
		}

	case "mdv":
		if !justCheck {
			if isMDVT(prefix) {
				return NewMDVT(), true, nil
			}
			return NewMDV(fMDVUnknown), true, nil
		}

	case "mdi":
		if !justCheck {
			return NewMDV(fMDVmdi), true, nil
		}

	case "z80":
		if !justCheck {
			return NewZ80(false), true, nil
		}

	case "sna":
		if !justCheck {
			return NewZ80(true), true, nil
		}

	case "tap":
		if !justCheck {
			return NewTAP(), true, nil
		}

	case "qdos":
		if !justCheck {
			return NewQDOS(), true, nil
		}

	default:
		ok = false
	}

	if !ok {
		return nil, false, fmt.Errorf("unsupported cartridge format: %s", typ)
	}
	return nil, ok, nil
}
