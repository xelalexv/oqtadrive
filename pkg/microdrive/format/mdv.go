/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2021, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package format

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"os"

	log "github.com/sirupsen/logrus"

	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/base"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/ql"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/raw"
	"codeberg.org/xelalexv/oqtadrive/pkg/util"
)

// Strangely, a sector in an MDV file is longer than what the QL actually writes
// during a format, which is 652 bytes (cf. Appendix D "Microdrive Format",
// Section 4 "Special Sector Structure" in "QL Advanced User Guide" by Adrian
// Dickens).
const MDVSectorLength = 686

const MDISectorLength = 534
const MDISectorCount = 255

// Qemulator mdump block lengths
const MdumpHeaderBytes = 46
const MdumpIxSectorLength = 16
const MdumpIxSectorCount = 18
const MdumpSectorBytes = 14
const MdumpRecordBytes = 2
const MdumpDataBytes = 512

const MDISectorBytes = 16
const MDIRecordBytes = 4
const MDIDataBytes = 512

// Qemulator mdump V1 & V2 preamble
var mDumpPreamble = []byte("Mdv*Dump")

const fMDVUnknown = 0
const fMDVmdv = 1
const fMDVmdi = 2
const fMDVmdump = 3

// MDV is a reader/writer for MDV (Qlay, Qemulator V1 & V2) and MDI formats.
// MDV files contain the sectors in reverted replay order. Cartridges are always
// written in QLay MDV format.
type MDV struct {
	format int
	//
	secLen   int
	secCount int
	//
	secBytes  int
	recBytes  int
	dataBytes int
}

// -
func NewMDV(format int) *MDV {
	return &MDV{format: format}
}

// -
func (m *MDV) Read(in io.Reader, strict, repair bool,
	p util.Params) (*base.Cartridge, error) {

	cart := ql.NewCartridge()
	ix := 0

	bin := bufio.NewReader(in)
	preamble, err := bin.Peek(MdumpHeaderBytes)
	if err != nil {
		return nil, fmt.Errorf("cannot determine MDV format: %v", err)
	}

	m.secCount = cart.Size()
	m.setFormat(preamble)

	log.WithFields(log.Fields{
		"format":        m.format,
		"sector length": m.secLen,
		"sector count":  m.secCount}).Debug("MDV format")

	if m.format == fMDVmdump {
		bin.Discard(MdumpHeaderBytes)
		// mdump format has no check sums, and for V2 sectors may be out of
		// order, so we need to always repair
		repair = true
	}

	for ; ix < m.secCount; ix++ {

		sector := make([]byte, MDVSectorLength)
		read, err := io.ReadFull(bin, sector[:m.secLen])

		if err != nil {
			if err == io.EOF && read == 0 {
				break
			}
			return nil, fmt.Errorf("error reading MDV file: %v", err)
		}

		if m.format == fMDVmdump || m.format == fMDVmdi {
			m.insertSyncPatterns(sector)
		}

		hd, err := ql.NewHeader(sector[:ql.HeaderLength], false)
		if err != nil && repair {
			if e := hd.FixChecksum(); e != nil {
				log.Warnf("cannot fix checksum of header at index %d: %v", ix, e)
			} else {
				log.Debugf("fixed checksum of header at index %d", ix)
				err = nil
			}
		}
		if err != nil {
			msg := fmt.Sprintf("defective header at index %d: %v", ix, err)
			if strict {
				return nil, fmt.Errorf(msg)
			}
			log.Warn(msg)
		}

		rec, err := ql.NewRecord(sector[ql.HeaderLength:ql.MaxSectorLength], false)
		if err != nil && repair {
			if e := rec.FixChecksums(); e != nil {
				log.Warnf("cannot fix checksums of record at index %d: %v", ix, e)
			} else {
				log.Debugf("fixed checksums of record at index %d", ix)
				err = nil
			}
		}
		if err != nil {
			msg := fmt.Sprintf("defective record at index %d: %v", ix, err)
			if strict {
				return nil, fmt.Errorf(msg)
			}
			log.Warn(msg)
		}

		sec, err := microdrive.NewSector(hd, rec)
		if err != nil {
			msg := fmt.Sprintf("defective sector at index %d: %v", ix, err)
			if strict {
				return nil, fmt.Errorf(msg)
			}
			log.Warn(msg)
		}

		log.Tracef("loaded sector with number %d", sec.Index())
		cart.SetPreviousSector(sec)

		if log.IsLevelEnabled(log.TraceLevel) {
			sec.Emit(os.Stdout)
		}
	}

	if repair {
		// TODO:
		// - replace incorrect 5a 5a bytes in extra data often found in the wild
		//   with aa 55
		RepairOrder(cart)
	}

	cart.SeekToStart()
	cart.RewindAccessIx(true)

	log.Debugf("%d sectors loaded", ix)
	cart.SetWriteProtected(false)
	cart.SetModified(false)

	return cart, nil
}

// -
func (m *MDV) setFormat(prefix []byte) {

	if m.format == fMDVmdi {
		m.secLen = MDISectorLength
		m.secCount = MDISectorCount
		m.secBytes = MDISectorBytes
		m.recBytes = MDIRecordBytes
		m.dataBytes = MDIDataBytes

	} else if bytes.Compare(mDumpPreamble, prefix[:len(mDumpPreamble)]) == 0 {
		m.format = fMDVmdump
		m.secLen = int(prefix[MdumpIxSectorLength])*256 +
			int(prefix[MdumpIxSectorLength+1])
		m.secCount = int(prefix[MdumpIxSectorCount])
		m.secBytes = MdumpSectorBytes
		m.recBytes = MdumpRecordBytes
		m.dataBytes = MdumpDataBytes

	} else {
		m.format = fMDVmdv
		m.secLen = MDVSectorLength
	}
}

// only for mdump and MDI
func (m *MDV) insertSyncPatterns(sector []byte) {

	// move sections to make space for sync patterns
	ix := ql.HeaderLength + ql.RecordHeaderLength // data block
	copy(sector[ix:ix+m.dataBytes], sector[m.secBytes+m.recBytes:])
	ix = ql.HeaderLength + raw.SyncPatternLength // record header
	copy(sector[ix:ix+m.recBytes], sector[m.secBytes:])
	ix = raw.SyncPatternLength // sector header
	copy(sector[ix:ix+m.secBytes], sector)

	// copy in sync patterns
	raw.CopySyncPattern(sector)                   // sector header
	raw.CopySyncPattern(sector[ql.HeaderLength:]) // record header
	raw.CopyDataSyncPattern(                      // data sync
		sector[ql.HeaderLength+raw.SyncPatternLength+4:])
}

// always writing QLay MDV format
func (m *MDV) Write(cart *base.Cartridge, out io.Writer, repair bool,
	p util.Params) error {

	padding := createPadding()
	cart.SeekToStart()
	cart.AdvanceAccessIx(false)

	for ix := 0; ix < cart.Size(); ix++ {

		sec := cart.GetPreviousSector()

		if sec == nil { // MDV requires all sectors; FIXME: add blank one
			return fmt.Errorf("missing sector %d", ix)
		}

		if repair {
			if err := sec.Header().FixChecksum(); err != nil {
				return err
			}
			if err := sec.Record().FixChecksums(); err != nil {
				return err
			}
		}

		missing := MDVSectorLength
		var written int
		var err error

		if written, err = out.Write(sec.Header().Demuxed()); err != nil {
			return err
		}
		missing -= written

		if written, err = out.Write(sec.Record().Demuxed()); err != nil {
			return err
		}
		missing -= written

		if missing > len(padding) {
			return fmt.Errorf("excessive padding, missing %d bytes", missing)
		}

		if missing > 0 {
			if _, err := out.Write(padding[0:missing]); err != nil {
				return err
			}
		}
	}

	return nil
}

// -
func createPadding() []byte {

	p := make([]byte, 256)

	for ix := 0; ix < len(p); {
		p[ix] = 0xaa
		ix++
		p[ix] = 0x55
		ix++
	}

	// the first 86 bytes replicate the extra bytes appended to a normal data
	// block during format; last two bytes are the (incorrect) checksum
	p[84] = 0x19
	p[85] = 0x3b

	return p
}
