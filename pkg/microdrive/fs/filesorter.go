/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2023, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package fs

import (
	"path/filepath"
	"sort"
)

//
func NewFileSorter(ascending bool) *FileSorter {
	return &FileSorter{ascending: ascending}
}

//
type FileSorter struct {
	heads     []string
	tails     []string
	ascending bool
}

//
func (fs *FileSorter) AddHeads(names ...string) {
	fs.heads = append(fs.heads, names...)
}

//
func (fs *FileSorter) AddTails(names ...string) {
	fs.tails = append(fs.tails, names...)
}

//
func (fs *FileSorter) matches(file string, patterns []string) bool {
	for _, p := range patterns {
		if ok, _ := filepath.Match(p, file); ok {
			return true
		}
	}
	return false
}

//
func swap(a, b int, files []*FileInfo) {
	files[a], files[b] = files[b], files[a]
}

//
func (fs *FileSorter) edges(files []*FileInfo) (first, last int) {

	first = 0
	last = len(files) - 1

	if last > 0 {
		for ix := 0; ix <= last; ix++ {
			f := files[ix]
			if fs.matches(f.Name(), fs.heads) {
				swap(first, ix, files)
				first++
			} else if fs.matches(f.Name(), fs.tails) {
				swap(last, ix, files)
				last--
			}
		}
	}

	return
}

//
func (fs *FileSorter) BySize(files []*FileInfo) {

	first, last := fs.edges(files)

	if last-first+1 > 1 {
		w := files[first : last+1]
		sort.Slice(w, func(i, j int) bool {
			if fs.ascending {
				return w[i].Size() < w[j].Size()
			}
			return w[i].Size() > w[j].Size()
		})
	}
}
