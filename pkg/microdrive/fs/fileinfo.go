/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2022, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package fs

import (
	"codeberg.org/xelalexv/oqtadrive/pkg/util"
)

// -
func NewFileInfo(name, path, group string, size int, typ FileType) *FileInfo {
	return &FileInfo{
		name:        name,
		path:        path,
		group:       group,
		size:        size,
		typ:         typ,
		Annotations: make(util.Annotations)}
}

// -
type FileInfo struct {
	name   string
	path   string // FIXME reflect in File as well
	group  string
	size   int
	typ    FileType
	header []byte
	util.Annotations
}

// -
func (f *FileInfo) Name() string {
	return f.name
}

// -
func (f *FileInfo) Path() string {
	return f.path
}

// -
func (f *FileInfo) Group() string {
	return f.group
}

// -
func (f *FileInfo) Size() int {
	return f.size
}

// -
func (f *FileInfo) Type() FileType {
	return f.typ
}

// -
func (f *FileInfo) Header() []byte {
	return f.header
}

// -
func (f *FileInfo) SetHeader(h []byte) {
	f.header = h
}
