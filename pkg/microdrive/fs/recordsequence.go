/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2023, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package fs

import (
	log "github.com/sirupsen/logrus"
)

//
func NewRecordSequence(top, bottom, gap int) *RecordSequence {
	return &RecordSequence{
		top:         top,
		bottom:      bottom,
		sectorCount: top - bottom + 1,
		gap:         gap,
	}
}

//
type RecordSequence struct {
	top         int // top sector number
	bottom      int // bottom sector number
	sectorCount int
	gap         int // minimum required gap between records
	// sequence of record indexes as they appear on cartridge
	records []int
	// sector index for each record in records; assumes the descending order of
	// sector indexes, as used in the cartridge
	sectors []int
}

//
func (rs *RecordSequence) Length() int {
	return len(rs.records)
}

//
func (rs *RecordSequence) Add(record, sector int) {
	rs.records = append(rs.records, record)
	rs.sectors = append(rs.sectors, sector)
}

//
func (rs *RecordSequence) delta(a, b int) int {

	l := rs.Length()
	if a < 0 || b < 0 || a >= l || b >= l {
		return -1
	}
	if a == b {
		return 0
	}

	a = rs.sectors[a]
	b = rs.sectors[b]

	if a > b {
		return a - b
	}
	return rs.sectorCount + a - b
}

//
func (rs *RecordSequence) Fragmentation(strict bool) float32 {
	return rs.frag(strict)
}

//
func (rs *RecordSequence) frag(strict bool) float32 {

	l := rs.Length()
	if l < 2 {
		return 1.0
	}

	log.Tracef("records: %v", rs.records)
	log.Tracef("sectors: %v", rs.sectors)

	// build set of existing records; this is needed since with IF1, same
	// record may appear twice if a file was saved with additional copy;
	// additionally, find 0 and last record
	recs := make(map[int]bool)
	zero := 0
	max := 0
	for ix, r := range rs.records {
		recs[r] = true
		if r == 0 {
			zero = ix
		} else if r > max {
			max = r
		}
	}

	for r := 0; r <= max; r++ { // make sure record sequence is complete
		if !recs[r] {
			log.Error("incomplete record sequence")
			return -1.0
		}
	}

	recCount := len(recs)
	// one full round per record is the absolute worst case,
	// we use this as a safety stop
	stop := recCount * rs.sectorCount
	visited := 1
	prevIx := zero
	prevRec := zero
	want := 0

	for ix := zero; visited < stop; ix = (ix + 1) % l {
		// log.Debugf("ix: %d, prev rec: %d, want: %d, r: %d", ix, prevRec, want, rs.records[ix])
		visited += rs.delta(prevIx, ix) // count all visited sectors
		prevIx = ix

		if r := rs.records[ix]; recs[r] && (!strict || r == want) {
			// found a not yet "loaded" record, that's additionally in sequence,
			// if we're doing a strict check: accept record as loadable if the
			// required gap to the previously "loaded" record is maintained, or
			// current record index is same as previous record index, which
			// happens at the very beginning and end
			if ix == prevRec || rs.delta(prevRec, ix) > rs.gap {
				prevRec = ix
				want++
				delete(recs, r)
				if len(recs) == 0 {
					break // all records found
				}
			} else if strict || len(recs) == 1 {
				// we're seeing a record for which the gap was not maintained;
				// for any record during strict check, or the last record
				// otherwise, we set previous record index to current record
				// index, in order to catch it in the next round and not go into
				// an endless loop; note that we could accept the record now and
				// add a penalty of sector count to avoid going for another
				// round, but if the record exists twice we'd miss it and the
				// penalty would be too large
				prevRec = ix
			}
		}
	}

	bIdeal := rs.idealRun(recCount)
	wIdeal := rs.worstCase(bIdeal)
	bReal := visited
	wReal := rs.worstCase(visited)

	log.WithFields(log.Fields{
		"strict": strict, "visited": visited, "ideal-best": bIdeal,
		"ideal-worst": wIdeal, "best": bReal, "worst": wReal, "zero": zero,
	}).Debug("fragmentation")

	return (float32(bReal)/float32(bIdeal) + float32(wReal)/float32(wIdeal)) / 2
}

//
func (rs *RecordSequence) idealRun(records int) int {
	if records < 2 {
		return 1
	}
	return 1 + (records-1)*(rs.gap+1)
}

//
func (rs *RecordSequence) worstCase(run int) int {
	return run + rs.sectorCount - run%rs.sectorCount
}
