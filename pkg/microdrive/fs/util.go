/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2022, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package fs

import (
	"fmt"
	"io"

	log "github.com/sirupsen/logrus"
)

// -
func CopyAll(dst, src FileSystem, sort func(files []*FileInfo)) error {

	_, files, err := src.Ls()
	if err != nil {
		return err
	}

	sort(files)

	for _, f := range files {

		log.WithFields(
			log.Fields{"name": f.Name(), "size": f.Size()}).Debug("copying file")

		fSrc, err := src.Open(f.Name(), READ, f.Type())
		if err != nil {
			return fmt.Errorf("cannot open source file '%s': %v", f.Name(), err)
		}

		fDst, err := dst.Open(fSrc.Name(), WRITE, f.Type())
		if err != nil {
			return fmt.Errorf(
				"cannot open destination file '%s': %v", f.Name(), err)
		}

		if fSrc.FileHeaderLength() > 0 {
			fDst.SetHeader(fSrc.Header())
			log.WithFields(log.Fields{
				"name": fDst.Name(),
				"size": fDst.Size(),
			}).Trace("copied file header")
		}

		if _, err = io.Copy(fDst, fSrc); err != nil {
			return err
		}

		if err = fSrc.Close(); err != nil {
			return err
		}
		if err = fDst.Close(); err != nil {
			return err
		}
	}

	return nil
}
