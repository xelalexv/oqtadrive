/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2023, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package fs

import (
	"errors"
	"strings"
)

// -
const FileExists = "file exists: %s"
const FileNotFound = "file not found"
const NoReadFromWriteFile = "trying to read from write file"
const CannotWriteToFile = "cannot write to file"
const MissingRecord = "missing record at index %d"
const NoHeaderForFile = "no header for %s"
const NotImplementedForWriteFiles = "not implemented for write files"
const NoSpaceLeft = "no space left on cartridge"
const NoSpaceLeftInDirectory = "no more space in directory"
const NoFileSystem = "file not backed by a file system"
const EmptyFileName = "empty file name"
const UnsupportedAccessMode = "unsupported file access mode"

// -
var ErrFileNotFound = errors.New(FileNotFound)
var ErrNoReadFromWriteFile = errors.New(NoReadFromWriteFile)
var ErrCannotWriteToFile = errors.New(CannotWriteToFile)
var ErrNotImplementedForWriteFiles = errors.New(NotImplementedForWriteFiles)
var ErrNoSpaceLeft = errors.New(NoSpaceLeft)
var ErrNoSpaceLeftInDirectory = errors.New(NoSpaceLeftInDirectory)
var ErrNoFileSystem = errors.New(NoFileSystem)
var ErrEmptyFileName = errors.New(EmptyFileName)
var ErrUnsupportedAccessMode = errors.New(UnsupportedAccessMode)

// -
func IsAnyOf(e error, kinds ...string) bool {
	for _, k := range kinds {
		if IsError(e, k) {
			return true
		}
	}
	return false
}

// -
func IsError(e error, kind string) bool {
	if e == nil {
		return false
	}
	pre := strings.Split(kind, `%`)[0]
	return strings.HasPrefix(e.Error(), pre)
}
