/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2022, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package fs

import (
	"fmt"
	"io"

	log "github.com/sirupsen/logrus"

	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/types"
)

// -
type FileMode int

const (
	READ FileMode = iota
	WRITE
	OVERWRITE
)

// This is the length of a file block contained in a record, i.e. the effective
// record payload. It is the same for Spectrum & QL. A file block starts at
// index 0 within the record data. In certain situations, e.g. during format
// using an Interface 1 with V2 ROM or a QL, the data section of a record may be
// longer than the file block length.
const FileBlockLength = 512

// -
type FileImpl interface {

	// length of a file header applicable to the record implementation; the file
	// header is only contained at the start of the first record of a file
	FileHeaderLength() int

	// render the file contents, as appropriate for the file type
	Render(w io.Writer) error

	// find count vacant sectors for appending to this file; if the file is not
	// empty, start is index of the currently last sector occupied by this file
	ClaimSectors(start, count int) ([]types.Sector, error)

	// Take all necessary actions to mark record r as belonging to this file,
	// including proper record header and check sums. When closing is false, ix
	// is the index of r within the file and length the number of bytes used in
	// the data section of r. This indicates that data has just been written
	// to the record. Closing set to true indicates that the file is being
	// closed and all writes have already occurred. This method is called twice
	// with closing set to true, once with ix set to -1 indicating that the last
	// record is passed in r, and once with 0 to indicate that the first record
	// is passed. In both cases, length is the total length of the file (including
	// header data, if there is one). This gives implementations the chance to
	// make final adjustments to first and last record as needed.
	CommitRecord(r types.Record, ix, length int, closing bool) error

	// Take all necessary actions to release the records in r as no longer
	// belonging to this file, making the corresponding sectors available again
	// for other files. Implementations need to return the actual number of
	// records that were released, counting from the end of r. For most file
	// systems, this will be the length of r, i.e. all records get released.
	// The CP/M file system however, always assigns sectors in pairs, so when
	// releasing an odd number of records, the first in r will actually be kept
	// to maintain the pair.
	ReleaseRecords(r []types.Record, purge bool) (int, error)
}

// -
func NewFile(name string, mode FileMode, typ FileType, size, lastSectorIx int,
	records []types.Record, impl FileImpl) *File {

	return &File{
		fInfo:        *NewFileInfo(name, "", "", size, typ),
		fImpl:        impl,
		mode:         mode,
		records:      records,
		lastSectorIx: lastSectorIx,
	}
}

// for keeping embedded anonymous fields package private
type fInfo = FileInfo
type fImpl = FileImpl

// -
type File struct {
	//
	fInfo
	fImpl
	mode FileMode
	//
	records      []types.Record
	readPos      int
	writePos     int
	lastSectorIx int
}

// -
func (f *File) Close() error {

	switch f.mode {

	case WRITE, OVERWRITE:
		l := f.RecordCount()
		if l == 0 { // zero length file without record, need to reserve one
			log.WithField("size", f.Size()).Trace("zero length file")
			if err := f.ensureSize(1); err != nil {
				return err
			}
		}
		// set up record of zero length file; the file already had one or we've
		// just created it above
		if l == 0 || f.Size() <= f.FileHeaderLength() {
			if err := f.CommitRecord(
				f.records[0], 0, f.Size(), false); err != nil {
				return err
			}
		}

		last := (f.writePos - 1) / FileBlockLength
		if last < 0 {
			last = 0
		}
		if l > 0 && last > l-1 {
			return fmt.Errorf(
				"inconsistent file state, write position beyond last record: write pos = %d, records = %d",
				f.writePos, l)
		}
		if err := f.CommitRecord( // commit last record
			f.records[last], -1, f.Size(), true); err != nil {
			return err
		}
		// and give chance to make final adjustments to first record
		if err := f.CommitRecord(f.records[0], 0, f.Size(), true); err != nil {
			return err
		}
		if f.mode == OVERWRITE { // release any unused records
			if last < l-1 {
				// FIXME parameterize purge
				count, err := f.fImpl.ReleaseRecords(f.records[last+1:], false)
				if err != nil {
					return err
				}
				if count > 0 {
					keep := len(f.records) - count
					log.WithFields(log.Fields{
						"count":   count,
						"keeping": keep}).Debug("removing records from file")
					f.records = f.records[:keep]
				}
			}
		}

	case READ: // no op
	}

	return nil
}

// -
func (f *File) RecordCount() int {
	return len(f.records)
}

// -
func (f *File) Bytes() ([]byte, error) {

	if f.mode == WRITE || f.mode == OVERWRITE {
		return nil, ErrNotImplementedForWriteFiles
	}

	b := make([]byte, f.Size())
	if n, err := f.Read(b); err != nil && (err != io.EOF || n == 0) {
		return nil, err
	} else {
		return b[:n], nil
	}
}

// -
func (f *File) Header() []byte {

	if f.mode == READ { // for read files, get header from first record
		if len(f.records) == 0 || f.records[0] == nil {
			return []byte{}
		}
		hd := make([]byte, f.FileHeaderLength())
		if len(hd) > 0 {
			copy(hd, f.records[0].Data()[:f.FileHeaderLength()])
		}
		return hd
	}

	return f.fInfo.Header() // for write files, return explicitly set header
}

// -
func (f *File) Read(p []byte) (int, error) {

	if f.mode != READ {
		return -1, ErrNoReadFromWriteFile
	}

	if len(p) == 0 {
		return 0, nil
	}

	skip := 0
	if f.readPos == 0 {
		skip = f.FileHeaderLength()
	}

	rIx, bIx, err := f.advanceReadPos(skip)
	if err != nil {
		return 0, err
	}

	read := 0

	for read < len(p) {

		if r := f.records[rIx]; r != nil {

			bEnd := FileBlockLength
			if rIx == f.RecordCount()-1 {
				if e := f.Size() % FileBlockLength; e > 0 {
					bEnd = e
				}
			}

			if bIx == bEnd {
				return read, io.EOF
			}

			n := copy(p[read:], r.Data()[bIx:bEnd])
			read += n

			if rIx, bIx, err = f.advanceReadPos(n); err != nil {
				return read, err
			}

		} else {
			return read, fmt.Errorf(MissingRecord, rIx)
		}
	}

	return read, nil
}

// -
func (f *File) advanceReadPos(n int) (recIx, blockIx int, err error) {

	f.readPos += n
	recIx = f.readPos / FileBlockLength
	blockIx = f.readPos % FileBlockLength
	err = nil

	if f.readPos >= f.Size() || recIx >= f.RecordCount() {
		err = io.EOF
	}

	return
}

// -
func (f *File) Write(p []byte) (int, error) {

	if f.mode != WRITE && f.mode != OVERWRITE {
		return -1, ErrNoReadFromWriteFile
	}

	// upon first write, enough space for file header (if used) is needed
	init := false
	if f.RecordCount() == 0 || (f.mode == OVERWRITE && f.writePos == 0) {
		f.writePos = f.FileHeaderLength()
		f.fInfo.size = f.writePos
		init = true
	}

	if err := f.ensureSize(f.writePos + len(p)); err != nil {
		return -1, err
	}

	wr := 0
	for wr < len(p) {

		rIx := f.writePos / FileBlockLength
		bIx := f.writePos % FileBlockLength
		rec := f.records[rIx]
		data := rec.Data()

		if init {
			if hd := f.Header(); len(hd) > 0 { // copy header if explicitly set
				if len(hd) != f.FileHeaderLength() {
					return -1, fmt.Errorf(
						"header with invalid length, want %d, got %d",
						f.FileHeaderLength(), len(hd))
				}
				copy(data, hd)
			} else { // zero out header area
				clear(data[:f.FileHeaderLength()])
			}
			init = false
		}

		cp := copy(rec.Data()[bIx:FileBlockLength], p[wr:])
		if cp == 0 {
			return -1, ErrCannotWriteToFile
		}

		f.writePos += cp
		wr += cp

		l := f.writePos % FileBlockLength
		if l == 0 { // block filled
			l = FileBlockLength
		}
		if l == FileBlockLength || wr >= len(p) {
			// only commit record when block is filled or end of data reached
			f.CommitRecord(rec, rIx, l, false)
		}
	}

	f.fInfo.size += wr
	return wr, nil
}

// -
func (f *File) ensureSize(s int) error {

	l := f.RecordCount()
	d := s/FileBlockLength - l
	if s%FileBlockLength != 0 {
		d++
	}

	if d <= 0 {
		return nil
	}

	ix := f.lastSectorIx
	if l == 0 {
		ix = -1
	}

	sec, err := f.ClaimSectors(ix, d)
	if err != nil {
		return err
	}

	var sLog []int
	if log.IsLevelEnabled(log.DebugLevel) {
		sLog = make([]int, len(sec))
	}

	last := -1
	for ix, s := range sec {
		f.records = append(f.records, s.Record())
		last = s.Index()
		if len(sLog) > 0 {
			sLog[ix] = s.Index()
		}
	}
	f.lastSectorIx = last

	log.Debugf("using sectors %v", sLog)

	return nil
}
