/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2023, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package fs

import (
	log "github.com/sirupsen/logrus"

	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/types"
)

// SectorVacant checks whether the sector with index ix exists and is vacant,
// i.e. it's neither in use, nor bad. The sector itself is returned, if it
// exists, for optimizations.
type SectorVacant func(ix int) (bool, types.Sector)

// -
type ClaimOpts struct {
	// start indicates at what sector index to start with a search, which could
	// for example be used to extend an existing file. In this case, start would
	// point to the currently last sector occupied by the file. If start is set
	// to -1, the strategy will pick a starting point for the search. This can
	// for example be used for new files.
	Start     int
	Count     int  // number of sectors to claim
	Top       int  // start of search area (inclusive)
	Bottom    int  // end of search area (inclusive)
	Ascending bool // whether sector order is ascending, e.g. for CP/M
	FirstGap  int  // required gap between start and first claimed sector
	SecGap    int  // required (min) gap in between claimed sectors
	// whether sectors need to be claimed in pairs; implies that index of first
	// sector in a pair is an even number
	Pairs bool
}

// ClaimStrategy tries to find the requested number of vacant sectors in the
// specified range. The actual strategy for finding and choosing sectors is
// defined by implementations. Checks for vacancy are performed using v. Found
// sectors are not modified, i.e not marked as in use. If fewer then the
// requested number of sectors were found, an error is returned.
type ClaimStrategy func(opts *ClaimOpts, v SectorVacant) ([]types.Sector, error)

// SimpleClaim is a simple strategy for finding vacant sectors.
func SimpleClaim(opts *ClaimOpts, v SectorVacant) ([]types.Sector, error) {

	if opts.Count < 1 {
		return []types.Sector{}, nil
	}

	log.WithFields(log.Fields{
		"top":        opts.Top,
		"bottom":     opts.Bottom,
		"first-gap":  opts.FirstGap,
		"sector-gap": opts.SecGap,
		"ascending":  opts.Ascending,
		"count":      opts.Count,
		"start":      opts.Start,
		"pairs":      opts.Pairs}).Debug("simple claim")

	var s, l int
	skip := opts.FirstGap

	if opts.Start < 0 { // search for start spot, e.g. new file on IF1
		s, l = findLongestRun(opts.Top, opts.Bottom, opts.Ascending, v)
		log.WithFields(log.Fields{"start": s, "length": l}).Debug("longest run")
		if l == 0 {
			return nil, ErrNoSpaceLeft
		}
	} else { // start searching from the given location, e.g. add to file
		s = opts.Start
		skip = opts.SecGap + 1
	}

	claimed := make(map[int]bool)
	ret := make([]types.Sector, opts.Count)

	sectors := opts.Top - opts.Bottom + 1
	if opts.Ascending {
		sectors = opts.Bottom - opts.Top + 1
	}

	offset := opts.Top - s
	if opts.Ascending {
		offset = opts.Top + s
	}

	stop := sectors
	var pos int
	var pair bool
	var prevSecIx int

	for i := 0; ; i++ {

		if stop--; stop == 0 {
			log.WithField("found", pos).Debug(
				"no more vacant sectors found in one full sweep")
			break
		}

		if skip > 0 {
			skip--
			continue
		}

		// We need to move through the top-to-bottom range, wrapping around
		// to top when going below bottom.

		var ix int
		if opts.Ascending {
			ix = opts.Top + ((offset + i) % sectors)
		} else {
			ix = opts.Top - ((offset + i) % sectors)
		}

		if vacant, sec := v(ix); vacant && !claimed[sec.Index()] {

			claimed[sec.Index()] = true

			if opts.Pairs {
				if pair {
					pair = false
					// second sector in pair needs to be adjacent to previous
					if sec.Index()-prevSecIx != 1 {
						pos--
						continue
					}
				} else {
					if sec.Index()%2 != 0 {
						// reject odd sector index at start of a pair
						continue
					}
					pair = true
				}
			}

			prevSecIx = sec.Index()
			ret[pos] = sec
			if pos++; pos == opts.Count {
				return ret, nil
			}

			skip = opts.SecGap // maintain sector gap between this and next
			stop = sectors     // reset stop counter
		}
	}

	return nil, ErrNoSpaceLeft
}

// findLongestRun searches for the longest contiguous run of vacant sectors
// in the top to bottom sector range (inclusive). Checks for vacancy are
// performed using v. Nil sectors are not considered a break in the run. Start
// sector index and length of the run are returned.
func findLongestRun(top, bottom int, ascending bool, v SectorVacant) (
	start, length int) {

	s := -1
	l := 0

	comp := func(ix, end int) bool {
		if ascending {
			return ix <= end
		}
		return ix >= end
	}

	move := func(ix int) int {
		if ascending {
			return ix + 1
		}
		return ix - 1
	}

	for ix := top; comp(ix, bottom); ix = move(ix) {
		if vacant, sec := v(ix); vacant {
			if l++; l == 1 {
				s = ix
			}
		} else if sec != nil {
			if l > length {
				start = s
				length = l
			}
			s = -1
			l = 0
		}
	}

	if l > length {
		start = s
		length = l
	}

	return start, length
}
