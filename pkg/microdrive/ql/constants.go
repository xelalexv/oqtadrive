/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2021, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package ql

import (
	"fmt"
	"strings"
)

const (
	HeaderLength       = 28
	HeaderLengthMux    = HeaderLength + 1
	RecordLength       = 538
	RecordLengthMux    = RecordLength + 1
	RecordHeaderLength = 24
	FormatExtraBytes   = 86

	MaxSectorLength = HeaderLength + RecordLength + FormatExtraBytes

	DataSyncStart = 16

	// sector numbers range from 0 through 254
	SectorCount = 255

	// sectors above this number need to be hidden during format verify
	TopmostGoodSector = 253

	// for satisfying MG & Minerva ROM format procedure,
	// we inject one faulty sector at this index
	FaultInjectionSector = 13

	// sector gaps
	FirstGap  = 20
	RecordGap = 12

	FileHeaderLength = 64 // only in first record of each file

	// block flags
	BHeaderFlag = 0xff

	// file numbers
	FMaxFileNumber = 0xf0
	FBadSector     = 0xff
	FVacantSector  = 0xfd

	// documentation states that sector map is bearing file number 0xf8,
	// but 0x80 is also often observed, not sure why
	FSectorMap    = 0xf8
	FSectorMapAlt = 0x80

	FDirectory             = 0
	DirectoryDefaultSector = 244 // sector to use when formatting a cartridge

	FSectorMapName = "#sectormap"
	FDirectoryName = "#directory"
)

//
func parseFileType(t string) FileType {
	r := BASIC
	switch strings.ToLower(t) {
	case "code":
		r = CODE
	case "sroff":
		r = SROFF
	case "thordir":
		r = THORDIR
	case "font":
		r = FONT
	case "pattern":
		r = PATTERN
	case "image1":
		r = IMAGE1
	case "image2":
		r = IMAGE2
	case "dir":
		r = DIR
	case "ascii":
		r = ASCII
	}
	return r
}

//
type FileType int

const (
	// native
	BASIC   FileType = 0
	CODE    FileType = 1
	SROFF   FileType = 2
	THORDIR FileType = 3
	FONT    FileType = 4
	PATTERN FileType = 5
	IMAGE1  FileType = 6
	IMAGE2  FileType = 11
	CONFIG  FileType = 23 // FIXME: our own made-up type
	DIR     FileType = 255
	// virtual
	ASCII   FileType = 256
	UNKNOWN FileType = -1
)

func (t FileType) String() string {
	r := "?"
	switch t {
	case BASIC:
		r = "BASIC"
	case CODE:
		r = "code"
	case SROFF:
		r = "SROFF"
	case THORDIR:
		r = "Thor dir"
	case FONT:
		r = "font"
	case PATTERN:
		r = "pattern"
	case IMAGE1, IMAGE2:
		r = "image"
	case DIR:
		r = "dir"
	case ASCII:
		r = "ASCII"
	}
	return r
}

//
func toQLCheckSum(sum int) int {
	return (0x0f0f + sum) % 0x10000
}

//
func verifyQLCheckSum(sum, check int) error {
	if sum != check {
		return fmt.Errorf("want %d, got %d", check, sum)
	}
	return nil
}
