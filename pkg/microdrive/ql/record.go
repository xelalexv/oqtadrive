/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2021, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package ql

import (
	"encoding/hex"
	"fmt"
	"io"

	log "github.com/sirupsen/logrus"

	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/raw"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/types"
	"codeberg.org/xelalexv/oqtadrive/pkg/util"
)

// -
var recordIndex = map[string][2]int{
	"flags":             {12, 1}, // more correctly, the file number
	"number":            {13, 1}, // block number
	"header":            {12, 2},
	"headerChecksum":    {14, 2},
	"data":              {24, 512},
	"fileHeader":        {24, 64}, // start of file header, only in first record
	"length":            {24, 4},  //                               of each file
	"accessKey":         {28, 1},
	"fileType":          {29, 1},
	"fileInfo":          {30, 8},
	"name":              {38, 38}, // 2 bytes for length + name chars
	"dateUpdate":        {76, 4},
	"dateReference":     {80, 4},
	"dateBackup":        {84, 4}, //                          end of file header
	"dataChecksum":      {536, 2},
	"extraData":         {538, 84},
	"extraDataChecksum": {622, 2},
}

// -
var fileHaederIndex = map[string][2]int{
	"length":        {0, 4},
	"accessKey":     {4, 1},
	"fileType":      {5, 1},
	"fileInfo":      {6, 8},
	"name":          {14, 38}, // 2 bytes for length + name chars
	"nameLength":    {14, 2},
	"dateUpdate":    {52, 4},
	"dateReference": {56, 4},
	"dateBackup":    {60, 4},
}

// -
type record struct {
	muxed []byte
	block *raw.Block
}

// -
func NewRecord(data []byte, isRaw bool) (*record, error) {

	r := &record{}

	if isRaw {
		data = raw.CutToSize(data, RecordLengthMux+FormatExtraBytes, RecordLengthMux)
		r.block = raw.NewBlock(recordIndex, raw.Demux(data, true))
		r.muxed = data
	} else {
		dmx := make([]byte, len(data))
		copy(dmx, data)
		r.block = raw.NewBlock(recordIndex, dmx)
		r.mux()
	}

	return r, r.Validate()
}

// -
func newBlankRecord() *record {
	dmx := make([]byte, RecordLength+FormatExtraBytes)
	raw.CopySyncPattern(dmx)
	raw.CopyDataSyncPattern(dmx[DataSyncStart:])
	return &record{block: raw.NewBlock(recordIndex, dmx)}
}

// -
func (r *record) Client() types.Client {
	return types.QL
}

// -
func (r *record) Muxed() []byte {
	return r.muxed
}

// -
func (r *record) Demuxed() []byte {
	return r.block.Data
}

// -
func (r *record) mux() {
	r.muxed = raw.Mux(r.block.Data, true)
}

// more correctly, the file number
func (r *record) Flags() byte {
	return r.block.GetByte("flags")
}

// -
func (r *record) setFlags(f byte) {
	r.block.SetByte("flags", f)
}

// block number
func (r *record) Index() int {
	return int(r.block.GetByte("number"))
}

// -
func (r *record) setIndex(i byte) {
	r.block.SetByte("number", i)
}

// -
func (r *record) Length() int {
	l := r.block.GetSlice("length")
	if len(l) != 4 {
		return -1
	}
	return util.ToIntBE(l)
}

// -
func (r *record) setLength(l int) {
	r.block.SetSlice("length", util.ToBytesBE(l))
}

// -
func (r *record) Name() string {
	return fileNameString(r.block.GetSlice("name"))
}

// -
func (r *record) setName(n string) {
	r.block.SetSlice("name", fileNameBytes(n))
}

// -
func (r *record) Type() byte {
	return r.block.GetByte("fileType")
}

// -
func (r *record) setType(t byte) {
	r.block.SetByte("fileType", t)
}

// -
func (r *record) fileHeader() *raw.Block {
	return raw.NewBlock(fileHaederIndex, r.block.GetSlice("fileHeader"))
}

// -
func (r *record) HeaderChecksum() int {
	return r.block.GetInt("headerChecksum")
}

// -
func (r *record) InUse() bool {
	return r.Flags() != FBadSector && r.Flags() != FVacantSector
}

// -
func (r *record) Data() []byte {
	return r.block.GetSlice("data")
}

// -
func (r *record) Payload(fileLength int, last bool) ([]byte, error) {

	d := r.Data()

	if last {
		if end := fileLength % len(d); end > 0 {
			d = d[:end]
		}
	}

	if r.Index() == 0 {
		if len(d) < FileHeaderLength {
			return nil, fmt.Errorf("incomplete record at index %d", r.Index())
		}
		d = d[FileHeaderLength:]
	}

	return d, nil
}

// -
func (r *record) DataChecksum() int {
	return r.block.GetInt("dataChecksum")
}

// -
func (r *record) CalculateHeaderChecksum() int {
	return toQLCheckSum(r.block.Sum("header"))
}

// -
func (r *record) CalculateDataChecksum() int {
	return toQLCheckSum(r.block.Sum("data"))
}

// -
func (r *record) CalculateExtraDataChecksum() int {
	return toQLCheckSum(r.block.Sum("extraData"))
}

// -
func (r *record) fixHeaderChecksum() error {
	if err := r.block.SetInt(
		"headerChecksum", r.CalculateHeaderChecksum()); err != nil {
		return err
	}
	return nil
}

// -
func (r *record) fixDataChecksum() error {
	if err := r.block.SetInt(
		"dataChecksum", r.CalculateDataChecksum()); err != nil {
		return err
	}
	return nil
}

// -
func (r *record) fixExtraDataChecksum() error {
	if err := r.block.SetInt(
		"extraDataChecksum", r.CalculateExtraDataChecksum()); err != nil {
		return err
	}
	return nil
}

// -
func (r *record) FixChecksums() error {
	if err := r.fixHeaderChecksum(); err != nil {
		return err
	}
	if err := r.fixDataChecksum(); err != nil {
		return err
	}
	if err := r.fixExtraDataChecksum(); err != nil {
		return err
	}
	r.mux()
	return r.Validate()
}

// -
func (r *record) Replaces(old types.Record, sectorIx int) *types.FileChange {

	if old == nil || r == nil {
		return nil
	}

	if or, ok := old.(*record); ok {

		o := or.Demuxed()
		n := r.Demuxed()

		if len(o) > len(n) {
			log.Debug("keeping old record extra data")
			copy(o, n)
			r.block.Data = o
			r.mux()
		}

		fc := &types.FileChange{}

		if sectorIx == 0 { // sector map, always modify
			fc.Name = FSectorMapName
			fc.Op = types.OpModified
		} else if r.Flags() == 0 { // directory, always modify
			fc.Name = FDirectoryName
			fc.Op = types.OpModified
		}
		// all other changes are caught via files snapshot comparison,
		// see daemon/listener.go
		return fc
	}

	return nil
}

// -
func (r *record) Validate() error {

	err := types.NewMediaError()

	// Minerva ROM writes incorrect record header check sum 0x1000 during format.
	// Correct sum is 0x100c. If calculating the checksum returns 0x100c, we
	// accept 0x1000 and 0x100c in the checksum field. Otherwise we do standard
	// checksum validation.
	sum := r.CalculateHeaderChecksum()
	cs := r.block.GetInt("headerChecksum")
	if e := verifyQLCheckSum(sum, cs); e != nil {
		if sum == 0x100c {
			switch cs {
			case 0x1000:
			case 0x100c:
			default:
				err.Add(fmt.Sprintf(
					"unexpected value %d in record header check sum field, accepted values: 0x1000, 0x100c", cs),
					types.Fatal)
			}
		} else {
			err.Add(fmt.Sprintf(
				"invalid record header check sum: %v", e), types.Fatal)
		}
	}

	// Minerva ROM writes incorrect record data check sum 0x0e00 during format.
	// Correct sum is 0x0e0f. If calculating the checksum returns 0x0e0f, we
	// accept 0x0e00 and 0x0e0f in the checksum field, but return a media error
	// of level benign if formatting. When not formatting, the wrong checks sum
	// cannot be accepted. Otherwise we do standard checksum validation.
	sum = r.CalculateDataChecksum()
	cs = r.block.GetInt("dataChecksum")
	if e := verifyQLCheckSum(sum, cs); e != nil {
		if sum == 0x0e0f {
			switch cs {
			case 0x0e00, 0x0e0f:
				err.Add(fmt.Sprintf(
					"invalid record data check sum: %v (benign)", e),
					types.BenignIfFormatting)
			default:
				err.Add(fmt.Sprintf(
					"unexpected value %d in record data check sum field, accepted values: 0x0e00, 0x0e0f", cs),
					types.Fatal)
			}
		} else {
			err.Add(fmt.Sprintf("invalid record data check sum: %v", e), types.Fatal)
		}
	}

	if r.block.Length() >= RecordLength+FormatExtraBytes {
		// Extra data checksum is written as 0x3b19, or 0x3afe on Minerva, which
		// is not correct. The actual sum would be 0x38e5. If calculating the
		// checksum returns 0x38e5, we accept 0x3b19, 0x3afe, and of course
		// 0x38e5 in the checksum field. Otherwise we do standard checksum
		// validation.
		sum = r.CalculateExtraDataChecksum()
		cs = r.block.GetInt("extraDataChecksum")
		if e := verifyQLCheckSum(sum, cs); e != nil {
			if sum == 0x38e5 {
				switch cs {
				case 0x38e5:
				case 0x3afe:
				case 0x3b19:
				default:
					err.Add(fmt.Sprintf(
						"unexpected value %d in record extra data check sum field, accepted values: 0x38e5, 0x3afe, 0x3b19", cs),
						types.Benign)
				}
			} else {
				err.Add(fmt.Sprintf(
					"invalid record extra data check sum: %v;", e), types.Benign)
			}
		}
	}

	if err.Level() > 0 {
		return err.Done()
	}
	return nil
}

// -
func (r *record) Emit(w io.Writer) {
	io.WriteString(w, fmt.Sprintf("\nRECORD: flag: %X, index: %d\n",
		r.Flags(), r.Index()))
	d := hex.Dumper(w)
	defer d.Close()
	d.Write(r.block.Data)
}
