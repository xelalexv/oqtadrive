/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2022, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package ql

import (
	"strings"
)

// FIXME: 0xb5 should be "Q\u032c"
var qlCharset = []rune{
	'-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '\n', '-', '-', '-', '-', '-',
	'-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-',
	' ', '!', '"', '#', '$', '%', '&', '\'', '(', ')', '*', '+', ',', '-', '.', '/',
	'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ':', ';', '<', '=', '>', '?',
	'@', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O',
	'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '[', '\\', ']', '^', '_',
	'£', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
	'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '{', '|', '}', '~', '©',
	'ä', 'ã', 'å', 'é', 'ö', 'õ', 'ø', 'ü', 'ç', 'ñ', 'æ', 'œ', 'á', 'à', 'â', 'ë',
	'è', 'ê', 'ï', 'í', 'ì', 'î', 'ó', 'ò', 'ô', 'ú', 'ù', 'û', 'ß', '¢', '¥', '`',
	'Ä', 'Ã', 'Å', 'É', 'Ö', 'Õ', 'Ø', 'Ü', 'Ç', 'Ñ', 'Æ', 'Œ', 'α', 'δ', 'θ', 'λ',
	'μ', 'π', 'ϕ', '¡', '¿', 'Q', '§', '¤', '°', '÷', '«', '»', '←', '→', '↑', '↓',
	'↖', '↗', '↙', '↘', 'Δ', 'η', 'Φ', 'Γ', '♠', '♥', '♦', '♣', 'Λ', '∇', '∞', 'Ω',
	'Π', 'Ψ', '®', 'Σ', 'Θ', 'Υ', '†', '‡', 'Ξ', '±', 'ς', '≡', '≤', '≠', '≥', '≈',
	'□', '■', '●', 'χ', '∂', 'ε', '₣', 'γ', 'ħ', 'ι', '¦', 'κ', '¼', '½', '¾', 'ω',
	'ψ', '⇒', 'ρ', 'σ', 'τ', 'υ', '√', '∛', 'ξ', '…', 'ζ', '⌠', '⎮', '⌡', '░', '▒',
}

// -
func Translate(s string) string {
	if strings.HasPrefix(s, "\x00") {
		return ""
	}
	var ret strings.Builder
	for _, b := range []byte(s) {
		ret.WriteRune(qlCharset[b])
	}
	return ret.String()
}
