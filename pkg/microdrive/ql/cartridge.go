/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2021, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package ql

import (
	"fmt"
	"math/rand"
	"os"

	log "github.com/sirupsen/logrus"

	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/base"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/fs"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/types"
)

// -
func NewCartridge() *base.Cartridge {
	impl := &cartridge{}
	cart := base.NewCartridge(types.QL, SectorCount, impl)
	impl.Cartridge = cart
	return cart
}

// -
type cartridge struct {
	*base.Cartridge
}

// -
func (c *cartridge) SectorSetAt(ix int, s types.Sector) {
}

// -
func (c *cartridge) Erased() {
}

// -
func (c *cartridge) Specials() string {
	return ""
}

// -
func (c *cartridge) FS() fs.FileSystem {
	return newFs(c.Cartridge)
}

// -
func (c *cartridge) Format(name string) error {

	log.WithField("name", name).Debug("formatting cartridge")

	// FIXME consider translation to QL character set
	name = base.ToCartridgeName(name)

	rnd := rand.Int()
	var err error

	for ix := 0; ix < c.Size(); ix++ {

		var h *header
		var r *record

		s := c.GetSectorAt(ix)
		if s == nil || s.Header() == nil || s.Record() == nil {
			h = newBlankHeader()
			r = newBlankRecord()
			if s, err = types.NewSector(h, r); err != nil {
				return err
			}
			c.SetSectorAt(ix, s)
		} else {
			h = s.Header().(*header)
			r = s.Record().(*record)
			r.block.ZeroOutSlice("data")
		}

		h.setName(name)
		h.setIndex(ix)
		h.setFlags(BHeaderFlag)
		h.setRandom(rnd)
		if err = h.FixChecksum(); err != nil {
			return err
		}

		r.setIndex(0)
		switch ix {

		case 0:
			r.setFlags(FSectorMap)
			sm := sectorMap{sectors: r.Data()}
			for ix := range sm.sectors {
				sm.setSector(ix, FVacantSector, 0)
			}
			sm.setSector(0, FSectorMap, 0)
			sm.setSector(DirectoryDefaultSector, 0, 0)

		case DirectoryDefaultSector:
			r.setFlags(FDirectory)
			r.setLength(FileHeaderLength)

		default:
			r.setFlags(FVacantSector)
		}

		if err = r.FixChecksums(); err != nil {
			return err
		}

		if log.IsLevelEnabled(log.TraceLevel) {
			s.Emit(os.Stdout)
		}
	}

	c.SetName(name)
	c.SeekToStart()
	c.SetModified(true)

	return err
}

// -
func (c *cartridge) Rename(n string) error {

	var err error
	// FIXME consider translation to QL character set
	n = base.ToCartridgeName(n)
	rand := -1

	for ix := 0; ix < c.Size() && err == nil; ix++ {
		if hd, ok := c.GetSectorAt(ix).Header().(*header); ok {
			if rand == -1 {
				rand = hd.Random() + 1
			}
			if err = hd.setName(n); err == nil {
				hd.setRandom(rand)
				err = hd.FixChecksum()
			}
		} else {
			err = fmt.Errorf("not a QL header")
		}
	}

	c.SetName(n)
	c.SetModified(true)

	return err
}

// -
func (c *cartridge) Reorganize() error {
	sort := fs.NewFileSorter(true)
	sort.AddHeads("*boot", "*run")
	return c.Cartridge.Reorg(NewCartridge(), sort.BySize)
}
