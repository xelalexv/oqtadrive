/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2022, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package ql

import (
	"encoding/hex"
	"fmt"
	"io"
	"strings"

	log "github.com/sirupsen/logrus"

	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/base"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/fs"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/raw"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/types"
	"codeberg.org/xelalexv/oqtadrive/pkg/util"
)

// -
type FileSystem interface {
	fs.FileSystem
	//
	ReclaimSectors(s []int) error
}

// -
type file struct {
	*fs.File
	//
	fSys   *fsys
	fileID int
}

// -
func (f *file) FileHeaderLength() int {
	return FileHeaderLength
}

// -
func (f *file) Render(w io.Writer) error {

	switch f.Type() {
	case BASIC, ASCII:
		return util.StreamTranslate(w, f, 128, 0,
			func(s string, left rune) string {
				return Translate(s)
			})
	}

	d := hex.Dumper(w)
	defer d.Close()
	_, err := io.Copy(d, f)
	return err
}

// -
func (f *file) ClaimSectors(start, count int) ([]types.Sector, error) {

	log.WithFields(log.Fields{
		"start": start, "count": count}).Debug("claim sectors")

	if count == 0 {
		return nil, fmt.Errorf("nothing requested")
	}

	if f.fSys == nil {
		return nil, fs.ErrNoFileSystem
	}

	sm, err := f.fSys.getSectorMap()
	if err != nil {
		return nil, err
	}

	vacant := func(ix int) (bool, types.Sector) {
		var s types.Sector
		if f.fSys.cart.SeekToSector(ix, false) {
			s = f.fSys.cart.GetSector()
		}
		return sm.isVacant(ix), s
	}

	top := f.fSys.cart.GetAnnotation(base.AnnoReducedLength).Int()
	if top == 0 {
		top = f.fSys.cart.Size() - 1
	}

	opts := &fs.ClaimOpts{
		Start:    start,
		Count:    count,
		Top:      top,
		Bottom:   0,
		FirstGap: RecordGap,
		SecGap:   RecordGap,
	}

	if opts.Start < 0 {
		if opts.Start = sm.getLastAssigned(); opts.Start == 0 {
			opts.Start = -1
		}
		opts.FirstGap = FirstGap
	}

	sec, err := fs.SimpleClaim(opts, vacant)
	if err != nil {
		return nil, err
	}

	if f.fileID < 0 { // if file ID has not been set, we need to claim one now
		id, err := sm.claimFileID()
		if err != nil {
			return nil, err
		}
		f.fileID = id
		log.WithField("id", id).Debug("file ID claimed")
	}

	offset := f.RecordCount()
	last := 0
	for ix, s := range sec {
		last = s.Index()
		sm.setSector(last, byte(f.fileID), byte(offset+ix)) // add to sector map
	}
	sm.setLastAssigned(last)
	sm.fixCheckSum()

	return sec, nil
}

// -
func (f *file) CommitRecord(r types.Record, ix, length int, closing bool) error {

	log.WithFields(log.Fields{
		"ix": ix, "length": length, "closing": closing}).Trace("commit records")

	rImpl, ok := r.(*record)
	if !ok {
		return fmt.Errorf("not a QL record")
	}

	var err error

	if closing {
		if ix == 0 { // total length is passed with closing commit, but is only
			// set in file header, i.e. first record, last record needs no
			// further adjustments; note that this length includes header bytes
			if length < f.FileHeaderLength() { // uninitialized zero length file
				length = f.FileHeaderLength()
			}
			rImpl.setLength(length)

			// and file header needs to be added to directory
			if err = rImpl.FixChecksums(); err == nil {
				var sm *sectorMap
				if sm, err = f.fSys.getSectorMap(); err == nil {
					var dir *directory
					if dir, err = sm.getDirectory(); err == nil {
						err = dir.setHeader(f.fileID, rImpl.fileHeader())
					}
				}
			}
		}

	} else {
		if ix < 0 {
			return fmt.Errorf("invalid record index: %d", ix)
		}
		rImpl.setIndex(byte(ix))
		rImpl.setFlags(byte(f.fileID))

		if ix == 0 { // handle file header, only first record
			if hd := f.Header(); len(hd) == 0 {
				log.Trace("no header set, adjusting")
				rImpl.setName(f.Name()) // FIXME set name anyway?
				ft := f.Type().(FileType)
				rImpl.setType(byte(ft))
				if ft == CODE {
					rImpl.block.GetSlice("fileInfo")[2] = 0x20 // FIXME 8k data space
				}
			} else {
				log.Trace("header explicitly set, copying")
				copy(rImpl.Data(), hd)
			}
		}
		err = rImpl.FixChecksums()
	}

	return err
}

// -
func (f *file) ReleaseRecords(r []types.Record, purge bool) (int, error) {
	if f.fileID == 0 {
		return 0, nil
	}
	if f.fSys == nil {
		return 0, fs.ErrNoFileSystem
	}
	f.fSys.releaseRecords(f.fileID, r, purge)
	return len(r), nil
}

// -
func newFs(cart *base.Cartridge) *fsys {
	return &fsys{cart: cart}
}

// -
type fsys struct {
	cart   *base.Cartridge
	secMap *sectorMap
}

// -
func (fsys *fsys) Open(name string, mode fs.FileMode, typ fs.FileType) (
	*fs.File, error) {

	log.WithFields(log.Fields{"name": name, "mode": mode}).Debug("open")

	if name == "" {
		return nil, fs.ErrEmptyFileName
	}

	ft := UNKNOWN
	ok := false
	if typ != nil {
		if ft, ok = typ.(FileType); !ok {
			if typ.String() == ASCII.String() {
				ft = ASCII
			} else {
				return nil, fmt.Errorf("not a QL file type: %v", typ)
			}
		}
	}

	switch mode {
	case fs.READ:
		return fsys.openRead(name, ft)
	case fs.WRITE, fs.OVERWRITE:
		return fsys.openWrite(name, mode, ft)
	default:
		return nil, fs.ErrUnsupportedAccessMode
	}
}

// -
func (fsys *fsys) openRead(name string, typ FileType) (*fs.File, error) {
	return fsys.open(name, fs.READ, typ)
}

// -
func (fsys *fsys) openWrite(name string, mode fs.FileMode, typ FileType) (
	*fs.File, error) {

	var ret *fs.File

	switch mode {

	case fs.WRITE:
		if fsys.exists(name) {
			return nil, fmt.Errorf(fs.FileExists, name)
		}

	case fs.OVERWRITE:
		var err error
		if ret, err = fsys.open(name, mode, typ); err == nil { // file exists
			return ret, nil
		} else if !fs.IsAnyOf(err, fs.FileNotFound, fs.NoHeaderForFile) {
			return nil, err
		}
	}

	impl := &file{fileID: -1, fSys: fsys}
	ret = fs.NewFile(name, mode, typ, 0, -1, nil, impl)
	impl.File = ret

	return ret, nil
}

// -
func (fsys *fsys) open(name string, mode fs.FileMode, typ FileType) (
	*fs.File, error) {

	num, hd, err := fsys.getHeader(name)
	if err != nil {
		return nil, err
	}

	rec, last, err := fsys.secMap.collectFileRecords(num)
	if err != nil {
		return nil, err
	}

	if len(rec) == 0 {
		return nil, fs.ErrFileNotFound
	}

	impl := &file{fSys: fsys, fileID: num}
	if typ == UNKNOWN {
		typ = FileType(rec[0].(*record).Type())
	}
	ret := fs.NewFile(name, mode, typ,
		util.ToIntBE(hd.GetSlice("length")), last, rec, impl)
	impl.File = ret

	return ret, nil
}

// -
func FIXME_ChangeMedia(cart *base.Cartridge) error {

	log.Debug("simulating media change")
	rand := -1

	for ix := 0; ix < cart.Size(); ix++ {
		if s := cart.GetSectorAt(ix); s != nil {
			hd, ok := s.Header().(*header)
			if !ok {
				return fmt.Errorf("not a QL cartridge")
			}
			if rand == -1 {
				rand = hd.Random() + 1
			}
			hd.setRandom(rand)
			hd.FixChecksum()
		}
	}

	return nil
}

// -
func (fsys *fsys) Delete(name string, purge bool) (bool, error) {

	num, hd, err := fsys.getHeader(name)
	if err != nil {
		return false, nil
	}

	var rec []types.Record

	if purge {
		rec, _, err = fsys.secMap.collectFileRecords(num)
		if err != nil {
			return false, err
		}
		hd.ZeroOutSlice("name")
		hd.SetByte("fileType", 0)

	} else {
		hd.ZeroOutSlice("nameLength")
	}

	hd.ZeroOutSlice("length")

	fsys.secMap.dir.fixChecksum(num)
	fsys.releaseRecords(num, rec, purge)

	return true, nil
}

// -
func (fsys *fsys) releaseRecords(file int, rec []types.Record, purge bool) {

	if purge {
		for _, r := range rec {
			if rImpl, ok := r.(*record); ok {
				clear(rImpl.Data())
				rImpl.fixDataChecksum()
			}
		}
	}

	fsys.secMap.releaseFileRecords(file, rec)
}

// -
func (fsys *fsys) ParseFileType(t string) fs.FileType {
	return parseFileType(t)
}

// -
func (fsys *fsys) exists(name string) bool {
	_, _, err := fsys.getHeader(name)
	return err == nil
}

// -
func (fsys *fsys) getHeader(name string) (int, *raw.Block, error) {

	sm, err := fsys.getSectorMap()
	if err != nil {
		return -1, nil, err
	}

	dir, err := sm.getDirectory()
	if err != nil {
		return -1, nil, err
	}

	return dir.getHeader(name)
}

// -
func (fsys *fsys) getSectorMap() (*sectorMap, error) {
	var err error
	if fsys.secMap == nil {
		fsys.secMap, err = newSectorMap(fsys)
	}
	return fsys.secMap, err
}

// -
func (fsys *fsys) Ls() (*fs.Stats, []*fs.FileInfo, error) {

	sm, err := fsys.getSectorMap()
	if err != nil {
		return nil, nil, err
	}

	dir, err := sm.getDirectory()
	if err != nil {
		return nil, nil, err
	}

	return fs.NewStats(SectorCount, fsys.secMap.used()), dir.ls(), nil
}

// -
func (fsys *fsys) ReclaimSectors(s []int) error {

	sm, err := fsys.getSectorMap()
	if err != nil {
		return err
	}

	for _, ix := range s {
		sm.setSector(ix, FVacantSector, 0)
	}

	sm.fixCheckSum()
	return nil
}

// neither bad nor free for use
func isInUse(r *record) bool {
	return r.Flags() <= FMaxFileNumber
}

// -
func canonicalName(n string) string {
	return strings.TrimSpace(Translate(n))
}

// -
func fileNameString(b []byte) string {
	if len(b) == 38 {
		l := (int(b[0]) << 8) | int(b[1])
		if 0 < l && l <= len(b)-2 {
			return string(b[2 : 2+l])
		}
	}
	return ""
}

// -
func fileNameBytes(n string) []byte {
	raw := []byte(n)
	l := len(raw)
	if l > 36 {
		l = 36
	}
	s := make([]byte, 38)
	s[0] = byte(l >> 8)
	s[1] = byte(l & 0xff)
	copy(s[2:], raw)
	return s
}
