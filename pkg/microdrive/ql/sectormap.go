/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2022, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package ql

import (
	"fmt"

	log "github.com/sirupsen/logrus"

	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/types"
)

// -
func newSectorMap(fs *fsys) (*sectorMap, error) {

	index := make(map[int]int)
	var zero types.Sector

	if fs.cart == nil {
		return nil, fmt.Errorf("file system without cartridge")
	}

	for ix := 0; ix < fs.cart.Size(); ix++ {
		if s := fs.cart.GetSectorAt(ix); s != nil {
			index[s.Index()] = ix
			if r := s.Record().(*record); r != nil {
				// documentation states that sector map is located in sector 0,
				// bearing file number 0xf8, but 0x80 is also often observed,
				// not sure why; we need to consider both
				if s.Index() == 0 &&
					(r.Flags() == FSectorMap || r.Flags() == FSectorMapAlt) {
					if zero != nil {
						return nil, fmt.Errorf("more than one zero block found")
					}
					zero = s
					continue
				}
			}
		}
	}

	if zero == nil {
		return nil, fmt.Errorf("no sector map sector")
	}

	sectors := zero.Record().(*record).Data()

	// double-check that we're really seeing a sector map, i.e. file number for
	// sector 0 should be that to the sector map (see comment above)
	if sectors[0] != FSectorMap && sectors[1] != FSectorMapAlt {
		return nil, fmt.Errorf("not a sector map")
	}

	return &sectorMap{
		fs:        fs,
		mapSector: zero,
		sectors:   sectors,
		index:     index}, nil
}

// -
type sectorMap struct {
	fs        *fsys
	mapSector types.Sector
	sectors   []byte
	index     map[int]int
	dir       *directory
}

// -
func (sm *sectorMap) getSector(ix int) (int, int) {
	if sm.isValidIndex(ix) {
		return int(sm.sectors[ix*2]), int(sm.sectors[ix*2+1])
	}
	return -1, -1
}

// -
func (sm *sectorMap) isVacant(ix int) bool {
	return sm.isValidIndex(ix) && sm.sectors[ix*2] == FVacantSector
}

// -
func (sm *sectorMap) setSector(ix int, num, block byte) {
	if sm.isValidIndex(ix) {
		log.Debugf("setting sector ix=%d, num=%d, block=%d", ix, num, block)
		sm.sectors[ix*2] = num
		sm.sectors[ix*2+1] = block
	}
}

// -
func (sm *sectorMap) getLastAssigned() int {
	return int(sm.sectors[len(sm.sectors)-1])
}

// -
func (sm *sectorMap) setLastAssigned(ix int) {
	sm.sectors[len(sm.sectors)-1] = byte(ix)
}

// -
func (sm *sectorMap) isValidIndex(ix int) bool {
	return 0 <= ix && 2*ix < len(sm.sectors)
}

// -
func (sm *sectorMap) fixCheckSum() {
	r := sm.mapSector.Record().(*record)
	r.fixDataChecksum()
	r.mux()
}

// records are in record order, i.e. as they appear in file
func (sm *sectorMap) collectFileRecords(number int) ([]types.Record, int, error) {
	return collectFile[types.Record](sm, number, false)
}

// sectors are in sector order, i.e. as they appear in cartridge
func (sm *sectorMap) collectFileSectors(number int) ([]types.Sector, int, error) {
	return collectFile[types.Sector](sm, number, true)
}

// -
func collectFile[T comparable](sm *sectorMap, number int, getSectors bool) (
	[]T, int, error) {

	var ret []T
	var null T
	var sIx int
	last := -1

	for s := 0; s < SectorCount; s++ {

		if sIx = s; getSectors {
			sIx = SectorCount - s
		}

		if fNum, rNum := sm.getSector(sIx); fNum == number {

			if !getSectors {
				if d := rNum + 1 - len(ret); d > 0 {
					ret = append(ret, make([]T, d)...)
				}
			}

			if ix, ok := sm.index[sIx]; ok {
				if sec := sm.fs.cart.GetSectorAt(ix); sec != nil {
					if rec := sec.Record(); rec != nil {
						if getSectors {
							ret = append(ret, sec.(T))
						} else {
							if ret[rNum] != null {
								return nil, -1,
									fmt.Errorf("duplicate record at: %d", rNum)
							}
							ret[rNum] = rec.(T)
						}
						if last == -1 {
							last = ix
						}
					} else {
						return nil, -1, fmt.Errorf("no record in sector: %d", s)
					}
				} else {
					return nil, -1, fmt.Errorf("no sector at index %d", s)
				}
			} else {
				return nil, -1, fmt.Errorf("sector not found: %d", s)
			}
		}
	}

	return ret, last, nil
}

// -
func (sm *sectorMap) releaseFileRecords(number int, rec []types.Record) {

	var ixs map[int]bool
	if len(rec) > 0 {
		ixs = make(map[int]bool)
		for _, r := range rec {
			ixs[r.Index()] = true
		}
	}

	mod := false
	for s := 0; s < SectorCount; s++ {
		if fNum, rIx := sm.getSector(s); fNum == number &&
			(len(rec) == 0 || ixs[rIx]) {
			sm.setSector(s, FVacantSector, 0)
			mod = true
		}
	}

	if mod {
		sm.fixCheckSum()
	}
}

// -
func (sm *sectorMap) used() int {
	used := SectorCount
	for ix := 0; ix < SectorCount; ix++ {
		if fNum, _ := sm.getSector(ix); fNum == FVacantSector {
			used--
		}
	}
	return used
}

// -
func (sm *sectorMap) claimFileID() (int, error) {

	used := make([]bool, 256)

	for ix := 0; ix < len(sm.sectors); ix += 2 {
		used[sm.sectors[ix]] = true
	}

	for ix, u := range used {
		if !u && ix <= FMaxFileNumber {
			return ix, nil
		}
	}

	return -1, fmt.Errorf("file IDs depleted")
}

// -
func (sm *sectorMap) getDirectory() (*directory, error) {
	if sm.dir == nil {
		rec, last, err := sm.collectFileRecords(0)
		if err != nil {
			return nil, err
		}
		if len(rec) == 0 {
			return nil, fmt.Errorf("no directory file")
		}
		sm.dir = &directory{fs: sm.fs, records: rec, lastSectorIx: last}
	}
	return sm.dir, nil
}
