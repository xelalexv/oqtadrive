/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2022, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package ql

import (
	"fmt"
	"sort"

	log "github.com/sirupsen/logrus"

	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/fs"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/raw"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/types"
	"codeberg.org/xelalexv/oqtadrive/pkg/util"
)

// -
type fileInfo struct {
	id       int
	name     string
	size     int
	typ      FileType
	sequence *fs.RecordSequence
	util.Annotations
}

// -
type directory struct {
	fs           *fsys
	records      []types.Record
	lastSectorIx int
}

// -
func (d *directory) length() int {
	if len(d.records) > 0 {
		if data := d.records[0].Data(); len(data) > 3 {
			return util.ToIntBE(data[:4])
		}
	}
	return 0
}

// -
func (d *directory) ls() []*fs.FileInfo {

	var ret []*fs.FileInfo

	dir := make(map[string]*fileInfo)

	if lDir := d.length(); lDir > FileHeaderLength {

		// 0 is directory itself, so we can skip very first header
		pos := FileHeaderLength
		posRec := pos

		b := raw.NewBlock(fileHaederIndex, nil)

	records:
		for _, r := range d.records {
			d := r.Data()
			for ; posRec < 512; posRec += FileHeaderLength {
				b.Data = d[posRec : posRec+FileHeaderLength]
				// FIXME: do things like this belong into FS methods?
				n := canonicalName(fileNameString(b.GetSlice("name")))
				dir[n] = &fileInfo{
					id:          pos / FileHeaderLength,
					name:        n,
					size:        util.ToIntBE(b.GetSlice("length")),
					typ:         FileType(b.GetByte("fileType")),
					sequence:    fs.NewRecordSequence(SectorCount-1, 0, RecordGap),
					Annotations: make(util.Annotations),
				}
				if pos += FileHeaderLength; pos > lDir {
					break records
				}
			}
			posRec = 0
		}

		var files []string
		for f := range dir {
			if f != "" {
				files = append(files, f)
			}
		}
		sort.Strings(files)

		ret = make([]*fs.FileInfo, len(files))
		for ix, name := range files {
			fi := dir[name]
			ret[ix] = fs.NewFileInfo(name, "", "", fi.size, fi.typ)
			ret[ix].AddAnnotations(fi.Annotations)
			if sectors, _, err := d.fs.secMap.collectFileSectors(fi.id); err == nil {
				for _, s := range sectors {
					fi.sequence.Add(s.Record().Index(), s.Index())
				}
			}
			ret[ix].Annotate("fragmentation", fi.sequence.Fragmentation(false))
		}
	}

	return ret
}

// -
func (d *directory) fixChecksum(id int) error {
	rec := id * FileHeaderLength / 512
	if 0 <= rec && rec < len(d.records) {
		return d.records[rec].FixChecksums()
	} else {
		return fmt.Errorf("no directory record for file id %d", id)
	}
}

// -
func (d *directory) getHeader(name string) (int, *raw.Block, error) {

	err := fmt.Errorf(fs.NoHeaderForFile, name)

	if lDir := d.length(); lDir > FileHeaderLength {

		// 0 is directory itself, so we can skip very first header
		pos := FileHeaderLength
		posRec := pos

		b := raw.NewBlock(fileHaederIndex, nil)

		for _, r := range d.records {
			d := r.Data()
			for ; posRec < 512; posRec += FileHeaderLength {
				b.Data = d[posRec : posRec+FileHeaderLength]
				if n := fileNameString(b.GetSlice("name")); n == name {
					return pos / FileHeaderLength, b, nil
				}
				if pos += FileHeaderLength; pos > lDir {
					return -1, nil, err
				}
			}
			posRec = 0
		}
	}

	return -1, nil, err
}

// -
func (d *directory) setHeader(fileID int, b *raw.Block) error {

	if fileID < 0 || fileID > FMaxFileNumber {
		return fmt.Errorf("invalid file ID: %d", fileID)
	}

	if len(b.Data) != FileHeaderLength {
		return fmt.Errorf("invalid file header length: want %d, got %d",
			FileHeaderLength, len(b.Data))
	}

	pos := fileID * FileHeaderLength  // absolute position in directory
	recIx := pos / fs.FileBlockLength // index of directory record
	recCount := len(d.records)

	if add := recIx - recCount + 1; add > 0 {
		impl := &file{fileID: 0, fSys: d.fs}
		f := fs.NewFile("", fs.WRITE, BASIC, 0, -1, d.records, impl)
		impl.File = f
		sec, err := f.ClaimSectors(d.lastSectorIx, add)
		if err != nil {
			return err
		}
		for ix, s := range sec {
			r := s.Record().(*record)
			r.block.ZeroOutSlice("data")
			if err = f.CommitRecord(r, recCount+ix, 0, false); err != nil {
				return err
			}
			d.records = append(d.records, r)
		}
	}

	start := pos % fs.FileBlockLength // position within record
	rec := d.records[recIx].(*record)
	copy(rec.Data()[start:start+FileHeaderLength], b.Data)
	if err := rec.FixChecksums(); err != nil {
		return err
	}

	if rec = d.records[0].(*record); rec.Length() <= pos {
		log.Debugf("adjusting directory length to %d", pos+FileHeaderLength)
		rec.setLength(pos + FileHeaderLength)
		return rec.FixChecksums()
	}

	return nil
}
