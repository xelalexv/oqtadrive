/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2021, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package base

import (
	"context"
	"fmt"
	"io"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"

	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/fs"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/types"
	"codeberg.org/xelalexv/oqtadrive/pkg/util"
)

// -
const (
	AnnoReducedLength = "reducedLength"
	AnnoNameHint      = "nameHint"
)

// -
func ToCartridgeName(n string) string {
	return fmt.Sprintf("%.10s", fmt.Sprintf("%-10s", n))
}

// NOTE:
// file change listeners are invoked asynchronously, but receive an already
// locked cartridge, which they may unlock at their discretion before returning;
// they should always only perform short tasks that finish quickly!
// -
type FileChangeListener func(drive int, c *Cartridge, fc *types.FileChange) error

// -
type CartridgeImpl interface {

	//
	FS() fs.FileSystem

	//
	Rename(name string) error

	//
	Format(name string) error

	//
	Reorganize() error

	//
	SectorSetAt(ix int, s types.Sector)

	//
	Erased()

	//
	Specials() string

	// FIXME add something like subtype?
}

// -
type CartridgeRange interface {
	// when ix is -1, round through cartridge has been completed;
	// s is nil in that case
	Next() (ix int, s types.Sector)
}

// -
type cartRange struct {
	cart  *Cartridge
	start types.Sector
	index int
}

// -
func (r *cartRange) Next() (int, types.Sector) {

	if r.index == -1 { // iteration was already finished
		return -1, nil
	}

	s := r.cart.GetNextSector()

	if r.start == nil { // start of iteration
		r.start = s
	} else if r.start == s { // end of iteration
		r.index = -1
		return -1, nil
	}

	ix := r.index
	r.index++
	return ix, s
}

// -
type cImpl = CartridgeImpl

// -
func NewCartridge(c types.Client, sectorCount int, impl CartridgeImpl) *Cartridge {
	return &Cartridge{
		cImpl:       impl,
		client:      c, // FIXME move
		Annotations: make(util.Annotations),
		sectors:     make([]types.Sector, sectorCount),
		accessIx:    sectorCount - 1,
		lastMod:     time.Now(),
		lock: util.NewLock(
			func(locked bool) {
				if locked {
					log.Trace("cartridge locked")
				} else {
					log.Debug("cartridge lock timed out")
				}
			},
			func(released bool) {
				if released {
					log.Trace("cartridge unlocked")
				} else {
					log.Debug("cartridge was already unlocked")
				}
			}),
	}
}

// -
type Cartridge struct {
	//
	cImpl
	//
	name           string
	writeProtected bool
	client         types.Client
	util.Annotations
	//
	sectors    []types.Sector
	accessIx   int
	modified   bool
	lastMod    time.Time
	autosaved  bool
	formatting bool
	//
	lock *util.Lock
}

// -
func (c *Cartridge) Lock(ctx context.Context) bool {
	return c.lock.Acquire(ctx)
}

// -
func (c *Cartridge) Unlock() {
	c.lock.Release()
}

// -
func (c *Cartridge) IsLocked() bool {
	return c.lock.IsLocked()
}

// -
func (c *Cartridge) Client() types.Client {
	return c.client
}

// -
func (c *Cartridge) Name() string {
	return c.name
}

// -
func (c *Cartridge) SetName(n string) {
	c.name = n
}

// Range returns a cartridge range that starts ranging at the top most sector
func (c *Cartridge) Range() CartridgeRange {
	c.SeekToStart()
	return &cartRange{cart: c}
}

// RangeFromSector returns a cartridge range that starts ranging at the given
// index
func (c *Cartridge) RangeFromSector(ix int) CartridgeRange {
	c.SeekToSector(ix, true)
	return &cartRange{cart: c}
}

// Size returns the capacity of the cartridge, i.e. the max possible number of
// sectors.
func (c *Cartridge) Size() int {
	return len(c.sectors)
}

// SectorCount returns the number of non-nil sectors currently held by this
// cartridge
func (c *Cartridge) SectorCount() int {
	var cnt int
	for ix := 0; ix < c.Size(); ix++ {
		if c.sectors[ix] != nil {
			cnt++
		}
	}
	return cnt
}

// SeekToStart sets the access index such that the next call to GetNextSector
// will retrieve the top-most sector, i.e. the sector with the highest sector
// number.
func (c *Cartridge) SeekToStart() {

	if !c.IsFormatted() {
		return
	}

	max := 0
	maxIx := -1

	for ix, sec := range c.sectors {
		if sec != nil && sec.Index() > max {
			max = sec.Index()
			maxIx = ix
		}
	}

	if maxIx > -1 {
		c.accessIx = maxIx
		c.RewindAccessIx(false)
	}
}

// SeekToSector sets the access index such that the next call to GetNextSector
// will retrieve the sector with the given index number, if cue is true.
// Otherwise, the access index will point directly at the sector. If the
// requested sector exists, true is returned, if not, false and the access
// position is arbitrary.
func (c *Cartridge) SeekToSector(ix int, cue bool) bool {

	if c.IsFormatted() {

		pos := -1

		// First try to seek by calculating the delta between current sector
		// and desired one, and moving by that amount. This should always work
		// for well-formatted, strictly monotone cartridges.
		if s := c.GetSector(); s != nil {
			if d := s.Index() - ix; d == 0 { // already at right sector
				pos = c.accessIx
			} else { // need to move
				if d > 0 {
					c.AdvanceAccessIxBy(d, false)
				} else {
					c.RewindAccessIxBy(-d, false)
				}
				if s = c.GetSector(); s != nil && s.Index() == ix {
					pos = c.accessIx // hit right sector, all good
				}
			}
		}

		if pos == -1 { // do a linear search in case of any irregularities
			log.Trace(
				"cartridge not strictly monotone, need to linear search for sector")
			for i, s := range c.sectors {
				if s != nil && s.Index() == ix {
					pos = i
					break
				}
			}
		}

		if pos > -1 {
			c.accessIx = pos
			if cue {
				c.RewindAccessIx(false)
			}
			return true
		}
	}

	return false
}

// Erase discards all sectors currently held by this cartridge, and resets
// the name
func (c *Cartridge) Erase() {
	for ix, _ := range c.sectors {
		c.sectors[ix] = nil
	}
	c.modified = true
	c.name = ""
	c.Erased()
}

// -
func (c *Cartridge) Revert() {
	s := c.sectors
	for l, r := 0, len(s)-1; l < r; l, r = l+1, r-1 {
		s[l], s[r] = s[r], s[l]
	}
}

// Reorg reorganizes this cartridge. Note that this quietly invalidates any
// previously acquired file system objects.
// -
// FIXME Continued use of stale file systems after reorg currently goes unnoticed.
func (c *Cartridge) Reorg(tmp *Cartridge, sort func(files []*fs.FileInfo)) error {

	if err := tmp.Format(c.Name()); err != nil {
		return err
	}

	if err := fs.CopyAll(tmp.FS(), c.FS(), sort); err != nil {
		return err
	}

	for ix := 0; ix < c.Size(); ix++ {
		c.SetSectorAt(ix, tmp.GetSectorAt(ix))
	}

	c.SeekToStart()
	c.SetModified(true)

	return nil
}

// GetNextSector gets the sector at the next access index, skipping slots
// with nil sectors. Access index points to the slot of the returned sector
// afterwards.
func (c *Cartridge) GetNextSector() types.Sector {
	return c.GetSectorAt(c.AdvanceAccessIx(true))
}

// GetPreviousSector gets the sector at the previous access index, skipping
// slots with nil sectors. Access index points to the slot of the returned
// sector afterwards.
func (c *Cartridge) GetPreviousSector() types.Sector {
	return c.GetSectorAt(c.RewindAccessIx(true))
}

// -
func (c *Cartridge) GetSector() types.Sector {
	return c.GetSectorAt(c.AccessIx())
}

// -
func (c *Cartridge) GetSectorAt(ix int) types.Sector {
	if 0 <= ix && ix < len(c.sectors) {
		log.Tracef("getting sector at index %d", ix)
		return c.sectors[ix]
	} else {
		log.Tracef("getting sector at invalid index %d", ix)
	}
	return nil
}

// SetNextSector sets the provided sector at the next access index, whether
// there is a sector present at that index or not. Access index points to the
// slot of the set sector afterwards.
func (c *Cartridge) SetNextSector(s types.Sector) {
	c.SetSectorAt(c.AdvanceAccessIx(false), s)
}

// SetPreviousSector sets the provided sector at the previous access index,
// whether there is a sector present at that index or not. Access index points
// to the slot of the set sector afterwards.
func (c *Cartridge) SetPreviousSector(s types.Sector) {
	c.SetSectorAt(c.RewindAccessIx(false), s)
}

// setSector sets the provided sector in this cartridge at the given index.
func (c *Cartridge) SetSectorAt(ix int, s types.Sector) {
	if 0 <= ix && ix < len(c.sectors) {
		log.Tracef("setting sector at index %d", ix)
		c.sectors[ix] = s
		if s != nil && strings.TrimSpace(s.Name()) != "" {
			c.name = s.Name()
		}
		c.SetModified(true)
		c.SectorSetAt(ix, s)
	} else {
		log.Errorf("trying to set sector at invalid index %d", ix)
	}
}

// -
func (c *Cartridge) IsFormatted() bool {
	for _, s := range c.sectors {
		if s != nil {
			return true
		}
	}
	return false
}

// -
func (c *Cartridge) IsFormatting() bool {
	return c.formatting
}

// -
func (c *Cartridge) SetFormatting(f bool) {
	c.formatting = f
}

// -
func (c *Cartridge) IsWriteProtected() bool {
	return c.writeProtected
}

// -
func (c *Cartridge) SetWriteProtected(p bool) {
	c.writeProtected = p
}

// -
func (c *Cartridge) IsModified() bool {
	return c.modified
}

// -
func (c *Cartridge) LastModified() time.Time {
	return c.lastMod
}

// -
func (c *Cartridge) SetModified(m bool) {
	if m || c.modified { // time of clearing modified flag also needs to be noted
		c.lastMod = time.Now()
		c.autosaved = false
	}
	c.modified = m
}

// -
func (c *Cartridge) IsAutoSaved() bool {
	return c.autosaved
}

// -
func (c *Cartridge) SetAutoSaved(a bool) {
	c.autosaved = a
}

// -
func (c *Cartridge) AccessIx() int {
	return c.accessIx
}

// -
func (c *Cartridge) AdvanceAccessIxBy(count int, skipEmpty bool) int {
	return c.moveAccessIxBy(count, true, skipEmpty)
}

// -
func (c *Cartridge) AdvanceAccessIx(skipEmpty bool) int {
	return c.moveAccessIx(true, skipEmpty)
}

// -
func (c *Cartridge) RewindAccessIxBy(count int, skipEmpty bool) int {
	return c.moveAccessIxBy(count, false, skipEmpty)
}

// -
func (c *Cartridge) RewindAccessIx(skipEmpty bool) int {
	return c.moveAccessIx(false, skipEmpty)
}

// -
func (c *Cartridge) moveAccessIxBy(count int, forward, skipEmpty bool) int {
	var ret int
	for ; count > 0; count-- {
		ret = c.moveAccessIx(forward, skipEmpty)
	}
	return ret
}

// -
func (c *Cartridge) moveAccessIx(forward, skipEmpty bool) int {

	from := c.accessIx

	if !skipEmpty || c.IsFormatted() {
		for {
			if forward {
				c.accessIx = c.ensureIx(c.accessIx - 1)
			} else {
				c.accessIx = c.ensureIx(c.accessIx + 1)
			}
			if !skipEmpty || c.sectors[c.accessIx] != nil {
				break
			}
		}
	}

	log.WithFields(
		log.Fields{"from": from, "to": c.accessIx}).Tracef("moving access ix")

	return c.accessIx
}

// -
func (c *Cartridge) ensureIx(ix int) int {
	if ix < 0 {
		return c.Size() - 1 - (-(ix + 1))%c.Size()
	}
	return ix % c.Size()
}

// -
func (c *Cartridge) Dump(wr io.Writer, file string, raw bool) error {

	if file != "" {

		f, err := c.FS().Open(file, fs.READ, nil)
		if err != nil {
			return err
		}

		if raw {
			if _, err = io.Copy(wr, f); err != nil {
				return err
			}

		} else {
			fmt.Fprintln(wr)
			if err := f.Render(wr); err != nil {
				return err
			}
			fmt.Fprintln(wr)
		}

	} else {
		c.Emit(wr)
	}

	return nil
}

// -
func (c *Cartridge) Emit(w io.Writer) {

	for r := c.Range(); ; {
		if ix, sec := r.Next(); ix == -1 {
			break
		} else if sec != nil {
			sec.Emit(w)
		}
	}
}
