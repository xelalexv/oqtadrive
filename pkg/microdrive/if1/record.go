/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2021, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package if1

import (
	"encoding/hex"
	"fmt"
	"io"

	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/if1/cpm"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/raw"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/types"
)

// -
var recordIndexIF1V1 = map[string][2]int{
	"flags":         {12, 1},
	"number":        {13, 1},
	"length":        {14, 2},
	"name":          {16, 10},
	"header":        {12, 14},
	"checksum":      {26, 1},
	"data":          {27, 512},
	"fileHeader":    {27, 9}, //      start of file header, only in first record
	"fileType":      {27, 1}, //                              of non-Print files
	"fileLength":    {28, 2},
	"startAddress":  {30, 2},
	"programLength": {32, 2},
	"lineNumber":    {34, 2}, //                              end of file header
	"dataChecksum":  {539, 1},
}

// -
var recordIndexIF1V2 = map[string][2]int{
	"flags":         {12, 1},
	"number":        {13, 1},
	"length":        {14, 2},
	"name":          {16, 10},
	"header":        {12, 14},
	"checksum":      {26, 1},
	"data":          {27, 610},
	"fileHeader":    {27, 9}, //      start of file header, only in first record
	"fileType":      {27, 1}, //                              of non-Print files
	"fileLength":    {28, 2},
	"startAddress":  {30, 2},
	"programLength": {32, 2},
	"lineNumber":    {34, 2}, //                              end of file header
	"dataChecksum":  {638, 1},
}

// -
var recordIndexCPM = map[string][2]int{
	"flags":         {12, 1},
	"number":        {-1, 0},
	"length":        {-1, 0},
	"name":          {-1, 0},
	"header":        {-1, 0},
	"checksum":      {-1, 0},
	"data":          {13, 512},
	"fileHeader":    {-1, 0},
	"fileType":      {-1, 0},
	"fileLength":    {-1, 0},
	"startAddress":  {-1, 0},
	"programLength": {-1, 0},
	"lineNumber":    {-1, 0},
	"dataChecksum":  {525, 1},
}

// -
var recordIndexCPMFormat = map[string][2]int{
	"flags":         {12, 1},
	"number":        {-1, 0},
	"length":        {-1, 0},
	"name":          {-1, 0},
	"header":        {-1, 0},
	"checksum":      {-1, 0},
	"data":          {13, 600},
	"fileHeader":    {-1, 0},
	"fileType":      {-1, 0},
	"fileLength":    {-1, 0},
	"startAddress":  {-1, 0},
	"programLength": {-1, 0},
	"lineNumber":    {-1, 0},
	"dataChecksum":  {614, 1},
}

// -
type record struct {
	muxed []byte
	block *raw.Block
	cpm   bool
}

// -
func NewRecord(data []byte, isRaw bool) (*record, error) {

	r := &record{}
	var dmx []byte

	if isRaw {
		data = raw.CutToSize(data,
			RecordLengthMux+FormatExtraBytes,       // 640
			RecordLengthCPMMux+FormatExtraBytesCPM, // 615
			RecordLengthMux,                        // 541
			RecordLengthCPMMux)                     // 527
		dmx = raw.Demux(data, false)
		r.muxed = data
	} else {
		dmx = make([]byte, len(data))
		copy(dmx, data)
	}

	if dmx[raw.SyncPatternLength] == 0xff { // CP/M
		if len(dmx) >= RecordLengthCPM+FormatExtraBytesCPM { // formatting
			r.block = raw.NewBlock(recordIndexCPMFormat, dmx)
		} else {
			r.block = raw.NewBlock(recordIndexCPM, dmx)
		}
		r.cpm = true

	} else if len(dmx) > RecordLength { // long FORMAT record from V2 ROM
		r.block = raw.NewBlock(recordIndexIF1V2, dmx)
	} else {
		r.block = raw.NewBlock(recordIndexIF1V1, dmx)
	}

	if !isRaw {
		r.mux()
	}

	return r, r.Validate()
}

// -
func NewBlankRecord(fill byte, cpm bool) *record {

	l := RecordLength
	ix := recordIndexIF1V1

	if cpm {
		l = RecordLengthCPM
		ix = recordIndexCPM
	}

	dmx := make([]byte, l)
	raw.CopySyncPattern(dmx)
	if fill > 0 {
		raw.Fill(dmx[raw.SyncPatternLength:], fill)
	}
	return &record{block: raw.NewBlock(ix, dmx), cpm: cpm}
}

// -
func (r *record) Client() types.Client {
	return types.IF1
}

// -
func (r *record) Muxed() []byte {
	return r.muxed
}

// -
func (r *record) Demuxed() []byte {
	return r.block.Data
}

// -
func (r *record) mux() {
	r.muxed = raw.Mux(r.block.Data, false)
}

// -
func (r *record) Flags() byte {
	return r.block.GetByte("flags")
}

// -
func (r *record) setFlags(f byte) {
	r.block.SetByte("flags", f)
}

// -
func (r *record) Index() int {
	return int(r.block.GetByte("number"))
}

// -
func (r *record) setIndex(i byte) {
	r.block.SetByte("number", i)
}

// -
func (r *record) Length() int {
	return r.block.GetInt("length")
}

// -
func (r *record) setLength(l int) {
	r.block.SetInt("length", l)
}

// -
func (r *record) Name() string {
	return r.block.GetString("name")
}

// -
func (r *record) setName(n string) {
	r.block.SetString("name", fmt.Sprintf("%-10s", n))
}

// -
func (r *record) HeaderChecksum() byte {
	return r.block.GetByte("checksum")
}

// neither bad nor free for use
// FIXME: this currently flags bad sectors as in use
func (r *record) InUse() bool {
	return (r.Length() > 0 || r.Flags() != 0)
}

// -
func (r *record) Data() []byte {
	return r.block.GetSlice("data")
}

// -
func (r *record) Payload(fileLength int, last bool) ([]byte, error) {
	start := 0
	if r.Index() == 0 { // FIXME: error handling?
		start += FileHeaderLength
	}
	return r.Data()[start:r.Length()], nil
}

// -
func (r *record) DataChecksum() byte {
	return r.block.GetByte("dataChecksum")
}

// -
func (r *record) CalculateHeaderChecksum() byte {
	return byte(r.block.Sum("header") % 255)
}

// -
func (r *record) CalculateDataChecksum() byte {
	sum := byte(r.block.Sum("data") % 255)
	if r.cpm {
		return cpm.ToCPMCheckSum(sum)
	}
	return sum
}

// -
func (r *record) fixHeaderChecksum() error {
	if err := r.block.SetByte(
		"checksum", r.CalculateHeaderChecksum()); err != nil {
		return err
	}
	return nil
}

// -
func (r *record) fixDataChecksum() error {
	if err := r.block.SetByte(
		"dataChecksum", r.CalculateDataChecksum()); err != nil {
		return err
	}
	return nil
}

// -
func (r *record) FixChecksums() error {
	if err := r.fixHeaderChecksum(); err != nil {
		return err
	}
	if err := r.fixDataChecksum(); err != nil {
		return err
	}
	r.mux()
	return r.Validate()
}

// -
func (r *record) Replaces(old types.Record, sectorIx int) *types.FileChange {

	if old == nil || r == nil {
		return nil
	}

	if r.cpm {
		return nil // no file change support for CP/M formatted cartridges
	}

	// nothing to do for IF1, just return type of change
	if or, ok := old.(*record); ok {
		// FIXME: validate
		//
		// On Spectrum Microdrives, files are never actually modified. To modify,
		// you instead need to delete the old file and then save the new version
		// under the same name. So there can only be CREATED and DELETED events.
		//
		// Also, when formatting with Interface 1, there are three distinct
		// rounds: initial format, verification, and final write round. The
		// first round is a separate drive activation with start & stop. We
		// need to defer the Formatted change event until the end of the last
		// round.
		if r.InUse() {
			// Writing an in-use record therefor means the file was created.
			return types.NewFileChange(canonicalName(r.Name()), types.OpCreated)
		} else if !or.cpm {
			// During final format round, IF1 writes records with 0 flags and
			// 0 length over records without name. This check however makes no
			// sense if old record is CP/M.
			//
			// FIXME This may incorrectly classify the deletion of a hidden file
			//       as a cartridge format. Also be careful when adapting
			//       canonicalName() to support hidden files!
			cn := canonicalName(or.Name())
			if cn == "" {
				return types.NewFileChange("", types.OpFormatted)
			}
			return types.NewFileChange(cn, types.OpDeleted)
		}
	}

	return nil
}

// -
func (r *record) Validate() error {

	err := types.NewMediaError()

	// FORMAT records from IF1 V2 ROM do not use correct check sums when
	// issuing format command directly; when running from a BASIC program,
	// check sum is correct, so we need to accept both
	formatV2ROM := r.block.Length() > RecordLength

	want := r.HeaderChecksum()
	got := r.CalculateHeaderChecksum()

	if want != got { // both always 0 for CP/M, since no record headers
		// During the initial format phase, we accept check sum errors in the
		// record header, since Spectrum/IF1 tends to send wrong values here.
		// If there should actually be a transmission error, Spectrum/IF1 would
		// recognize this when reading back the records during the validation
		// phase.
		if !formatV2ROM || got != 2 {
			err.Add(fmt.Sprintf(
				"invalid record descriptor check sum, want %d, got %d", want, got),
				types.BenignIfFormatting)
		}
	}

	want = r.DataChecksum()
	got = r.CalculateDataChecksum()

	if want != got {
		// IF1:  calculate check sum only based on actual record data length
		//       background: during ERASE, there always seems to be a bit set
		//       somewhere, although all should be zero...
		//       	FIXME: is this really correct?
		//
		// CP/M: error when not formatting; for formatting, same argument as
		//       above applies
		//
		el := types.NotAnError

		if r.cpm {
			if r.block.Length() <= RecordLengthCPM {
				// block without extra data, i.e. not formatting
				el = types.Fatal
			}

		} else if r.Flags() != 0 && (!formatV2ROM || (got != 210 && got != 54)) {
			el = types.Fatal
		}

		if el > types.NotAnError {
			err.Add(fmt.Sprintf(
				"invalid record data check sum, want %d, got %d", want, got), el)
		}
	}

	if err.Level() > 0 {
		return err.Done()
	}
	return nil
}

// -
func (r *record) Emit(w io.Writer) {
	io.WriteString(w, fmt.Sprintf(
		"\nRECORD: %+q - flag: %X, index: %d, length: %d\n",
		r.Name(), r.Flags(), r.Index(), r.Length()))
	d := hex.Dumper(w)
	defer d.Close()
	d.Write(r.block.Data)
}
