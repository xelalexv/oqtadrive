/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2021, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package if1

import (
	"encoding/hex"
	"fmt"
	"io"

	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/if1/cpm"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/raw"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/types"
)

// -
var headerIndex = map[string][2]int{
	"flags":    {12, 1},
	"number":   {13, 1},
	"spares":   {14, 2},
	"name":     {16, 10},
	"header":   {12, 14},
	"checksum": {26, 1},
}

// -
var headerIndexCPM = map[string][2]int{
	"flags":    {12, 1},
	"spares":   {13, 2},
	"number":   {15, 1},
	"header":   {12, 4},
	"checksum": {16, 1},
	"name":     {-1, 0},
}

// -
type header struct {
	muxed []byte
	block *raw.Block
	cpm   bool
}

// -
func NewHeader(data []byte, isRaw bool) (*header, error) {

	h := &header{}
	var dmx []byte

	if isRaw {
		data = raw.CutToSize(data, HeaderLengthMux, HeaderLengthCPMMux)
		dmx = raw.Demux(data, false)
		h.muxed = data
	} else {
		dmx = make([]byte, len(data))
		copy(dmx, data)
	}

	if dmx[raw.SyncPatternLength] == 0 { // CP/M header
		h.block = raw.NewBlock(headerIndexCPM, dmx)
		h.cpm = true
	} else {
		h.block = raw.NewBlock(headerIndex, dmx)
	}

	if !isRaw {
		h.mux()
	}

	return h, h.Validate()
}

// -
func NewBlankHeader(ix int, cpm bool) *header {

	l := HeaderLength
	hIx := headerIndex

	if cpm {
		l = HeaderLengthCPM
		hIx = headerIndexCPM
	}

	dmx := make([]byte, l)
	raw.CopySyncPattern(dmx)
	ret := &header{block: raw.NewBlock(hIx, dmx), cpm: cpm}
	if ix >= 0 {
		ret.setIndex(ix)
		ret.FixChecksum()
	}
	return ret
}

// -
func (h *header) Client() types.Client {
	return types.IF1
}

// -
func (h *header) Muxed() []byte {
	return h.muxed
}

// -
func (h *header) Demuxed() []byte {
	return h.block.Data
}

// -
func (h *header) mux() {
	h.muxed = raw.Mux(h.block.Data, false)
}

// -
func (h *header) Flags() byte {
	return h.block.GetByte("flags")
}

// -
func (h *header) setFlags(f byte) {
	h.block.SetByte("flags", f)
}

// -
func (h *header) Index() int {
	return int(h.block.GetByte("number"))
}

// -
func (h *header) setIndex(ix int) {
	h.block.SetByte("number", byte(ix))
}

// -
func (h *header) Name() string {
	if h.cpm {
		return fmt.Sprintf("CP/M %x", h.block.GetInt("spares"))
	}
	return h.block.GetString("name")
}

// -
func (h *header) setName(n string) error {
	return h.block.SetString("name", n)
}

// -
func (h *header) setSpares(s int) {
	h.block.SetInt("spares", s)
}

// -
func (h *header) Checksum() int {
	return int(h.block.GetByte("checksum"))
}

// -
func (h *header) CalculateChecksum() byte {
	sum := byte(h.block.Sum("header") % 255)
	if h.cpm {
		return cpm.ToCPMCheckSum(sum)
	}
	return sum
}

// -
func (h *header) FixChecksum() error {
	if err := h.block.SetByte("checksum", h.CalculateChecksum()); err != nil {
		return err
	}
	h.mux()
	return h.Validate()
}

// -
func (h *header) Validate() error {

	want := h.block.GetByte("checksum")
	got := h.CalculateChecksum()

	if want != got {
		return fmt.Errorf(
			"invalid sector header check sum, want %d, got %d", want, got)
	}
	return nil
}

// -
func (h *header) Emit(w io.Writer) {
	io.WriteString(w, fmt.Sprintf("\nHEADER: %+q - flag: %X, index: %d\n",
		h.Name(), h.Flags(), h.Index()))
	d := hex.Dumper(w)
	defer d.Close()
	d.Write(h.block.Data)
}
