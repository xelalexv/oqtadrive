/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2021, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package if1

import (
	"strings"
)

// -
const (
	HeaderLength        = 27
	HeaderLengthMux     = HeaderLength + 1
	HeaderLengthCPM     = 17
	HeaderLengthCPMMux  = HeaderLengthCPM + 1
	RecordLength        = 540
	RecordLengthMux     = RecordLength + 1
	RecordLengthCPM     = 526
	RecordLengthCPMMux  = RecordLengthCPM + 1
	RecordHeaderLength  = 27
	FormatExtraBytes    = 99
	FormatExtraBytesCPM = 88

	FileHeaderLength = 9 // only in first record of some file types

	HeaderFlag byte = 0x01

	// record flags
	RecordFlagLastRecord    byte = 0x02
	RecordFlagNotAPrintFile byte = 0x04

	// sector numbers range from 1 through 254
	SectorCount = 254
	TopSector   = 254

	SectorCountCPM = 192

	// null name
	NullName = "\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

	SpecialsCPM = "CP/M"
)

// -
func parseFileType(t string) FileType {
	r := BASIC
	switch strings.ToLower(t) {
	case "array1":
		r = ARRAY1
	case "array2":
		r = ARRAY2
	case "code":
		r = CODE
	case "data":
		r = DATA
	case "print":
		r = PRINT
	case "ascii":
		r = ASCII
	}
	return r
}

// -
type FileType int

const (
	// native
	BASIC  FileType = 0
	ARRAY1 FileType = 1
	ARRAY2 FileType = 2
	CODE   FileType = 3
	DATA   FileType = 6
	// virtual
	ASCII   FileType = 256
	PRINT   FileType = 257
	UNKNOWN FileType = -1
)

func (t FileType) String() string {
	r := "?"
	switch t {
	case BASIC:
		r = "BASIC"
	case ARRAY1, ARRAY2:
		r = "array"
	case CODE:
		r = "code"
	case DATA:
		r = "data"
	case PRINT:
		r = "PRINT"
	case ASCII:
		r = "ASCII"
	}
	return r
}
