/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2023, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package cpm

import (
	"fmt"
	"strings"

	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/fs"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/raw"
)

// -
var extentIndex = map[string][2]int{
	"user":        {0, 1},
	"name":        {1, 8},
	"type":        {9, 3},
	"extentL":     {12, 1},
	"bCount":      {13, 1}, // not supported in CP/M 2.2
	"extentH":     {14, 1},
	"rCount":      {15, 1},
	"allocations": {16, 16},
}

// -
func newExtent(data []byte) *extent {
	return &extent{raw.NewBlock(extentIndex, data)}
}

// -
type extent struct {
	data *raw.Block
}

// -
func (e *extent) setData(d []byte) {
	e.data.Data = d
}

// -
func (e *extent) release() {
	e.data.SetByte("user", Unused)
}

// -
func (e *extent) getCanonicalName() string {

	name := strings.TrimSpace(e.data.GetString("name"))
	typ := strings.TrimSpace(e.data.GetString("type"))

	var ret []byte
	if typ == "" {
		ret = []byte(fmt.Sprintf("%.8s", name))
	} else {
		ret = []byte(fmt.Sprintf("%.8s.%.3s", name, typ))
	}

	for ix, ch := range ret {
		ret[ix] = ch & FileNameCharMask
	}

	return string(ret)
}

// -
func (e *extent) setCanonicalName(n string) {

	parts := strings.SplitN(n, ".", 2)
	if len(parts) < 2 {
		parts = append(parts, "")
	}
	name := fmt.Sprintf("%.8s", fmt.Sprintf("%-8s", parts[0]))
	typ := fmt.Sprintf("%.3s", fmt.Sprintf("%-3s", parts[1]))

	e.data.SetString("name", name)
	e.data.SetString("type", typ)
}

// -
func (e *extent) getUser() byte {
	return e.data.GetByte("user")
}

// -
func (e *extent) setUser(u byte) {
	e.data.SetByte("user", u)
}

// -
func (e *extent) isVacant() bool {
	return e.getUser() == Unused
}

// -
func (e *extent) getRCount() int {
	return int(e.data.GetByte("rCount"))
}

// -
func (e *extent) setRCount(r int) {
	e.data.SetByte("rCount", byte(r))
}

// -
func (e *extent) getBCount() int {
	return int(e.data.GetByte("bCount"))
}

// -
func (e *extent) setBCount(b int) {
	e.data.SetByte("bCount", byte(b))
}

// -
func (e *extent) getExtentNumber() int {
	return int(uint16(e.data.GetByte("extentL")&ExtentLMask) |
		(uint16(e.data.GetByte("extentH")&ExtentHMask) << 5))
}

// -
func (e *extent) setExtentNumber(n int) {
	e.data.SetByte("extentL", byte(n)&ExtentLMask)
	e.data.SetByte("extentH", byte((uint16(n)>>5))&ExtentHMask)
}

// -
func (e *extent) getAllocations() []byte {
	return e.data.GetSlice("allocations")
}

// -
func (e *extent) getAllocation(ix int) int {
	return int(e.getAllocations()[ix])
}

// -
func (e *extent) setAllocation(ix, record int) {
	al := e.getAllocations()
	if 0 <= ix && ix < len(al) {
		al[ix] = byte(record)
	}
}

// -
type entry struct {
	name        string
	length      int
	extents     int
	user        byte
	allocations []int
}

// FIXME reject non-matching data?
func (e *entry) merge(ext *extent) {

	e.user = ext.getUser()
	e.length += ext.getRCount() * CPMRecordLength
	e.extents++

	extNo := ext.getExtentNumber()
	if d := (extNo+1)*AllocationCount - len(e.allocations); d > 0 {
		e.allocations = append(e.allocations, make([]int, d)...)
	}

	extOffset := extNo * AllocationCount
	for ix, a := range ext.getAllocations() {
		e.allocations[extOffset+ix] = int(a)
	}
}

// -
func (e *entry) finalize() {
	al := make([]int, 0, len(e.allocations))
	for _, a := range e.allocations {
		if a > 0 {
			al = append(al, a)
		}
	}
	e.allocations = al
}

// -
func (e *entry) createRecordSequence() *fs.RecordSequence {

	ret := fs.NewRecordSequence(191, 0, 0) // FIXME constant

	for ix, r := range e.allocations {
		if r == 0 {
			continue
		}
		for p := 0; p < SectorsPerAllocation; p++ {
			ret.Add(ix*SectorsPerAllocation+p, 191-allocationToSectorIx(r, p))
		}
	}

	return ret
}
