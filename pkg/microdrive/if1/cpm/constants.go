/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2023, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package cpm

import (
	"fmt"
	"path"
	"strconv"
)

// -
const (

	// directory
	DirectoryRecordCount = 2
	DirectoryEntryCount  = 32

	ExtentLength         = 32
	CPMRecordLength      = 128
	CPMRecordsPerExtent  = 128
	AllocationCount      = 16
	SectorsPerAllocation = 2

	ExtentLMask = 0x1f
	ExtentHMask = 0x3f

	// user
	UserFirst   = 0x00
	UserLast    = 0x0e
	UserDefault = UserFirst
	DiscLabel   = 0x20
	Unused      = 0xe5

	// files
	FileNameCharMask = 0x7f
)

// -
func ToCPMCheckSum(c byte) byte {
	if c == 0 {
		return 0xff
	}
	return c
}

// -
func isValidUser(usr byte) bool {
	return UserFirst <= usr && usr <= UserLast
}

// -
func allocationToSectorIx(a, offset int) int {
	return a*SectorsPerAllocation + offset
}

// -
func sectorToAllocationIx(ix int) (allocation int, offset int) {
	allocation = ix / SectorsPerAllocation
	offset = ix % SectorsPerAllocation
	return
}

// -
func toPath(name string, user int) string {
	return fmt.Sprintf("/%d/%s", user, name)
}

// -
func splitPath(p string) (userId int, fileName string, err error) {

	var user string
	userId = UserDefault
	user, fileName = path.Split(p)

	if user != "" {
		if userId, err = strconv.Atoi(user[1 : len(user)-1]); err != nil {
			err = fmt.Errorf("invalid path: %s", p)
		}
	}

	return
}
