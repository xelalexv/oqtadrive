/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2023, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package cpm

import (
	"encoding/hex"
	"fmt"
	"io"

	log "github.com/sirupsen/logrus"

	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/fs"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/types"
)

// -
type FileType int

const (
	UNKNOWN FileType = -1
	CODE    FileType = 0
)

func (t FileType) String() string {
	r := "?"
	switch t {
	case CODE:
		r = "code"
	}
	return r
}

// -
type file struct {
	*fs.File
	//
	fSys *fsys
	user byte
}

// -
func (f *file) FileHeaderLength() int {
	return 0
}

// -
func (f *file) Render(w io.Writer) error {

	d := hex.Dumper(w)
	defer d.Close()
	_, err := io.Copy(d, f)
	return err
}

// -
func (f *file) ClaimSectors(start, count int) ([]types.Sector, error) {

	log.WithFields(log.Fields{
		"start": start, "count": count}).Debug("claim sectors")

	if count == 0 {
		return nil, fmt.Errorf("nothing requested")
	} else if count%2 == 1 {
		count++ // always need to claim pairs
	}

	if f.fSys == nil {
		return nil, fs.ErrNoFileSystem
	}

	dir, err := f.fSys.getDirectory()
	if err != nil {
		return nil, err
	}

	vacant := func(ix int) (bool, types.Sector) {
		var s types.Sector
		if f.fSys.cart.SeekToSector(ix, false) {
			s = f.fSys.cart.GetSector()
		}
		return dir.isVacant(ix), s
	}

	opts := &fs.ClaimOpts{
		Start:     start,
		Count:     count,
		Top:       2,
		Bottom:    f.fSys.cart.SectorCount() - 1,
		Ascending: true,
		FirstGap:  0,
		SecGap:    0,
		Pairs:     true,
	}

	sectors, err := fs.SimpleClaim(opts, vacant)
	if err != nil {
		return nil, err
	}

	from := f.RecordCount()
	if from%2 == 1 {
		from++
	}

	if err = dir.assign(f.Name(), int(f.user), sectors, from); err != nil {
		return nil, err
	}

	return sectors, dir.fixCheckSums()
}

// -
func (f *file) CommitRecord(r types.Record, ix, length int, closing bool) error {

	if !closing {
		log.WithFields(log.Fields{
			"record": ix, "length": length}).Debug("committing record")
		return r.FixChecksums()
	}

	if ix != 0 {
		return nil
	}

	log.WithFields(log.Fields{
		"path":    toPath(f.Name(), int(f.user)),
		"records": f.RecordCount(),
		"length":  f.Size()}).Debug("closing file")

	dir, err := f.fSys.getDirectory()
	if err != nil {
		return err
	}

	exts, err := dir.getExtents(f.Name(), int(f.user), false, true)
	if err != nil {
		return err
	}

	if len(exts) > 0 { // calculate record count for last extent
		// byte count in last extent
		rc := length % (CPMRecordsPerExtent * CPMRecordLength)
		if rc%CPMRecordLength > 0 {
			// byte count not multiple of record length, need 1 extra record
			rc = rc/CPMRecordLength + 1
		} else {
			// byte count is multiple of record length, no extra record
			rc = rc / CPMRecordLength
		}
		// when byte count is exactly max number of records per extent, rc will
		// be 0 here; need to set to max number of records
		if rc == 0 {
			rc = CPMRecordsPerExtent
		}
		log.WithFields(log.Fields{
			"length": length,
			"from":   exts[len(exts)-1].getRCount(),
			"to":     rc}).Debug("adjusting rcount")
		exts[len(exts)-1].setRCount(rc)
	}
	return dir.fixCheckSums()
}

// -
func (f *file) ReleaseRecords(rec []types.Record, purge bool) (int, error) {

	log.WithFields(log.Fields{
		"total":  f.RecordCount(),
		"remove": len(rec),
		"purge":  purge}).Debug("releasing records")

	count := len(rec)
	start := f.RecordCount() - count
	if start%2 == 1 { // maintain record pairs
		start++
		count--
	}

	if count < 1 {
		log.Debug("nothing to release")
		return 0, nil
	}

	dir, err := f.fSys.getDirectory()
	if err != nil {
		return 0, err
	}

	dCount := count
	if dCount%2 == 1 {
		dCount++
	}

	if err = dir.deassign(f.Name(), int(f.user), start, dCount); err != nil {
		return 0, err
	}
	return count, dir.fixCheckSums()
}
