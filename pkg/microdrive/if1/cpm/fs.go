/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2023, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package cpm

import (
	"fmt"

	log "github.com/sirupsen/logrus"

	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/base"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/fs"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/types"
)

// -
func NewFs(cart *base.Cartridge) *fsys {
	return &fsys{cart: cart}
}

// -
type fsys struct {
	cart *base.Cartridge
	dir  *directory
}

// -
func (fsys *fsys) getDirectory() (*directory, error) {
	var err error
	if fsys.dir == nil {
		fsys.dir, err = newDirectory(fsys)
		if err == nil && fsys.dir != nil {
			err = fsys.dir.load()
		}
	}
	return fsys.dir, err
}

// -
func (fsys *fsys) Open(path string, mode fs.FileMode, typ fs.FileType) (
	*fs.File, error) {

	if path == "" {
		return nil, fs.ErrEmptyFileName
	}

	ft := UNKNOWN
	ok := false
	if typ != nil {
		if ft, ok = typ.(FileType); !ok {
			if typ.String() == CODE.String() {
				ft = CODE
			} else {
				return nil, fmt.Errorf("not a CP/M file type: %v", typ)
			}
		}
	}

	log.WithFields(log.Fields{
		"path": path, "mode": mode, "type": ft.String()}).Debug("open")

	switch mode {
	case fs.READ:
		return fsys.open(path, mode, ft)
	case fs.WRITE, fs.OVERWRITE:
		return fsys.openWrite(path, mode, ft)
	default:
		return nil, fs.ErrUnsupportedAccessMode
	}
}

// -
func (fsys *fsys) open(path string, mode fs.FileMode, typ FileType) (
	*fs.File, error) {

	dir, err := fsys.getDirectory()
	if err != nil {
		return nil, err
	}

	userId, name, err := splitPath(path)
	if err != nil {
		return nil, err
	}

	entry, err := dir.getEntry(name, userId)
	if err != nil {
		return nil, err
	}

	impl := &file{fSys: fsys, user: entry.user}
	if typ == UNKNOWN {
		typ = CODE
	}

	sectors := make(map[int]types.Sector)
	for r := fsys.cart.Range(); ; {
		if ix, s := r.Next(); ix == -1 {
			break
		} else {
			sectors[s.Index()] = s
		}
	}

	rec := make([]types.Record, SectorsPerAllocation*len(entry.allocations))
	total := 0
	last := 0

	for ix, r := range entry.allocations {
		if r == 0 {
			continue
		}
		for p := 0; p < SectorsPerAllocation; p++ {
			s := sectors[allocationToSectorIx(r, p)]
			if s == nil {
				return nil, fmt.Errorf(
					"sector %d not found for file %s", r+p, name)
			}
			rec[ix*SectorsPerAllocation+p] = s.Record()
			last = s.Index()
			total++
		}
	}

	log.WithFields(log.Fields{
		"path":        toPath(name, userId),
		"length":      entry.length,
		"records":     total,
		"allocations": len(entry.allocations),
		"extents":     entry.extents,
		"last":        last}).Debug("opened")

	rec = rec[:total]
	ret := fs.NewFile(name, mode, typ, entry.length, last, rec, impl)
	impl.File = ret

	return ret, nil
}

// -
func (fsys *fsys) openWrite(path string, mode fs.FileMode, typ FileType) (
	*fs.File, error) {

	var ret *fs.File

	switch mode {

	case fs.WRITE:
		if ok, err := fsys.exists(path); err != nil {
			return nil, err
		} else if ok {
			return nil, fmt.Errorf(fs.FileExists, path)
		}

	case fs.OVERWRITE:
		var err error
		if ret, err = fsys.open(path, mode, typ); err == nil { // file exists
			return ret, nil
		} else if err != fs.ErrFileNotFound {
			return nil, err
		}
	}

	impl := &file{fSys: fsys}
	ret = fs.NewFile(path, mode, typ, 0, -1, nil, impl)
	impl.File = ret

	return ret, nil
}

// -
func (fsys *fsys) Delete(path string, purge bool) (bool, error) {

	dir, err := fsys.getDirectory()
	if err != nil {
		return false, err
	}

	userId, name, err := splitPath(path)
	if err != nil {
		return false, err
	}

	// FIXME purge
	return dir.delete(name, userId)
}

// -
func (fsys *fsys) exists(path string) (bool, error) {

	dir, err := fsys.getDirectory()
	if err != nil {
		return false, err
	}

	userId, name, err := splitPath(path)
	if err != nil {
		return false, err
	}

	exts, err := dir.getExtents(name, userId, false, false)
	return err == nil && len(exts) > 0, nil
}

// -
func (fsys *fsys) Ls() (*fs.Stats, []*fs.FileInfo, error) {

	dir, err := fsys.getDirectory()
	if err != nil {
		return nil, nil, err
	}

	return dir.ls()
}

// -
func (fsys *fsys) ParseFileType(t string) fs.FileType {
	return UNKNOWN
}

// -
func (fsys *fsys) ReclaimSectors(s []int) error {
	return fmt.Errorf("reclaiming sectors not applicable for CP/M")
}
