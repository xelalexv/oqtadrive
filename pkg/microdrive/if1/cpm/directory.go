/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2023, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package cpm

import (
	"cmp"
	"fmt"
	libpath "path"
	"slices"
	"sort"

	log "github.com/sirupsen/logrus"

	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/fs"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/types"
	"codeberg.org/xelalexv/oqtadrive/pkg/util"
)

// -
type fileInfo struct {
	name     string
	typ      string
	size     int
	sequence *fs.RecordSequence
	util.Annotations
}

// -
func newDirectory(fsys *fsys) (*directory, error) {

	if fsys == nil {
		return nil, fmt.Errorf("cannot create directory without file system")
	}

	if fsys.cart == nil {
		return nil, fmt.Errorf(
			"cannot create directory, file system without cartridge")
	}

	d := &directory{fs: fsys}

	for r := fsys.cart.RangeFromSector(0); len(d.records) < DirectoryRecordCount; {
		if ix, sec := r.Next(); ix == -1 {
			break
		} else if sec != nil && sec.Index() == len(d.records) {
			d.records = append(d.records, sec.Record())
		}
	}

	if len(d.records) != DirectoryRecordCount {
		return nil, fmt.Errorf("not enough directory records - want %d, got %d",
			DirectoryRecordCount, len(d.records))
	}

	return d, nil
}

// -
type directory struct {
	fs        *fsys
	records   []types.Record
	sectorMap map[int]bool
}

// -
func (d *directory) load() error {

	d.sectorMap = make(map[int]bool)
	sm := d.sectorMap
	sm[0] = true
	sm[1] = true

	exts, err := d.getExtents("", -1, false, false)
	if err != nil {
		return err
	}

	for _, ext := range exts {
		for _, a := range ext.getAllocations() {
			secIx := allocationToSectorIx(int(a), 0)
			sm[secIx] = true
			secIx++
			sm[secIx] = true
		}
	}

	return nil
}

// -
func (d *directory) ls() (*fs.Stats, []*fs.FileInfo, error) {

	exts, err := d.getExtents("", -1, false, false)
	if err != nil {
		return nil, nil, err
	}

	dir := make(map[string]*entry)
	var paths []string

	for _, ext := range exts {
		n := ext.getCanonicalName()
		p := toPath(n, int(ext.getUser()))
		ent := dir[p]
		if ent == nil {
			ent = &entry{name: n}
			dir[p] = ent
			paths = append(paths, p)
		}
		ent.merge(ext)
	}

	sort.Strings(paths)

	ret := make([]*fs.FileInfo, len(paths))
	used := DirectoryRecordCount
	ix := 0

	for _, p := range paths {
		ent := dir[p]
		ent.finalize()
		d, f := libpath.Split(p)
		ret[ix] = fs.NewFileInfo(f, d, fmt.Sprintf("User %d", ent.user),
			ent.length, CODE) // FIXME CODE
		ret[ix].Annotate(
			"fragmentation", ent.createRecordSequence().Fragmentation(false))
		used += SectorsPerAllocation * len(ent.allocations)
		ix++
	}

	return fs.NewStats(d.fs.cart.SectorCount(), used), ret, nil
}

// -
func (d *directory) assign(name string, user int, sec []types.Sector, start int) error {

	log.WithFields(log.Fields{
		"path":  toPath(name, user),
		"count": len(sec),
		"start": start}).Debug("assigning sectors to allocations")

	if start%2 != 0 {
		return fmt.Errorf("start not aligned to pairs")
	}
	if len(sec)%2 != 0 {
		return fmt.Errorf("cannot assign odd number of sectors")
	}

	exts, err := d.getExtents(name, user, false, true)
	if err != nil && !fs.IsError(err, fs.FileNotFound) {
		return err
	}

	var ext *extent

	if start < 0 {
		start = 0
	}

	for ix, s := range sec {

		extIx := (start + ix) / SectorsPerAllocation / AllocationCount

		switch diff := len(exts) - extIx; {
		case diff > 0:
			if ext = exts[extIx]; ext == nil {
				return fmt.Errorf("unexpected nil-extent")
			}
		case diff == 0:
			if ext, err = d.assignExtent(name, user); err != nil {
				return err
			}
			ext.setExtentNumber(extIx)
			exts = append(exts, ext)
		default:
			return fmt.Errorf(
				"trying to append beyond end of file - len: %d, pos: %d",
				len(exts), extIx)
		}

		secIx := s.Index()
		if secIx < DirectoryRecordCount {
			return fmt.Errorf("must not assign directory sector %d", secIx)
		}
		al, _ := sectorToAllocationIx(secIx)
		alIx := ((start + ix) / 2) % AllocationCount
		ext.setAllocation(alIx, al)
		ext.setRCount(16 * 512 * SectorsPerAllocation / CPMRecordLength)
		d.sectorMap[secIx] = true

		log.WithFields(log.Fields{
			"sector":     s.Index(),
			"allocation": al,
			"extent":     fmt.Sprintf("%d:%d", extIx, alIx),
		}).Debug("assigned sector")
	}

	return nil
}

// -
func (d *directory) deassign(name string, user, start, count int) error {

	log.WithFields(log.Fields{
		"path":  toPath(name, user),
		"count": count,
		"start": start}).Debug("deassigning records from allocations")

	if start%2 != 0 {
		return fmt.Errorf("start not aligned to pairs")
	}
	if count%2 != 0 {
		return fmt.Errorf("cannot deassign odd number of sectors")
	}

	exts, err := d.getExtents(name, user, false, true)
	if err != nil {
		return err
	}

	for ix := start; ix < start+count; ix++ {

		extIx := ix / SectorsPerAllocation / AllocationCount

		if extIx >= len(exts) {
			return fmt.Errorf("trying to deassign non-assigned allocation")
		}

		ext := exts[extIx]
		alIx := (ix / 2) % AllocationCount

		secIx := allocationToSectorIx(ext.getAllocation(alIx), 0)
		if secIx > 0 {
			d.sectorMap[secIx] = false
			secIx++
			d.sectorMap[secIx] = false
			ext.setAllocation(alIx, 0)
		}
		if alIx == 0 {
			ext.release()
		}
		log.WithFields(log.Fields{
			"record index": ix,
			"extent":       fmt.Sprintf("%d:%d", extIx, alIx),
		}).Debug("deassigned sectors")
	}

	return nil
}

// -
func (d *directory) delete(name string, user int) (bool, error) {

	exts, err := d.getExtents(name, user, false, false)
	if err != nil && !fs.IsError(err, fs.FileNotFound) {
		return false, err
	}
	if err != nil || len(exts) == 0 {
		return false, nil
	}

	for _, ext := range exts {
		ext.release()
		for _, a := range ext.getAllocations() {
			if a > 0 {
				secIx := allocationToSectorIx(int(a), 0)
				d.sectorMap[secIx] = false
				secIx++
				d.sectorMap[secIx] = false
			}
		}
	}

	return true, d.fixCheckSums()
}

// -
func (d *directory) getEntry(name string, user int) (*entry, error) {

	if name == "" {
		return nil, fmt.Errorf("need to specify name for entry")
	}

	if user < 0 {
		user = UserDefault
	}

	if !isValidUser(byte(user)) {
		return nil, fmt.Errorf("invalid user for getting entry: %d", user)
	}

	exts, err := d.getExtents(name, user, false, false)
	if err != nil {
		return nil, err
	}

	ret := &entry{name: name}
	for _, ext := range exts {
		ret.merge(ext)
	}

	ret.finalize()
	return ret, nil
}

// -
func (d *directory) getExtents(name string, user int, unassigned, sorted bool) (
	[]*extent, error) {

	var ret []*extent
	ext := newExtent(nil)
	ix := 0

	for _, r := range d.records {

		data := r.Data()
		for pos := 0; pos < 512; pos += ExtentLength {
			if len(data) < pos+ExtentLength {
				break
			}
			ext.setData(data[pos : pos+ExtentLength])
			n := ""
			if name != "" {
				n = ext.getCanonicalName()
			}
			if n == name && (unassigned || isValidUser(ext.getUser())) &&
				(user < 0 || user == int(ext.getUser())) {
				ret = append(ret, ext)
				ext = newExtent(nil)
			}
			if ix++; ix > DirectoryEntryCount {
				break
			}
		}
	}

	if len(ret) > 0 {
		if sorted {
			slices.SortFunc(ret, func(a, b *extent) int {
				if a == nil && b == nil {
					return 0
				}
				if a == nil {
					return 1
				}
				if b == nil {
					return -1
				}
				return cmp.Compare(a.getExtentNumber(), b.getExtentNumber())
			})
		}
	} else if name != "" {
		return nil, fs.ErrFileNotFound
	}

	return ret, nil
}

// -
func (d *directory) assignExtent(name string, user int) (*extent, error) {

	ext := newExtent(nil)

	for _, r := range d.records {

		data := r.Data()
		for pos := 0; pos < 512; pos += ExtentLength {
			if len(data) < pos+ExtentLength {
				break
			}
			ext.setData(data[pos : pos+ExtentLength])
			if ext.isVacant() {
				ext.setUser(byte(user))
				ext.setCanonicalName(name)
				ext.setBCount(0)
				ext.setRCount(0)
				clear(ext.getAllocations())
				return ext, nil
			}
		}
	}

	return nil, fs.ErrNoSpaceLeftInDirectory
}

// -
func (d *directory) isVacant(ix int) bool {
	return ix >= DirectoryRecordCount && !d.sectorMap[ix]
}

// -
func (d *directory) fixCheckSums() error {
	for _, r := range d.records {
		if err := r.FixChecksums(); err != nil {
			return err
		}
	}
	return nil
}
