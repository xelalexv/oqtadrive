/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2021, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package if1

import (
	"fmt"
	"io"
	"strings"

	basic "codeberg.org/xelalexv/oqtadrive/pkg/basic/spectrum"
)

// -
func Translate(s string, left rune) string {

	if strings.HasPrefix(s, "\x00") {
		return ""
	}

	var sb strings.Builder
	var pendingSpace bool
	prev := left

	for _, b := range []byte(s) {

		if pendingSpace {
			switch b {
			case '.', ',', '?', '!': // don't need space before these characters
			default:
				sb.WriteRune(' ')
				prev = ' '
			}
			pendingSpace = false
		}

		if basic.KeywordStart <= b && b <= basic.KeywordEnd { // BASIC token
			if prev != ' ' && prev != '\n' {
				sb.WriteRune(' ')
			}
			kw := basic.KeywordString(b)
			sb.WriteString(kw)
			prev = rune(kw[len(kw)-1])
			// space doesn't need to be appended if last char of token
			// is '#' or already a space
			pendingSpace = prev != '#' && prev != ' '

		} else {
			prev = toRune(b)
			sb.WriteRune(prev)
		}
	}

	return sb.String()
}

// -
func toRune(b byte) rune {

	if b == '\x0d' || b == '\x0a' {
		return '\n'
	}

	if b < '\x20' || b > '\xa2' { // non-printable or token
		return '-'
	}

	if b > '\x8f' { // user defined graphics presets (A - U)
		return rune(b - 0x4f)
	}

	switch b {
	case '\x60':
		return '\xa3'
	case '\x7f':
		return '\xa9'
	case '\x80':
		return '\x20'
	case '\x81': // quadrant upper right
		return '\u259d'
	case '\x82': // quadrant upper left
		return '\u2598'
	case '\x83': // upper half block
		return '\u2580'
	case '\x84': // quadrant lower right
		return '\u2597'
	case '\x85': // right half block
		return '\u2590'
	case '\x86': // quadrant upper left and lower right
		return '\u259a'
	case '\x87': // quadrant upper left and upper right and lower right
		return '\u259c'
	case '\x88': // quadrant lower left
		return '\u2596'
	case '\x89': // quadrant upper right and lower left
		return '\u259e'
	case '\x8a': // left half block
		return '\u258c'
	case '\x8b': // quadrant upper left and upper right and lower left
		return '\u259b'
	case '\x8c': // lower half block
		return '\u2584'
	case '\x8d': // quadrant upper right and lower left and lower right
		return '\u259f'
	case '\x8e': // quadrant upper left and lower left and lower right
		return '\u2599'
	case '\x8f': // full block
		return '\u2588'
	}

	return rune(b)
}

// -
func parseBasic(w io.Writer, r io.Reader, programLen int) {

	lines, err := basic.Parse(r, programLen)
	var prev string

	for _, l := range lines {

		prev = fmt.Sprintf("%5d ", l.Number)
		fmt.Fprintf(w, "%s", prev)

		for _, t := range l.Tokens {

			if !t.Visible() {
				continue
			}

			if t.Spaced() && len(prev) > 0 && prev[len(prev)-1] != ' ' {
				fmt.Fprint(w, " ")
			}

			s := t.String()

			switch t.Length() {
			case 0:
				continue
			case 1:
				fmt.Fprintf(w, "%c", toRune(t.Bytes()[0]))
			default:
				fmt.Fprintf(w, s)
			}

			prev = s
		}

		fmt.Fprintln(w)
	}

	if err != nil {
		fmt.Fprintf(w, "\n%s\n", err.Error())
	}
}
