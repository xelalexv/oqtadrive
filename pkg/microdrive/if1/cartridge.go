/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2021, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package if1

import (
	"fmt"
	"math/rand"
	"os"
	"strings"

	log "github.com/sirupsen/logrus"

	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/base"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/fs"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/if1/cpm"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/types"
)

// -
func NewCartridge() *base.Cartridge {
	return newCartridge(false)
}

// -
func NewCartridgeCPM() *base.Cartridge {
	return newCartridge(true)
}

// -
func newCartridge(cpm bool) *base.Cartridge {
	impl := &cartridge{cpm: cpm}
	cart := base.NewCartridge(types.IF1, SectorCount, impl)
	impl.Cartridge = cart
	cart.RewindAccessIx(false)
	return cart
}

// -
type cartridge struct {
	*base.Cartridge
	//
	cpm bool
}

// -
func (c *cartridge) SectorSetAt(ix int, s types.Sector) {
	if s != nil {
		if hd, ok := s.Header().(*header); ok && hd != nil && hd.cpm {
			c.cpm = true
			if n := c.GetAnnotation(base.AnnoNameHint).String(); n != "" {
				c.SetName(n)
			}
		}
	}
}

// -
func (c *cartridge) Erased() {
	c.cpm = false
	c.RemoveAnnotation(base.AnnoNameHint)
}

// -
func (c *cartridge) Specials() string {
	if c.cpm {
		return SpecialsCPM
	}
	return ""
}

// -
func (c *cartridge) FS() fs.FileSystem {
	if c.cpm {
		return cpm.NewFs(c.Cartridge)
	}
	return newFs(c.Cartridge)
}

// -
func (c *cartridge) Format(name string) error {

	if c.cpm {
		return c.formatCPM(name)
	}

	log.WithField("name", name).Debug("formatting cartridge")

	// FIXME consider translation to Spectrum character set
	name = base.ToCartridgeName(name)

	var err error

	for ix := 0; ix < c.Size(); ix++ {

		var h *header
		var r *record

		s := c.GetSectorAt(ix)
		if s == nil || s.Header() == nil || s.Record() == nil {
			h = NewBlankHeader(-1, false)
			r = NewBlankRecord(0, false)
			if s, err = types.NewSector(h, r); err != nil {
				return err
			}
			c.SetSectorAt(ix, s)
		} else {
			h = s.Header().(*header)
			r = s.Record().(*record)
			r.block.ZeroOutSlice("data")
		}

		h.setName(name)
		h.setIndex(ix + 1)
		h.setFlags(1)
		if err = h.FixChecksum(); err != nil {
			return err
		}

		r.setIndex(0)
		r.setFlags(0)
		r.setLength(0)
		r.setName(NullName)
		if err = r.FixChecksums(); err != nil {
			return err
		}

		if log.IsLevelEnabled(log.TraceLevel) {
			s.Emit(os.Stdout)
		}
	}

	c.SetName(name)
	c.SeekToStart()
	c.SetModified(true)

	return err
}

// -
func (c *cartridge) formatCPM(name string) error {

	log.Debug("formatting CP/M cartridge")

	rnd := rand.Int()
	var err error

	for ix := 0; ix < c.Size(); ix++ {

		if ix >= SectorCountCPM {
			c.SetSectorAt(ix, nil)
			continue
		}

		var h *header
		var r *record

		s := c.GetSectorAt(ix)
		if s == nil || s.Header() == nil || s.Record() == nil {
			h = NewBlankHeader(-1, true)
			r = NewBlankRecord(0xe5, true)
			if s, err = types.NewSector(h, r); err != nil {
				return err
			}
			c.SetSectorAt(ix, s)
		} else {
			h = s.Header().(*header)
			r = s.Record().(*record)
			r.block.ZeroOutSlice("data")
		}

		secIx := SectorCountCPM - ix
		if ix%2 == 0 {
			secIx = SectorCountCPM/2 + secIx/2 - 1
		} else {
			secIx = (secIx - 1) / 2
		}

		h.setIndex(secIx)
		h.setFlags(0)
		h.setSpares(rnd)
		if err = h.FixChecksum(); err != nil {
			return err
		}

		r.setFlags(0xff)
		if err = r.FixChecksums(); err != nil {
			return err
		}

		if secIx < 2 { // FIXME constants
			r.block.FillSlice("data", 0xe5)
		}

		if log.IsLevelEnabled(log.TraceLevel) {
			s.Emit(os.Stdout)
		}
	}

	c.SetName(name)
	c.SeekToSector(0, true)
	c.SetModified(true)

	return err
}

// -
func (c *cartridge) Rename(n string) error {

	var err error
	n = base.ToCartridgeName(n)

	if c.cpm {
		n = strings.TrimSpace(n)
		c.Annotate(base.AnnoNameHint, n)

	} else {
		// FIXME consider translation to Spectrum character set
		for ix := 0; ix < c.Size() && err == nil; ix++ {
			if hd, ok := c.GetSectorAt(ix).Header().(*header); ok {
				if err = hd.setName(n); err == nil {
					err = hd.FixChecksum()
				}
			} else {
				err = fmt.Errorf("not an IF1 header")
			}
		}
	}

	c.SetName(n)
	c.SetModified(true)

	return err
}

// -
func (c *cartridge) Reorganize() error {

	var cart *base.Cartridge
	sort := fs.NewFileSorter(true)

	if c.cpm {
		cart = NewCartridgeCPM()
	} else {
		cart = NewCartridge()
		sort.AddHeads("run")
		sort.AddTails("title $SCREEN")
	}

	return c.Cartridge.Reorg(cart, sort.BySize)
}
