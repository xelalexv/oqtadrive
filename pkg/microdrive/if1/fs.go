/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2022, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package if1

import (
	"bytes"
	"encoding/hex"
	"fmt"
	"io"
	"sort"
	"strings"

	log "github.com/sirupsen/logrus"

	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/base"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/fs"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/types"
	"codeberg.org/xelalexv/oqtadrive/pkg/util"
)

// -
type FileHeader struct {
	Type    FileType
	Length  int // total length
	Start   int // start address of block, 0x5d05/23813 for BASIC
	Program int // length of program, same as length if BASIC, 0xffff if code
	Line    int // line number if LINE used, 0xffff otherwise
}

// -
func (fh *FileHeader) Bytes() []byte {
	var b bytes.Buffer
	b.WriteByte(byte(fh.Type))
	util.WriteUInt16(&b, fh.Length, false)
	util.WriteUInt16(&b, fh.Start, false)
	util.WriteUInt16(&b, fh.Program, false)
	util.WriteUInt16(&b, fh.Line, false)
	return b.Bytes()
}

// -
type fileInfo struct {
	name     string
	size     int
	typ      FileType
	sequence *fs.RecordSequence
	util.Annotations
}

// -
type file struct {
	*fs.File
	//
	fSys *fsys
}

// -
func (f *file) FileHeaderLength() int {

	if f == nil {
		return 0
	}

	switch f.Type() {
	case PRINT, ASCII, nil:
		return 0
	case DATA:
		return 1
	}
	return FileHeaderLength
}

// -
func (f *file) Render(w io.Writer) error {

	switch f.Type() {
	case BASIC:
		parseBasic(w, f, f.GetAnnotation("programLength").Int())
		return nil
	case PRINT:
		util.StreamTranslate(w, f, 0, 32, Translate)
		return nil
	case ASCII:
		util.StreamTranslate(w, f, 0, 0, Translate)
		return nil
	}

	d := hex.Dumper(w)
	defer d.Close()
	_, err := io.Copy(d, f)
	return err
}

// -
func (f *file) ClaimSectors(start, count int) ([]types.Sector, error) {

	if count == 0 {
		return nil, fmt.Errorf("nothing requested")
	}

	if f.fSys == nil {
		return nil, fs.ErrNoFileSystem
	}

	vacant := func(ix int) (bool, types.Sector) {
		if 0 < ix && ix <= SectorCount && f.fSys.cart.SeekToSector(ix, false) {
			if s := f.fSys.cart.GetSector(); s != nil {
				if r := s.Record(); r != nil {
					return !r.InUse(), s
				}
			}
		}
		return false, nil
	}

	top := f.fSys.cart.GetAnnotation(base.AnnoReducedLength).Int()
	if top == 0 {
		top = f.fSys.cart.Size()
	}

	opts := &fs.ClaimOpts{
		Start:    start,
		Count:    count,
		Top:      top,
		Bottom:   1,
		FirstGap: 1,
		SecGap:   1,
	}

	if start < 0 {
		opts.FirstGap = 2
	}

	return fs.SimpleClaim(opts, vacant)
}

// -
func (f *file) CommitRecord(r types.Record, ix, length int, closing bool) error {

	rImpl, ok := r.(*record)
	if !ok {
		return fmt.Errorf("not an IF1 record")
	}
	if length < 0 {
		return fmt.Errorf("invalid record length: %d", length)
	}

	fl := RecordFlagNotAPrintFile
	if f.Type() == PRINT {
		fl = 0
	}

	if closing {
		if ix < 0 {
			rImpl.setFlags(fl | RecordFlagLastRecord)
			f.AddAnnotations(fileAnnotations(nil, nil, rImpl))
		} else if ix == 0 {
			f.AddAnnotations(fileAnnotations(nil, rImpl, nil))
		}

	} else {
		if ix < 0 {
			return fmt.Errorf("invalid record index: %d", ix)
		}
		rImpl.setIndex(byte(ix))
		rImpl.setLength(length)
		rImpl.setName(f.Name())
		rImpl.setFlags(fl)
	}

	return rImpl.FixChecksums()
}

// -
func (f *file) ReleaseRecords(rec []types.Record, purge bool) (int, error) {
	for _, r := range rec {
		if err := releaseRecord(r, purge); err != nil {
			return 0, err
		}
	}
	return len(rec), nil
}

// -
func newFs(cart *base.Cartridge) *fsys {
	return &fsys{cart: cart}
}

// -
type fsys struct {
	cart *base.Cartridge
}

// -
func (fsys *fsys) Open(name string, mode fs.FileMode, typ fs.FileType) (
	*fs.File, error) {

	if name == "" {
		return nil, fs.ErrEmptyFileName
	}

	ft := UNKNOWN
	ok := false
	if typ != nil {
		if ft, ok = typ.(FileType); !ok {
			if typ.String() == ASCII.String() {
				ft = ASCII
			} else {
				return nil, fmt.Errorf("not an IF1 file type: %v", typ)
			}
		}
	}

	log.WithFields(log.Fields{
		"name": name, "mode": mode, "type": ft.String()}).Debug("open")

	switch mode {
	case fs.READ:
		return fsys.openRead(name, ft)
	case fs.WRITE, fs.OVERWRITE:
		return fsys.openWrite(name, mode, ft)
	default:
		return nil, fs.ErrUnsupportedAccessMode
	}
}

// -
func (fsys *fsys) openRead(name string, typ FileType) (*fs.File, error) {
	return fsys.open(name, fs.READ, typ)
}

// -
func (fsys *fsys) openWrite(name string, mode fs.FileMode, typ FileType) (
	*fs.File, error) {

	var ret *fs.File

	switch mode {

	case fs.WRITE:
		if fsys.exists(name) {
			return nil, fmt.Errorf(fs.FileExists, name)
		}

	case fs.OVERWRITE:
		var err error
		if ret, err = fsys.open(name, mode, typ); err == nil { // file exists
			return ret, nil
		} else if err != fs.ErrFileNotFound {
			return nil, err
		}
	}

	impl := &file{fSys: fsys}
	ret = fs.NewFile(name, mode, typ, 0, -1, nil, impl)
	impl.File = ret

	return ret, nil
}

// -
func (fsys *fsys) open(name string, mode fs.FileMode, typ FileType) (
	*fs.File, error) {

	var records []types.Record
	rFound := 0
	size := 0
	count := 0
	lastSec := -1

	for ix := 0; ix < fsys.cart.Size(); ix++ {

		if s := fsys.cart.GetSectorAt(ix); s != nil {
			if r, ok := s.Record().(*record); ok && r != nil && r.InUse() {

				if canonicalName(r.Name()) != name {
					continue
				}

				if d := r.Index() + 1 - len(records); d > 0 {
					records = append(records, make([]types.Record, d)...)
				}

				if records[r.Index()] == nil {
					records[r.Index()] = r
					size += r.Length()
					rFound++
				}
				if isLast(r) {
					count = r.Index() + 1
					lastSec = s.Index()
				}
				if rFound == count {
					break
				}
			}
		}
	}

	if len(records) == 0 || records[0] == nil {
		return nil, fs.ErrFileNotFound
	}

	if len(records) != rFound {
		return nil, fmt.Errorf(
			"file corrupted: want %d records, got %d", len(records), rFound)
	}

	first := records[0].(*record)
	last := records[len(records)-1].(*record)

	impl := &file{fSys: fsys}
	typ = extractFileType(typ, first, last)
	// use name from first record to have it in Spectrum char set
	ret := fs.NewFile(first.Name(), mode, typ, size, lastSec, records, impl)
	impl.File = ret

	ret.AddAnnotations(fileAnnotations(nil, first, last))

	return ret, nil
}

// -
func (fsys *fsys) exists(name string) bool {

	for ix := 0; ix < fsys.cart.Size(); ix++ {
		if s := fsys.cart.GetSectorAt(ix); s != nil {
			if r, ok := s.Record().(*record); ok && r != nil && r.InUse() {
				if canonicalName(r.Name()) == name {
					return true
				}
			}
		}
	}

	return false
}

// -
func (fsys *fsys) Delete(name string, purge bool) (bool, error) {

	del := false

	// We need to iterate through all records, because the file may have been
	// saved with multiple copies setting.
	for ix := 0; ix < fsys.cart.Size(); ix++ {
		if s := fsys.cart.GetSectorAt(ix); s != nil {
			if r := s.Record(); r != nil && r.InUse() {
				if canonicalName(r.Name()) == name {
					releaseRecord(r, purge)
					del = true
				}
			}
		}
	}

	if del {
		fsys.cart.SetModified(true)
	}

	return del, nil
}

// -
func releaseRecord(r types.Record, purge bool) error {
	if r, ok := r.(*record); ok && r != nil {
		r.setName(NullName)
		r.setFlags(0)
		r.setIndex(0)
		r.setLength(0)
		r.fixHeaderChecksum()
		if purge {
			d := r.Data()
			for ix := range d {
				d[ix] = 0
			}
			r.fixDataChecksum()
		}
		r.mux()
	}
	return nil
}

// -
func (fsys *fsys) ReclaimSectors(s []int) error {
	for _, ix := range s {
		if fsys.cart.SeekToSector(ix, true) {
			if sec := fsys.cart.GetNextSector(); sec != nil {
				if r := sec.Record(); r != nil {
					if err := releaseRecord(r, false); err != nil {
						return err
					}
				}
			}
		}
	}
	return nil
}

// -
func (fsys *fsys) Ls() (*fs.Stats, []*fs.FileInfo, error) {

	dir := make(map[string]*fileInfo)
	used := 0

	for ix := 0; ix < fsys.cart.Size(); ix++ {

		if sec := fsys.cart.GetNextSector(); sec != nil {
			if rec, ok := sec.Record().(*record); ok && rec != nil && rec.InUse() {

				name := canonicalName(rec.Name())
				if name == "" {
					continue
				}

				used++

				fi, ok := dir[name]
				if ok {
					fi.size += rec.Length()
				} else {
					fi = &fileInfo{
						name:        name,
						size:        rec.Length(),
						sequence:    fs.NewRecordSequence(SectorCount, 1, 1),
						Annotations: make(util.Annotations),
					}
					dir[name] = fi
				}

				if rec.Index() == 0 {
					fi.typ = extractFileType(UNKNOWN, rec, nil)
					fileAnnotations(fi.Annotations, rec, nil)
				}
				if rec.Flags()&RecordFlagLastRecord != 0 {
					fi.typ = extractFileType(fi.typ, nil, rec)
					fileAnnotations(fi.Annotations, nil, rec)
				}

				fi.sequence.Add(rec.Index(), sec.Index())
			}
		}
	}

	var files []string
	for f, fi := range dir {
		if len(fi.Annotations) > 0 { // if first record wasn't seen, file does not exist
			files = append(files, f)
		}
	}
	sort.Strings(files)

	ret := make([]*fs.FileInfo, len(files))
	for ix, name := range files {
		fi := dir[name]
		ret[ix] = fs.NewFileInfo(name, "", "", fi.size, fi.typ)
		ret[ix].AddAnnotations(fi.Annotations)
		ret[ix].Annotate("fragmentation", fi.sequence.Fragmentation(false))
	}

	return fs.NewStats(fsys.cart.Size(), used), ret, nil
}

// -
func (fsys *fsys) ParseFileType(t string) fs.FileType {
	return parseFileType(t)
}

// -
func isLast(r *record) bool {
	return r.Length() > 0 && r.Flags()&RecordFlagLastRecord != 0
}

// -
func canonicalName(n string) string {
	return strings.TrimSpace(Translate(n, ' '))
}

// -
func extractFileType(present FileType, first, last *record) FileType {

	ret := present

	if ret == UNKNOWN && first != nil {
		ret = FileType(int(first.block.GetByte("fileType")))
	}

	if last != nil && ret != DATA { // last record may override file type
		if last.Flags() != 0 && last.Flags()&RecordFlagNotAPrintFile == 0 {
			ret = PRINT
		}
	}

	return ret
}

// -
func fileAnnotations(a util.Annotations, first, last *record) util.Annotations {

	if a == nil {
		a = make(util.Annotations)
	}

	if first != nil {
		// FIXME invalid for PRINT files
		a.Annotate("line", first.block.GetInt("lineNumber"))
		a.Annotate("programLength", first.block.GetInt("programLength"))
		a.Annotate("fileLength", first.block.GetInt("fileLength"))
		//
	}

	return a
}
