/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2021, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package types

import (
	"io"
)

//
type Header interface {

	// Client returns the type of client for which the header is intended
	Client() Client

	// Muxed returns the muxed data bytes of the header as needed for replay
	Muxed() []byte

	// Demuxed returns the plain data bytes of the header
	Demuxed() []byte

	//
	Flags() byte

	//
	Index() int

	// Name returns the name of the cartridge the header belongs to
	Name() string

	// Emit emits the header
	Emit(w io.Writer)

	// Fix the header's check sum
	FixChecksum() error

	// Validate validates the header
	Validate() error
}
