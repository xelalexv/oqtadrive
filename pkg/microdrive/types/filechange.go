/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2022, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package types

import (
	"fmt"
	"strings"
)

// -
type Op int

const (
	OpCreated Op = 1 << iota
	OpModified
	OpDeleted
	OpFormatted

	OpNone = 0
	OpAny  = OpCreated | OpModified | OpDeleted | OpFormatted
)

// -
func (op Op) String() string {
	switch op {
	case OpCreated:
		return "created"
	case OpModified:
		return "modified"
	case OpDeleted:
		return "deleted"
	case OpFormatted:
		return "formatted"
	case OpNone:
		return "none"
	default:
		return "<unknown>"
	}
}

// -
func NewFileChange(name string, op Op) *FileChange {
	return &FileChange{Name: name, Op: op}
}

// -
type FileChange struct {
	Name string // file name or cartridge name in case of format
	Op   Op
}

// -
func (fc *FileChange) Is(op Op) bool {
	return fc != nil && fc.Op == op
}

// -
func (fc *FileChange) Merge(op Op) {

	if fc == nil || fc.Op == OpFormatted {
		return
	}

	switch op {
	case OpFormatted, OpCreated:
		fc.Op = op
	case OpModified:
		if !fc.Is(OpCreated) {
			fc.Op = op
		}
	}
}

// -
type FileChangeSet map[string]*FileChange

// -
func (fcs FileChangeSet) Contains(op Op) bool {
	if fcs != nil && op != OpNone {
		for _, v := range fcs {
			if v.Is(op) {
				return true
			}
		}
	}
	return false
}

// -
func (fcs FileChangeSet) Merge(c *FileChange) {
	if c == nil || fcs == nil || c.Op == OpNone {
		return
	}
	if fc, ok := fcs[c.Name]; ok {
		fc.Merge(c.Op)
	} else {
		fcs[c.Name] = c
	}
}

// -
func (fcs FileChangeSet) List() string {
	var sb strings.Builder
	if fcs != nil {
		for k, v := range fcs {
			fmt.Fprintf(&sb, "%s: %s, ", k, v.Op.String())
		}
	}
	return sb.String()
}
