/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2021, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package types

import (
	"fmt"
	"strings"
)

// -
const (
	NotAnError         = 0
	Benign             = 1
	BenignIfFormatting = 2
	Fatal              = 3
)

// -
type MediaError struct {
	error
	level   int
	message strings.Builder
}

// -
func NewMediaError() *MediaError {
	return &MediaError{}
}

// -
func (e *MediaError) Add(msg string, level int) {
	if e.message.Len() > 0 {
		e.message.WriteString("; ")
	}
	e.message.WriteString(msg)
	e.SetLevel(level)
}

// -
func (e *MediaError) Done() error {
	e.error = fmt.Errorf(e.message.String())
	return e
}

// -
func (e *MediaError) Level() int {
	return e.level
}

// -
func (e *MediaError) SetLevel(l int) {
	if l > e.level {
		e.level = l
	}
}

// -
func (e *MediaError) ResetLevel(l int) {
	e.level = l
}

// -
func IsFatal(e error, formatting bool) bool {
	if e == nil {
		return false
	}
	err, ok := e.(*MediaError)
	return !ok ||
		(formatting && err.level > BenignIfFormatting) ||
		(!formatting && err.Level() > Benign)
}
