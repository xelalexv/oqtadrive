/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2021, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package types

import (
	"io"
)

//
type Record interface {

	// Client returns the type of client for which the record is intended
	Client() Client

	// Muxed returns the muxed data bytes of the record as needed for replay
	Muxed() []byte

	// Demuxed returns the plain data bytes of the record
	Demuxed() []byte

	// Data returns the raw data of the record, without header data, but
	// possibly including file header and extraneous data
	Data() []byte

	//
	Flags() byte

	//
	Index() int

	//
	Length() int

	// InUse returns whether the record is in use, i.e. neither bad nor free
	// for use
	InUse() bool

	// Name returns the name of the record, if applicable; for the QL, only the
	// first record of a file carries the name of the file
	Name() string

	// Emit emits the record
	Emit(w io.Writer)

	// Fix the record's check sums
	FixChecksums() error

	// Validate validates the record
	Validate() error

	// Replaces checks old record for anything that may need to be copied into
	// this record before it replaces old. The sector index indicates at which
	// sector the replacement is happening.
	Replaces(old Record, sectorIndex int) *FileChange
}
