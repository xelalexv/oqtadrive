/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2021, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package daemon

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"time"

	log "github.com/sirupsen/logrus"

	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/if1"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/ql"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/types"
	"codeberg.org/xelalexv/oqtadrive/pkg/util"
)

// -
const (
	MinProtocolVersion = 6
	MaxProtocolVersion = 6

	commandLength       = 4
	sendBufferLength    = 1024
	receiveBufferLength = 1024
	stopMarkerLength    = 4

	headerFlagIndex = 12

	boxA = 21
	boxB = 533
	boxC = 632
)

// -
var helloDaemon = []byte("hlod")
var helloIF1 = []byte("hloi")
var helloQL = []byte("hloq")
var byteBuf = []byte{0}

var transmissionBoxes = []int{boxA, boxB, boxC}

// -
type conduit struct {
	//
	headerLengthMux int
	recordLengthMux int
	//
	client types.Client
	port   io.ReadWriteCloser
	lck    *util.Lock
	//
	hwGroupStart  int
	hwGroupEnd    int
	hwGroupLocked bool
	//
	rumbleLevel byte
	//
	vProtocol int
	vFirmware int
	//
	sendBuf []byte
}

// -
func newConduit(port string, baudRate uint) (*conduit, error) {
	ret := &conduit{
		sendBuf:      make([]byte, sendBufferLength),
		hwGroupStart: -1,
		hwGroupEnd:   -1,
		lck:          util.NewLock(nil, nil),
	}
	var err error
	ret.port, err = openPort(port, baudRate)
	if err == nil {
		ret.lock()
	}
	return ret, err
}

// -
func (c *conduit) lock() bool {
	return c.lockWait(0)
}

// -
func (c *conduit) lockWait(timeout time.Duration) bool {
	if timeout == 0 {
		timeout = 10 * time.Millisecond
	}
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()
	return c.lck.Acquire(ctx)
}

// -
func (c *conduit) unlock() {
	c.lck.Release()
}

// -
func (c *conduit) close() error {
	return c.port.Close()
}

// -
func (c *conduit) syncOnHello(d *Daemon) error {

	c.vProtocol = -1
	c.vFirmware = -1

	log.Info("syncing with adapter")

align:
	hello := make([]byte, commandLength)

	for {
		if c.isHello(hello) {
			break
		}
		shiftLeft(hello)
		if err := c.receive(hello[len(hello)-1:]); err != nil {
			return err
		}
		if err := d.checkForStop(); err != nil {
			c.close()
			return err
		}
	}

	var cmd *command
	var err error

	for { // find last live hello from adapter
		start := time.Now()
		if cmd, err = c.receiveCommand(); err != nil {
			return err
		}
		if cmd.cmd() == CmdHello {
			if time.Since(start) > 1000*time.Millisecond {
				break
			}
		} else {
			log.Debugf("invalid command, re-aligning: %s %v",
				string(cmd.data), cmd.data)
			goto align
		}
		if err := d.checkForStop(); err != nil {
			c.close()
			return err
		}
	}

	log.WithField("client", c.client).Info("received hello")

	if err := c.send(helloDaemon); err != nil {
		return fmt.Errorf("error sending daemon hello: %v", err)
	}

	if cmd, err = c.receiveCommand(); err != nil {
		return fmt.Errorf("error receiving protocol version: %v", err)
	}

	if cmd.cmd() != CmdVersion {
		log.Warnf("adapter did not send protocol version, re-aligning: %v", cmd)
		goto align
	}

	c.vProtocol = int(cmd.arg(0))
	c.vFirmware = int(cmd.arg(1))
	if c.vProtocol < MinProtocolVersion || c.vProtocol > MaxProtocolVersion {
		return fmt.Errorf("unsupported protocol version: %d", c.vProtocol)
	}

	log.WithFields(log.Fields{
		"protocol version": c.vProtocol,
		"firmware version": c.vFirmware}).Info("synced")

	// FIXME move into separate method
	log.Info("getting config")
	configItemCount := int(cmd.arg(2))
	for ix := 0; ix < configItemCount; ix++ {
		if cmd, err = c.receiveCommand(); err != nil {
			return fmt.Errorf("error receiving config item: %v", err)
		}
		if cmd.cmd() != CmdConfig {
			return fmt.Errorf("adapter did not send config item: %v", cmd)
		}
		switch cmd.arg(0) {
		case CmdConfigRumble:
			c.rumbleLevel = cmd.arg(1)
			log.WithField("rumble level", cmd.arg(1)).Info("got config item")
		default:
			log.WithField("item", cmd.arg(0)).Error("unknown config item")
		}
	}

	return nil
}

// -
func (c *conduit) isHello(h []byte) bool {

	if bytes.Equal(h, helloIF1) {
		c.client = types.IF1
		c.headerLengthMux = if1.HeaderLengthMux
		c.recordLengthMux = if1.RecordLengthMux

	} else if bytes.Equal(h, helloQL) {
		c.client = types.QL
		c.headerLengthMux = ql.HeaderLengthMux
		c.recordLengthMux = ql.RecordLengthMux

	} else {
		return false
	}

	return true
}

// -
func (c *conduit) receive(data []byte) error {
	_, err := io.ReadFull(c.port, data)
	return err
}

// -
func (c *conduit) send(data []byte) error {
	_, err := c.port.Write(data)
	return err
}

// -
func (c *conduit) sendByte(b byte) error {
	byteBuf[0] = b
	_, err := c.port.Write(byteBuf)
	return err
}

// -
func (c *conduit) receiveCommand() (*command, error) {
	data := make([]byte, commandLength)
	if err := c.receive(data); err != nil {
		return nil, err
	}
	return newCommand(data), nil
}

// -
func (c *conduit) fillBlock(s types.Sector, headersOnly bool) int {

	header := s.Header().Muxed()
	c.sendBuf[0] = byte(len(header))
	copy(c.sendBuf[1:], header)

	var record []byte
	if !headersOnly {
		record = s.Record().Muxed()
	}
	copy(c.sendBuf[len(header)+1:], record)

	return len(header) + 1 + len(record)
}

// -
func (c *conduit) fillBlockRaw(offset int, data []byte) int {
	return copy(c.sendBuf[offset:], data)
}

// -
func (c *conduit) injectError(ix int) {
	if 0 <= ix && ix < len(c.sendBuf) {
		c.sendBuf[ix] += 13
	}
}

// -
func (c *conduit) sendBlock(length int) error {
	if _, err := c.port.Write(c.sendBuf[0:length]); err != nil {
		return fmt.Errorf("error sending block: %v", err)
	}
	return nil
}

// -
func (c *conduit) receiveBlock() ([]byte, error) {

	raw := make([]byte, receiveBufferLength)
	read, err := c.boxedReceive(raw[c.fillPreamble(raw):])
	if err != nil {
		return nil, fmt.Errorf("error reading block: %v", err)
	}

	read += 12 // preamble is not transmitted
	log.WithField("length", read).Trace("received block")
	return raw[:read], nil
}

// -
func (c *conduit) boxedReceive(b []byte) (int, error) {

	var start, end, ix int

	for ix, end = range transmissionBoxes {
		if err := c.receive(b[start:end]); err != nil {
			return -1, fmt.Errorf("error receiving box %d: %v", ix, err)
		}
		log.Tracef("received box %d", ix)
		if b[end-1] == 0 {
			break
		}
		start = end
	}

	if b[end-1] != 0 {
		return -1, fmt.Errorf("block transmission too long")
	}

	if err := c.receive(b[end : end+4]); err != nil {
		return -1, fmt.Errorf("error receiving box suffix: %v", err)
	}

	for ix, box := range transmissionBoxes[:2] {
		if end > box {
			log.Tracef("replacing byte at %d (%x > %x)", box-1, b[box-1], b[end+ix])
			b[box-1] = b[end+ix]
		}
	}

	return int(b[end+2]) + int(b[end+3])*256, nil
}

// -
func (c *conduit) fillPreamble(raw []byte) int {
	if len(raw) < 12 {
		return 0
	}
	clear(raw[:10])
	if c.client == types.QL {
		raw[10] = 0xf0
	} else {
		raw[10] = 0x0f
	}
	raw[11] = 0xff
	return 12
}

// FIXME: validate
func (c *conduit) verifyBlock(expected []byte) {

	//l := c.blockLength()
	l := 0 // FIXME
	if l != len(expected) {
		log.Errorf("length mismatch, want %d, got %d", len(expected), l)
		return
	}

	raw := make([]byte, l)
	if _, err := io.ReadFull(c.port, raw); err != nil {
		log.Errorf("read error: %v", err)
		return
	}

	errors := 0
	for ix := range expected {
		if expected[ix] != raw[ix] {
			errors++
		}
	}

	if errors == 0 {
		log.Info("OK")
	} else {
		log.Errorf("NG: %d", errors)
	}
}

// -
func shiftLeft(buf []byte) {
	if len(buf) > 1 {
		for ix := 0; ix < len(buf)-1; ix++ {
			buf[ix] = buf[ix+1]
		}
	}
}
