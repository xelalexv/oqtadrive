/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2022, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package daemon

import (
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/base"
)

//
type State int

const (
	DriveStarted State = 1 << iota
	DriveStopped
	DriveLoaded
	DriveUnloaded

	StateNone = 0
	StateAny  = DriveStarted | DriveStopped | DriveLoaded | DriveUnloaded
)

//
func (s State) String() string {
	switch s {
	case DriveStarted:
		return "started"
	case DriveStopped:
		return "stopped"
	case DriveLoaded:
		return "loaded"
	case DriveUnloaded:
		return "unloaded"
	case StateNone:
		return "none"
	default:
		return "<unknown>"
	}
}

// DriveChangeListener are invoked synchronously and receive an already locked
// cartridge, no further attempts at locking or unlocking(!) should be taken.
type DriveChangeListener func(c *base.Cartridge, dc *DriveChange) error

//
func NewDriveChange(drive int, s State) *DriveChange {
	return &DriveChange{Drive: drive, State: s}
}

//
type DriveChange struct {
	Drive int
	State State
}
