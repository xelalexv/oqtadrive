/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2021, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package daemon

import (
	"context"
	"errors"
	"fmt"
	"sync/atomic"
	"time"

	log "github.com/sirupsen/logrus"

	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/base"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/format/helper"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/types"
	"codeberg.org/xelalexv/oqtadrive/pkg/util"
	"codeberg.org/xelalexv/oqtadrive/pkg/zxnet/hub"
)

// -
const (
	DriveCount = 8
	//
	StatusEmpty    = "empty"
	StatusIdle     = "idle"
	StatusBusy     = "busy"
	StatusHardware = "hardware"
)

// -
var ErrDaemonStopped = errors.New("daemon stopped")

// the daemon that manages communication with the Interface 1/QL
type Daemon struct {
	//
	cartridges  []atomic.Value
	conduit     *conduit
	forceClient types.Client
	port        string
	baudRate    uint
	synced      bool
	//
	mru        *mru
	autoSave   bool
	debugStart time.Time
	// listeners to be attached to each cartridge upon load
	lstDriveChange util.ListenerMap[DriveChangeListener]
	lstFileChange  util.ListenerMap[base.FileChangeListener]
	//
	hubClient *hub.Client
	//
	ctrlRun chan func() error
	ctrlAck chan error
	//
	stop chan bool
}

// -
func NewDaemon(port string, baudRate uint, force types.Client) *Daemon {
	return &Daemon{
		cartridges:     make([]atomic.Value, DriveCount),
		port:           port,
		baudRate:       baudRate,
		forceClient:    force,
		mru:            &mru{},
		lstDriveChange: make(util.ListenerMap[DriveChangeListener]),
		lstFileChange:  make(util.ListenerMap[base.FileChangeListener]),
		ctrlRun:        make(chan func() error),
		ctrlAck:        make(chan error),
		stop:           make(chan bool),
	}
}

// -
func (d *Daemon) Serve() error {

	d.hubClient = hub.NewClient()
	d.hubClient.Run(func() { networkNotify(d) })

	return d.listen()
}

// -
func (d *Daemon) Stop() {

	log.Info("daemon stopping...")
	d.stop <- true

	if d.hubClient != nil {
		d.hubClient.Stop()
	}
}

// -
func (d *Daemon) AddDriveChangeListener(id string, lst DriveChangeListener) {
	log.WithField("id", id).Debug("adding drive change listener")
	d.lstDriveChange[id] = lst
}

// -
func (d *Daemon) RemoveDriveChangeListener(id string) {
	log.WithField("id", id).Debug("removing drive change listener")
	delete(d.lstDriveChange, id)
}

// -
func (d *Daemon) AddFileChangeListener(id string, lst base.FileChangeListener) {
	log.WithField("id", id).Debug("adding file change listener")
	d.lstFileChange[id] = lst
}

// -
func (d *Daemon) RemoveFileChangeListener(id string) {
	log.WithField("id", id).Debug("removing file change listener")
	delete(d.lstFileChange, id)
}

// -
func (d *Daemon) checkForStop() error {
	select {
	case s := <-d.stop:
		if s {
			return ErrDaemonStopped
		}
	default:
	}
	return nil
}

// -
func (d *Daemon) listen() error {

	d.loadCartridges()

	if err := d.ResetConduit(); err != nil {
		return err
	}

	var cmd *command
	var lastCmd byte
	var keepUnlocked bool
	var locked bool = true // we always start with a locked conduit
	var err error

	for ; ; cmd = nil {

		if err = d.checkForStop(); err != nil {
			if d.conduit != nil {
				d.conduit.close()
			}
			return err
		}

		if d.synced { // command dispatch
			// The conduit needs to be locked during the dispatch of each
			// command, with the exception of debug commands, which do not
			// change the locking state of the conduit. When locked, it is kept
			// locked until the next ping has occurred (see also notes in loop()
			// in oqtadrive.ino). A ping exchange then unlocks the conduit so
			// that the daemon can send flags. Afterwards it is kept unlocked
			// until the first non-debug command occurs.
			if keepUnlocked || lastCmd == CmdPing {
				log.Trace("unlocking conduit")
				d.conduit.unlock()
				locked = false
				keepUnlocked = true
			}

			if cmd, err = d.conduit.receiveCommand(); err == nil {
				// debug commands must not change locking state of conduit
				if cmd.cmd() != CmdDebug {
					keepUnlocked = false
					if !locked {
						log.Trace("locking conduit")
						if locked = d.conduit.lock(); !locked {
							err = fmt.Errorf(
								"could not lock conduit after receiving command")
						}
					}
				}
				lastCmd = cmd.cmd()
			} else {
				err = fmt.Errorf("error receiving command: %v", err)
			}

			if err == nil {
				if log.IsLevelEnabled(log.TraceLevel) {
					log.WithFields(log.Fields{
						"locked":  d.conduit.lck.IsLocked(),
						"command": string(cmd.cmd())}).Trace("dispatching command")
				}
				if err = cmd.dispatch(d); err != nil {
					err = fmt.Errorf("error dispatching command: %v", err)
				}
			}

		} else { // sync with adapter
			if err = d.conduit.syncOnHello(d); err != nil {
				if err == ErrDaemonStopped {
					return nil
				}
				err = fmt.Errorf("error syncing with adapter: %v", err)
			} else {
				d.synced = true
				for ix := 1; ix <= DriveCount; ix++ {
					if cart := d.getCartridge(ix); cart != nil {
						cart.Unlock()
					}
				}
				if d.forceClient != types.UNKNOWN &&
					d.conduit.client != d.forceClient {
					log.WithField("client", d.forceClient).Info(
						"resyncing with adapter to force client type")
					go d.Resync(d.forceClient, false)
				}
			}
		}

		if err != nil {
			log.Errorf("%v", err)
			d.mru.reset()
			if err = d.ResetConduit(); err != nil {
				return err
			}
			keepUnlocked = false
			locked = true // reset created new conduit, which is locked
			lastCmd = 0
		}
	}
}

// -
func (d *Daemon) ResetConduit() error {

	logger := log.WithFields(log.Fields{"port": d.port, "baud rate": d.baudRate})
	d.synced = false

	if d.conduit != nil {
		logger.Info("closing serial port")
		if err := d.conduit.close(); err != nil {
			log.Errorf("error closing serial port: %v", err)
		}
		d.conduit = nil
	}

	logger.Info("opening serial port")
	maxBackoff := 15 * time.Second
	quiet := false

	for backoff := time.Millisecond; ; {
		if err := d.checkForStop(); err != nil {
			return err
		}
		if con, err := newConduit(d.port, d.baudRate); err != nil {
			if !quiet {
				logger.Warnf("cannot open serial port: %v", err)
			}

			if backoff < maxBackoff {
				backoff = backoff * 5 / 4
			} else if !quiet {
				logger.Warn(
					"repeatedly failed to open serial port, will keep trying but stop logging about it")
				quiet = true
			}
			if backoff < time.Second {
				time.Sleep(time.Second)
			} else {
				time.Sleep(backoff)
			}

		} else {
			logger.Info("serial port opened")
			d.conduit = con
			return nil
		}
	}
}

// -
func (d *Daemon) loadCartridges() {
	for ix := 1; ix <= len(d.cartridges); ix++ {
		if cart, err := helper.AutoLoad(ix, true); err != nil {
			log.Errorf(
				"failed loading auto-saved cartridge for drive %d: %v", ix, err)
		} else if cart != nil {
			d.SetCartridge(ix, cart, true)
		}
	}
}

// -
func (d *Daemon) fillEmptyDrives() {
	for ix := 1; ix <= len(d.cartridges); ix++ {
		if d.getCartridge(ix) == nil {
			if cart, err := microdrive.NewCartridge(d.conduit.client); err == nil {
				d.SetCartridge(ix, cart, true)
			}
		}
	}
}

// -
func (d *Daemon) flushAutoSave() {
	log.Info("flushing auto-save")
	for ix := 1; ix <= len(d.cartridges); ix++ {
		if cart, _ := d.GetCartridge(ix, 5*time.Millisecond); cart != nil {
			if err := helper.AutoSave(
				ix, cart, cart.HasAnnotation(AnnoFixCheckSums)); err != nil {
				log.Errorf("auto-saving drive %d failed: %v", ix, err)
			}
			cart.RemoveAnnotation(AnnoFixCheckSums)
			cart.Unlock()
		}
	}
}

// -
func (d *Daemon) UnloadCartridge(ix int, force bool) error {
	return d.SetCartridge(ix, nil, force)
}

// SetCartridge sets the cartridge at slot ix (1-based).
func (d *Daemon) SetCartridge(ix int, c *base.Cartridge, force bool) error {

	if present, ok := d.GetCartridge(ix, time.Second); !ok {
		return fmt.Errorf("could not lock present cartridge")

	} else if present != nil {
		present.Unlock()
		log.WithFields(log.Fields{
			"modified": present.IsModified(),
			"drive":    ix,
			"force":    force}).Debug("unloading cartridge")
		if !force && present.IsModified() {
			return fmt.Errorf("present cartridge is modified")
		}
		invokeDriveChangeListeners(present, NewDriveChange(ix, DriveUnloaded))
	}

	d.setCartridge(ix, c)

	if c == nil || !c.IsFormatted() {
		if err := helper.AutoRemove(ix); err != nil {
			log.Errorf("removing auto-save file for drive %d failed: %v", ix, err)
		}

	} else if !c.IsAutoSaved() {
		if err := helper.AutoSave(ix, c, false); err != nil {
			log.Errorf("auto-saving drive %d failed: %v", ix, err)
		}
	}

	return nil
}

// -
func (d *Daemon) setCartridge(ix int, c *base.Cartridge) {
	if 0 < ix && ix <= len(d.cartridges) {
		d.cartridges[ix-1].Store(c)
		if c != nil {
			for id, lst := range d.lstFileChange {
				addFileChangeListener(c, id, lst)
			}
			for id, lst := range d.lstDriveChange {
				addDriveChangeListener(c, id, lst)
			}
			invokeDriveChangeListeners(c, NewDriveChange(ix, DriveLoaded))
		}
	}
}

// GetClient gets the type of currently connected adapter
func (d *Daemon) GetClient() string {
	return d.GetClientType().String()
}

// FIXME: not atomic
func (d *Daemon) GetClientType() types.Client {
	if d.synced {
		return d.conduit.client
	}
	return types.UNKNOWN
}

// GetAdapterVersion gets protocol & firmware versions of currently connected
// adapter; FIXME: not atomic
func (d *Daemon) GetAdapterVersion() (protocol, firmware string) {

	protocol = "-"
	firmware = "-"

	if d.synced {
		if d.conduit.vProtocol > -1 {
			protocol = fmt.Sprintf("%d", d.conduit.vProtocol)
		}
		if d.conduit.vFirmware > -1 {
			firmware = fmt.Sprintf("%d", d.conduit.vFirmware)
		}
	}

	return
}

// GetStatus gets the status of cartridge at slot ix (1-based)
func (d *Daemon) GetStatus(ix int) string {
	start, end, _ := d.GetHardwareDrives()
	if start <= ix && ix <= end {
		return StatusHardware
	}
	if cart := d.getCartridge(ix); cart != nil {
		if cart.IsLocked() {
			return StatusBusy
		}
		return StatusIdle
	}
	return StatusEmpty
}

// GetCartridge gets the cartridge at slot ix (1-based)
func (d *Daemon) GetCartridge(ix int, timeout time.Duration) (*base.Cartridge, bool) {
	if cart := d.getCartridge(ix); cart != nil {
		if LockCartridge(cart, timeout) {
			return cart, true
		} else {
			return nil, false
		}
	}
	return nil, true
}

// -
func (d *Daemon) getCartridge(ix int) *base.Cartridge {
	if 0 < ix && ix <= len(d.cartridges) {
		if cart := d.cartridges[ix-1].Load(); cart != nil {
			return cart.(*base.Cartridge)
		}
	}
	return nil
}

// FIXME: not atomic
func (d *Daemon) GetHardwareDrives() (int, int, bool) {
	if d.synced {
		return d.conduit.hwGroupStart, d.conduit.hwGroupEnd, d.conduit.hwGroupLocked
	}
	return -1, -1, false
}

// -
func (d *Daemon) MapHardwareDrives(start, end int) error {

	if d.synced && d.conduit.hwGroupLocked {
		return fmt.Errorf("hardware drive settings are locked")
	}

	if start < 0 || start > DriveCount {
		return fmt.Errorf("illegal start index for hardware drive: %d", start)
	}

	if end < 0 || end > DriveCount || end < start {
		return fmt.Errorf("illegal end index for hardware drive: %d", end)
	}

	if (start > 0 && end == 0) || (end > 0 && start == 0) {
		return fmt.Errorf(
			"either both hardware drive indexes are 0 or none: start = %d, end = %d",
			start, end)
	}

	return d.runControl(true, func() error {
		if d.synced {
			return d.conduit.send([]byte{CmdMap, byte(start), byte(end), 0})
		}
		return fmt.Errorf("not synced with adapter")
	})
}

// -
func (d *Daemon) Resync(cl types.Client, reset bool) error {

	if !d.synced {
		return fmt.Errorf("not connected to adapter")
	}

	if d.forceClient != types.UNKNOWN && cl != d.forceClient {
		log.Warnf(
			"daemon was started with forced client type '%s', cannot override",
			d.forceClient)
		cl = d.forceClient
	}

	var p byte
	switch cl {
	case types.IF1:
		p |= MaskIF1
	case types.QL:
		p |= MaskQL
	}

	if reset {
		d.synced = false
		if err := d.conduit.close(); err != nil {
			return err
		}
		if p == 0 {
			return nil
		}
	}

	return d.runControl(true, func() error {
		if d.synced {
			return d.conduit.send([]byte{CmdResync, p, 0, 0})
		}
		return fmt.Errorf("not connected to adapter")
	})
}

// -
func (d *Daemon) GetHubClient() *hub.Client {
	return d.hubClient
}

// -
func (d *Daemon) GetConfig(item string) (interface{}, error) {

	if !d.synced {
		return nil, nil
	}

	switch item {

	case CmdConfigItemRumble:
		return d.conduit.rumbleLevel, nil
	}

	return nil, fmt.Errorf("illegal config item: %s", item)
}

// -
func (d *Daemon) SetConfig(item string, arg1, arg2 byte) error {

	var code byte
	set := func() {}

	switch item {

	case CmdConfigItemRumble:
		if arg1 < CmdConfigRumbleMin || arg1 > CmdConfigRumbleMax {
			return fmt.Errorf("illegal rumble level %d (use %d through %d",
				arg1, CmdConfigRumbleMin, CmdConfigRumbleMax)
		}
		code = CmdConfigRumble
		set = func() { d.conduit.rumbleLevel = arg1 }

	default:
		return fmt.Errorf("illegal config item: %s", item)
	}

	return d.runControl(true, func() error {
		if d.synced {
			err := d.conduit.send([]byte{CmdConfig, code, arg1, arg2})
			if err == nil {
				set()
			}
			return err
		}
		return fmt.Errorf("not synced with adapter")
	})
}

// -
func (d *Daemon) SendFlag(f byte) error {
	return d.SendFlagWait(f, 0)
}

// -
func (d *Daemon) SendFlagWait(f byte, timeout time.Duration) error {

	if !d.synced {
		return fmt.Errorf("not connected to adapter")
	}

	if !d.conduit.lockWait(timeout) {
		return fmt.Errorf("could not lock conduit for sending flag")
	}
	defer d.conduit.unlock()

	log.WithField("flag", string(f)).Debug("sending flag")
	return d.conduit.sendByte(f)
}

// queues control function and waits for it to be run
func (d *Daemon) runControl(ring bool, f func() error) error {

	if ring {
		log.Debug("sending ring flag")
		if err := d.SendFlag(FlgRing); err != nil {
			log.Warnf("error sending ring flag: %v", err)
		}
	}

	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()

	log.Debug("queuing control command")
	select {
	case d.ctrlRun <- f:
		log.Debug("control command queued")
		break
	case <-ctx.Done():
		return fmt.Errorf("queuing control command timed out")
	}

	select {
	case err := <-d.ctrlAck:
		log.Debug("control command finished")
		return err
	case <-ctx.Done():
		return fmt.Errorf("running control command timed out")
	}
}

// -
func (d *Daemon) processControl() {

	var f func() error

	select {
	case f = <-d.ctrlRun:
		break
	default:
		log.Trace("no control command")
		return
	}

	log.Debug("running control command")
	err := f()

	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	select {
	case d.ctrlAck <- err:
		break
	case <-ctx.Done():
		log.Warn("control command client went away")
		break
	}
}

// -
func LockCartridge(cart *base.Cartridge, timeout time.Duration) bool {
	if cart == nil {
		return true
	}
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()
	return cart.Lock(ctx)
}
