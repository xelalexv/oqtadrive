/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2021, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package daemon

import (
	"fmt"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"

	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/base"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/types"
)

// -
func (c *command) put(d *Daemon) error {

	drive, err := c.drive()
	if err != nil {
		return err
	}

	// ignore canceled PUT - 1 = stopped by state switch, 2 = timeout
	if c.arg(2) != 0 {
		log.WithFields(
			log.Fields{"drive": drive, "code": c.arg(2)}).Debug("PUT canceled")
		return nil
	}

	data, err := d.conduit.receiveBlock()
	if err != nil {
		return err
	}

	// on first put, make sure we have a correct cartridge in place
	cart := d.getCartridge(drive)
	if cart == nil || cart.Client() != d.conduit.client {
		log.Debug("creating new cartridge")
		if cart, err = microdrive.NewCartridge(d.conduit.client); err != nil {
			return err
		}
		if !LockCartridge(cart, 5*time.Millisecond) {
			return fmt.Errorf("could not lock new cartridge")
		}
		d.setCartridge(drive, cart)
	}

	if cart == nil {
		return fmt.Errorf("PUT severe error - no cartridge")
	}

	if len(data) < 200 {
		hd, err := microdrive.NewHeader(d.conduit.client, data, true)
		if err != nil {
			return fmt.Errorf("error creating header: %v", err)
		}
		if err = d.mru.setHeader(hd); err != nil {
			return err
		}
		if !cart.IsFormatting() { // header put means we're formatting
			cart.SetFormatting(true)
			cart.RemoveAnnotations(base.AnnoReducedLength, AnnoHeadersOnly)
			cart.Erase()
		}

	} else {
		rec, err := microdrive.NewRecord(d.conduit.client, data, true)
		if err != nil {
			if types.IsFatal(err, cart.IsFormatting()) {
				return fmt.Errorf("error creating record: %v", err)
			} else {
				log.Debugf("non-fatal error creating record: %v", err)
				cart.Annotate(AnnoFixCheckSums, true)
			}
		}

		if chg, err := d.mru.setRecord(rec); err != nil {
			return err
		} else if !cart.IsFormatting() {
			// For IF1, cart.IsFormatting is only true during the initial format
			// round. Formatted events are reported in the third and last round
			// via mru.setRecord. We only need to merge the first of these,
			// however, and set the cartridge name, since it is not filled in by
			// the record.
			if chg.Is(types.OpFormatted) && chg.Name == "" {
				// FIXME canonical cart name
				chg.Name = strings.TrimSpace(cart.Name())
			}
			getFileChanges(cart, false).Merge(chg)
		}

		if d.mru.isRecordUpdate() {
			defer d.mru.reset()
			log.WithFields(log.Fields{
				"drive":  drive,
				"sector": d.mru.sector.Index(),
			}).Debug("PUT record")
		}
	}

	if d.mru.isNewSector() {
		sec, err := d.mru.createSector()
		if err != nil {
			return fmt.Errorf("error creating sector: %v", err)
		}
		cart.SetNextSector(sec)
		log.WithFields(log.Fields{
			"drive":  drive,
			"sector": sec.Index(),
		}).Debug("PUT sector complete")
	}

	cart.SetModified(true)

	return nil
}
