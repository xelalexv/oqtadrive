/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2023, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package daemon

import (
	"encoding/hex"
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"

	"codeberg.org/xelalexv/oqtadrive/pkg/zxnet"
)

// -
const (
	CmdNetIngress     = 1
	CmdNetEgress      = 2
	CmdNetIngressStat = 11
	CmdNetEgressStat  = 12
)

// -
var packetExpiration time.Duration = 30 * time.Second

var pktIngress uint32 = 1
var pktIngressErr uint32
var pktEgress uint32 = 1
var pktEgressErr uint32

// -
func networkNotify(d *Daemon) {
	err := d.SendFlagWait(FlgNet, 500*time.Millisecond)
	if err != nil {
		log.Debugf("error sending flag: %v", err)
	}
	log.WithField("ok", err == nil).Info("network notify")
}

// -
func (c *command) network(d *Daemon) error {

	switch c.arg(0) {

	case CmdNetIngress: // receiving a packet from the network hub
		return c.handleIngress(d)

	case CmdNetEgress: // sending a packet to the network hub
		return c.handleEgress(d)

	case CmdNetIngressStat:
		pktIngress += uint32(c.arg(1))
		pktIngressErr += uint32(c.arg(2))
		log.Debugf("INGRESS: %d of %d packets failed, %d%% error rate",
			pktIngressErr, pktIngress, 100*pktIngressErr/pktIngress)

	case CmdNetEgressStat:
		pktEgress += uint32(c.arg(1))
		pktEgressErr += uint32(c.arg(2))
		log.Debugf("EGRESS: %d of %d packets failed, %d%% error rate",
			pktEgressErr, pktEgress, 100*pktEgressErr/pktEgress)

	default:
		return fmt.Errorf("invalid network command: %d", c.arg(0))
	}

	return nil
}

// -
func (c *command) handleIngress(d *Daemon) error {

	cl := d.GetHubClient()
	if cl == nil {
		return nil
	}

	var msg *zxnet.Message
	var exp *zxnet.Packet // expiration point
	var pkt *zxnet.Packet
	var more bool
	var err error

	repeat := c.arg(1) == 1

	for {
		if repeat {
			log.Debug("repeating last packet")
			msg, more = cl.Repeat()
		} else {
			msg, more = cl.Next()
		}

		if msg == nil {
			log.Warn("no more packet to receive")
			d.conduit.send([]byte{0, 0}) // let adapter know
			return nil
		}

		if pkt, err = zxnet.NewPacketFromRaw(msg.Payload()); err != nil {
			return fmt.Errorf("received faulty packet: %v", err)
		}

		if exp == nil || !exp.IsParent(pkt) {
			if msg.Expired(packetExpiration) {
				exp = pkt // expiration point packet, i.e. start of expired strand
				repeat = false
				log.WithFields(log.Fields{
					"from":   pkt.Self(),
					"to":     pkt.IRIS(),
					"number": pkt.Number()}).Debug("start of expired strand")
				continue
			}
			break // packet to send found
		} else { // discard remainder of strand
			log.WithFields(log.Fields{
				"from":   pkt.Self(),
				"to":     pkt.IRIS(),
				"number": pkt.Number()}).Debug("discarding expired packet")
		}
	}

	toSend := d.conduit.fillBlockRaw(0, pkt.Raw())

	var b byte // `more` flag
	if more {
		b = 1
	}
	toSend += d.conduit.fillBlockRaw(toSend, []byte{b})

	log.WithFields(log.Fields{
		"direction": "ingress",
		"from":      pkt.Self(),
		"to":        pkt.IRIS(),
		"bytes":     toSend,
		"more":      more}).Debug("NET")

	if log.IsLevelEnabled(log.TraceLevel) {
		fmt.Printf("%s\n", hex.Dump(pkt.Raw()))
	}

	d.conduit.send([]byte{byte(toSend), byte(toSend >> 8)})
	if err = d.conduit.sendBlock(toSend); err != nil {
		return err
	}

	return nil
}

// -
func (c *command) handleEgress(d *Daemon) error {

	cl := d.GetHubClient()
	if cl == nil {
		return nil
	}

	length := int(c.arg(1)) + 256*int(c.arg(2))
	msg, err := zxnet.NewMessage(
		zxnet.Blank(length), zxnet.WithCommand(zxnet.CmdForward))
	if err != nil {
		return err
	}
	d.conduit.receive(msg.Payload())

	if pkt, err := zxnet.NewPacketFromRaw(msg.Payload()); err == nil {
		log.WithFields(log.Fields{
			"direction": "egress",
			"from":      pkt.Self(),
			"to":        pkt.IRIS(),
			"bytes":     length}).Debug("NET")
		if !cl.Send(msg) {
			log.Warn("dropping egress package")
		}
	}

	return nil
}
