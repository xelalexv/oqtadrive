/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2021, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package daemon

import (
	"fmt"

	log "github.com/sirupsen/logrus"

	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/types"
)

// -
type mru struct {
	sector types.Sector
	header types.Header
	record types.Record
}

// -
func (m *mru) reset() {
	log.Trace("MRU reset")
	m.sector = nil
	m.header = nil
	m.record = nil
}

// -
func (m *mru) setSector(s types.Sector) error {

	if m.header != nil {
		log.Warn("processing next sector while pending header present")
		m.header = nil
	}

	if m.record != nil {
		log.Warn("processing next sector while pending record present")
		m.record = nil
	}

	if s != nil {
		log.WithField("sector", s.Index()).Trace("MRU")
	} else {
		log.WithField("sector", "(nil)").Trace("MRU")
	}

	m.sector = s
	return nil
}

// -
func (m *mru) createSector() (types.Sector, error) {
	defer m.reset()
	return microdrive.NewSector(m.header, m.record)
}

// -
func (m *mru) setHeader(h types.Header) error {

	if m.header != nil {
		// During a CP/M format, several headers without following records may
		// be written at the start of the format procedure. We switched the
		// error we used to raise here to a logged warning and just discard the
		// previous header. This should be safe in general.
		log.Warn(
			"processing next header while pending header present, discarding")
	}

	if m.record != nil {
		return fmt.Errorf("processing next header while pending record present")
	}

	m.sector = nil

	if h != nil {
		log.WithField("header", h.Index()).Trace("MRU")
	} else {
		log.WithField("header", "(nil)").Trace("MRU")
	}

	m.header = h
	return nil
}

// -
func (m *mru) setRecord(r types.Record) (*types.FileChange, error) {

	var change *types.FileChange

	if m.header == nil {
		if m.sector == nil {
			return nil,
				fmt.Errorf("processing next record without sector or header")
		}
		change = r.Replaces(m.sector.Record(), m.sector.Index())
		m.sector.SetRecord(r)
	}

	if m.record != nil {
		return nil,
			fmt.Errorf("processing next record while pending record present")
	}

	if r != nil {
		log.WithField("record", r.Index()).Trace("MRU")
	} else {
		log.WithField("record", "(nil)").Trace("MRU")
	}

	m.record = r
	return change, nil
}

// -
func (m *mru) isNewSector() bool {
	return m.sector == nil && m.header != nil && m.record != nil
}

// -
func (m *mru) isRecordUpdate() bool {
	return m.sector != nil && m.header == nil && m.record != nil
}
