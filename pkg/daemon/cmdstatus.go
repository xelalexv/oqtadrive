/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2021, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package daemon

import (
	"fmt"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"

	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/base"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/if1"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/ql"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/types"
	"codeberg.org/xelalexv/oqtadrive/pkg/util"
)

//
const (
	flagLoaded   = 1
	flagFormated = 2
	flagReadonly = 4
)

//
func (c *command) status(d *Daemon) error {

	var cart *base.Cartridge
	var state byte = 0x80

	drive, err := c.drive()
	msg := ""

	if err == nil {
		cart = d.getCartridge(drive)
		state = 0x00

		if cart == nil {
			msg = "empty"
		} else {
			if cart.IsFormatted() {
				msg = "formatted"
				state = flagLoaded | flagFormated
			} else {
				msg = "blank"
				state = flagLoaded
			}

			if cart.IsWriteProtected() {
				msg += ", write protected"
				state |= flagReadonly
			}
		}
	}

	action := "stopped"
	if c.arg(1) == 1 {
		action = "started"
	}

	d.mru.reset()

	log.WithFields(log.Fields{
		"drive": drive, "action": action, "state": msg}).Info("STATUS")

	if c.arg(1) == 1 { // drive started, send cartridge state to adapter
		d.conduit.send([]byte{state})
		if cart != nil {
			if cart.IsLocked() {
				log.WithField("drive", drive).Warn(
					"cartridge locked, assuming already started")
				return nil
			}
			invokeDriveChangeListeners(cart, NewDriveChange(drive, DriveStarted))
			if !LockCartridge(cart, 5*time.Millisecond) {
				return fmt.Errorf("could not lock cartridge in drive %d", drive)
			}
			resetFileChanges(cart)
		}

	} else if cart != nil { // drive stopped
		if cart.IsFormatting() {
			if cart.Client() == types.QL {

				log.Debug("reclaiming sectors")
				sec := []int{
					ql.FaultInjectionSector - 1,
					ql.FaultInjectionSector,
					ql.FaultInjectionSector + 1}
				for ix := ql.TopmostGoodSector; ix < ql.SectorCount; ix++ {
					sec = append(sec, ix)
				}
				if err := cart.FS().ReclaimSectors(sec); err != nil {
					return err
				}

				// The QL does not stop the drive during a format, so if the
				// cartridge is flagged as formatting and the drive is stopping,
				// it's time to send the Formatted event. For Interface 1, the
				// initial format round however is followed by a verification
				// round and a final write round. Between first and second round
				// the drive stops and starts again, so we have two separate
				// drive activations. However we need to defer the Formatted
				// event until the end of the last round, i.e. second activation.
				// For that, see pkg/microdrive/if1/record.go.
				resetFileChanges(cart).Merge(types.NewFileChange(
					// FIXME canonical cart name
					strings.TrimSpace(cart.Name()), types.OpFormatted))
			}
			cart.SetFormatting(false)
		}

		d.autoSave = d.autoSave || !cart.IsAutoSaved()

		invokeDriveChangeListeners(cart, NewDriveChange(drive, DriveStopped))

		fcs := getFileChanges(cart, true)
		fcl := getFileChangeListeners(cart)

		if cart.Client() == types.IF1 && fcs.Contains(types.OpFormatted) {
			log.Debug("reclaiming sector 254")
			if err := cart.FS().ReclaimSectors([]int{if1.TopSector}); err != nil {
				return err
			}
		}

		cart.Unlock()

		if len(fcs) > 0 {
			log.Debug("invoking file change listeners (asynchronous)")
			go invokeFileChangeListeners(drive, cart, fcl, fcs)
		} else {
			log.Debug("no file changes")
		}
	}

	return err
}

//
func invokeDriveChangeListeners(cart *base.Cartridge, chg *DriveChange) {

	log.Debug("invoking drive change listeners (synchronous)")

	dcl := getDriveChangeListeners(cart)
	for id, lst := range dcl {
		log.WithField("id", id).Debug("drive change listener")
		if err := lst(cart, chg); err != nil {
			log.Errorf("error running drive change listener %s: %v", id, err)
		}
	}
}

//
func invokeFileChangeListeners(drive int, cart *base.Cartridge,
	fcl util.ListenerMap[base.FileChangeListener], fcs types.FileChangeSet) {

	for id, lst := range fcl {
		// lock separately for each listener, rather than for complete
		// invocation round; gives others the chance to lock as well
		if LockCartridge(cart, time.Second) {
			log.WithField("id", id).Debug("file change listener")
			for _, fc := range fcs {
				if err := lst(drive, cart, fc); err != nil {
					log.Errorf(
						"error running file change listener %s: %v",
						id, err)
				}
			}
			cart.Unlock()
		} else {
			log.WithField("id", id).Warn(
				"could not lock cartridge, skipping file change listener")
		}
	}
}
