/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2021, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package daemon

import (
	"bytes"
	"fmt"

	log "github.com/sirupsen/logrus"
)

const (
	// command codes & parameters
	CmdHello     = 'h' // hello (send/receive to/from IF1/QL)
	CmdVersion   = 'v' // protocol version (send/receive to/from IF1/QL)
	CmdPing      = 'P' // ping/pong (send/receive to/from IF1/QL)
	CmdStatus    = 's' // get drive state (send to IF1/QL)
	CmdGet       = 'g' // get sector (send to IF1/QL)
	CmdPut       = 'p' // put sector (receive from IF1/QL)
	CmdVerify    = 'y' // verify sector bytes sent for previous get
	CmdTimeStart = 't' // start stop watch
	CmdTimeEnd   = 'q' // stop stop watch
	CmdMap       = 'm' // h/w drive mapping (receive from IF1/QL)
	CmdDebug     = 'd' // debug message (receive from IF1/QL)
	CmdResync    = 'r' // resync with adapter (send to IF1/QL)
	CmdConfig    = 'c' // remote config of adapter (send to IF1/QL)
	CmdNet       = 'n' // network handling (send/receive to/from IF1/QL)
	CmdQuiet     = 'Q' // start of Microdrive activity (receive from IF1/QL)

	// flags - send to IF1/QL
	FlgRing = 'R' // ring the adapter; used to shorten current ping period
	FlgNet  = 'N' // notify adapter that network data is available

	// config items
	CmdConfigItemRumble = "rumble" // rumble strength config item
	CmdConfigRumble     = 'r'      // rumble strength item code
	CmdConfigRumbleMin  = 0        // minimum rumble strength
	CmdConfigRumbleMax  = 255      // maximum rumble strength

	// annotations
	AnnoFixCheckSums         = "needsCheckSumFix"
	AnnoFileChanges          = "fileChanges"
	AnnoFileChangeListeners  = "fileChangeListeners"
	AnnoFilesSnapshot        = "filesSnapshot"
	AnnoDriveChangeListeners = "driveChangeListeners"
	AnnoHeadersOnly          = "headersOnly"

	// masks
	MaskIF1 = 1
	MaskQL  = 2
)

var ping = []byte("Ping")
var pong = []byte("Pong")

// -
func newCommand(data []byte) *command {
	return &command{data: data}
}

// -
type command struct {
	data []byte
}

// -
func (c *command) dispatch(d *Daemon) error {

	switch c.cmd() {

	case CmdHello:
		d.synced = false
		return nil

	case CmdPing:
		if bytes.Equal(c.data, ping) {
			log.Debugf("ping from %s", d.conduit.client)
			if err := d.conduit.send(pong); err != nil {
				return err
			}
			d.processControl()
			if d.autoSave {
				d.flushAutoSave()
				d.autoSave = false
			}
		}
		return nil

	case CmdQuiet:
		log.Trace("acknowledging quiet command")
		return d.conduit.sendByte(CmdQuiet)

	case CmdStatus:
		return c.status(d)

	case CmdGet:
		return c.get(d)

	case CmdPut:
		return c.put(d)

	case CmdNet:
		return c.network(d)

	case CmdDebug:
		return c.debug(d)

	case CmdTimeStart:
		return c.timer(true, d)

	case CmdTimeEnd:
		return c.timer(false, d)

	case CmdMap:
		return c.driveMap(d)

		// case CMD_VERIFY: FIXME
	}

	return fmt.Errorf("unknown command: %v", c.String())
}

// -
func (c *command) cmd() byte {
	return c.data[0]
}

// -
func (c *command) arg(ix int) byte {
	if 0 <= ix && ix < len(c.data)-1 {
		return c.data[ix+1]
	}
	return 0
}

// drive returns the 1-based drive number, or an error if the drive number
// contained in this command is not within 1 through 8
func (c *command) drive() (int, error) {
	drive := c.arg(0)
	if drive < 1 || drive > 8 {
		return -1, fmt.Errorf("illegal drive number: %d", drive)
	}
	return int(drive), nil
}

// -
func (c *command) String() string {
	return fmt.Sprintf("%s - %v", string(c.data), c.data)
}
