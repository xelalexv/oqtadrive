/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2022, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package daemon

import (
	log "github.com/sirupsen/logrus"

	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/base"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/fs"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/types"
	"codeberg.org/xelalexv/oqtadrive/pkg/util"
)

//
func resetFileChanges(cart *base.Cartridge) types.FileChangeSet {

	if cart.Client() == types.QL {
		if _, files, err := cart.FS().Ls(); err != nil {
			log.Warnf("error creating files snapshot: %v", err)
		} else {
			logFilesSnapshot("annotating", files)
			cart.Annotate(AnnoFilesSnapshot, files)
		}
	}

	ret := make(types.FileChangeSet)
	cart.Annotate(AnnoFileChanges, ret)
	return ret
}

//
func getFileChanges(cart *base.Cartridge, final bool) types.FileChangeSet {

	var ret types.FileChangeSet

	if a := cart.GetAnnotation(AnnoFileChanges); a != nil {
		if fcs, ok := a.Value().(types.FileChangeSet); ok {
			ret = fcs
		}
	}

	if ret == nil {
		ret = make(types.FileChangeSet)
	}

	// For QL, collecting file changes during a drive activation is not useful,
	// because there's no guarantee in which order file, directory, and sector
	// changes are written. The correct file changes can only be determined at
	// the end of the drive activation.
	if final && cart.Client() == types.QL {
		if a := cart.GetAnnotation(AnnoFilesSnapshot); a != nil {
			if snap, ok := a.Value().([]*fs.FileInfo); ok {
				logFilesSnapshot("retrieved", snap)
				if _, now, err := cart.FS().Ls(); err == nil {
					addFileChanges(ret, snap, now)
				} else {
					log.Warnf("error getting files snapshot: %v", err)
				}
			}
		}
		cart.RemoveAnnotation(AnnoFilesSnapshot)
	}

	if log.IsLevelEnabled(log.TraceLevel) {
		log.Tracef("found file changes: %s", ret.List())
	}

	return ret
}

//
func addFileChanges(chg types.FileChangeSet, then, now []*fs.FileInfo) {

	ref := make(map[string]*fs.FileInfo)
	for _, f := range then {
		ref[f.Name()] = f
	}

	for _, f := range now {
		n := f.Name()
		if r, ok := ref[n]; ok {
			// FIXME does not catch modifications that maintain same size
			if f.Size() != r.Size() {
				chg.Merge(types.NewFileChange(n, types.OpModified))
			}
			delete(ref, n)
		} else {
			chg.Merge(types.NewFileChange(n, types.OpCreated))
		}
	}

	for _, f := range ref {
		chg.Merge(types.NewFileChange(f.Name(), types.OpDeleted))
	}
}

//
func logFilesSnapshot(action string, files []*fs.FileInfo) {
	if log.IsLevelEnabled(log.TraceLevel) {
		names := make([]string, len(files))
		for ix, f := range files {
			names[ix] = f.Name()
		}
		log.Debugf("%s files snapshot: %v", action, names)
	}
}

//
func addFileChangeListener(cart *base.Cartridge, id string,
	l base.FileChangeListener) {
	getFileChangeListeners(cart)[id] = l
}

//
func removeFileChangeListener(cart *base.Cartridge, id string) {
	delete(getFileChangeListeners(cart), id)
}

//
func getFileChangeListeners(
	cart *base.Cartridge) util.ListenerMap[base.FileChangeListener] {
	return getListeners[base.FileChangeListener](cart, AnnoFileChangeListeners)
}

//
func addDriveChangeListener(cart *base.Cartridge, id string,
	l DriveChangeListener) {
	getDriveChangeListeners(cart)[id] = l
}

//
func removeDriveChangeListener(cart *base.Cartridge, id string) {
	delete(getDriveChangeListeners(cart), id)
}

//
func getDriveChangeListeners(
	cart *base.Cartridge) util.ListenerMap[DriveChangeListener] {
	return getListeners[DriveChangeListener](cart, AnnoDriveChangeListeners)
}

//
func getListeners[L DriveChangeListener | base.FileChangeListener](
	cart *base.Cartridge, anno string) util.ListenerMap[L] {

	var ret util.ListenerMap[L]

	if cart != nil {
		ok := false
		if a := cart.GetAnnotation(anno); a != nil {
			ret, ok = a.Value().(util.ListenerMap[L])
		}
		if !ok {
			ret = make(util.ListenerMap[L])
			cart.Annotate(anno, ret)
		}
	}

	return ret
}
