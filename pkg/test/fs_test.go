/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2023, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package test

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
	"testing"

	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/base"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/format"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/fs"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/if1"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/ql"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/types"
	"codeberg.org/xelalexv/oqtadrive/pkg/run"
)

// -
func init() {
	run.DieOnError(nil) // trigger log setup
}

// -
func TestAddFileIF1(t *testing.T) {
	addFileTest(t, types.IF1, 13*1024+371, "")
}

// -
func TestAddFileIF1CPM(t *testing.T) {
	addFileTest(t, types.IF1, 34*1024+256, if1.SpecialsCPM)
	addFileTest(t, types.IF1, 34*1024+300, if1.SpecialsCPM)
}

// -
func TestAddFileQL(t *testing.T) {
	addFileTest(t, types.QL, 13*1024+371, "")
}

// -
func addFileTest(t *testing.T, cl types.Client, length int, specials string) {

	th := NewTestHelper(t)

	data := generateData(length, 7)
	cart := newCartridge(th, "addfile", cl, specials)
	fsys := cart.FS()
	th.AssertNotNil(fsys)

	addFile(th, fsys, "data", "code", data)
	fdata := readFile(th, fsys, "data")
	if specials == if1.SpecialsCPM {
		fdata = fdata[:length]
	}
	th.AssertEqualSlices(data, fdata)

	name := fmt.Sprintf("fs/t_edit.%s", cl.DefaultFormat())
	writeCartridge(th, cart, name)
	cart = readCartridge(th, name)
	fsys = cart.FS()
	th.AssertNotNil(fsys)
	fdata = readFile(th, fsys, "data")
	if specials == if1.SpecialsCPM {
		fdata = fdata[:length]
	}
	th.AssertEqualSlices(data, fdata)
}

// -
func TestOverwriteFileIF1(t *testing.T) {
	overwriteFileTest(t, types.IF1, "")
}

// -
func TestOverwriteFileIF1CPM(t *testing.T) {
	overwriteFileTest(t, types.IF1, if1.SpecialsCPM)
}

// -
func TestOverwriteFileQL(t *testing.T) {
	overwriteFileTest(t, types.QL, "")
}

// -
func overwriteFileTest(t *testing.T, cl types.Client, specials string) {

	th := NewTestHelper(t)

	cart := newCartridge(th, "overwrite", cl, specials)
	fsys := cart.FS()
	th.AssertNotNil(fsys)
	name := fmt.Sprintf("fs/t_over_%%d.%s", cl.DefaultFormat())

	// need to adjust expected record counts two even numbers for CP/M
	cpm := specials == if1.SpecialsCPM

	data := generateData(5*1024+256, 3) // overwrite non existing file
	expRecs := 11
	if cpm {
		expRecs = 12
	}
	overwriteFile(th, fsys, "data", "code", data, expRecs)
	writeCartridge(th, cart, fmt.Sprintf(name, 1))

	data = generateData(5*1024+256, 5) // exact same size
	overwriteFile(th, fsys, "data", "code", data, expRecs)
	writeCartridge(th, cart, fmt.Sprintf(name, 2))

	data = generateData(5*1024+128, 4) // smaller with same records count
	overwriteFile(th, fsys, "data", "code", data, expRecs)
	writeCartridge(th, cart, fmt.Sprintf(name, 3))

	data = generateData(4*1024+256, 3) // smaller with less records
	expRecs = 9
	if cpm {
		expRecs = 10
	}
	overwriteFile(th, fsys, "data", "code", data, expRecs)
	writeCartridge(th, cart, fmt.Sprintf(name, 4))

	data = generateData(4*1024+384, 7) // larger with same records count
	overwriteFile(th, fsys, "data", "code", data, expRecs)
	writeCartridge(th, cart, fmt.Sprintf(name, 5))

	data = generateData(6*1024+256, 6) // larger with more records
	expRecs = 13
	if cpm {
		expRecs = 14
	}
	overwriteFile(th, fsys, "data", "code", data, expRecs)
	writeCartridge(th, cart, fmt.Sprintf(name, 6))
}

// -
func TestDeleteFileIF1(t *testing.T) {
	deleteFileTest(t, types.IF1, "")
}

// -
func TestDeleteFileIF1CPM(t *testing.T) {
	deleteFileTest(t, types.IF1, if1.SpecialsCPM)
}

// -
func TestDeleteFileQL(t *testing.T) {
	deleteFileTest(t, types.QL, "")
}

// -
func deleteFileTest(t *testing.T, cl types.Client, specials string) {

	th := NewTestHelper(t)

	data := generateData(9*1024+256, 5)
	cart := newCartridge(th, "delfile", cl, specials)
	fsys := cart.FS()
	th.AssertNotNil(fsys)

	stats, _, err := fsys.Ls()
	th.AssertNoError(err)
	free := stats.Free()

	name := "data"
	addFile(th, fsys, name, "code", data)
	th.AssertEqualSlices(data, readFile(th, fsys, name))
	stats, lst, err := fsys.Ls()
	th.AssertNoError(err)
	th.AssertTrue(free > stats.Free())
	th.AssertEqual(lst[0].Name(), name)

	deleteFile(th, fsys, name)
	stats, lst, err = fsys.Ls()
	th.AssertNoError(err)
	th.AssertEqual(free, stats.Free())
	th.AssertEqual(len(lst), 0)
}

// -
func TestFillCartridgeIF1(t *testing.T) {
	fillCartridgeTest(t, types.IF1, "")
}

// -
func TestFillCartridgeIF1CPM(t *testing.T) {
	fillCartridgeTest(t, types.IF1, if1.SpecialsCPM)
}

// -
func TestFillCartridgeQL(t *testing.T) {
	fillCartridgeTest(t, types.QL, "")
}

// -
func fillCartridgeTest(t *testing.T, cl types.Client, specials string) {

	th := NewTestHelper(t)

	cart := newCartridge(th, "fillcart", cl, specials)
	fsys := cart.FS()
	th.AssertNotNil(fsys)

	sets := 10
	if cl == types.IF1 && specials == if1.SpecialsCPM {
		sets = 8
	}

	data := make([][]byte, sets)
	for n := 0; n < len(data); n++ {
		name := fmt.Sprintf("data%d", n)
		data[n] = generateData(10*1024+n*128, n+3)
		addFile(th, fsys, name, "code", data[n])
		th.AssertEqualSlices(data[n], readFile(th, fsys, name))
	}

	name := fmt.Sprintf("fs/t_fill.%s", cl.DefaultFormat())
	writeCartridge(th, cart, name)
	cart = readCartridge(th, name)
	fsys = cart.FS()
	th.AssertNotNil(fsys)
	for n := 0; n < len(data); n++ {
		name := fmt.Sprintf("data%d", n)
		th.AssertEqualSlices(data[n], readFile(th, fsys, name))
	}

	big := generateData(19*1024, 5)
	f, err := fsys.Open("big", fs.WRITE, fsys.ParseFileType("code"))
	th.AssertNoError(err)
	_, err = f.Write(big)
	th.AssertError(err, "no space left")
}

// -
func TestReorgIF1(t *testing.T) {

	th := NewTestHelper(t)

	cart := newCartridge(th, "reorg", types.IF1, "")
	fsys := cart.FS()
	th.AssertNotNil(fsys)

	// check file consistency after reorg
	sData := 2 * 1024
	data := generateData(sData, 3)
	addFile(th, fsys, "data", "code", data)
	th.AssertNoError(cart.Reorganize())

	fdata := readFile(th, fsys, "data")
	th.AssertEqualSlices(data, fdata)

	// try reorg with zero length file
	addFile(th, fsys, "print", "print", []byte{})
	th.AssertNoError(cart.Reorganize())

	_, info, err := fsys.Ls()
	th.AssertNil(err)

	var fData, fPrint bool
	for _, f := range info {
		switch f.Name() {
		case "data":
			// code files have file header
			th.AssertEqual(sData+if1.FileHeaderLength, f.Size())
			fData = true
		case "print":
			// print files have no file header
			th.AssertEqual(0, f.Size())
			fPrint = true
		}
	}
	th.AssertTrue(fData)
	th.AssertTrue(fPrint)

	name := fmt.Sprintf("fs/t_reorg.%s", types.IF1.DefaultFormat())
	writeCartridge(th, cart, name)
}

// -
func TestReorgQL(t *testing.T) {

	th := NewTestHelper(t)

	cart := newCartridge(th, "reorg", types.QL, "")
	fsys := cart.FS()
	th.AssertNotNil(fsys)

	// check file consistency after reorg
	sData := 2 * 1024
	data := generateData(sData, 3)
	addFile(th, fsys, "data", "code", data)
	th.AssertNoError(cart.Reorganize())

	fsys = cart.FS() // fs invalid after reorg
	th.AssertNotNil(fsys)
	fdata := readFile(th, fsys, "data")
	th.AssertEqualSlices(data, fdata)

	// try reorg with zero length file
	addFile(th, fsys, "print", "ascii", []byte{})
	th.AssertNoError(cart.Reorganize())

	fsys = cart.FS() // fs invalid after reorg
	th.AssertNotNil(fsys)
	_, info, err := fsys.Ls()
	th.AssertNil(err)

	var fData, fPrint bool
	for _, f := range info {
		switch f.Name() {
		case "data":
			th.AssertEqual(f.Size(), sData+ql.FileHeaderLength)
			fData = true
		case "print":
			th.AssertEqual(f.Size(), ql.FileHeaderLength)
			fPrint = true
		}
	}
	th.AssertTrue(fData)
	th.AssertTrue(fPrint)

	name := fmt.Sprintf("fs/t_reorg.%s", types.QL.DefaultFormat())
	writeCartridge(th, cart, name)
}

// -
func newCartridge(th *TestHelper, name string, cl types.Client,
	specials string) *base.Cartridge {

	var cart *base.Cartridge
	if cl == types.IF1 {
		if specials == if1.SpecialsCPM {
			cart = if1.NewCartridgeCPM()
		} else {
			cart = if1.NewCartridge()
		}
	} else {
		cart = ql.NewCartridge()
	}
	th.AssertNoError(cart.Format(name))

	return cart
}

// -
func readCartridge(th *TestHelper, name string) *base.Cartridge {

	in := GetFixture(name)
	f, err := os.Open(in)
	th.AssertNoError(err)
	defer f.Close()

	_, typ, comp := format.SplitNameTypeCompressor(in)

	rd, err := format.NewCartReader(ioutil.NopCloser(bufio.NewReader(f)), comp)
	th.AssertNoError(err)

	if typ == "" {
		typ = rd.Type()
	}

	form, err := format.NewFormat(typ, rd.Prefix())
	th.AssertNoError(err)

	cart, err := form.Read(rd, false, false, nil)
	th.AssertNoError(err)

	return cart
}

// -
func writeCartridge(th *TestHelper, cart *base.Cartridge, name string) {

	out := GetFixture(name)
	f, err := os.Create(out)
	th.AssertNoError(err)
	defer f.Close()

	_, typ, _ := format.SplitNameTypeCompressor(out)
	th.AssertNotEqual(typ, "")

	form, err := format.NewFormat(typ, nil)
	th.AssertNoError(err)

	th.AssertNoError(form.Write(cart, f, false, nil))
}

// -
func addFile(th *TestHelper, fsys fs.FileSystem, name, typ string, data []byte) {
	f, err := fsys.Open(name, fs.WRITE, fsys.ParseFileType(typ))
	th.AssertNoError(err)
	th.AssertNotNil(f)
	wr, err := f.Write(data)
	th.AssertNoError(err)
	th.AssertNoError(f.Close())
	th.AssertEqual(len(data), wr)
}

// -
func overwriteFile(th *TestHelper, fsys fs.FileSystem, name, typ string,
	data []byte, expRecCount int) {

	f, err := fsys.Open(name, fs.OVERWRITE, fsys.ParseFileType(typ))
	th.AssertNoError(err)
	th.AssertNotNil(f)
	wr, err := f.Write(data)
	th.AssertNoError(err)
	th.AssertNoError(f.Close())
	th.AssertEqual(len(data), wr)
	th.AssertEqual(expRecCount, f.RecordCount())
	th.AssertEqualSlices(data, readFile(th, fsys, name))
}

// -
func readFile(th *TestHelper, fsys fs.FileSystem, name string) []byte {
	f, err := fsys.Open(name, fs.READ, nil)
	th.AssertNoError(err)
	th.AssertNotNil(f)
	data, err := f.Bytes()
	th.AssertNoError(err)
	th.AssertNoError(f.Close())
	return data
}

// -
func deleteFile(th *TestHelper, fsys fs.FileSystem, name string) {
	del, err := fsys.Delete(name, false)
	th.AssertNoError(err)
	th.AssertTrue(del)
}

// -
func generateData(size, step int) []byte {
	data := make([]byte, size)
	for ix := range data {
		data[ix] = byte(ix * step)
	}
	return data
}
