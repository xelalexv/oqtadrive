/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2023, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package test

import (
	"testing"

	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/fs"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/if1"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/if1/cpm"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/types"
)

// -
func TestReadWriteCPM(t *testing.T) {

	th := NewTestHelper(t)

	name := "cart/mdvt-cpm.mdv"
	cart := readCartridge(th, name)
	th.AssertNotNil(cart)
	th.AssertEqual(192, cart.SectorCount())

	name = "fs/t_mdvt-cpm.mdr"
	writeCartridge(th, cart, name)
	cart = readCartridge(th, name)
	th.AssertNotNil(cart)
	th.AssertEqual(192, cart.SectorCount())
}

// -
func TestRecordAlignmentCPM(t *testing.T) {

	th := NewTestHelper(t)

	cart := newCartridge(th, "cpmfiles", types.IF1, if1.SpecialsCPM)
	fsys := cart.FS()
	th.AssertNotNil(fsys)

	// 5 CP/M records, 2 file records
	data := make([]byte, 5*cpm.CPMRecordLength)
	cpmWriteFile(th, fsys, "data", "code", data, 2)

	// --- crossing CP/M record border

	// 8 CP/M records - 1 byte, still 2 file records
	data = make([]byte, 8*cpm.CPMRecordLength-1)
	cpmWriteFile(th, fsys, "data", "code", data, 2)
	data = append(data, 0) // 8 CP/M records, still 2 file records
	cpmWriteFile(th, fsys, "data", "code", data, 2)
	data = append(data, 0) // 8 CP/M records + 1 byte, now 4 file records
	cpmWriteFile(th, fsys, "data", "code", data, 4)

	// --- crossing directory extend border

	// 128 CP/M records - 1 byte = 1 extent - 1 byte, 32 file records
	data = make([]byte, cpm.CPMRecordsPerExtent*cpm.CPMRecordLength-1)
	cpmWriteFile(th, fsys, "data", "code", data, 32)
	data = append(data, 0) // 128 CP/M records = 1 extent, still 32 file records
	cpmWriteFile(th, fsys, "data", "code", data, 32)
	data = append(data, 0) // 128 CP/M records + 1 byte = 2 extents, 34 file records
	cpmWriteFile(th, fsys, "data", "code", data, 34)
}

// -
func cpmWriteFile(th *TestHelper, fsys fs.FileSystem, name, typ string,
	data []byte, expRecCount int) {

	f, err := fsys.Open(name, fs.OVERWRITE, fsys.ParseFileType(typ))
	th.AssertNoError(err)
	th.AssertNotNil(f)
	wr, err := f.Write(data)
	th.AssertNoError(err)
	th.AssertNoError(f.Close())
	th.AssertEqual(len(data), wr)
	th.AssertEqual(expRecCount, f.RecordCount())
	th.AssertEqualSlices(data, readFile(th, fsys, name)[:len(data)])
}
