/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2023, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package test

import (
	"codeberg.org/xelalexv/oqtadrive/pkg/run"

	testable "github.com/larschri/testable-example-output"
)

// -
func init() {
	run.DieOnError(nil) // trigger log setup
}

// tests basic reading in of all supported formats, plain and in all supported
// archive formats

// -
func ExampleMDR() {
	// we need this to fix problems with trailing white space in the output,
	// which cannot be represented in the Output: sections
	done := testable.FixExampleOutput()
	defer done()
	runList("cart/mdr.mdr")
	// Output:
	// fillcart
	//
	// data0              10249  BASIC  1.0x
	// data1              10372  BASIC  1.0x
	// data2              10495  BASIC  1.0x
	// data3              10618  BASIC  1.0x
	// data4              10741  BASIC  1.0x
	// data5              10864  BASIC  1.0x
	// data6              10987  BASIC  1.0x
	// data7              11110  BASIC  1.0x
	// data8              11233  BASIC  1.0x
	// data9              11356  BASIC  1.0x
	//
	// 216 of 254 sectors used (19kb free)
}

// -
func ExampleMDRCPM() {
	done := testable.FixExampleOutput()
	defer done()
	runList("cart/mdr-cpm.zip")
	// Output:
	// CP/M f5c7
	//
	// User 0:
	// DDT.COM             4864  code   1.0x
	// DUMP.COM             512  code   1.0x
	// ED.COM              6656  code   1.0x
	// PIP.COM             7424  code   1.0x
	// STAT.COM            5248  code   1.0x
	// SUBMIT.COM          1280  code   1.0x
	// XSUB.COM             768  code   1.0x
	//
	// 62 of 192 sectors used (65kb free)
}

// -
func ExampleMDRGZ() {
	done := testable.FixExampleOutput()
	defer done()
	runList("cart/mdr.gz")
	// Output:
	// fillcart
	//
	// data0              10249  BASIC  1.0x
	// data1              10372  BASIC  1.0x
	// data2              10495  BASIC  1.0x
	// data3              10618  BASIC  1.0x
	// data4              10741  BASIC  1.0x
	// data5              10864  BASIC  1.0x
	// data6              10987  BASIC  1.0x
	// data7              11110  BASIC  1.0x
	// data8              11233  BASIC  1.0x
	// data9              11356  BASIC  1.0x
	//
	// 216 of 254 sectors used (19kb free)
}

// -
func ExampleMDRZip() {
	done := testable.FixExampleOutput()
	defer done()
	runList("cart/mdr.zip")
	// Output:
	// fillcart
	//
	// data0              10249  BASIC  1.0x
	// data1              10372  BASIC  1.0x
	// data2              10495  BASIC  1.0x
	// data3              10618  BASIC  1.0x
	// data4              10741  BASIC  1.0x
	// data5              10864  BASIC  1.0x
	// data6              10987  BASIC  1.0x
	// data7              11110  BASIC  1.0x
	// data8              11233  BASIC  1.0x
	// data9              11356  BASIC  1.0x
	//
	// 216 of 254 sectors used (19kb free)
}

// -
func ExampleMDR7z() {
	done := testable.FixExampleOutput()
	defer done()
	runList("cart/mdr.7z")
	// Output:
	// fillcart
	//
	// data0              10249  BASIC  1.0x
	// data1              10372  BASIC  1.0x
	// data2              10495  BASIC  1.0x
	// data3              10618  BASIC  1.0x
	// data4              10741  BASIC  1.0x
	// data5              10864  BASIC  1.0x
	// data6              10987  BASIC  1.0x
	// data7              11110  BASIC  1.0x
	// data8              11233  BASIC  1.0x
	// data9              11356  BASIC  1.0x
	//
	// 216 of 254 sectors used (19kb free)
}

// -
func ExampleZ80() {
	done := testable.FixExampleOutput()
	defer done()
	runList("cart/z80.z80")
	// Output:
	// Z80onMDR
	//
	// -oqtadrive           405  BASIC  1.0x
	// 0                   2885  code   1.0x
	// M                  18664  code   1.0x
	// run                  259  BASIC  1.0x
	//
	// 45 of 254 sectors used (104kb free)
}

// -
func ExampleZ80GZ() {
	done := testable.FixExampleOutput()
	defer done()
	runList("cart/z80.gz")
	// Output:
	// Z80onMDR
	//
	// -oqtadrive           405  BASIC  1.0x
	// 0                   2885  code   1.0x
	// M                  18664  code   1.0x
	// run                  259  BASIC  1.0x
	//
	// 45 of 254 sectors used (104kb free)
}

// -
func ExampleZ80Zip() {
	done := testable.FixExampleOutput()
	defer done()
	runList("cart/z80.zip")
	// Output:
	// Z80onMDR
	//
	// -oqtadrive           405  BASIC  1.0x
	// 0                   2885  code   1.0x
	// M                  18664  code   1.0x
	// run                  259  BASIC  1.0x
	//
	// 45 of 254 sectors used (104kb free)
}

// -
func ExampleZ807z() {
	done := testable.FixExampleOutput()
	defer done()
	runList("cart/z80.7z")
	// Output:
	// Z80onMDR
	//
	// -oqtadrive           405  BASIC  1.0x
	// 0                   2885  code   1.0x
	// M                  18664  code   1.0x
	// run                  259  BASIC  1.0x
	//
	// 45 of 254 sectors used (104kb free)
}

// -
func ExampleSNA() {
	done := testable.FixExampleOutput()
	defer done()
	runList("cart/sna.sna")
	// Output:
	// Z80onMDR
	//
	// -oqtadrive           405  BASIC  1.0x
	// 0                   1291  code   1.0x
	// M                  13369  code   1.0x
	// run                  259  BASIC  1.0x
	//
	// 32 of 254 sectors used (111kb free)
}

// -
func ExampleSNAGZ() {
	done := testable.FixExampleOutput()
	defer done()
	runList("cart/sna.gz")
	// Output:
	// Z80onMDR
	//
	// -oqtadrive           405  BASIC  1.0x
	// 0                   1291  code   1.0x
	// M                  13369  code   1.0x
	// run                  259  BASIC  1.0x
	//
	// 32 of 254 sectors used (111kb free)
}

// -
func ExampleSNAZip() {
	done := testable.FixExampleOutput()
	defer done()
	runList("cart/sna.zip")
	// Output:
	// Z80onMDR
	//
	// -oqtadrive           405  BASIC  1.0x
	// 0                   1291  code   1.0x
	// M                  13369  code   1.0x
	// run                  259  BASIC  1.0x
	//
	// 32 of 254 sectors used (111kb free)
}

// -
func ExampleSNA7z() {
	done := testable.FixExampleOutput()
	defer done()
	runList("cart/sna.7z")
	// Output:
	// Z80onMDR
	//
	// -oqtadrive           405  BASIC  1.0x
	// 0                   1291  code   1.0x
	// M                  13369  code   1.0x
	// run                  259  BASIC  1.0x
	//
	// 32 of 254 sectors used (111kb free)
}

// -
func ExampleMDV() {
	done := testable.FixExampleOutput()
	defer done()
	runList("cart/mdv.mdv")
	// Output:
	// fillcart
	//
	// data0              10304  code   1.0x
	// data1              10427  code   1.0x
	// data2              10550  code   1.0x
	// data3              10673  code   1.0x
	// data4              10796  code   1.0x
	// data5              10919  code   1.0x
	// data6              11042  code   1.0x
	// data7              11165  code   1.0x
	// data8              11288  code   1.0x
	// data9              11411  code   1.0x
	//
	// 221 of 255 sectors used (17kb free)
}

// -
func ExampleMDVGZ() {
	done := testable.FixExampleOutput()
	defer done()
	runList("cart/mdv.gz")
	// Output:
	// fillcart
	//
	// data0              10304  code   1.0x
	// data1              10427  code   1.0x
	// data2              10550  code   1.0x
	// data3              10673  code   1.0x
	// data4              10796  code   1.0x
	// data5              10919  code   1.0x
	// data6              11042  code   1.0x
	// data7              11165  code   1.0x
	// data8              11288  code   1.0x
	// data9              11411  code   1.0x
	//
	// 221 of 255 sectors used (17kb free)
}

// -
func ExampleMDVZip() {
	done := testable.FixExampleOutput()
	defer done()
	runList("cart/mdv.zip")
	// Output:
	// fillcart
	//
	// data0              10304  code   1.0x
	// data1              10427  code   1.0x
	// data2              10550  code   1.0x
	// data3              10673  code   1.0x
	// data4              10796  code   1.0x
	// data5              10919  code   1.0x
	// data6              11042  code   1.0x
	// data7              11165  code   1.0x
	// data8              11288  code   1.0x
	// data9              11411  code   1.0x
	//
	// 221 of 255 sectors used (17kb free)
}

// -
func ExampleMDV7z() {
	done := testable.FixExampleOutput()
	defer done()
	runList("cart/mdv.7z")
	// Output:
	// fillcart
	//
	// data0              10304  code   1.0x
	// data1              10427  code   1.0x
	// data2              10550  code   1.0x
	// data3              10673  code   1.0x
	// data4              10796  code   1.0x
	// data5              10919  code   1.0x
	// data6              11042  code   1.0x
	// data7              11165  code   1.0x
	// data8              11288  code   1.0x
	// data9              11411  code   1.0x
	//
	// 221 of 255 sectors used (17kb free)
}

// -
func ExampleQDOS() {
	done := testable.FixExampleOutput()
	defer done()
	runList("cart/qdos.zip")
	// Output:
	// QDOSonMDV
	//
	// BOOT                 572  BASIC  1.0x
	// PRINTER_DAT          149  BASIC  1.0x
	// QUILL              53156  code   1.0x
	// QUIL_HOB           24640  BASIC  1.0x
	// clone                462  BASIC  1.0x
	//
	// 159 of 255 sectors used (48kb free)
}

// -
func ExampleMDVT() {
	done := testable.FixExampleOutput()
	defer done()
	runList("cart/mdvt.mdv")
	// Output:
	// Alex
	//
	// alex.bas             166  BASIC  1.0x
	// cat10x.bas            99  BASIC  1.0x
	// hello.bas            236  BASIC  1.0x
	//
	// 3 of 254 sectors used (125kb free)
}

// -
func ExampleMDVTCPM() {
	done := testable.FixExampleOutput()
	defer done()
	runList("cart/mdvt-cpm.mdv")
	// Output:
	// CP/M f5c7
	//
	// User 0:
	// DDT.COM             4864  code   1.0x
	// DUMP.COM             512  code   1.0x
	// ED.COM              6656  code   1.0x
	// PIP.COM             7424  code   1.0x
	// STAT.COM            5248  code   1.0x
	// SUBMIT.COM          1280  code   1.0x
	// XSUB.COM             768  code   1.0x
	//
	// 62 of 192 sectors used (65kb free)
}

// -
func runList(name string) {
	run.NewList().Execute([]string{"load", "-i", GetFixture(name)})
}
