/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2023, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package test

import (
	"codeberg.org/xelalexv/oqtadrive/pkg/run"

	testable "github.com/larschri/testable-example-output"
)

// -
func init() {
	run.DieOnError(nil) // trigger log setup
}

// tests various commands and their outputs

// -
func ExampleDumpFileMDR() {
	done := testable.FixExampleOutput()
	defer done()
	run.NewDump().Execute([]string{
		"dump", "-i", GetFixture("cart/bits.mdr.zip"), "-f", "hello.bas"})
	// Output:
	// 10  BEEP 1,9
	// 20  BEEP 1,8
	// 30  BEEP 1,9
	// 40  BEEP 1,8
	// 50  BEEP  1,7
	// 60  BEEP 1,6
	// 70  BEEP 2,8
	// 100 FOR x=1 TO 10
	// 110 PRINT "Hello World!"
	// 120 NEXT x
}

// -
func ExampleDumpFileMDV() {
	done := testable.FixExampleOutput()
	defer done()
	run.NewDump().Execute([]string{
		"dump", "-i", GetFixture("cart/bits.mdv.zip"), "-f", "hello.bas"})
	// Output:
	// 10 FOR x=1 TO 10
	// 20 PRINT "Hello World!"
	// 30 NEXT x
}
