/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2021, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package spectrum

import (
	"fmt"
	"io"

	"codeberg.org/xelalexv/oqtadrive/pkg/util"
)

//
const KeywordStart = 0xa3
const KeywordEnd = 0xff

const NumInteger = 0x0e
const NumFloat = 0x7e

const LineEnd = 0x0d

const CharStart = 0x20
const CharEnd = 0x8f

//
var keywords = []string{
	"SPECTRUM",  // 0xa3
	"PLAY",      // 0xa4
	"RND",       // 0xa5
	"INKEY$",    // 0xa6
	"PI",        // 0xa7
	"FN",        // 0xa8
	"POINT",     // 0xa9
	"SCREEN$",   // 0xaa
	"ATTR",      // 0xab
	"AT",        // 0xac
	"TAB",       // 0xad
	"VAL$",      // 0xae
	"CODE",      // 0xaf
	"VAL",       // 0xb0
	"LEN",       // 0xb1
	"SIN",       // 0xb2
	"COS",       // 0xb3
	"TAN",       // 0xb4
	"ASN",       // 0xb5
	"ACS",       // 0xb6
	"ATN",       // 0xb7
	"LN",        // 0xb8
	"EXP",       // 0xb9
	"INT",       // 0xba
	"SQR",       // 0xbb
	"SGN",       // 0xbc
	"ABS",       // 0xbd
	"PEEK",      // 0xbe
	"IN",        // 0xbf
	"USR",       // 0xc0
	"STR$",      // 0xc1
	"CHR$",      // 0xc2
	"NOT",       // 0xc3
	"BIN",       // 0xc4
	"OR",        // 0xc5
	"AND",       // 0xc6
	"<=",        // 0xc7
	">=",        // 0xc8
	"<>",        // 0xc9
	"LINE",      // 0xca
	"THEN",      // 0xcb
	"TO",        // 0xcc
	"STEP",      // 0xcd
	"DEF FN",    // 0xce
	"CAT",       // 0xcf
	"FORMAT",    // 0xd0
	"MOVE",      // 0xd1
	"ERASE",     // 0xd2
	"OPEN #",    // 0xd3
	"CLOSE #",   // 0xd4
	"MERGE",     // 0xd5
	"VERIFY",    // 0xd6
	"BEEP",      // 0xd7
	"CIRCLE",    // 0xd8
	"INK",       // 0xd9
	"PAPER",     // 0xda
	"FLASH",     // 0xdb
	"BRIGHT",    // 0xdc
	"INVERSE",   // 0xdd
	"OVER",      // 0xde
	"OUT",       // 0xdf
	"LPRINT",    // 0xe0
	"LLIST",     // 0xe1
	"STOP",      // 0xe2
	"READ",      // 0xe3
	"DATA",      // 0xe4
	"RESTORE",   // 0xe5
	"NEW",       // 0xe6
	"BORDER",    // 0xe7
	"CONTINUE",  // 0xe8
	"DIM",       // 0xe9
	"REM",       // 0xea
	"FOR",       // 0xeb
	"GO TO",     // 0xec
	"GO SUB",    // 0xed
	"INPUT",     // 0xee
	"LOAD",      // 0xef
	"LIST",      // 0xf0
	"LET",       // 0xf1
	"PAUSE",     // 0xf2
	"NEXT",      // 0xf3
	"POKE",      // 0xf4
	"PRINT",     // 0xf5
	"PLOT",      // 0xf6
	"RUN",       // 0xf7
	"SAVE",      // 0xf8
	"RANDOMIZE", // 0xf9
	"IF",        // 0xfa
	"CLS",       // 0xfb
	"DRAW",      // 0xfc
	"CLEAR",     // 0xfd
	"RETURN",    // 0xfe
	"COPY",      // 0xff
}

//
func KeywordString(k byte) string {
	ix := int(k - KeywordStart)
	if 0 <= ix && ix < len(keywords) {
		return keywords[ix]
	}
	return ""
}

//
func Parse(r io.Reader, programLen int) ([]*Line, error) {

	var ret []*Line
	read := 0

	for {
		if read >= programLen { // reached variable table or end of program
			break
		}

		lineNum, err := util.ReadUInt16(r, true)
		if lineNum == -1 || err != nil {
			return ret, fmt.Errorf("premature end of program")
		}
		read += 2

		txtLen, err := util.ReadUInt16(r, false)
		if txtLen == -1 || err != nil {
			return ret, fmt.Errorf("premature end of file")
		}
		read += 2

		txt := make([]byte, txtLen)
		if _, err := io.ReadFull(r, txt); err != nil {
			return ret, fmt.Errorf("premature end of file")
		}

		l := &Line{Number: lineNum}
		l.parse(txt)
		ret = append(ret, l)
		read += txtLen
	}

	return ret, nil
}

//
type Line struct {
	Number int
	Tokens []Token
}

//
func (l *Line) parse(txt []byte) {

	for ix := 0; ix < len(txt); {

		b := txt[ix]

		switch {

		case b == LineEnd:
			return

		case b == NumInteger || b == NumFloat:
			// integral or floating point numerical constant
			if ix+6 > len(txt) {
				return // FIXME better handling of this error case
			}
			c := &Const{}
			c.set(b == NumFloat, txt[ix:ix+6])
			l.addToken(c)
			ix += 6

		case b >= KeywordStart:
			l.addToken(&Keyword{code: b})
			ix++

		case CharStart <= b && b <= CharEnd:
			l.addToken(&Char{code: b})
			ix++

		default: // skip
			ix++
		}
	}
}

//
func (l *Line) addToken(t Token) {
	l.Tokens = append(l.Tokens, t)
}

//
type Token interface {
	Length() int
	Visible() bool
	Spaced() bool
	Bytes() []byte
	String() string
}

//
type Const struct {
	isFloat bool
	data    []byte
}

func (c *Const) set(isFloat bool, d []byte) {
	c.isFloat = isFloat
	c.data = make([]byte, len(d))
	copy(c.data, d)
}

func (c *Const) Length() int {
	return len(c.data)
}

func (c *Const) Visible() bool {
	return false
}

func (c *Const) Spaced() bool {
	return false
}

func (c *Const) Bytes() []byte {
	return c.data
}

func (c *Const) String() string {
	return ""
}

//
type Keyword struct {
	code byte
}

func (k *Keyword) Length() int {
	return len(k.String())
}

func (k *Keyword) Visible() bool {
	return true
}

func (k *Keyword) Spaced() bool {
	return true
}

func (k *Keyword) Bytes() []byte {
	return []byte(k.String())
}

func (k *Keyword) String() string {
	return KeywordString(k.code) + " "
}

//
type Char struct {
	code byte
}

func (c *Char) Length() int {
	return 1
}

func (c *Char) Visible() bool {
	return true
}

func (c *Char) Spaced() bool {
	return false
}

func (c *Char) Bytes() []byte {
	return []byte{c.code}
}

func (c *Char) String() string {
	return string(rune(c.code))
}
