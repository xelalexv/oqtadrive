/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2021, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package control

import (
	"fmt"
	"net/http"
	"os"
	"strings"
	"sync"
	"sync/atomic"

	log "github.com/sirupsen/logrus"

	"codeberg.org/xelalexv/oqtadrive/pkg/repo"
	"codeberg.org/xelalexv/oqtadrive/pkg/repo/zxdb"
)

//
const MaxCachedSearchResults = 10

//
var searchCount uint64
var searches *searchCache

//
func init() {
	searches = &searchCache{
		cache: make(map[uint64]*repo.SearchResult),
	}
}

//
type searchCache struct {
	cache map[uint64]*repo.SearchResult
	lock  sync.RWMutex
}

//
func (sc *searchCache) add(r *repo.SearchResult) {
	if r != nil {
		sc.lock.Lock()
		sc.cache[r.ID] = r
		sc.prune()
		sc.lock.Unlock()
	}
}

//
func (sc *searchCache) get(key int) *repo.SearchResult {
	sc.lock.RLock()
	defer sc.lock.RUnlock()
	return sc.cache[uint64(key)]
}

// expects r/w lock has been acquired
func (sc *searchCache) prune() {
	if cutoff := len(sc.cache) - MaxCachedSearchResults; cutoff > 0 {
		for k, _ := range sc.cache {
			if k < uint64(cutoff) {
				delete(sc.cache, k)
			}
		}
	}
}

//
func (a *api) search(w http.ResponseWriter, req *http.Request) {

	src := getArg(req, "source")
	switch src {
	case "zxdb", "repo":
	case "":
		src = "repo"
	default:
		handleError(fmt.Errorf("invalid source: %s", src),
			http.StatusUnprocessableEntity, w)
		return
	}

	log.WithField("source", src).Debugf("searching")

	if src == "repo" {
		a.indexLock.RLock()
		defer a.indexLock.RUnlock()

		if a.index == nil {
			handleError(fmt.Errorf("search index not available"),
				http.StatusServiceUnavailable, w)
			return
		}
	}

	page, _ := getIntArg(req, "page", 25)
	var res *repo.SearchResult

	if id, _ := getIntArg(req, "id", 0); id > 0 { // result page requested
		res = searches.get(id)
		if res == nil {
			handleError(fmt.Errorf("invalid search ID: %d", id),
				http.StatusUnprocessableEntity, w)
			return
		}

		start, _ := getIntArg(req, "start", 0)
		if start < 0 || start >= len(res.Hits) {
			handleError(fmt.Errorf("invalid start item: %d", start),
				http.StatusUnprocessableEntity, w)
			return
		}
		res = a.createPage(start, page, res)

	} else { // actual search
		items, err := getIntArg(req, "items", 500)
		if handleError(err, http.StatusUnprocessableEntity, w) {
			return
		}

		term := getArg(req, "term")
		excl := splitFormats(getArg(req, "exclude"))

		switch src {
		case "repo":
			res, err = a.index.Search(term, excl, items)
		case "zxdb":
			res, err = zxdb.Search(term, excl, items)
		}

		if handleError(err, http.StatusUnprocessableEntity, w) {
			return
		}

		res.ID = atomic.AddUint64(&searchCount, 1)
		searches.add(res)
		res = a.createPage(0, page, res)
	}

	if wantsJSON(req) {
		sendJSONReply(res, http.StatusOK, w)

	} else {
		var sb strings.Builder
		for _, h := range res.Hits {
			sb.WriteString(fmt.Sprintf("%s\n", h.Render()))
		}
		sb.WriteString(fmt.Sprintf("\ntotal hits: %d\n", res.Total))
		sendReply([]byte(sb.String()), http.StatusOK, w)
	}
}

//
func (a *api) createPage(start, count int, r *repo.SearchResult) *repo.SearchResult {

	end := start + count
	if end > len(r.Hits) {
		end = len(r.Hits)
	}

	return &repo.SearchResult{
		Hits:      r.Hits[start:end],
		Start:     start,
		Total:     r.Total,
		Available: r.Available,
		ID:        r.ID,
	}
}

//
func (a *api) reindex(w http.ResponseWriter, req *http.Request) {

	a.indexLock.Lock()
	defer a.indexLock.Unlock()

	if a.index == nil {
		handleError(fmt.Errorf("search index not available"),
			http.StatusServiceUnavailable, w)
		return
	}

	base := a.index.Base()
	repo := a.index.Repo()
	logger := log.WithFields(log.Fields{"base": base, "repo": repo})

	logger.Info("stopping index")
	a.index.Stop()
	a.index = nil

	logger.Info("removing index")
	if handleError(os.RemoveAll(base), http.StatusInternalServerError, w) {
		return
	}

	logger.Info("starting index")
	a.startIndex()

	sendReply([]byte("index rebuild triggered"), http.StatusOK, w)
}

//
func splitFormats(f string) map[string]bool {
	parts := strings.Split(f, ",")
	ret := make(map[string]bool)
	for _, p := range parts {
		ret[strings.ToLower(strings.TrimSpace(p))] = true
	}
	return ret
}
