/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2022, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package control

import (
	"bufio"
	"fmt"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
)

//
func newBasicAuth() (*basicAuth, error) {

	wd, err := os.Getwd()
	if err != nil {
		return nil, err
	}

	credsFile := filepath.Join(wd, ".credentials")

	if info, err := os.Stat(credsFile); err != nil {
		if os.IsNotExist(err) {
			return nil, nil
		}
		return nil, err

	} else {
		if info.Size() > 256 {
			return nil, fmt.Errorf("credentials file too large")
		}
		if info.Mode()&077 != 0 {
			log.Warnf("permissions of credentials file %s too liberal: %v",
				credsFile, info.Mode())
		}
	}

	creds, err := os.Open(credsFile)
	if err != nil {
		return nil, err
	}
	defer creds.Close()

	scanner := bufio.NewScanner(creds)

	for scanner.Scan() {
		if ln := scanner.Text(); !strings.HasPrefix(ln, "#") {
			parts := strings.SplitN(ln, ":", 2)
			ret := &basicAuth{user: parts[0]}
			if len(parts) > 1 {
				ret.pass = parts[1]
			}
			return ret, nil
		}
	}

	if err := scanner.Err(); err != nil {
		return nil, err
	}

	return nil, fmt.Errorf("no credentials line in %s", credsFile)
}

//
type basicAuth struct {
	user string
	pass string
}

//
func (ba *basicAuth) auth(inner http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		//
		// A more secure way of doing the verification (with constant time
		// compare) is described at below link. However, I don't think we
		// really need to be that secure...
		//
		//	https://www.alexedwards.net/blog/basic-authentication-in-go
		//
		if u, p, ok := r.BasicAuth(); ok {
			if u == ba.user && p == ba.pass {
				inner.ServeHTTP(w, r)
				return
			}
		}

		log.WithFields(log.Fields{
			"remote": r.RemoteAddr,
			"method": r.Method,
			"path":   r.RequestURI,
		}).Info("API UNAUTHORIZED")

		time.Sleep(2 * time.Second)

		w.Header().Set("WWW-Authenticate",
			`Basic realm="restricted", charset="UTF-8"`)
		handleError(fmt.Errorf("Unauthorized"), http.StatusUnauthorized, w)
	})
}

//
func (ba *basicAuth) creds() string {
	return fmt.Sprintf("%s:%s", ba.user, ba.pass)
}
