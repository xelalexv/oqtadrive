/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2023, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package control

import (
	"fmt"
	"net/http"

	log "github.com/sirupsen/logrus"

	"codeberg.org/xelalexv/oqtadrive/pkg/repo"
)

// -
func (a *api) repoTrash(w http.ResponseWriter, req *http.Request) {

	var reference *repo.Reference

	ref, err := getRef(req, false)
	if err == nil {
		if ref != "" {
			if reference, err = repo.Resolve(
				ref, a.repository, false, false); err == nil {
				if reference.Schema != repo.RefSchemaRepo {
					err = fmt.Errorf("not a repo path: %s", ref)
				}
			}
		} else {
			err = fmt.Errorf("no path provided")
		}
	}
	if handleError(err, http.StatusNotAcceptable, w) {
		return
	}

	log.WithField("path", reference.Path).Info("moving file to repo trash")

	if a.index == nil {
		handleError(fmt.Errorf("search index not available"),
			http.StatusServiceUnavailable, w)
		return
	}

	if handleError(a.index.MoveToTrash(reference.Path),
		http.StatusUnprocessableEntity, w) {
		return
	}

	sendReply([]byte("moved file to repo trash folder"), http.StatusOK, w)
}
