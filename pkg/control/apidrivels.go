/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2021, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package control

import (
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strings"
	"time"

	"codeberg.org/xelalexv/oqtadrive/pkg/daemon"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/base"
)

// -
func (a *api) dump(w http.ResponseWriter, req *http.Request) {
	a.driveInfo(w, req, "dump")
}

// -
func (a *api) driveList(w http.ResponseWriter, req *http.Request) {
	a.driveInfo(w, req, "ls")
}

// -
func (a *api) driveInfo(w http.ResponseWriter, req *http.Request, info string) {

	drive := getDrive(w, req)
	if drive == -1 {
		return
	}

	if a.daemon.GetStatus(drive) == daemon.StatusHardware {
		sendReply([]byte(fmt.Sprintf(
			"hardware drive mapped to slot %d", drive)),
			http.StatusOK, w)
		return
	}

	cart, ok := a.daemon.GetCartridge(drive, time.Second)

	if !ok {
		handleError(fmt.Errorf("drive %d busy", drive), http.StatusLocked, w)
		return
	}

	if cart == nil {
		handleError(fmt.Errorf("no cartridge in drive %d", drive),
			http.StatusUnprocessableEntity, w)
		return
	}

	defer cart.Unlock()

	setHeaders(w.Header(), false)
	w.WriteHeader(http.StatusOK)

	switch info {
	case "dump":
		if err := cart.Dump(
			w, getArg(req, "file"), isFlagSet(req, "raw")); err != nil {
			fmt.Fprintf(w, "\nerror dumping: %v\n\n", err)
		}
	case "ls":
		WriteFileList(w, cart, isFlagSet(req, "annotate"), drive)
	}
}

// -
func WriteFileList(w io.Writer, c *base.Cartridge, annotate bool, drive int) {

	fmt.Fprintf(w, "\n%s\n\n", c.Name())

	if stats, files, err := c.FS().Ls(); err == nil {

		var group string

		for _, f := range files {

			name := f.Name()
			if f.Group() != group {
				group = f.Group()
				fmt.Fprintf(w, "\n%s:\n", group)
			}

			if annotate {
				name = strings.TrimSpace(name)
				fill := ""
				if cnt := 16 - len(name); cnt > 0 {
					fill = strings.Repeat(" ", cnt)
				}
				fmt.Fprintf(w,
					"<a href=\"/drive/%d/dump?file=%s\" target=\"_blank\">%s</a>%s",
					drive, url.QueryEscape(f.Path()+name), name, fill)
			} else {
				fmt.Fprintf(w, "%-16s", name)
			}

			if f.HasAnnotation("fragmentation") {
				fmt.Fprintf(w, "%8d  %-6v %2.1fx\n",
					f.Size(), f.Type().String(),
					f.GetAnnotation("fragmentation").Value().(float32))
				// fmt.Fprintf(w, "%8d  %-6v %dx\n",
				// 	f.Size(), f.Type().String(),
				// 	f.GetAnnotation("fragmentation").Value().(int))
			} else {
				fmt.Fprintf(w, "%8d  %-6v\n", f.Size(), f.Type().String())
			}
		}

		fmt.Fprintf(w, "\n%d of %d sectors used (%dkb free)\n\n",
			stats.Used(), stats.Sectors(), (stats.Sectors()-stats.Used())/2)

	} else {
		fmt.Fprintf(w, "\nerror listing files: %v\n\n", err)
	}
}
