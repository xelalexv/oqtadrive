/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2022, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package control

import (
	"fmt"
	"io"
	"net/http"
	"os/exec"
	"strings"
)

//
func (a *api) setWifi(w http.ResponseWriter, req *http.Request) {

	ssid := getArg(req, "ssid")
	if ssid == "" {
		handleError(fmt.Errorf("no SSID provided"),
			http.StatusUnprocessableEntity, w)
		return
	}
	ssid = strings.TrimSpace(ssid)

	// max PSK passphrase length is 63
	b, err := io.ReadAll(http.MaxBytesReader(nil, req.Body, 63))
	if handleError(err, http.StatusInternalServerError, w) {
		return
	}
	pass := string(b)

	var command string
	var args []string

	if _, err := exec.LookPath("raspi-config"); err == nil { // RaspberryPi
		command = "raspi-config"
		args = []string{"nonint", "do_wifi_ssid_passphrase", ssid}
		if pass != "" {
			args = append(args, pass)
		}

	} else if _, err := exec.LookPath("nmcli"); err == nil { // BananaPi
		command = "nmcli"
		// it's best to first run `nmcli dev wifi`, to make sure the network
		// manager has a fresh station list; error is not interesting here
		exec.Command(command, "dev", "wifi").Run()
		args = []string{"dev", "wifi", "connect", ssid}
		if pass != "" {
			args = append(args, "password", pass)
		}

	} else if _, err := exec.LookPath("uci"); err == nil { // Linino One
		var script strings.Builder
		script.WriteString(
			fmt.Sprintf("uci set wireless.@wifi-iface[0].ssid=%s; ", ssid))
		if pass != "" {
			script.WriteString(
				fmt.Sprintf("uci set wireless.@wifi-iface[0].key=%s; ", pass))
		}
		script.WriteString("uci commit wireless; wifi down; wifi up")
		command = "sh"
		args = []string{"-c", script.String()}

	} else {
		handleError(
			fmt.Errorf("no WiFi command available (raspi-config, nmcli, uci)"),
			http.StatusNotImplemented, w)
		return
	}

	if err := exec.Command(command, args...).Run(); err != nil {
		handleError(fmt.Errorf("error setting WiFi: %v", err),
			http.StatusInternalServerError, w)
		return
	}

	sendReply([]byte(fmt.Sprintf("WiFi successfully changed to %s", ssid)),
		http.StatusOK, w)
}
