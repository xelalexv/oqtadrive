/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2023, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package control

import (
	"fmt"
	"net/http"
	"time"

	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/format/helper"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/types"
)

//
func (a *api) format(w http.ResponseWriter, req *http.Request) {

	drive := getDrive(w, req)
	if drive == -1 {
		return
	}

	name := getArg(req, "name")
	if name == "" {
		name = "NONAME"
	}

	// a forced format discards all changes, so we don't check for write-back
	if force := isFlagSet(req, "force"); !force && handleError(
		a.checkForWriteBack(drive), http.StatusInternalServerError, w) {
		return
	}

	cart, ok := a.daemon.GetCartridge(drive, time.Second)
	if !ok {
		handleError(fmt.Errorf("drive %d busy", drive), http.StatusLocked, w)
		return
	}

	set := cart == nil

	if set {
		cl := a.daemon.GetClientType()
		if cl == types.UNKNOWN {
			cl = types.IF1
		}
		var err error
		if cart, err = microdrive.NewCartridge(cl); handleError(
			err, http.StatusInternalServerError, w) {
			return
		}
	} else {
		defer cart.Unlock()
	}

	if handleError(cart.Format(name), http.StatusInternalServerError, w) {
		return
	}

	if set && handleError(a.daemon.SetCartridge(drive, cart, false),
		http.StatusInternalServerError, w) {
		return
	}

	cart.RemoveAnnotation(AnnoCartridgeInfo)
	if handleError(helper.AutoSave(drive, cart, false),
		http.StatusInternalServerError, w) {
		return
	}

	sendReply([]byte(
		fmt.Sprintf("formatted cartridge in drive %d", drive)), http.StatusOK, w)
}
