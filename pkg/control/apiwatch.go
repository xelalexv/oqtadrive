/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2021, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package control

import (
	"net/http"
	"strconv"
	"time"

	log "github.com/sirupsen/logrus"

	"codeberg.org/xelalexv/oqtadrive/pkg/daemon"
)

// -
func (a *api) watch(w http.ResponseWriter, req *http.Request) {

	logger := log.WithField("remote", req.RemoteAddr)

	if a.stopped {
		logger.Debug("rejecting watch while stopping")
		sendReply([]byte{}, http.StatusGone, w)
		return
	}

	timeout, err := strconv.Atoi(req.URL.Query().Get("timeout"))
	if err != nil || timeout < 0 || 1800 < timeout {
		timeout = 600
	}

	logger.WithField("timeout", timeout).Info("starting watch")
	update := make(chan *Change)

	select {
	case a.longPollQueue <- update:
	case <-time.After(time.Duration(timeout) * time.Second):
		logger.Info("closing watch after timeout")
		sendReply([]byte{}, http.StatusRequestTimeout, w)
		return
	}

	upd := <-update
	if upd != nil {
		logger.Info("sending daemon change")
	} else {
		logger.Debug("discarding long poll client")
	}
	sendJSONReply(upd, http.StatusOK, w)
}

// -
func (a *api) watchDaemon() {

	log.Info("start watching for daemon changes")

	var client string
	var netUpdate time.Time

	driveStatus := make([]string, daemon.DriveCount)
	prevStatus := make(map[string]string)

	for !a.stopped {

		time.Sleep(2 * time.Second)
		change := &Change{}

		l, changed := GetCartridges(a.daemon)
		for ix, c := range l {
			if c.Status != driveStatus[ix] {
				log.WithFields(log.Fields{
					"drive": ix + 1,
					"from":  driveStatus[ix],
					"to":    c.Status}).Debug("drive status change")
				driveStatus[ix] = c.Status
				changed = true
			}
		}

		force := len(a.forceNotify) > 0
		if force || changed {
			change.Drives = l
		}
		if force {
			<-a.forceNotify
		}

		if c := a.daemon.GetClient(); c != client {
			change.Client = c
			client = c
		}

		if h := a.daemon.GetHubClient(); h != nil {
			if upd := h.Update(); upd.After(netUpdate) {
				change.Network = &Network{
					Hub:       h.HubURL(),
					Addresses: h.Addresses(),
					Users:     h.Users(),
				}
				change.Network.SetDescription()
				netUpdate = upd
			}
		}

		st := &State{}
		if s, ok := a.checkProgressChange(ProgressUpgrade, prevStatus); ok {
			st.Upgrade = &s
			st.changed = true
		}
		if s, ok := a.checkProgressChange(ProgressReindex, prevStatus); ok {
			st.Reindex = &s
			st.changed = true
		}
		if st.changed {
			change.State = st
		}

		if !change.HasChange() {
			continue
		}

		log.Info("daemon changes")

	Loop:
		for {
			select {
			case cl := <-a.longPollQueue:
				log.Info("notifying long poll client")
				cl <- change
			default:
				log.Info("all long poll clients notified")
				break Loop
			}
		}
	}

	log.Info("stopped watching for daemon changes")
}

// -
func (a *api) checkProgressChange(
	key string, prev map[string]string) (string, bool) {

	if s := a.getProgress(key); s != prev[key] {
		log.WithFields(
			log.Fields{"old": prev[key], "new": s}).Debug("progress change")
		prev[key] = s
		return s, true
	}
	return "", false
}

// -
func (a *api) discardLongPollClients() {

	log.Info("discarding long poll clients...")

	msg := "stopping daemon"
	var c *Change
	if a.getProgress(ProgressUpgrade) != "" {
		c = &Change{
			State: &State{
				Upgrade: &msg,
			},
		}
	}

Loop:
	for {
		select {
		case cl := <-a.longPollQueue:
			cl <- c
		default:
			log.Info("all long poll clients discarded")
			break Loop
		}
	}
}
