/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2021, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package control

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"

	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/base"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/format"
	"codeberg.org/xelalexv/oqtadrive/pkg/repo"
	"codeberg.org/xelalexv/oqtadrive/pkg/util"
)

// -
func (a *api) load(w http.ResponseWriter, req *http.Request) {

	drive := getDrive(w, req)
	if drive == -1 {
		return
	}

	var in io.ReadCloser
	var reference *repo.Reference
	var err error

	if ref, err := getRef(req, true); ref != "" {
		if err == nil {
			reference, err = repo.Resolve(ref, a.repository, false, true)
		}
		if err != nil {
			handleError(err, http.StatusNotAcceptable, w)
			return
		}
		in = reference.Source
	} else {
		in = http.MaxBytesReader(nil, req.Body, 1048576) // FIXME make constant
	}

	cr, err := format.NewCartReader(ioutil.NopCloser(in), getArg(req, "compressor"))
	if err != nil {
		handleError(err, http.StatusUnprocessableEntity, w)
		return
	}
	defer cr.Close()

	typ := getArg(req, "type")
	if typ == "" {
		typ = cr.Type()
	}

	reader, err := format.NewFormat(typ, cr.Prefix())
	if handleError(err, http.StatusUnprocessableEntity, w) {
		return
	}

	params := util.Params{
		"name":     getArg(req, "name"),
		"launcher": getArg(req, "launcher"),
	}
	cart, err := reader.Read(cr, true, isFlagSet(req, "repair"), params)
	if handleError(err, http.StatusUnprocessableEntity, w) {
		return
	}

	if handleError(req.Body.Close(), http.StatusInternalServerError, w) {
		return
	}

	annotateWriteBack(cart, reference)

	if handleError(a.checkForWriteBack(drive), http.StatusInternalServerError, w) {
		return
	}

	if err := a.daemon.SetCartridge(drive, cart, isFlagSet(req, "force")); err != nil {
		if strings.Contains(err.Error(), "could not lock") {
			handleError(fmt.Errorf("drive %d busy", drive), http.StatusLocked, w)
		} else if strings.Contains(err.Error(), "is modified") {
			handleError(fmt.Errorf(
				"cartridge in drive %d is modified", drive), http.StatusConflict, w)
		} else {
			handleError(err, http.StatusInternalServerError, w)
		}

	} else {
		sendReply([]byte(
			fmt.Sprintf("loaded data into drive %d", drive)), http.StatusOK, w)
	}

	a.forceNotify <- true
}

// -
func annotateWriteBack(cart *base.Cartridge, ref *repo.Reference) bool {
	if ref.RequiresWriteBack() {
		cart.Annotate(AnnoWriteBackPath, ref.Path)
		log.WithField("path", ref.Path).Debug("setting write-back path")
		return true
	}
	return false
}

// -
func (a *api) checkForWriteBack(drive int) error {
	if cur, ok := a.daemon.GetCartridge(drive, time.Second); ok && cur != nil {
		if cur.IsModified() {
			if written, err := a.writeBackCart(cur); err != nil {
				err = fmt.Errorf("could not write back cartridge: %v", err)
				cur.Unlock()
				return err
			} else if written {
				cur.SetModified(false)
				cur.SetAutoSaved(true)
			}
		}
		cur.Unlock()
	}
	return nil
}

// -
func (a *api) writeBackCart(cart *base.Cartridge) (bool, error) {

	if cart == nil {
		return false, nil
	}

	wbp := strings.TrimSpace(cart.GetAnnotation(AnnoWriteBackPath).String())
	if wbp == "" {
		return false, nil
	}

	// We're not yet supporting compressed output files, so if the cart was read
	// from an archive, we need to change the extension of the write-back path.
	// However, this may look something like this: foo/bar/cart.mdr.gzip
	// So we check the last two extensions. If last is not what we want, we drop
	// it and check the next. If that's also not correct, we add the right one,
	// e.g.:
	//
	//		foo/bar/cart.mdr.gz ---> foo/bar/cart.mdr
	//		foo/bar/cart.one.gz ---> foo/bar/cart.one.mdr
	//
	p := wbp
	def := "." + cart.Client().DefaultFormat()
	ext := filepath.Ext(p)
	if ext != def {
		p = p[:len(p)-len(ext)]
		ext = filepath.Ext(p)
		if ext != def {
			p = fmt.Sprintf("%s%s", p, def)
		}
	}

	log.WithFields(
		log.Fields{"path": wbp, "effective": p}).Debug("writing back cartridge")

	writer, err := format.NewFormat(def[1:], nil)
	if err != nil {
		return true, err
	}

	f, err := os.Create(filepath.Join(a.repository, p))
	if err != nil {
		return true, err
	}
	defer f.Close()

	out := bufio.NewWriter(f)
	defer out.Flush()

	return true, writer.Write(cart, out, false, nil)
}
