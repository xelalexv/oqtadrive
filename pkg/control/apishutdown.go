//go:build linux

/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2022, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package control

import (
	"fmt"
	"net/http"
	"os/exec"
	"os/user"
	"syscall"

	log "github.com/sirupsen/logrus"
)

//
func (a *api) shutdown(w http.ResponseWriter, req *http.Request) {

	var msg string

	command := getArg(req, "command")
	switch command {

	case "halt":
		msg = "halt the system"

	case "poweroff":
		msg = "power off the system"

	case "reboot":
		msg = "reboot the system"

	default:
		handleError(fmt.Errorf("unknown shutdown command: '%s'", command),
			http.StatusUnprocessableEntity, w)
		return
	}

	log.Info(msg)
	syscall.Sync()

	sendReply([]byte(fmt.Sprintf("attempting to %s", msg)), http.StatusOK, w)

	var cmd *exec.Cmd
	if usr, err := user.Current(); err != nil || usr.Username != "root" {
		cmd = exec.Command("sudo", command)
	} else {
		cmd = exec.Command(command)
	}

	if err := cmd.Start(); err != nil {
		log.Errorf("%s failed: %v", command, err)
	}
}
