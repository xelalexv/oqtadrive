/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2021, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package control

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	"golang.org/x/net/webdav"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"

	"codeberg.org/xelalexv/oqtadrive/pkg/daemon"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/base"
	"codeberg.org/xelalexv/oqtadrive/pkg/repo"
	"codeberg.org/xelalexv/oqtadrive/pkg/util"
)

// -
const (
	ControlFile      = "oqctl"
	ControlCartridge = "oqtactl"

	AnnoWriteBackPath = "writeBackPath"
	AnnoCartridgeInfo = "cartridgeInfo"

	ProgressUpgrade = "ProgressUpgrade"
	ProgressReindex = "ProgressReindex"
)

// -
var (
	LogSponge       *util.LogSponge = util.NewLogSponge(5, 2*1024*1024)
	defaultLogLevel log.Level
)

// -
type APIServer interface {
	Serve() error
	Stop() error
}

// -
func NewAPIServer(addr, repo string, d *daemon.Daemon) APIServer {
	return &api{address: addr, repository: repo, daemon: d}
}

// -
type api struct {
	//
	address    string
	repository string
	//
	daemon      *daemon.Daemon
	server      *http.Server
	index       *repo.Index
	indexLock   sync.RWMutex
	upgradeLock sync.Mutex
	//
	longPollQueue chan chan *Change
	forceNotify   chan bool
	stopped       bool
	//
	// for tracking progress info, in particular of long running actions such as
	// upgrade or index rebuild; for reporting back to API clients, mostly web UI
	progressMap *sync.Map
}

// -
func (a *api) Serve() error {

	a.progressMap = &sync.Map{}

	defaultLogLevel = log.GetLevel()
	router := mux.NewRouter().StrictSlash(true)

	addRoute(router, "status", "GET", "/status", a.status)
	addRoute(router, "watch", "GET", "/watch", a.watch)
	addRoute(router, "ls", "GET", "/list", a.list)
	addRoute(router, "load", "PUT", "/drive/{drive:[1-8]}", a.load)
	addRoute(router, "unload", "GET", "/drive/{drive:[1-8]}/unload", a.unload)
	addRoute(router, "rename", "PUT", "/drive/{drive:[1-8]}/rename", a.rename)
	addRoute(router, "format", "PUT", "/drive/{drive:[1-8]}/format", a.format)
	addRoute(router, "reorg", "PUT", "/drive/{drive:[1-8]}/reorg", a.reorg)
	addRoute(router, "save", "GET", "/drive/{drive:[1-8]}", a.saveRemote)
	addRoute(router, "save", "PUT", "/drive/{drive:[1-8]}/save", a.saveRepo)
	addRoute(router, "dump", "GET", "/drive/{drive:[1-8]}/dump", a.dump)
	addRoute(router, "map", "GET", "/map", a.getDriveMap)
	addRoute(router, "map", "PUT", "/map", a.setDriveMap)
	addRoute(router, "drivels", "GET", "/drive/{drive:[1-8]}/list", a.driveList)
	addRoute(router, "network", "POST", "/network", a.netReceive)
	addRoute(router, "netconfig", "GET", "/network/config", a.netGetConfig)
	addRoute(router, "netconfig", "POST", "/network/config", a.netSetConfig)
	addRoute(router, "resync", "PUT", "/resync", a.resync)
	addRoute(router, "config", "GET", "/config", a.getConfig)
	addRoute(router, "config", "PUT", "/config", a.setConfig)
	addRoute(router, "search", "GET", "/search", a.search)
	addRoute(router, "repotrash", "POST", "/repo/trash", a.repoTrash)
	addRoute(router, "reindex", "POST", "/reindex", a.reindex)
	addRoute(router, "upgrade", "POST", "/upgrade", a.upgrade)
	addRoute(router, "version", "GET", "/version", a.version)
	addRoute(router, "log", "GET", "/log", a.getLog)
	addRoute(router, "log", "PUT", "/log", a.configureLog)
	addRoute(router, "shutdown", "PUT", "/shutdown", a.shutdown)
	addRoute(router, "wifi", "PUT", "/wifi", a.setWifi)

	if a.repository != "" {

		a.startIndex() // repo index

		wdHandler := webdav.Handler{ // repo WebDAV
			FileSystem: webdav.Dir(a.repository),
			LockSystem: webdav.NewMemLS(),
			Prefix:     "/webdav",
		}

		hfWD := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if r.Method == http.MethodHead {
				// avoid error on HEAD sent by webdav-js (and possibly others)
				w.WriteHeader(http.StatusOK)
				return
			}
			wdHandler.ServeHTTP(w, r)
		})

		router.PathPrefix("/webdav").Handler(requestLogger(hfWD, "webdav"))

		// rewrite handler for webdav-js
		hfUI := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			r.URL.Path = "/webdav/" + r.URL.Path[6:]
			r.URL.RawPath = r.URL.EscapedPath()
			hfWD.ServeHTTP(w, r)
		})
		router.PathPrefix("/repo/").Handler(requestLogger(hfUI, "repo"))
	}

	router.PathPrefix("/").Handler(
		requestLogger(http.FileServer(http.Dir("./ui/web/")), "webui"))

	if ba, err := newBasicAuth(); err != nil {
		log.Errorf(
			"error checking for credentials, endpoint not protected: %v", err)
	} else {
		if ba != nil {
			if err := os.Setenv(util.EnvCredentials, ba.creds()); err != nil {
				log.Errorf("could not enable password protection: %v", err)
			} else {
				router.Use(ba.auth)
				log.Info("endpoint password protected")
			}
		} else {
			log.Warn("endpoint not password protected")
		}
	}

	addr := a.address
	if len(strings.Split(addr, ":")) < 2 {
		addr = fmt.Sprintf("%s:8888", a.address)
	}

	log.Infof("OqtaDrive API starts listening on %s", addr)
	a.server = &http.Server{Addr: addr, Handler: router}

	a.longPollQueue = make(chan chan *Change)
	a.forceNotify = make(chan bool, 1)
	go a.watchDaemon()

	err := a.server.ListenAndServe()
	if err != nil && err != http.ErrServerClosed {
		return err
	}
	return nil
}

// -
func (a *api) startIndex() {
	var err error
	a.index, err = repo.NewIndex("repo.index", a.repository)
	if err != nil {
		log.Errorf("failed to open/create index: %v", err)
	} else {
		go func() {
			a.setProgress(ProgressReindex, "building index")
			defer a.setProgress(ProgressReindex, "")
			if err := a.index.Start(); err != nil {
				log.Errorf("error starting index: %v", err)
			}
		}()
	}
}

// -
func (a *api) Stop() error {

	a.stopped = true

	a.indexLock.Lock()
	defer a.indexLock.Unlock()

	if a.index != nil {
		log.Info("index stopping...")
		a.index.Stop()
		a.index = nil
		log.Info("index stopped")
	}

	a.discardLongPollClients()

	if a.server != nil {
		log.Info("API server stopping...")
		err := a.server.Shutdown(context.Background())
		a.server = nil
		return err
	}

	return nil
}

// -
func GetCartridges(d *daemon.Daemon) ([]*Cartridge, bool) {

	ret := make([]*Cartridge, daemon.DriveCount)
	changes := false

	for drive := 1; drive <= daemon.DriveCount; drive++ {

		status := d.GetStatus(drive)
		var c *Cartridge

		if status == daemon.StatusIdle {

			if cart, ok := d.GetCartridge(drive, time.Second); cart != nil {

				if a := cart.GetAnnotation(AnnoCartridgeInfo); a != nil {
					if info, ok := a.Value().(*Cartridge); ok {
						c = info
					}
				}
				if c == nil {
					c = &Cartridge{Status: status}
					cart.Annotate(AnnoCartridgeInfo, c)
				}
				c.Status = status

				if cart.LastModified().After(c.created) {
					log.WithField("drive", drive).Debug("filling cart info")
					c.fill(cart)
					changes = true
				}
				cart.Unlock()

			} else if !ok {
				status = daemon.StatusBusy
			}
		}

		if c == nil {
			c = &Cartridge{Status: status}
		}

		ret[drive-1] = c
	}

	return ret, changes
}

// -
func addRoute(r *mux.Router, name, method, pattern string,
	handler http.HandlerFunc) {
	r.Methods(method).
		Path(pattern).
		Name(name).
		Handler(requestLogger(handler, name))
}

// -
func requestLogger(inner http.Handler, name string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		log.WithFields(log.Fields{
			"remote": r.RemoteAddr,
			"method": r.Method,
			"path":   r.RequestURI,
		}).Debugf("API BEGIN | %s", name)

		start := time.Now()
		inner.ServeHTTP(w, r)

		log.WithFields(log.Fields{
			"remote":   r.RemoteAddr,
			"method":   r.Method,
			"path":     r.RequestURI,
			"duration": time.Since(start),
		}).Debugf("API END   | %s", name)
	})
}

// -
func (a *api) getProgress(key string) string {
	if v, ok := a.progressMap.Load(key); ok {
		if ret, ok := v.(string); ok {
			return ret
		}
	}
	return ""
}

// -
func (a *api) setProgress(key, val string) {
	a.progressMap.Store(key, val)
}

// -
func getDrive(w http.ResponseWriter, req *http.Request) int {
	vars := mux.Vars(req)
	drive, err := strconv.Atoi(vars["drive"])
	if handleError(err, http.StatusUnprocessableEntity, w) {
		return -1
	}
	return drive
}

// -
func getRef(req *http.Request, checkFlag bool) (string, error) {
	if checkFlag && !isFlagSet(req, "ref") {
		return "", nil
	}
	return getPostPutData(req)
}

// -
func getPostPutData(req *http.Request) (string, error) {
	buf := new(strings.Builder)
	if _, err := io.Copy(buf, io.LimitReader(req.Body, 1024)); err != nil {
		return "", err
	}
	return buf.String(), nil
}

// -
func isFlagSet(req *http.Request, flag string) bool {
	return getArg(req, flag) == "true"
}

// -
func getArg(req *http.Request, arg string) string {
	return req.URL.Query().Get(arg)
}

// -
func getIntArg(req *http.Request, arg string, def int) (int, error) {
	if val := getArg(req, arg); val == "" {
		if def > -1 {
			return def, nil
		}
		return -1, fmt.Errorf("int argument not set: %s", arg)
	} else if ret, err := strconv.Atoi(val); err != nil {
		return def, err
	} else {
		return ret, nil
	}
}

// -
func setHeaders(h http.Header, json bool) {
	util.SetHeaders(h, json)
}

// -
func handleError(e error, statusCode int, w http.ResponseWriter) bool {
	if e == nil {
		return false
	}
	log.Errorf("%v", e)
	util.HandleError(e, statusCode, w)
	return true
}

// -
func sendReply(body []byte, statusCode int, w http.ResponseWriter) {
	setHeaders(w.Header(), false)
	w.WriteHeader(statusCode)
	if _, err := fmt.Fprintf(w, "%s\n", body); err != nil {
		log.Errorf("problem sending reply: %v", err)
	}
}

// -
func sendStreamReply(r io.Reader, statusCode int, w http.ResponseWriter) {
	setHeaders(w.Header(), false)
	w.WriteHeader(statusCode)
	if _, err := io.Copy(w, r); err != nil {
		log.Errorf("problem sending reply: %v", err)
	}
}

// -
func sendJSONReply(obj interface{}, statusCode int, w http.ResponseWriter) {
	setHeaders(w.Header(), true)
	w.WriteHeader(statusCode)
	if err := json.NewEncoder(w).Encode(obj); err != nil {
		log.Errorf("problem writing error: %v", err)
	}
}

// FIXME: make more tolerant
func wantsJSON(req *http.Request) bool {
	return req.Header.Get("Content-Type") == "application/json"
}

// -
func IsControlCart(cart *base.Cartridge) bool {
	if cart == nil {
		return false
	}
	return strings.ToLower(strings.TrimSpace(cart.Name())) == ControlCartridge
}
