/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2022, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package control

import (
	"context"
	"fmt"
	"net/http"
	"time"

	log "github.com/sirupsen/logrus"
)

//
func (a *api) getLog(w http.ResponseWriter, req *http.Request) {

	setHeaders(w.Header(), false)
	w.WriteHeader(http.StatusOK)

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	handleError(LogSponge.Fprint(ctx, w), http.StatusInternalServerError, w)
}

//
func (a *api) configureLog(w http.ResponseWriter, req *http.Request) {
	if level := getArg(req, "level"); level != "" {
		var l log.Level
		if level == "default" {
			l = defaultLogLevel
		} else {
			var err error
			l, err = log.ParseLevel(level)
			if handleError(err, http.StatusUnprocessableEntity, w) {
				return
			}
		}
		log.SetLevel(l)
		sendReply([]byte(fmt.Sprintf("log level changed to '%v'", l)),
			http.StatusOK, w)
	}
}
