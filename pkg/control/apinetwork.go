/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2023, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package control

import (
	"fmt"
	"io"
	"net/http"
	"net/url"

	"codeberg.org/xelalexv/oqtadrive/pkg/daemon"
	"codeberg.org/xelalexv/oqtadrive/pkg/zxnet"
)

// -
func (a *api) netGetConfig(w http.ResponseWriter, req *http.Request) {

	res := "not connected to hub"
	net := &Network{}

	if cl := a.daemon.GetHubClient(); cl != nil && cl.HubURL() != "" {
		net.Hub = cl.HubURL()
		net.Addresses = cl.Addresses()
		net.Users = cl.Users()
		net.SetDescription()
		res = fmt.Sprintf("connected to: %s\n\n%s", net.Hub, net.Description)
	}

	if wantsJSON(req) {
		sendJSONReply(net, http.StatusOK, w)
	} else {
		sendReply([]byte(res), http.StatusOK, w)
	}
}

// -
func (a *api) netSetConfig(w http.ResponseWriter, req *http.Request) {

	if !isFlagSet(req, "hub") {
		handleError(fmt.Errorf("no supported config item set"),
			http.StatusUnprocessableEntity, w)
		return
	}

	hub, err := getPostPutData(req)
	if handleError(err, http.StatusUnprocessableEntity, w) {
		return
	}

	hubUrl, err := url.Parse(hub)
	if err != nil {
		handleError(fmt.Errorf("invalid hub URL: %v", err),
			http.StatusUnprocessableEntity, w)
		return
	}

	addresses, err := getIntArg(req, "addresses", 1)
	if err != nil || addresses < 1 {
		addresses = 1
	}

	cl := a.daemon.GetHubClient()
	if cl == nil {
		handleError(fmt.Errorf("no hub client"), http.StatusInternalServerError, w)
		return
	}

	if cl.Close(); hub == "" {
		sendReply([]byte("connection to hub closed"), http.StatusOK, w)
		return
	}

	if err := cl.Open(hub, addresses); err != nil {
		handleError(
			fmt.Errorf("could not connect to hub %s: %v", hubUrl.Redacted(), err),
			http.StatusUnprocessableEntity, w)
		return
	}

	sendReply([]byte("connected to hub"), http.StatusOK, w)
}

// FIXME remove?
func (a *api) netReceive(w http.ResponseWriter, req *http.Request) {

	number, err := getIntArg(req, "number", 0)
	if handleError(err, http.StatusUnprocessableEntity, w) {
		return
	}

	iris, err := getIntArg(req, "iris", 0)
	if handleError(err, http.StatusUnprocessableEntity, w) {
		return
	}

	self, err := getIntArg(req, "self", 1)
	if handleError(err, http.StatusUnprocessableEntity, w) {
		return
	}

	buf, err := io.ReadAll(io.LimitReader(req.Body, 256))
	if handleError(err, http.StatusUnprocessableEntity, w) {
		return
	}

	if len(buf) == 0 {
		sendReply([]byte("no network data"), http.StatusBadRequest, w)
	}

	// FIXME queue into hub client for injecting test packets
	_, err = zxnet.NewPacket(iris, self, number, isFlagSet(req, "eof"), buf)
	if handleError(err, http.StatusUnprocessableEntity, w) {
		return
	}

	a.daemon.SendFlag(daemon.FlgNet)

	sendReply([]byte("forwarding network data"), http.StatusOK, w)
}
