/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2021, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package control

import (
	"fmt"
	"strings"
	"time"

	"codeberg.org/xelalexv/oqtadrive/pkg/daemon"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/base"
	"codeberg.org/xelalexv/oqtadrive/pkg/zxnet/hub"
)

// -
type Status struct {
	Client string   `json:"client"`
	Drives []string `json:"drives"`
}

// -
func (s *Status) Add(d string) {
	s.Drives = append(s.Drives, d)
}

// -
func (s *Status) String() string {
	ret := fmt.Sprintf("client: %s\n", s.Client)
	for ix, d := range s.Drives {
		ret = fmt.Sprintf("%s%d: %s\n", ret, ix+1, d)
	}
	return ret
}

// -
type Version struct {
	Daemon          string `json:"daemon"`
	AdapterProtocol string `json:"adapterProtocol"`
	AdapterFirmware string `json:"adapterFirmware"`
	LatestRelease   string `json:"latestRelease"`
	Outdated        bool   `json:"outdated"`
	Text            string `json:"text"`
}

// -
func (v *Version) String() string {

	var b strings.Builder
	b.WriteString(fmt.Sprintf(`daemon:          %s
adapter:         protocol %s, firmware %s`,
		v.Daemon, v.AdapterProtocol, v.AdapterFirmware))
	if v.LatestRelease != "" {
		b.WriteString(fmt.Sprintf("\nlatest release:  %s", v.LatestRelease))
	}
	return b.String()
}

// -
type DriveMap struct {
	Start  int  `json:"start"`
	End    int  `json:"end"`
	Locked bool `json:"locked"`
}

// -
type Cartridge struct {
	Name           string `json:"name"`
	Status         string `json:"status"`
	Formatted      bool   `json:"formatted"`
	WriteProtected bool   `json:"writeProtected"`
	Modified       bool   `json:"modified"`
	Control        bool   `json:"control"`
	Client         string `json:"client"`
	Format         string `json:"format"`
	Specials       string `json:"specials"`
	FSSupport      bool   `json:"fsSupport"`
	Sectors        int    `json:"sectors"`
	Used           int    `json:"used"`
	Files          int    `json:"files"`
	//
	created time.Time
}

// -
func (c *Cartridge) fill(cart *base.Cartridge) {

	c.Name = strings.TrimSpace(cart.Name())
	c.Formatted = cart.IsFormatted()
	c.WriteProtected = cart.IsWriteProtected()
	c.Modified = cart.IsModified()
	c.Client = cart.Client().String()
	c.Format = cart.Client().DefaultFormat()
	c.Control = IsControlCart(cart)

	if stats, files, err := cart.FS().Ls(); err == nil {
		c.FSSupport = true
		c.Sectors = stats.Sectors()
		c.Used = stats.Used()
		c.Files = len(files)
	} else {
		c.FSSupport = false
	}
	c.Specials = cart.Specials()
	c.created = time.Now()
}

// -
func (c *Cartridge) String(compact bool) string {

	if c.Status != daemon.StatusIdle {
		return fmt.Sprintf("<%s>", c.Status)
	}

	name := c.Name

	if name == "" {
		name = "<no name>"
	}

	cl := "SP"
	if c.Client != "Interface 1" {
		cl = c.Client
	}

	format := 'b'
	if c.Formatted {
		format = 'f'
	}

	write := 'w'
	if c.WriteProtected {
		write = 'r'
	}

	mod := ' '
	if c.Modified {
		mod = '*'
	}

	free := (c.Sectors - c.Used) / 2

	if compact {
		return fmt.Sprintf("%-11s%-3s%3d %3dkb %c%c%c",
			name, cl, c.Files, free, format, write, mod)
	}
	return fmt.Sprintf("%-16s  %-5s%3d      %3d       %c%c%c",
		name, cl, c.Files, free, format, write, mod)
}

// -
type Change struct {
	Client  string       `json:"client"`
	Drives  []*Cartridge `json:"drives"`
	Network *Network     `json:"network"`
	State   *State       `json:"state"`
}

// -
func (c *Change) HasChange() bool {
	return c != nil &&
		(c.Drives != nil || c.Client != "" || c.Network != nil || c.State.HasChange())
}

// -
type State struct {
	Upgrade *string `json:"upgrade"`
	Reindex *string `json:"reindex"`
	changed bool
}

// -
func (s *State) HasChange() bool {
	return s != nil && s.changed
}

// -
type Network struct {
	Hub         string      `json:"hub"`
	Users       []*hub.User `json:"users"`
	Addresses   []int       `json:"addresses"`
	Description string      `json:"description"`
}

// -
func (n *Network) SetDescription() {

	var b strings.Builder

	b.WriteString(fmt.Sprintf("ASSIGNED ADDRESSES: %v\n\n", n.Addresses))

	b.WriteString("USERS              ADDRESSES\n")
	for _, u := range n.Users {
		b.WriteString(u.String())
		b.WriteRune('\n')
	}

	n.Description = b.String()
}
