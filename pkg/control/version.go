/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2022, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package control

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	log "github.com/sirupsen/logrus"
	"golang.org/x/mod/semver"

	"codeberg.org/xelalexv/oqtadrive/pkg/util"
)

// -
const UnknownRelease = "-"

// -
func (a *api) version(w http.ResponseWriter, req *http.Request) {

	ver := &Version{Daemon: util.OqtaDriveVersion}
	ver.AdapterProtocol, ver.AdapterFirmware = a.daemon.GetAdapterVersion()

	if !isFlagSet(req, "nolatest") {
		ver.LatestRelease = GetLatestRelease()
		if ver.LatestRelease != UnknownRelease {
			here := fmt.Sprintf("v%s", ver.Daemon)
			there := fmt.Sprintf("v%s", ver.LatestRelease)
			ver.Outdated = semver.IsValid(here) && semver.IsValid(there) &&
				semver.Compare(here, there) < 0
		}
	}
	ver.Text = ver.String()

	if wantsJSON(req) {
		sendJSONReply(ver, http.StatusOK, w)
	} else {
		sendReply([]byte(ver.Text), http.StatusOK, w)
	}
}

// -
type release struct {
	ID      int    `json:"id"`
	Name    string `json:"name"`
	TagName string `json:"tag_name"`
}

// -
func GetLatestRelease() string {

	req, err := http.NewRequest(http.MethodGet,
		// "https://postman-echo.com/delay/10", // for testing timeout
		"https://codeberg.org/api/v1/repos/xelalexv/oqtadrive/releases/latest",
		nil)

	if err == nil {
		req.Header.Add("Content-Type", "application/json")
		req.Header.Add("Accept", "application/json")
		util.AddOqtaDriveAgent(req)

		var resp *http.Response

		client := &http.Client{
			Timeout: 3 * time.Second,
			Transport: &http.Transport{
				ResponseHeaderTimeout: 3 * time.Second,
				TLSClientConfig:       &tls.Config{InsecureSkipVerify: true},
			},
		}

		if resp, err = client.Do(req); err == nil {

			defer resp.Body.Close()

			if 200 <= resp.StatusCode && resp.StatusCode <= 299 {
				var body []byte
				if body, err = ioutil.ReadAll(resp.Body); err == nil {
					var r release
					if err = json.Unmarshal(body, &r); err == nil {
						return r.Name
					}
				}
			} else {
				err = fmt.Errorf("received HTTP error code %d", resp.StatusCode)
			}
		}
	}

	if err != nil {
		log.Errorf("could not check for latest release: %v", err)
	}
	return UnknownRelease
}
