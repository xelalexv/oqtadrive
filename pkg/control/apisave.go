/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2021, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package control

import (
	"bytes"
	"fmt"
	"net/http"
	"time"

	log "github.com/sirupsen/logrus"

	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/format"
	"codeberg.org/xelalexv/oqtadrive/pkg/microdrive/format/helper"
	"codeberg.org/xelalexv/oqtadrive/pkg/repo"
)

// -
func (a *api) saveRemote(w http.ResponseWriter, req *http.Request) {
	a.save(false, w, req)
}

// -
func (a *api) saveRepo(w http.ResponseWriter, req *http.Request) {
	a.save(true, w, req)
}

// -
func (a *api) save(toRepo bool, w http.ResponseWriter, req *http.Request) {

	drive := getDrive(w, req)
	if drive == -1 {
		return
	}

	cart, ok := a.daemon.GetCartridge(drive, time.Second)

	if !ok {
		handleError(fmt.Errorf("drive %d busy", drive), http.StatusLocked, w)
		return
	}

	if cart == nil {
		handleError(fmt.Errorf("no cartridge in drive %d", drive),
			http.StatusUnprocessableEntity, w)
		return
	}

	defer cart.Unlock()

	if toRepo {
		var reference *repo.Reference
		ref, err := getRef(req, false)
		if err == nil && ref != "" {
			reference, err = repo.Resolve(ref, a.repository, true, false)
		}
		if err != nil {
			handleError(err, http.StatusNotAcceptable, w)
			return
		}

		if reference != nil {
			if annotateWriteBack(cart, reference) {
				helper.AutoSaveAnnotations(drive, cart)
			} else {
				handleError(fmt.Errorf("not a write-back path"),
					http.StatusUnprocessableEntity, w)
				return
			}

		} else if !cart.HasAnnotation(AnnoWriteBackPath) {
			handleError(fmt.Errorf("cartridge has no write-back path"),
				http.StatusUnprocessableEntity, w)
			return
		}

		_, err = a.writeBackCart(cart)
		if handleError(err, http.StatusInternalServerError, w) {
			return
		}

		cart.SetModified(false)
		sendReply([]byte(fmt.Sprintf("saved data from drive %d", drive)),
			http.StatusOK, w)

	} else {
		typ := getArg(req, "type")
		if typ == "" {
			typ = cart.Client().DefaultFormat()
		}

		writer, err := format.NewFormat(typ, nil)
		if handleError(err, http.StatusUnprocessableEntity, w) {
			return
		}

		var out bytes.Buffer
		if handleError(writer.Write(cart, &out, false, nil),
			http.StatusInternalServerError, w) {
			return
		}

		// a cartridge with a write-back path needs to remain flagged as
		// modified after a plain download-save
		if !cart.HasAnnotation(AnnoWriteBackPath) {
			cart.SetModified(false)
		}

		w.WriteHeader(http.StatusOK)
		w.Write(out.Bytes())
	}

	// we need to auto-save the cartridge so that the cleared modified flag gets
	// persisted in the auto-save
	if !cart.IsAutoSaved() {
		if err := helper.AutoSave(drive, cart, false); err != nil {
			log.Errorf("auto-saving drive %d failed: %v", drive, err)
		}
	}
}
