/*
   OqtaDrive - Sinclair Microdrive emulator
   Copyright (c) 2021, Alexander Vollschwitz

   This file is part of OqtaDrive.

   OqtaDrive is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   OqtaDrive is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
*/

package main

import (
	"fmt"
	"os"

	"codeberg.org/xelalexv/oqtadrive/pkg/run"
)

// -
func synopsis() {
	fmt.Print(`synopsis: oqtactl {action} ...

available actions:

  config       change configuration of daemon & adapter
  dump         dump cartridges & files from daemon drives or files
  edit         add & remove files to/from cartridge (WIP)
  format       format a cartridge in a daemon drive
  hub          run Sinclair Network hub server (WIP)
  load         load cartridges into daemon drives
  ls           get cartridge/file list from daemon
  map          map group of hardware drives
  network      manage connection to Sinclair Network hub
  rename       rename a cartridge in a daemon drive
  save         get cartridges from daemon drives and save
  search       search cartridges in daemon repo, rebuild search index
  serve        run daemon & API server
  reorg        reorganize a cartridge in a drive
  resync       resync with, and reset the adapter
  unload       unload cartridges from daemon drives
  version      get daemon & adapter version info

run 'oqtactl {action} -h|--help' to see detailed info

`)
}

// -
func main() {

	var action string
	var args []string

	if len(os.Args) > 1 {
		action = os.Args[1]
	}

	if len(os.Args) > 2 {
		args = os.Args[2:]
	}

	switch action {

	case "serve":
		if len(args) == 0 || (args[0] != "-h" && args[0] != "--help") {
			run.PrintVersion("")
		}
		run.DieOnError(run.NewServe().Execute(args))

	case "load":
		run.DieOnError(run.NewLoad().Execute(args))

	case "rename":
		run.DieOnError(run.NewRename().Execute(args))

	case "reorg":
		run.DieOnError(run.NewReorg().Execute(args))

	case "format":
		run.DieOnError(run.NewFormat().Execute(args))

	case "unload":
		run.DieOnError(run.NewUnload().Execute(args))

	case "save":
		run.DieOnError(run.NewSave().Execute(args))

	case "ls":
		run.DieOnError(run.NewList().Execute(args))

	case "dump":
		run.DieOnError(run.NewDump().Execute(args))

	case "edit":
		run.DieOnError(run.NewEdit().Execute(args))

	case "map":
		run.DieOnError(run.NewMap().Execute(args))

	case "search":
		run.DieOnError(run.NewSearch().Execute(args))

	case "resync":
		run.DieOnError(run.NewResync().Execute(args))

	case "config":
		run.DieOnError(run.NewConfig().Execute(args))

	case "hub":
		run.DieOnError(run.NewHub().Execute(args))

	case "network":
		run.DieOnError(run.NewNetwork().Execute(args))

	case "version":
		run.DieOnError(run.NewVersion().Execute(args))

	case "", "-h", "--help":
		run.PrintVersion("")
		synopsis()

	default:
		run.Die("unknown action: %s\n", action)
	}
}
