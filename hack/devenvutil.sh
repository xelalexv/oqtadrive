#!/usr/bin/env bash

#
#   OqtaDrive - Sinclair Microdrive emulator
#   Copyright (c) 2021, Alexander Vollschwitz
#
#   This file is part of OqtaDrive.
#
#   OqtaDrive is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   OqtaDrive is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
#

#
# Note: All variables defined in Makefile can be directly accessed here.
#

# shellcheck disable=SC2034
{
# formatting
BLD="\e[1m"
DIM="\e[2m"
ITL="\e[3m"
NRM="\e[0m"
OK="\e[01;32m"
ERR="\e[01;31m"
}

#
#
#
function synopsis {

    files=()

    command -v gawk > /dev/null || echo \
        "Note: proper help display requires gawk! (e.g. sudo apt install gawk)"

    for file in ${MAKEFILE_LIST}; do
        if [[ "$(basename "${file}")" == "Makefile" ]]; then
            files+=( "../${file}" )
        fi
    done

    echo -e "\n${BLD}TARGETS${NRM}"
    print_sorted_help "$(cat "${files[@]}" \
        | gawk '{FS=":"}
            /^[a-zA-Z0-9][-a-zA-Z0-9_\.]+:{1,2}[-a-zA-Z0-9_\. \/\$\()]*$/{f=1; printf "\n${ITL}${BLD}%s${NRM}\n", $1; next}
            /^[^#].*$/{f=0} f' \
        | tr -d '#')"

    echo -e "\n${BLD}NOTES${NRM}\n"

    # .makerc settings
    print_formatted_help "$(cat "${files[@]}" \
        | gawk '/^## makerc$/{f=1; next} /^[^#].*$/{f=0} /^$/{f=0} f' \
        | tr -d '#')"
    echo

    # env settings
    print_formatted_help "$(cat "${files[@]}" \
        | gawk '/^## env$/{f=1; next} /^[^#].*$/{f=0} /^$/{f=0} f' \
        | tr -d '#')"

    # other notes
    print_formatted_help "$(cat "${files[@]}" \
        | gawk '/^##$/{f=1; printf "%s", $0; next} /^[^#].*$/{f=0} /^$/{f=0} f' \
        | tr -d '#')"
    echo
}

#
# $1    help text
#
function print_sorted_help {
    local sorting="cat"
    in_busybox || sorting="sort -z"
    print_formatted_help "$1" \
        | gawk 'BEGIN{print "\0"}
            /^$/{printf "\0"} {print $0}' \
        | ${sorting} \
        | tr -d '\000' \
        | tail -n+2
}

#
# $1    help text
#
function print_formatted_help {
    echo -e "$(apply_shell_expansion "$1")" | uniq
}

#
# $1    string to expand
#
function apply_shell_expansion {
    declare data="$1"
    declare delimiter="__apply_shell_expansion_delimiter__"
    declare command="cat <<${delimiter}"$'\n'"${data}"$'\n'"${delimiter}"
    eval "${command}"
}

#
#
#
function run_tests {

    # shellcheck disable=SC2086
    docker run --rm --user "$(id -u):$(id -g)" \
        -v "${ROOT}/${BINARIES}:/go/bin" ${CACHE_VOLS} \
        -v "${ROOT}:/go/src/${REPO}" -w "/go/src/${REPO}" \
        -e CGO_ENABLED=0 -e GOOS=linux -e GOARCH=amd64 \
        -e LOG_LEVEL=debug -e LOG_FORMAT=text -e LOG_FORCE_COLORS=true \
        "${GO_IMAGE}" sh -c "\
            go test ${TEST_OPTS} \
                -coverpkg=./... -coverprofile=${BUILD_OUTPUT}/coverage.out \
                -covermode=count ./... && \
            go tool cover -html=${BUILD_OUTPUT}/coverage.out \
                -o ${BUILD_OUTPUT}/coverage.html"

    echo -e "\ncoverage report is in ${BUILD_OUTPUT}/coverage.html\n"
}

#
# build command binary
#
# $1    command
# $2    target OS
# $3    target architecture; omit for `amd64`
# $4    `keep` for keeping the binary, not just the archive; requires $3
#
function build_binary {

    local arch="amd64"
    [[ -z "$3" ]] || arch="$3"

    local suffix
    [[ "$2" != "windows" ]] || suffix=".exe"

    local binary="${BINARIES}/$1${suffix}"

    local extra_env
    [[ "${arch}" != "arm" ]] || extra_env="-e GOARM=6"
    [[ "${arch}" != "mips" ]] || extra_env="-e GOMIPS=softfloat"

    echo -e "\nbuilding ${binary} for $2/${arch}"

    # shellcheck disable=SC2086
    docker run --rm --user "$(id -u):$(id -g)" \
        -v "${ROOT}/${BINARIES}:/go/bin" ${CACHE_VOLS} \
        -v "${ROOT}:/go/src/${REPO}" -w "/go/src/${REPO}" \
        -e CGO_ENABLED=0 -e GOOS="$2" -e GOARCH="${arch}" ${extra_env} \
        "${GO_IMAGE}" bash -c \
            "go mod tidy && go build -v -tags netgo -installsuffix netgo -ldflags \
            \"-s -w -X codeberg.org/xelalexv/oqtadrive/pkg/util.OqtaDriveVersion=${OQTADRIVE_VERSION}\" \
            -o \"${binary}\" \"./cmd/$1/\""

    local specifier="_${OQTADRIVE_RELEASE}_$2_${arch}"
    zip -j "../${binary}${specifier}.zip" "../${binary}"

    if [[ "$4" == "keep" ]]; then
        mv "../${binary}" "../${binary}${specifier}"
    else
        rm -f "../${binary}"
    fi
}

#
# $...  names of JavaScript files to include (without path); order matters
#
function minify_js {

    local oqta="${UI_BASE}/js/oqta.js"

    pushd "${UI_BASE}/js/oqta" > /dev/null || return 1
    cat "$@" > "${oqta}"
    popd > /dev/null || return 1

    docker run --rm --user "$(id -u):$(id -g)" -v "${UI_BASE}/js:/data" -w /data \
        "${JSMINIFY_IMAGE}" minify -o oqta.min.js oqta.js
    rm -f "${oqta}"
}

#
#
#
function cache_bust {

    # shellcheck disable=SC2086
    rm -f ${UI_BASE}/js/oqta.min.*.js ${UI_BASE}/cover.*.css

    local hash
    hash="$(sha1sum "${UI_BASE}/js/oqta.min.js" | head -c 6)"
    mv "${UI_BASE}/js/oqta.min.js" "${UI_BASE}/js/oqta.min.${hash}.js"
    sed -Ei "s:oqta\.min\.[[:alnum:]]+\.js:oqta.min.${hash}.js:g" \
        "${UI_BASE}/index.html"

    hash="$(sha1sum "${UI_BASE}/cover.css" | head -c 6)"
    cp "${UI_BASE}/cover.css" "${UI_BASE}/cover.${hash}.css"
    sed -Ei "s:cover\.[[:alnum:]]+\.css:cover.${hash}.css:g" \
        "${UI_BASE}/index.html" "${UI_BASE}/repo"
}

#
#
#
function download_oqtactl {

    local arch
    arch="$(get_architecture)" || return 1

    local os
    os="$(get_os "${arch}")" || return 1

    local marker="${os,,}_${arch}"
    local url

    if [[ -z "${BUILD_URL}" ]]; then # get from release page
        url="$(get_asset_url "${marker}.zip")"

    else # get from custom build page
        url="$(curl -fsSL "${BUILD_URL}" \
            | grep "${marker}\.zip" \
            | cut -d '"' -f 8)" && url="${BUILD_URL}/${url}"
    fi

    if [[ -z "${url}" ]]; then
        echo -e \
            "\nNo download available for architecture '${arch}' on OS '${os}' in version '$(version_label "${VERSION}")'.\n" >&2
        return 1
    fi

    [[ ! -f "${OQTACTL}" ]] || mv -f "${OQTACTL}" "${OQTACTL}.bak"

    echo "  from ${url}"
    curl -fsSL "${url}" -o oqtactl.zip && \
        unzip -o oqtactl.zip && \
        rm oqtactl.zip && \
        chmod +x oqtactl && \
        mv oqtactl "${OQTACTL}" && \
        return 0

    echo "Download failed!" >&2
    mv -f "${OQTACTL}.bak" "${OQTACTL}"
    return 1
}

#
#
#
function download_ui {

    local url

    if [[ -z "${BUILD_URL}" ]]; then # get from release page
        url="$(get_asset_url ui.zip)"

    else # get from custom build page
        url="$(curl -fsSL "${BUILD_URL}" \
            | grep ui.zip \
            | cut -d '"' -f 8)" && url="${BUILD_URL}/${url}"
    fi

    if [[ -z "${url}" ]]; then
        echo -e "\nNo UI available in version '$(version_label "${VERSION}")'.\n" >&2
        return
    fi

    rm -rf "${ROOT}/ui.bak"
    [[ ! -d "${ROOT}/ui" ]] || mv -f "${ROOT}/ui" "${ROOT}/ui.bak"

    echo "  from ${url}"
    curl -fsSL "${url}" -o ui.zip && \
        rm -rf ui "${ROOT}/ui" && \
        unzip ui.zip && \
        rm ui.zip && \
        mv ui "${ROOT}/ui" && \
        return 0

    echo "Download failed!" >&2
    mv -f "${ROOT}/ui.bak" "${ROOT}/ui"
    return 1
}

#
#
#
function download_firmware {

    local url

    if [[ "${TARGET}" == "linino" ]]; then
        if [[ -z "${BUILD_URL}" ]]; then # get from release page
            url="$(get_asset_url oqtadrive.linino.hex)"
        else # get from custom build page
            url="${BUILD_URL}/oqtadrive.linino.hex"
        fi

    else
        if [[ -z "${BUILD_URL}" ]]; then # get from repo
            [[ -n "${VERSION}" ]] || VERSION="$(get_latest_release)"
            url="${BASE_URL}/${VERSION}/arduino/oqtadrive.ino"
        else # get from custom build page
            url="${BUILD_URL}/oqtadrive.ino"
        fi
    fi

    [[ ! -f "${SKETCH}.org" ]] || mv -f "${SKETCH}.org" "${SKETCH}.org.bak"
    echo "  from ${url}"
    curl -fsSL -o "${SKETCH}.org" "${url}" && return 0

    echo "Download failed!" >&2
    mv -f "${SKETCH}.org.bak" "${SKETCH}.org"
    return 1
}

#
#
#
function flash_firmware {

    echo "port: ${PORT}, FQBN: ${FQBN}"

    if "${ARDUINO_CLI}" compile --clean --verify --upload --port "${PORT}" \
        --fqbn "${FQBN}" "${SKETCH}"; then
        echo "flashing adapter succeeded"
        return
    fi

    echo "flashing adapter failed"

    local try
    case "${FQBN}" in
        arduino:avr:nano)
            try="arduino:avr:nano:cpu=atmega328old"
            ;;
        arduino:avr:nano:cpu=atmega328old)
            try="arduino:avr:nano"
            ;;
    esac

    if [[ -n "${try}" ]]; then
        echo "retrying with FQBN ${try}"
        if "${ARDUINO_CLI}" compile --clean --verify --upload --port "${PORT}" \
            --fqbn "${try}" "${SKETCH}"; then
            echo "flashing adapter succeeded"
            return
        fi
        echo "also failed, giving up"
    fi

    return 1
}

#
#
#
function install_arduino_cli {

    local ver
    ver="$(${ARDUINO_CLI} version | cut -d ' ' -f 4)"
    echo "Arduino CLI version - required: ${ARDUINO_CLI_VERSION}, installed: ${ver}"

    if [[ "${ver}" == "${ARDUINO_CLI_VERSION}" ]]; then
        echo "Arduino CLI already installed and at required version"
    else
        echo -e "\nArduino CLI not installed or needs upgrade, downloading..."
        mkdir -p "${ARDUINO_DIR}"
        curl -fsSL \
            https://raw.githubusercontent.com/arduino/arduino-cli/master/install.sh \
                | PATH="${PATH}:${ARDUINO_DIR}" BINDIR="${ARDUINO_DIR}" \
                    sh -s "${ARDUINO_CLI_VERSION}"
        echo -e "\nconfiguring Arduino CLI"
        "${ARDUINO_CLI}" config init --overwrite
    fi

    ver="$(${ARDUINO_CLI} core list | grep "arduino:avr" | cut -d ' ' -f 2)"
    echo "AVR core - required: ${AVR_VERSION}, installed: ${ver}"

    if [[ "${ver}" == "${AVR_VERSION}" ]]; then
        echo "AVR core already installed and at required version"
    else
        echo -e "\nAVR core not installed or needs upgrade, updating index"
        "${ARDUINO_CLI}" core update-index
        echo -e "\ninstalling core"
        "${ARDUINO_CLI}" core install "arduino:avr@${AVR_VERSION}"
        echo
    fi
}

#
# $1    file name with path, relative to project root
# $2    `copy` to only create a copy with current version (optional), this is
#       for example needed when upgrading this shell script, since replacing it
#       while it is being used may cause errors; default is to replace file if
#       more current version is available
#
function assure_current {

    cd "${ROOT}" || { # when called from Makefile, we're in `hack`
        echo "corrupted project structure" >&2
        return 1
    }

    local ld
    if ! ld="$(to_utc "$1" ref)"; then
        ld="1970-01-01T01:00:00Z"
    fi

    # migration to codeberg: `since` not supported, get all with `path`,
    # but `limit` is ignored in that case, use first element from array
    local commits
    if ! commits="$(repo_api_call "commits?path=$1&sha=${BRANCH}&page=1")"; then
        echo "cannot determine latest commit" >&2
        return 1
    fi

    local len
    if ! len="$(echo "${commits}" | jq -r '. | length')" || [[ ${len} -eq 0 ]]; then
        echo "invalid commit reply from repo service" >&2
        return 1
    fi

    local rd
    if rd="$(echo "${commits}" | jq -r '.[0].commit.committer.date')" \
        && [[ -n "${rd}" ]]; then
        # Codeberg returns commit times in local time, convert to UTC
        rd="$(to_utc "${rd}")"
    else
        rd="$(to_utc)"
    fi

    if [[ ! "${rd}" > "${ld}" ]]; then
        return 0
    fi

    echo -e "$1 needs update (local date: ${ld}, remote date: ${rd}) ..."

    local dir
    dir="$(dirname "$1")"
    [[ -z "${dir}" ]] || mkdir -p "${dir}"

    local current="$1.current"
    if curl -fsSL -o "${current}" "${BASE_URL}/${BRANCH}/$1"; then
        # set to date of commit, but note that rd is in UTC, so we need to
        # convert to local time; note that for compatibility with BusyBox,
        # we use -I instead of --iso-8601 (also further below)
        touch_resource "${current}" "${rd}"
        chmod +x "${current}"
        [[ $# -gt 1 && "$2" == "copy" ]] || mv -f "${current}" "$1"
        echo "$1 updated"
    else
        echo "download of $1 failed" >&2
        rm -f "${current}"
        return 1
    fi
}

#
# $1    file
# $2    date (optional, will be converted to UTC, but may be ignored depending
#       on platform)
#
function touch_resource {
    if [[ -z "$2" ]] || in_busybox; then
        touch "$1"
    else
        touch --date="$(date --date="$2" -Iseconds)" "$1"
    fi
}

#
# $1    date string to convert; optional, current time when dropped
# $2    `ref` if $1 is a file path
#
function to_utc {

    local d

    if [[ -z "$1" ]]; then
        d="$(date --utc -Iseconds)"

    elif [[ $# -gt 1 && "$2" == "ref" ]]; then
        d="$(date --reference="$1" --utc -Iseconds 2>/dev/null)" \
            && [[ -n "${d}" ]] || return 1
    else
        d="$1"
        if in_busybox; then
            d="$(echo -n "${d}" | tr -s 'UTC' ' ')"
        fi
        d="$(date --date="${d}" --utc -Iseconds)"
    fi

    if [[ "${d}" == *UTC ]]; then
        echo -n "${d}"
    else
        echo -n "$(echo -n "${d}" | cut -d '+' -f 1)Z"
    fi
}

#
# $1    filter
#
function get_asset_url {

    # latest release; Codeberg does not accept `releases/latest`
    local path="releases?draft=false&pre-release=false&limit=1"
    local prefilter=".[0]"

    if [[ -n "${VERSION}" && "${VERSION}" != "latest" ]]; then
        path="releases/tags/${VERSION}"
        prefilter=""
    fi

    repo_api_call "${path}" 2>/dev/null \
        | jq -r "${prefilter}.assets[]
            | select(.name | contains(\"$1\"))
            | .browser_download_url"
}

#
#
#
function get_latest_release {
    repo_api_call "releases?draft=false&pre-release=false&limit=1" 2>/dev/null \
        | jq -r ".[0].name"
}

#
# $1    version; 'latest', '', or omit for latest version
#
function version_label {
    local l="latest"
    [[ -z "$1" ]] || l="$1"
    echo -n "${l}"
}

#
# $1    path
#
function repo_api_call {
    curl -fsSL -H "accept: application/json" \
        "https://codeberg.org/api/v1/repos/xelalexv/oqtadrive/$1"
}

#
# $...  actions, `install|remove|start|stop|status|enable|disable`
#
function manage_service {

    local unit="/etc/systemd/system/oqtadrive.service"
    local sys_ctl="sudo systemctl"
    local sudo_if_needed="sudo"

    if [[ "${TARGET}" == "desktop" ]]; then
        # On desktop target, we run the OqtaDrive service as non-root. It's more
        # adequate there, and avoids upgrade failure.
        unit="${HOME}/.config/systemd/user"
        mkdir -p "${unit}"
        unit+="/oqtadrive.service"
        sys_ctl="systemctl --user"
        sudo_if_needed=
    fi

    for a in "$@"; do

        case "${a}" in

            install)
                local extra_args
                if [[ -n "${REPO}" ]]; then
                    mkdir -p "${REPO}" || {
                        echo -e "\nCannot create repo folder '${REPO}'\n" >&2
                        return 1
                    }
                    extra_args+=" -r ${REPO}"
                fi
                curl -fsSL "${BASE_URL}/${BRANCH}/hack/oqtadrive.service" \
                    | sed -E -e "s;^ExecStart=.*$;ExecStart=${OQTACTL} serve -d ${PORT} -b ${BAUD_RATE} ${extra_args};g" \
                          -e "s;^WorkingDirectory=.*$;WorkingDirectory=${ROOT};g" \
                          -e "s;^User=.*$;User=${USER};g" \
                          -e "s;^Environment=.*$;Environment=LOG_LEVEL=info PORT=${PORT} BAUD_RATE=${BAUD_RATE} OLD_NANO=${OLD_NANO} RESET_PIN=${RESET_PIN} FQBN=${FQBN};g" \
                    | ${sudo_if_needed} tee "${unit}" > /dev/null
                if [[ "${TARGET}" == "desktop" ]]; then
                    # for user systemd service, we must not use the explicit
                    # User setting, or we'll get an error on service start:
                    #
                    #   Failed at step GROUP spawning /...: Operation not permitted
                    #
                    # Furthermore, we need to switch WantedBy to default.target,
                    # since multi-user is not applicable for a user service.
                    #
                    sed -i -e "s;^User=.*$;;g" \
                        -e "s;^WantedBy=multi-user.target$;WantedBy=default.target;g" \
                        "${unit}"
                fi
                ${sys_ctl} daemon-reload
                ;;

            remove)
                ${sudo_if_needed} rm --force "${unit}"
                ;;

            start|stop|status|enable|disable)
                [[ ! -f "${unit}" ]] || {
                    echo "running daemon service action: ${a}"
                    ${sys_ctl} "${a}" "${OQTADRIVE_SERVICE}"
                }
                ;;

            *)
                echo -e "\nUnknown service command: ${a}\n" >&2
                return 1
                ;;
        esac

        sleep 1
    done
}

#
# $...  actions, `install|remove|start|stop|restart|enable|disable`
#
function manage_init {

    local unit="/etc/init.d/oqtadrive"

    for a in "$@"; do

        case "${a}" in

            install)
                local extra_args
                if [[ -n "${REPO}" ]]; then
                    mkdir -p "${ROOT}/${REPO}" || {
                        echo -e "\nCannot create repo folder '${ROOT}/${REPO}'\n" >&2
                        return 1
                    }
                    extra_args+=" -r ${ROOT}/${REPO}"
                fi
                curl -fsSL "${BASE_URL}/${BRANCH}/hack/oqtadrive.init" \
                    | sed -e "s;@ROOT@;${ROOT};g" \
                          -e "s;@PORT@;${PORT};g" \
                          -e "s;@FQBN@;${FQBN};g" \
                          -e "s;@BAUD_RATE@;${BAUD_RATE};g" \
                          -e "s;@EXTRA_ARGS@;${extra_args};g" \
                        > ${unit}
                chmod +x ${unit}
                ;;

            remove)
                rm ${unit}
                ;;

            start|stop|restart|enable|disable)
                [[ ! -f ${unit} ]] || {
                    echo "running daemon init action: ${a}"
                    ${unit} "${a}"
                }
                ;;

            *)
                echo -e "\nUnknown init command: ${a}\n" >&2
                return 1
                ;;
        esac

        sleep 1
    done
}

#
#
#
function patch_avrdude {

    local avrdude
    avrdude="$(find ~/.arduino15/ -type f -name avrdude.org)"
    [[ -z "${avrdude}" ]] || {
        echo "avrdude already patched!"
        return 0
    }

    avrdude="$(find ~/.arduino15/ -type f -name avrdude)"

    [[ -n "${avrdude}" ]] || {
        echo "avrdude not installed!"
        return 1
    }

    mv "${avrdude}" "${avrdude}.org"

    local dir
    dir="$(dirname "${avrdude}")"

    local autoreset="${dir}/autoreset"
    curl -fsSL -o "${autoreset}" "${BASE_URL}/${BRANCH}/hack/autoreset"
    chmod +x "${autoreset}"

    cat <<EOF > "${avrdude}"
#!/bin/sh
sudo --preserve-env strace -o "|${autoreset}" -eioctl "${dir}/avrdude.org" \$@ 2>&1 | grep -vi "broken pipe"
EOF
    chmod +x "${avrdude}"
}

#
#
#
function unpatch_avrdude {

    local avrdude
    avrdude="$(find ~/.arduino15/ -type f -name avrdude.org)"
    [[ -n "${avrdude}" ]] || {
        echo "avrdude not patched!"
        return 0
    }

    local dir
    dir="$(dirname "${avrdude}")"

    mv --force "${avrdude}" "${dir}/avrdude"
    rm --force "${dir}/autoreset"
}

#
# $...  prerequisites
#
function prereqs {

    local missing

    for p in "$@"; do
        type "${p}" >/dev/null 2>&1 || {
            missing+=" ${p}"
        }
    done

    [[ -z "${missing}" ]] || {
        echo -e "\nYou need to install these dependencies:${missing}\n"
        return 1
    }
}

#
#
#
function sanity {
    local arch
    arch="$(get_architecture)" || return 1
    get_os "${arch}" > /dev/null || return 1
}

#
#
#
function in_busybox {
    [[ "$(readlink -f "${SHELL}")" == "/bin/busybox" ]]
}

#
#
#
function get_architecture {

    local arch
    arch="$(uname -m)"

    case ${arch} in
        x86_64)
            echo -n "amd64"
            ;;
        x86|i386|i686)
            echo -n "386"
            ;;
        armv5*|armv6*|armv7*)
            echo -n "arm"
            ;;
        aarch64)
            echo -n "arm64"
            ;;
        mips)
            echo -n "mips"
            ;;
        *)
            echo -e "\nUnsupported architecture: ${arch}\n" >&2
            return 1
            ;;
    esac
}

#
# $1    architecture
#
function get_os {

    local os
    os="$(uname -s)"

    local err=1

    case ${os} in

        Linux)
            case $1 in
                amd64|amd|arm|386|mips)
                    err=0
                    ;;
            esac
            ;;

        Darwin)
            case $1 in
                amd64|arm64)
                    err=0
                    ;;
            esac
            echo -e "\nWARNING: This has not been tested on MacOS yet!\n" >&2
            ;;

        *)
            echo -e "\n${os} not supported.\n" >&2
            return 1
            ;;
    esac

    [[ ${err} -eq 0 ]] || \
        {
            echo -e "\nArchitecture $1 not supported on ${os}.\n" >&2
            return 1
        }

    echo -n "${os}"
}

#
#
#

cd "$(dirname "$0")" || exit 1
"$@"
