#ifndef OQTADRIVE_CONFIG
#define OQTADRIVE_CONFIG

#define BAUD_RATE 500000

#define LED_RW_IDLE_ON    false
#define LED_SYNC_WAIT     true
#define RUMBLE_LEVEL      35
#define DRIVE_OFFSET_IF1  0
#define DRIVE_OFFSET_QL   -1
#define HW_GROUP_START    0
#define HW_GROUP_END      0
#define HW_GROUP_LOCK     true
#define FORCE_IF1         false
#define FORCE_QL          false

// set this to true if you added the write protect boost circuit
#define WR_PROTECT_BOOST false

#endif
