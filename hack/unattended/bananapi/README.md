# Building *Armbian*-based Unattended Setup Image for *BananaPi M2 Zero*

1. Get the *Armbian* build system, or `git pull` for updates:

    ```sh
    git clone https://github.com/armbian/build
    ```

    Check which is the latest stable branch and check that one out. Last time, this was `v23.11`.

2. Remove the `armbian` build system *Docker* image if still present from a previous build, and run *Dockerized* build:

    ```sh
    cd ./{repo root}
    sudo rm -rf userpatches .tmp cache
    ./compile.sh KERNEL_CONFIGURE=no BUILD_MINIMAL=no BUILD_DESKTOP=no EXTERNAL=yes INSTALL_HEADERS=yes BOARD=bananapim2zero BRANCH=current RELEASE=jammy ARMBIAN_MIRROR=https://mirror-eu-de1.armbian.airframes.io USE_TORRENT=no
    ```

    When dropping the env settings, manually choose in dialog:

    - Full OS image for flashing
    - Do not change kernel configuration
    - Show CSC/WIP/EOS/TVB
    - choose bananapim2zero
    - choose current kernel
    - chose Ubuntu 22.04 LTS Jammy
    - Image with console interface (server)
    - Standard image with console interface

3. Copy the (still uncompressed) base image we've just built (`output/images`) to a new location. This is going to be the image for unattended setup. The needed additions are made from root of *OqtaDrive* project. Make sure you're on `master` branch and up to date!

    ```sh
    git checkout master
    git pull
    IMG={path to copied image} make imgsetup_bananapi
    ```

4. Compress image and create check sum:

    ```sh
    cd {image folder}
    xz {image file}
    sha256sum {image file}.xz > {image file}.xz.sha256
    ```
