#!/bin/sh
#
# Copyright (c) Authors: https://www.armbian.com/authors
#
# This file is licensed under the terms of the GNU General Public
# License version 2. This program is licensed "as is" without any
# warranty of any kind, whether express or implied.

# First login as root?
if [ -w /root/ -a -f /root/.not_logged_in_yet ]; then
    flock /root/.setup bash /usr/lib/armbian/armbian-firstlogin \
    	>> /root/setup.log 2>&1
fi

# OqtaDrive bootstrapper
if [ ! -f /root/.not_logged_in_yet ]; then
    # FIXME user hard coded
    flock /home/pi/.setup sudo -u pi bash -c "cd /home/pi; [[ -f .unattended_setup_completed ]] \
        || make unattended_setup >> setup.log 2>&1 \
        || sudo reboot"
fi
