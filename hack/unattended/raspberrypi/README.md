# Building *RaspberryPi OS*-based Unattended Setup Image for *RaspberryPi*s

1. Download latest [*RaspberryPi OS* image](https://www.raspberrypi.com/software/operating-systems/). Pick *RaspberryPi OS (Legacy) Lite*, 32-bit version for compatibility with *RaspberryPi Zero 1*. Don't use newer *Bookworm* based image, since it breaks our current workflow & tooling.

2. Copy the downloaded image to a new location and un-compress. This is going to be the image for unattended setup. The needed additions are made from root of *OqtaDrive* project. Make sure you're on `master` branch and up to date!

    ```sh
    git checkout master
    git pull
    IMG={path to uncompressed image} make imgsetup_raspberrypi
    ```

3. Compress image and create check sum:

    ```sh
    cd {image folder}
    xz {image file}
    sha256sum {image file}.xz > {image file}.xz.sha256
    ```
