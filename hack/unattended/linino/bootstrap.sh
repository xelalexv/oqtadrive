#!/bin/sh

cd "$(dirname "$0")" || exit 1

[ ! -f .unattended_setup_completed ] || exit 0

#
# $1    source binary
# $1    symlink target
#
symlink_binary() {
    [ -f "$2" ] || ln -s "$1" "$2"

}

#
#
#
disable_services() {
    for svc in "$@"; do
        echo "disabling service ${svc}..."
        "/etc/init.d/${svc}" stop
        "/etc/init.d/${svc}" disable
    done
}

#
#
#

# make sure ntpd is running
/etc/init.d/sysntpd start

# symlink required binaries
symlink_binary "${PWD}/bin/bash" /bin/bash
symlink_binary "${PWD}/bin/gawk" /usr/bin/gawk
symlink_binary "${PWD}/bin/make" /usr/bin/make
symlink_binary "${PWD}/bin/gojq" /usr/bin/jq
symlink_binary "${PWD}/bin/unzip" /usr/bin/unzip

# symlink more recent curl
[ -f /usr/bin/curl.org ] || [ ! -f /usr/bin/curl ] \
    || mv -f /usr/bin/curl /usr/bin/curl.org
[ ! -f /usr/bin/curl ] || rm -f /usr/bin/curl
ln -s "${PWD}/bin/curl" /usr/bin/curl

# SSL certificate bundle
mkdir -p /etc/ssl
cp ./bin/cert.pem /etc/ssl/

disable_services mcuio alljoyn alljoynfixboot

# run setup
make unattended_setup >> setup.log 2>&1
