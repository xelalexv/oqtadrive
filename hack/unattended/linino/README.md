# Building Unattended Setup Image for *Linino One*

The image for the *Linino One* is not a full OS image. It only contains resources needed for bootstrapping the installation. There are no longer any OS images available that we could build upon, so we use the version of *OpenWRT* that comes with the *Linino*. Even though this version is totally outdated and the package repos have long been gone, it's still good enough for our purposes.

## Preparing *MIPS* Binaries
The following *MIPS* binaries need to be present in `hack/unattended/linino/bin`:

- `bash`
- `curl`
- `gawk`
- `gojq`
- `make`
- `unzip`

Except for `gojq`, all of these binaries are built using [*static-binary-zoo*](https://codeberg.org/xelalexv/static-binary-zoo). `gojq` is built using [this project](https://github.com/itchyny/gojq) (need to compile for MIPS).

## Building the Image

1. Pick a location for the image to be built. This is going to be the image for unattended setup. The needed additions are made from root of *OqtaDrive* project. Make sure you're on `master` branch and up to date!

    ```sh
    git checkout master
    git pull
    IMG={path for new image} make imgsetup_linino
    ```

2. The resulting image is already compressed. Create check sum:

    ```sh
    cd {image folder}
    sha256sum {image file}.xz > {image file}.xz.sha256
    ```
