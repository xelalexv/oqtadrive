#!/usr/bin/env bash

#
#   OqtaDrive - Sinclair Microdrive emulator
#   Copyright (c) 2022, Alexander Vollschwitz
#
#   This file is part of OqtaDrive.
#
#   OqtaDrive is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   OqtaDrive is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with OqtaDrive. If not, see <http://www.gnu.org/licenses/>.
#

#
# Note: All variables defined in Makefile can be directly accessed here.
#

#
# $1    destination folder
# $2    source folder
#
function prepare_boot_raspberrypi {

    echo "modifying boot..."

    sudo --preserve-env --shell << EOF
touch "$1/ssh"
cp "$2/wpa_supplicant.conf" "$1/"
echo '${DEFAULT_USER}:$(echo "${DEFAULT_PASSWORD}" | openssl passwd -6 -stdin)' \
    > "$1/userconf.txt"
if ! grep --silent "enable_uart=1" "$1/config.txt"; then
    sed -i '/^\[all]$/d' "$1/config.txt"
    echo -e "[all]\nenable_uart=1" >> "$1/config.txt"
fi
sed -i 's/console=serial0,115200 //g' "$1/cmdline.txt"
EOF
}

#
# $1    destination folder
# $2    source folder
#
function prepare_boot_bananapi {

    echo "modifying boot ..."

    sudo --preserve-env --shell << EOF
cp "$2/armbian_first_run.txt" "$1/"
if ! grep --silent "overlays=uart3" "$1/armbianEnv.txt"; then
    echo -e "overlays=uart3" >> "$1/armbianEnv.txt"
fi
EOF
}

#
# $1    destination folder
# $2    source folder
#
function prepare_root {

    echo "modifying root..."

    sudo --preserve-env --shell << EOF
mkdir -p "$1/home/${DEFAULT_USER}/repo"
cp hack/Makefile "$1/home/${DEFAULT_USER}/"
mkdir -p "$1/home/${DEFAULT_USER}/oqtadrive"
cp "$2/config.h" "$1/home/${DEFAULT_USER}/oqtadrive/"
cp "$2/oqtadrive.makerc" "$1/home/${DEFAULT_USER}/"
chown -R 1000:1000 "$1/home/${DEFAULT_USER}"
mkdir -p "$1/root/.ssh"
touch "$1/root/.ssh/authorized_keys"
chmod 600 "$1/root/.ssh/authorized_keys"
EOF
}

#
# $1    destination folder
# $2    source folder
#
function prepare_root_raspberrypi {
    sudo --preserve-env --shell << EOF
if ! grep --silent "# OqtaDrive bootstrapper" "$1/etc/rc.local"; then
    sed -i '/^exit 0$/d' "$1/etc/rc.local"
    cat "$2/rc.local" >> "$1/etc/rc.local"
fi
EOF
}

#
# $1    destination folder
# $2    source folder
#
function prepare_root_bananapi {
    sudo --preserve-env --shell << EOF
cp -f "$2/armbian-firstlogin" "$1/usr/lib/armbian/armbian-firstlogin"
chmod +x "$1/usr/lib/armbian/armbian-firstlogin"
cp -f "$2/armbian-check-first-login.sh" "$1/etc/profile.d/armbian-check-first-login.sh"
sed -Ei 's/,commit=[0-9]+,/,commit=5,/' "$1/etc/fstab"
EOF
}

#
# $1    image
# $2    mount point
#
function prepare_image_linino {

    local size=64 # MiB

    echo -e "\ncreating empty image..."
    rm -f "$1" "$1.xz"
    dd if=/dev/zero of="$1" bs=4k count=16384

    echo -e "\ncreating partition..."
    sudo parted "$1" mklabel msdos
    sudo parted "$1" mkpart primary 1MiB $((size-1))MiB

    local loop_dev
    echo -e "\nsetting up loop device..."
    if ! loop_dev="$(sudo losetup -o 1MiB --sizelimit "$((size-2))MiB" -f "$1" --show)" \
        || [[ -z "${loop_dev}" ]]; then
        echo "could not create loop device: ${loop_dev}"
        return 1
    fi

    echo -e "\ncreating file system on ${loop_dev}..."
    sudo mkfs.ext4 "${loop_dev}" -O ^metadata_csum

    echo "mounting & copying files..."
    sudo mount "${loop_dev}" "$2"
    sudo mkdir -p "$2/oqtadrive/bin"
    sudo cp -a hack/Makefile "$2/oqtadrive/"
    sudo cp -a hack/unattended/linino/bin/* "$2/oqtadrive/bin/"
    sudo cp -a \
        hack/unattended/linino/bootstrap.sh hack/unattended/linino/oqtadrive.makerc \
        "$2/oqtadrive/"
    sudo chmod +x "$2/oqtadrive/bin/"* "$2/oqtadrive/"*.sh

    echo -e "\nretrieving CA cert bundle..."
    sudo curl https://curl.se/ca/cacert.pem -o "$2/oqtadrive/bin/cert.pem"

    sync

    echo -e "\nunmounting & detaching loop device ${loop_dev}"
    sudo umount "$2"
    sudo losetup -d "${loop_dev}"

    echo -e "\ncompressing image $1..."
    xz "$1"

    echo -e "\ndone\n"
}

#
# $1    image
# $2    partition filter
# $3    mount point
#
function mount_img {

    if findmnt "$3" > /dev/null; then
        echo "image already mounted"
        return
    fi

    local offset
    if ! offset="$(get_partition_offset "${1}" "${2}")" || [[ -z "${offset}" ]]; then
        echo "could not determine partition offset - image: ${1}, filter: ${2}" >&2
        return 1
    fi

    echo "mounting $1, offset ${offset}..."
    sudo mount -o loop,offset="${offset}" "${1}" "${3}"
}

#
# $1    mount point
#
function umount_img {
    sync
    sudo umount "$1"
}

#
# $1    image
# $2    partition filter
#
function get_partition_offset {
    parted -s "${1}" unit B print \
        | grep -E "${2}" \
        | sed 's/^ //g' \
        | tr -s ' ' \
        | tr -d 'B' \
        | cut -d ' ' -f 2
}

#
#
#

"$@"
